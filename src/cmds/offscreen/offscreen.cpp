#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <GL/gl.h>
#include <GL/glx.h>

int main() {
  size_t width(32);
  size_t height(32);
  //
  std::cout << "XXX 1" << std::endl;
#if 1
  typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
  typedef Bool (*glXMakeContextCurrentARBProc)(Display*, GLXDrawable, GLXDrawable, GLXContext);
  static glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL;
  static glXMakeContextCurrentARBProc   glXMakeContextCurrentARB   = NULL;

  // Fetch the function pointers of the GLX functions needed
  glXCreateContextAttribsARB =
    (glXCreateContextAttribsARBProc) glXGetProcAddressARB((const GLubyte*) "glXCreateContextAttribsARB" );
  std::cout << "XXX 2" << std::endl;
  glXMakeContextCurrentARB =
    (glXMakeContextCurrentARBProc) glXGetProcAddressARB((const GLubyte*) "glXMakeContextCurrent");
  std::cout << "XXX 3" << std::endl;

  // Open a connection to the display.
  const char* displayName = NULL;
  Display* display = XOpenDisplay(displayName);
  std::cout << "XXX 4" << std::endl;
  if (! display) {
    std::cerr << "Failed connecting with the display!" << std::endl;
  }

  std::cout << "XXX 5" << std::endl;
  // Define the requirements of the framebuffer and query all possible
  // configuration that match the requirements
  static int visualAttribs[] = {None};
  int numberOfFramebufferConfigurations = 0;
  GLXFBConfig* fbConfigs = glXChooseFBConfig(display, DefaultScreen(display),
                                             visualAttribs,
                                             &numberOfFramebufferConfigurations);
  std::cout << "XXX 6 " << numberOfFramebufferConfigurations << std::endl;
  if (! fbConfigs) {
    std::cerr << "No configurations that meet the requirements are available!"
              << std::endl;
  }

  // Obtain an OpenGL context
  int context_attribs[] = {
    GLX_CONTEXT_MAJOR_VERSION_ARB, 3,
    GLX_CONTEXT_MINOR_VERSION_ARB, 3,
    GLX_CONTEXT_FLAGS_ARB, GLX_CONTEXT_DEBUG_BIT_ARB,
    GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
    None
  };
  GLXContext openGLContext =
    glXCreateContextAttribsARB(display, fbConfigs[0], 0, True, context_attribs);

  std::cout << "XXX 7" << std::endl;
  // Make the context current
  int pbufferAttribs[] = {
    GLX_PBUFFER_WIDTH,  32,
    GLX_PBUFFER_HEIGHT, 32,
    None
  };
  GLXPbuffer pbuffer = glXCreatePbuffer(display, fbConfigs[0], pbufferAttribs);

  // clean up:
  XFree(fbConfigs);
  XSync(display, False);

  std::cout << "XXX 8" << std::endl;
  if (!glXMakeContextCurrent(display, pbuffer, pbuffer, openGLContext))
  {
    std::cerr << "something went wrong" << std::endl;
  }
  std::cout << "XXX 9" << std::endl;

#endif
#if 0

  glGenFramebuffers(1, &m_framebuffer1);
  glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffer1);

  glGenRenderbuffers(1, &m_colorRenderbuffer1);
  glBindRenderbuffer(GL_RENDERBUFFER, m_colorRenderbuffer1);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8, width, height);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, m_colorRenderbuffer1);

  glGenRenderbuffers(1, &m_depthRenderbuffer);
  glBindRenderbuffer(GL_RENDERBUFFER, m_depthRenderbuffer);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_depthRenderbuffer);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, m_depthRenderbuffer);

  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    cout << "Failed to make complete framebuffer object"
         << glCheckFramebufferStatus(GL_FRAMEBUFFER) << std::endl;
#else

#if 0

  // Use Framebuffer Objects
  // Initialize
  GLuint fbo, render_buf;
  glGenFramebuffers(1, &fbo);
  glGenRenderbuffers(1, &render_buf);
  glBindRenderbuffer(render_buf);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_BGRA8, width, height);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER​,fbo);
  glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, render_buf);

  // Deinit:
  glDeleteFramebuffers(1, &fbo);
  glDeleteRenderbuffers(1, &render_buf);

  // Before drawing
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER​, fbo);

  // after drawing
  std::vector<std::uint8_t> data(width*height*4);
  glReadBuffer(GL_COLOR_ATTACHMENT0);
  glReadPixels(0, 0, width, height, GL_BGRA, GL_UNSIGNED_BYTE,&data[0]);
  // Return to onscreen rendering:
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER​, 0);

#else
#if 1
  // Use pixel buffer objects

  // Initialize
  GLuint pbo;
  glGenBuffers(1, &pbo);
  glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo);
  glBufferData(GL_PIXEL_PACK_BUFFER, width*height*4, NULL, GL_DYNAMIC_READ);

  // Deinit:
  glDeleteBuffers(1, &pbo);

  // Reading:
  glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo);
  // 0 instead of a pointer, it is now an offset in the buffer.
  glReadPixels(0, 0, width, height, GL_BGRA, GL_UNSIGNED_BYTE, 0);
  // DO SOME OTHER STUFF (otherwise this is a waste of your time)
  glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo); //Might not be necessary...
  auto* pixel_data = glMapBuffer(GL_PIXEL_PACK_BUFFER, GL_READ_ONLY);

  // Clean
  glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
  glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
#endif
#endif
#endif

  return 0;
}
