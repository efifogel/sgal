// Copyright (c) 2015 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <string>
#include <list>

#include <boost/shared_ptr.hpp>

#include "SGAL/sgal.hpp"
#include "SGAL/Plugin_api.hpp"
#include "SGAL/Polyhedron_attributes_array.hpp"

#include "Player_scene.hpp"
#include "Player_option_parser.hpp"

SGAL_BEGIN_NAMESPACE

class Scene_graph;
class Configuration;
class Background;
class Color_background;
class Snapshotter;
class Image_writer;
class Node;

SGAL_END_NAMESPACE

class Player {
public:
  typedef boost::shared_ptr<SGAL::String_array>         Shared_string_array;
  typedef boost::shared_ptr<SGAL::Container>            Shared_container;
  typedef boost::shared_ptr<SGAL::Snapshotter>          Shared_snapshotter;
  typedef boost::shared_ptr<SGAL::Image_writer>         Shared_image_writer;

  typedef boost::shared_ptr<SGAL::Shared_container_array>
    Shared_shared_container_array;

  typedef boost::shared_ptr<SGAL::Plugin_api>           Shared_plugin_api;
  typedef boost::shared_ptr<SGAL::Plugin_api>           (Plugin_api_create_t)();
  typedef boost::function<Plugin_api_create_t>          Shared_plugin_creator;

  /*! Construct default. */
  Player();

  /*! Construct. */
  Player(int argc, char* argv[]);

  /*! Construct. */
  Player(SGAL::String_array args);

  /*! Destruct. */
  ~Player();

  /*! Load the plugins. */
  void load_plugins(int argc, char* argv[]);

  /*! Initialize the player. */
  void init(int argc, char* argv[]);

  /*! Initialize the player. */
  void init(SGAL::String_array args);

  /*! Clear the player. */
  void clear();

  /*! Configure the scene.
   * Constructs the scene-graph and configure it using the information from
   * the command-line option parser. In particular, use the option parser of
   * the player, which subsumes the option parser of SGAL, and the option
   * parsers of every dynamically loaded library.
   */
  void configure();

  /*! Create the scene. */
  void create();

  /*! Create the scene from a file.
   */
  void create(const std::string& filename);

  /*! Create the scene from a string.
   */
  void create(const char* str);

  /*! Create the scene from a string and a filename.
   * \patam str the input string.
   * \param filename the name of the input file. It is not used for accessing
   *        the file. It is used for two purposes: (i) the file extension, if
   *        exists, is used as a hint for the format of the file, or (ii)
   *        informative messages.
   */
  void create(const char* str, const std::string& filename);

  /*! Create the scene from binary data.
   */
  void create(const unsigned char* data, size_t size);

  /*! Create the scene from binary data and a filename.
   * \patam data the binary data.
   * \patam size the size of the binary data in bytes.
   * \param filename the name of the input file. It is not used for accessing
   *        the file. It is used for two purposes: (i) the file extension, if
   *        exists, is used as a hint for the format of the file, or (ii)
   *        informative messages.
   */
  void create(const unsigned char* data, size_t size,
              const std::string& filename);

  /*! Create the scene from a base64 encoded string.
   */
  void create_from_encoded(const char* encoded, const std::string& filename);

  /*! Create the scene from base64 encoded binary data.
   */
  void create_from_encoded(const char* encoded, size_t size,
                           const std::string& filename);

  /*! Create the scene from base64 encoded binary data.
   */
  void create_from_encoded(const char* encoded, size_t size);

  /*! Indulge the user. */
  void indulge();

  /*! Determine whether the scene has visual.
   */
  bool do_have_visual() const;

  /*! Visualize the scene. */
  void visualize();

  /*! Initialize the visual. */
  void init_visual();

  /*! Process the visual. */
  void process_visual();

  /*! Clear the visual. */
  void clear_visual();

  /*! Destroy the scene. */
  void destroy();

  /*! Obtain the accumulated volume of all polyhedrons in the scene.
   */
  float volume();

  /*! Obtain the accumulated surface area of all polyhedrons in the scene.
   */
  float surface_area();

  /*! Obtain the attributes of all polyhedrons.
   */
  const SGAL::Polyhedron_attributes_array& get_polyhedron_attributes_array();

  /*! Obtains the scene graph.
   */
  SGAL::Scene_graph* get_scene_graph();

  /*! Obtains a container given by its name.
   */
  Shared_container get_container(const std::string& name);

  /*! Obtain the configuration node.
   * \todo Expose as Scene_graph::get_configuration (when we expose Scene_graph).
   */
  SGAL::Configuration* get_configuration();

  /*! Obtain the background node.
   * \todo Expose as Scene_graph::get_background (when we expose Scene_graph).
   */
  SGAL::Background* get_background();

  /*! Export the scene.
   */
  /// @brief Create Spam instances.
  void export_scene(const std::string& fullname, const std::string& format,
                    bool is_binary = false);

  /*! Export the scene.
   */
  std::string export_scene(const std::string& format_name);

  /*! Snapshot the scene.
   */
  void snapshot_scene(const std::string& fullname, const std::string& format,
                      size_t width, size_t height);

  //! Bindable stack handlers
  //@{

  /*! Construct a Color_background node and insert it into the scene.
   */
  SGAL::Color_background* add_color_background();

  //@}

private:
  //! The scene.
  Player_scene m_scene;

  //! The option parser.
  Player_option_parser* m_option_parser;

  //! The construct that holds the attributes of all polyhedrons.
  SGAL::Polyhedron_attributes_array m_polyhedron_attributes_array;

  //! The snapshotter engine, in case needed.
  Shared_snapshotter m_snapshotter;

  //! The image-writter engine, in case needed.
  Shared_image_writer m_image_writer;

  //! Plugins successfully loaded.
  std::list<std::pair<Shared_plugin_creator, Shared_plugin_api>> m_plugins;

  /*! Dynamically load a shared library.
   */
  void load_shared_library(const std::string& name);

  /*! Determines whether a given path points at a shared library.
   */
  static bool is_shared_library(const fi::path& p);
};

#endif
