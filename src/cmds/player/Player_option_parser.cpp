// Copyright (c) 2004 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#if defined(PLAYER_CONTROL)
#include "stdafx.h"
#endif

#include <string>

#include "Player_option_parser.hpp"

#include "SGAL/Window_option_parser.hpp"
#if (defined SGAL_USE_GLUT)
#include "SGLUT/Glut_window_manager.hpp"
#elif defined(_WIN32)
#include "SGAL/Windows_window_manager.hpp"
#else
#include "SGAL/X11_window_manager.hpp"
#endif

namespace po = boost::program_options;

// Default values
const SGAL::String Player_option_parser::s_def_name_re("^[A-Za-z_][0-9A-Za-z_]*");
const SGAL::String Player_option_parser::s_def_tag_re("^[A-Za-z_][0-9A-Za-z_]*");

//! \brief constructs
Player_option_parser::Player_option_parser() :
  m_player_opts("Player options"),
  m_grid(false)
{
  m_player_opts.add_options()
    ("grid,G", po::value<SGAL::Boolean>(&m_grid)->default_value(false),
     "draw grid")
    ("display-texture-info",
     po::value<SGAL::Boolean>(&m_display_texture_info)->default_value(false),
     "display texture information")
    ("display-geometry-info",
     po::value<SGAL::Boolean>(&m_display_geometry_info)->default_value(false),
     "display geometry information")
    ("display-polyhedra-info",
     po::value<SGAL::Boolean>(&m_display_polyhedra_info)->default_value(false),
     "display polyhedra information")
     ("bench", po::value<SGAL::Boolean>(&m_bench)->default_value(false),
      "bench")
    ("name-re", po::value<SGAL::String>(&m_name_re)->
     default_value(s_def_name_re),
     "Name filter regular expression")
    ("tag-re", po::value<SGAL::String>(&m_tag_re)->
     default_value(s_def_tag_re),
     "Tag filter regular expression")
    ("display-names", po::value<SGAL::Boolean>(&m_display_names)->
     default_value(false),
     "Display names")
    ;
}

//! \brief initializes
void Player_option_parser::init()
{
  m_visible_opts.add(m_window_opts).add(m_player_opts);
  m_cmd_line_opts.add(m_window_opts).add(m_player_opts);
  m_config_file_opts.add(m_window_opts).add(m_player_opts);
  m_environment_opts.add(m_window_opts).add(m_player_opts);
}

//! \brief parses the options.
void Player_option_parser::parse(int argc, char* argv[], bool allow_unregistered)
{ SGAL::Main_option_parser::operator()(argc, argv, allow_unregistered); }

//! \brief applies the options.
void Player_option_parser::apply()
{
  SGAL::Main_option_parser::apply();
  SGAL::Window_option_parser::apply(m_variable_map);
}

//! \brief determines whether the application renders off screen.
SGAL::Boolean Player_option_parser::do_render_offscreen() const
{
  return ((is_interactive_defaulted() || ! is_interactive()) &&
          (! is_interactive() || do_snapshot()));
}
