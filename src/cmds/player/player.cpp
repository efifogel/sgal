// Copyright (c) 2004 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <iostream>
#include <fstream>
#include <vector>
#include <exception>

#include <boost/function.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem/exception.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/dll/shared_library.hpp>
#include <boost/dll/import.hpp>

#if (defined _MSC_VER)
#define NOMINMAX 1
#endif

#include "SGAL/sgal.hpp"
#include "SGAL/Loader_errors.hpp"
#include "SGAL/Plugin_errors.hpp"
#include "SGAL/Scene_graph.hpp"
#include "SGAL/Writer.hpp"
#include "SGAL/Geometry_format.hpp"
#include "SGAL/Image_format.hpp"
#include "SGAL/Node.hpp"
#include "SGAL/Color_background.hpp"
#include "SGAL/Image.hpp"
#include "SGAL/Image_writer.hpp"
#include "SGAL/Snapshotter.hpp"
#include "SGAL/Group.hpp"
#include "SGAL/Field.hpp"
#include "SGAL/Context.hpp"
#include "SGAL/Plugin_api.hpp"

// The preprocessor symbol 'Success' is defined, possibly by the header file X.h
// It is also defined by Eigen3...
#undef Success

// The following symbols are defined, possibly by the header file X.h
// It is also defined by v8...
#undef True
#undef False
#undef None
#undef Status

#include "Player_scene.hpp"
#include "Player_option_parser.hpp"
#include "Player.hpp"

#if (defined SGAL_USE_SCGAL)
SGAL_BEGIN_NAMESPACE
extern void scgal_init();
SGAL_END_NAMESPACE
#endif

namespace fi = boost::filesystem;
namespace dll = boost::dll;

//! \breif
static std::string& error_message(std::string& str)
{
  str.append(": ");
#if (defined _MSC_VER)
  DWORD err;
  LPVOID lpMsgBuf;
  if ((err = ::GetLastError()) != NO_ERROR &&
       ::FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER |
                        FORMAT_MESSAGE_FROM_SYSTEM,
                        nullptr,
                        err,
                        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                        (LPSTR) &lpMsgBuf,
                        0,
                        nullptr) != 0)
  {
    str.append((LPSTR) lpMsgBuf);
    ::LocalFree(lpMsgBuf);
  }
#else
  str.append(dlerror());
#endif
  return str;
}

//! \brief determines whether a given path points at a shared library.
bool Player::is_shared_library(const fi::path& p)
{
  const std::string s = p.string();
  return (s.find(".dll") != std::string::npos ||
          s.find(".so") != std::string::npos ||
          s.find(".dylib") != std::string::npos) &&
    s.find(".lib") == std::string::npos &&
    s.find(".exp") == std::string::npos &&
    s.find(".pdb") == std::string::npos &&
    s.find(".manifest") == std::string::npos &&
    s.find(".rsp") == std::string::npos &&
    s.find(".obj") == std::string::npos &&
    s.find(".a") == std::string::npos;
}

//! \breif dynamically loads a shared library.
void Player::load_shared_library(const std::string& name)
{
  auto verbose_level = m_option_parser->get_verbosity_level();
  if (verbose_level> 0)
    std::cout << "Loading shared library " << name << std::endl;

  namespace fs = ::boost::dll::fs;

  fs::path library_path(name);
  if (library_path.is_absolute()) {
    if (! fs::is_regular_file(library_path)) {
      throw SGAL::Not_regular_file_error(name);
      return;
    }
    if (! is_shared_library(library_path)) {
      throw SGAL::Not_shared_library_error(name);
      return;
    }
  }

  // We found a file. Trying to load it
  boost::dll::fs::error_code error;
  auto mode = dll::load_mode::rtld_lazy |
    dll::load_mode::rtld_global |
    dll::load_mode::search_system_folders |
    dll::load_mode::append_decorations;
  dll::shared_library plugin(library_path, mode, error);
  if (error) {
    throw SGAL::Shared_library_load_error(name);
    return;
  }
  if (verbose_level> 0)
    std::cout << "Shared library (" << plugin.native() << "):"
              << library_path << "loaded" << std::endl;

  std::string create_str("create_plugin");
  if (! plugin.has(create_str)) {
    throw SGAL::Shared_library_has_error(name, create_str);
    return;
  }

  // library has symbol, importing...
  Shared_plugin_creator creator =
    dll::import_alias<Plugin_api_create_t>(boost::move(plugin), "create_plugin");
  boost::shared_ptr<SGAL::Plugin_api> plugin_api = creator();

  if (verbose_level> 0)
    std::cout << "Pluging " << plugin_api->name() << " matched" << std::endl;
  plugin_api->init(m_option_parser);
  if (verbose_level> 0)
    std::cout << "Pluging " << plugin_api->name()
              << " initialized" << std::endl;
  m_plugins.push_back(std::make_pair(creator, plugin_api));
}

//! Main entry used only for debugging.
// When building python bindings, the player is build as a shared library
// without a main() function.
#if 0
int main(int argc, char* argv[])
{
  std::ifstream is("tetrahedron.wrl", std::ifstream::binary);
  if (!is.good()) {
    std::cerr << "Error: Failed to open tetrahedron.wrl!" << std::endl;
    return -1;
  }
  is.seekg(0, is.end);
  auto size = is.tellg();
  if (size < 0) {
    std::cerr << "Error: Failed to size tetrahedron.wrl!" << std::endl;
    return -1;
  }
  is.seekg (0, is.beg);
  auto* data = new char [size];
  if (!data) {
    std::cerr << "Error: Failed to allocate!" << std::endl;
    return -1;
  }
  is.read(data, size);    // read data as a block
  if (!is) std::cout << "Error: only " << is.gcount() << " could be read";
  is.close();
  player.create_scene(data, size);                      // create the scene
  player.start_scene();                                 // start the scene
  player.indulge_user();                                // indulge the user
  if (player.do_have_visual()) player.visualize();      // visualize the scene
  player.destroy_scene();                               // destroy the scene
  delete[] data;
  return 0;
}
#endif

//! \brief determines whether the scene has visual.
bool Player::do_have_visual() const { return m_scene.do_have_visual(); }

//! \brief visualize
void Player::visualize()
{
  m_scene.init_visual();
  m_scene.process_visual();
  m_scene.clear_visual();
}

void Player::init_visual() { m_scene.init_visual(); }
void Player::process_visual() { m_scene.process_visual(); }
void Player::clear_visual() { m_scene.clear_visual(); }

//! \brief constructs default
Player::Player() : m_option_parser(nullptr) {}

//! \brief constructs
Player::Player(int argc, char* argv[]) : m_option_parser(nullptr)
{ init(argc, argv); }

//! \brief constructs.
Player::Player(SGAL::String_array arguments) : m_option_parser(nullptr)
{ init(arguments); }

//! \brief destructs.
Player::~Player() { clear(); }

//! \brief initializes the player.
void Player::init(int argc, char* argv[])
{
  m_option_parser = new Player_option_parser;
  m_option_parser->init();
  m_option_parser->parse(argc, argv, true);

  // Load plugins
  m_option_parser->for_each_plugin([&](const std::string& plugin)
                                   { load_shared_library(plugin); });
  SGAL::initialize(argc, argv);
#if (defined SGAL_USE_SCGAL)
  SGAL::scgal_init();
#endif

  // Parse program options:
  if (! m_option_parser) {
    m_option_parser = new Player_option_parser;
    m_option_parser->init();
  }

  // Parse again in case a dynamicly loaded library added options.
  m_option_parser->parse(argc, argv);

  // Apply special handling (e.g., throwing an exception)
  m_option_parser->apply();

  m_scene.set_option_parser(m_option_parser);
}

//! \brief initializes the player.
void Player::init(SGAL::String_array args)
{
  auto argc = static_cast<int>(args.size()) + 1;
  char** argv = new char*[argc];
  const auto* progname = "player";
  argv[0] = const_cast<char*>(progname);
  size_t i(1);
  std::for_each(args.begin(), args.end(),
                [&](const std::string& arg)
                { argv[i++] = const_cast<char*>(arg.c_str()); });

  init(argc, argv);

  if (argv) {
    delete [] argv;
    argv = nullptr;
  }
  argc = 0;
}

//! \brief cleans the player.
void Player::clear()
{
  SGAL::clear();

  if (m_option_parser) {
    delete m_option_parser;
    m_option_parser = nullptr;
  }

  m_plugins.clear();
}

//! \brief configures the scene.
void Player::configure()
{
  m_scene.configure_scene();
  auto* scene_graph = m_scene.get_scene_graph();
  for (auto& plugin : m_plugins) plugin.second->configure(scene_graph);
}

//! \brief creates the scene.
void Player::create()
{
  configure();                          // create a scene-graph and configure it
  m_scene.create_scene();
}

//! \brief creates the scene from a file.
void Player::create(const std::string& filename)
{
  configure();                          // create a scene-graph and configure it
  m_scene.create_scene(filename);
}

//! \brief creates the scene from a string.
void Player::create(const char* str) {
  configure();                          // create a scene-graph and configure it
  m_scene.create_scene(str);
}

//! \brief creates the scene from a string and a filename.
void Player::create(const char* str, const std::string& filename) {
  configure();                          // create a scene-graph and configure it
  m_scene.create_scene(str, filename);
}

//! \brief creates the scene from binary data.
void Player::create(const unsigned char* data, size_t size) {
  configure();                          // create a scene-graph and configure it
  m_scene.create_scene(data, size);
}

//! \brief creates the scene from binary data and a filename.
void Player::create(const unsigned char* data, size_t size,
                    const std::string& filename) {
  configure();                          // create a scene-graph and configure it
  m_scene.create_scene(data, size, filename);
}

//! \brief creates the scene from a base64 encoded string.
void Player::create_from_encoded(const char* encoded,
                                 const std::string& filename) {
  configure();                          // create a scene-graph and configure it
  m_scene.create_scene_from_encoded(encoded, filename);
}

//! \brief creates the scene from base64 encoded binary data.
void Player::create_from_encoded(const char* encoded, size_t size,
                                 const std::string& filename) {
  configure();                          // create a scene-graph and configure it
  m_scene.create_scene_from_encoded(encoded, size, filename);
}

//! \brief creates the scene from base64 encoded binary data.
void Player::create_from_encoded(const char* encoded, size_t size) {
  configure();                          // create a scene-graph and configure it
  m_scene.create_scene_from_encoded(encoded, size);
}

//! \brief indulges the user. */
void Player::indulge()
{
  m_scene.indulge_user();
  auto* scene_graph = m_scene.get_scene_graph();
  for (auto& plugin : m_plugins) plugin.second->indulge(scene_graph);
}

//! \brief destroys
void Player::destroy()
{
  m_snapshotter.reset();
  m_image_writer.reset();
  m_scene.destroy_scene();
}

//! \brief obtains the accumulated volume of all polyhedrons in the scene.
float Player::volume() { return m_scene.volume(); }

//! \brief obtains the accumulated surface area of all polyhedrons in the scene.
float Player::surface_area() { return m_scene.surface_area(); }

//! \brief obtains the attributes of all polyhedrons.
const SGAL::Polyhedron_attributes_array&
Player::get_polyhedron_attributes_array()
{
  m_scene.process_polyhedron_attributes_array(m_polyhedron_attributes_array);
  return m_polyhedron_attributes_array;
}

//! \brief obtains the names of the instance containers

//! \brief main entry.
int main(int argc, char* argv[])
{
  Player player;
  try {
    player.init(argc, argv);
  }
  catch(Player_option_parser::Generic_option_exception& /* e */) {
    player.clear();
    return 0;
  }
  catch(std::exception& e) {
    std::cerr << e.what() << std::endl;
    player.clear();
    return -1;
  }
  try {
    player.create();                                    // create the scene
    player.indulge();                                   // indulge the user
    if (player.do_have_visual()) player.visualize();    // visualize the scene
    player.destroy();                                   // destroy the scene
  }
  catch(SGAL::File_error& e) {
    player.destroy();
    std::cerr << e.what();
    if (! e.filename().empty()) std::cerr;
    std::cerr << std::endl;
    return -1;
  }
  catch(std::exception& e) {
    player.destroy();
    std::cerr << e.what() << std::endl;
    return -1;
  }
  player.clear();
  return 0;
}

//! \brief exports the scene.
void Player::export_scene(const std::string& fullname,
                          const std::string& format_name,
                          bool is_binary)
{
  auto& writer = *(SGAL::Writer::get_instance());
  auto* sg = m_scene.get_scene_graph();
  auto* geom_format = SGAL::Geometry_format::get_instance();
  auto format_code = geom_format->find_code(format_name);
  writer(sg, fullname, format_code, is_binary);
}

//! \brief exports the scene.
std::string Player::export_scene(const std::string& format_name)
{
  auto& writer = *(SGAL::Writer::get_instance());
  auto* sg = m_scene.get_scene_graph();
  auto* geom_format = SGAL::Geometry_format::get_instance();
  auto format_code = geom_format->find_code(format_name);
  return writer(sg, format_code);
}

//! \brief snapshots the scene.
void Player::snapshot_scene(const std::string& fullname,
                            const std::string& format_name,
                            size_t width, size_t height)
{
  auto* sg = m_scene.get_scene_graph();
  auto root = sg->get_root();

  auto* image_format = SGAL::Image_format::get_instance();
  auto format_code = image_format->find_code(format_name);

  auto image = m_scene.get_image();
  if (! image) {
    image.reset(new SGAL::Image);
    m_scene.set_image(image);
  }
  image->set_width(width);
  image->set_height(height);
  // image->set_format(format);
  m_scene.adjust_offscreen_rendering(width, height);

  if (! m_snapshotter) {
    m_snapshotter.reset(new SGAL::Snapshotter);
    SGAL_assertion(m_snapshotter);
    root->add_child(m_snapshotter);
    m_snapshotter->add_to_scene(sg);
    auto mode = SGAL::Snapshotter::MODE_COLOR_ATTACHMENT;
    m_snapshotter->set_mode(mode);
    m_snapshotter->set_image(image);
  }

  if (! m_image_writer) {
    m_image_writer.reset(new SGAL::Image_writer);
    SGAL_assertion(m_image_writer);
    root->add_child(m_image_writer);
    m_image_writer->add_to_scene(sg);
    m_image_writer->set_image(image);
  }

  m_image_writer->set_file_name(fullname);
  m_image_writer->set_file_format(format_code);

  auto* src_field = m_snapshotter->get_source_field("image");
  SGAL_assertion(src_field);
  auto* dst_field = m_image_writer->get_destination_field("trigger");
  SGAL_assertion(dst_field);
  src_field->disconnect(dst_field); // disconnect old connections if exists
  src_field->connect(dst_field);

  m_snapshotter->trigger();
}

//! \brief obtainss the scene graph.
SGAL::Scene_graph* Player::get_scene_graph()
{ return m_scene.get_scene_graph(); }

//! \brief obtains the configuration node.
SGAL::Configuration* Player::get_configuration()
{ return m_scene.get_scene_graph()->get_configuration(); }

//! \brief obtains the background node.
SGAL::Background* Player::get_background()
{ return m_scene.get_scene_graph()->get_active_background(); }

//! \brief obtains a container given by its name.
Player::Shared_container Player::get_container(const std::string& name)
{ return m_scene.get_scene_graph()->get_container(name); }

//! \brief constructs a Color_background node and insert it into the scene.
SGAL::Color_background* Player::add_color_background()
{
  auto* sg = m_scene.get_scene_graph();
  auto* cbg = new SGAL::Color_background;
  cbg->add_to_scene(sg);
  sg->get_background_stack().push(cbg);
  sg->get_background_stack().bind_top();
  return cbg;
}
