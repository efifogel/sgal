// Copyright (c) 2004 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef PLAYER_SCENE_HPP
#define PLAYER_SCENE_HPP

/*! The concrete player scene. The create_scene() and destroy_scene() virtual
 * member functions create and destory the scene respectively. These functions
 * must not issue window or graphics related commands. The init_scene() and
 * clear_scene() are responssible for initializing the wndow and graphics
 * contexts and clear them when not needed any longer.
 */

#include <string>
#include <list>
#include <stdexcept>

#include <boost/filesystem/path.hpp>
#include <boost/shared_ptr.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/Types.hpp"
#include "SGAL/Scene_graph.hpp"
#include "SGAL/Scene.hpp"
#include "SGAL/Indexed_face_set.hpp"
#include "SGAL/Box.hpp"
#if defined(_WIN32)
#include "SGAL/Windows_window_manager.hpp"
#endif

#include "Player_option_parser.hpp"

namespace fi = boost::filesystem;

#if defined(SGAL_USE_V8)
namespace v8 {
  class Isolate;
};
#endif

SGAL_BEGIN_NAMESPACE

class Scene_graph;
class Tick_event;
class Draw_event;
class Reshape_event;
class Keyboard_event;
class Mouse_event;
class Motion_event;
class Passive_motion_event;
class Context;
class Configuration;
class Indexed_face_set;
class Box;
class Image;
class Matrix4f;
class Polyhedron_attributes_array;

#if (defined SGAL_USE_GLUT)
class Glut_window_manager;
class Glut_window_item;
#elif defined(_WIN32)
#if defined(PLAYER_CONTROL)
class Windows_window_item;
#else
//class Windows_window_manager;
class Windows_window_item;
#endif
#else
class X11_window_manager;
class X11_window_item;
#endif

SGAL_END_NAMESPACE

// CPlayerControlCtrl not in SGAL namespace
#if defined(_WIN32) && defined(PLAYER_CONTROL)
class CPlayerControlCtrl;
#endif

class Player_scene : public SGAL::Scene {
public:
  typedef boost::shared_ptr<SGAL::Image>          Shared_image;

#if (defined SGAL_USE_GLUT)
  typedef SGAL::Glut_window_manager               Window_manager;
  typedef SGAL::Glut_window_item                  Window_item;
#elif defined(_WIN32)
#if defined(PLAYER_CONTROL)
  typedef CPlayerControlCtrl                      Window_manager;
  typedef SGAL::Windows_window_item               Window_item;
#else
  typedef SGAL::Windows_window_manager            Window_manager;
  typedef SGAL::Windows_window_item               Window_item;
#endif
#else
  typedef SGAL::X11_window_manager                Window_manager;
  typedef SGAL::X11_window_item                   Window_item;
#endif

  /*! Construct default. */
  Player_scene();

  /*! Construct. */
  Player_scene(Player_option_parser* option_parser);

  /*! Destructor. */
  virtual ~Player_scene(void);

  /*! Configure the scene according to the options selected by the user.
   * \param[in] option_parser the player option parser that holds the options.
   */
  virtual void configure_scene();

  /*! Create the scene from an input file.
   * \param filename the full name of the input file.
   */
  virtual void create_scene(const std::string& fullname);

  /*! Create the scene from a string.
   * \patam str the input string.
   */
  virtual void create_scene(const char* str);

  /*! Create the scene from a string and a filename.
   * \patam str the input string.
   * \param filename the name of the input file. It is not used for accessing
   *        the file. It is used for two purposes: (i) the file extension, if
   *        exists, is used as a hint for the format of the file, or (ii)
   *        informative messages.
   */
  virtual void create_scene(const char* str, const std::string& filename);

  /*! Create the scene from binary data.
   * \patam data the binary data.
   * \patam size the size of the binary data in bytes.
   */
  virtual void create_scene(const unsigned char* data, size_t size);

  /*! Create the scene from binary data.
   * \patam data the binary data.
   * \patam size the size of the binary data in bytes.
   * \param filename the name of the input file. It is not used for accessing
   *        the file. It is used for two purposes: (i) the file extension, if
   *        exists, is used as a hint for the format of the file, or (ii)
   *        informative messages.
   */

  virtual void create_scene(const unsigned char* data, size_t size,
                            const std::string& filename);

  /*! Create the scene from a base64 encoded string.
   * \param encoded the encoded string.
   * \param filename the name of the input file. It is not used for accessing
   *        the file. It is used for two purposes: (i) the file extension, if
   *        exists, is used as a hint for the format of the file, or (ii)
   *        informative messages.
   */
  virtual void create_scene_from_encoded(const char* encoded,
                                         const std::string& filename);

  /*! Create the scene from base64 encoded binary data.
   * \param encoded the encoded string.
   * \patam size the size of the (decoded) binary data in bytes.
   * \param filename the name of the input file. It is not used for accessing
   *        the file. It is used for two purposes: (i) the file extension, if
   *        exists, is used as a hint for the format of the file, or (ii)
   *        informative messages.
   */
  virtual void create_scene_from_encoded(const char* encoded, size_t size,
                                         const std::string& filename);

  /*! Create the scene from base64 encoded binary data.
   * \param encoded the encoded string.
   * \patam size the size of the (decoded) binary data in bytes.
   */
  virtual void create_scene_from_encoded(const char* encoded, size_t size);

  /*! Create the scene. */
  virtual void create_scene();

  /*! Start the scene. */
  virtual void start_scene();

  /*! Indulge user requests from the command line. */
  virtual void indulge_user();

  /*! Initialize the visual. */
  virtual void init_visual();

  /*! Process the visual. */
  virtual void process_visual();

  /*! Destroy the scene. */
  virtual void destroy_scene();

  /*! Clear the scene */
  virtual void clear_visual();

  /*! Create a window.
   */
  virtual void create_window();

  /*! Destroy a window.
   */
  virtual void destroy_window(SGAL::Window_item* window_item);

  /*! Initialize the window. Typically used to create a context.
   * \param[in] window_item the window to initialize
   */
  virtual void init_window(SGAL::Window_item* window_item,
                           SGAL::Uint width, SGAL::Uint height);

  /*! Clear a window. Used to clear a context.
   * \param[in] window_item the window to initialize
   */
  virtual void clear_window(SGAL::Window_item* window_item);

  /*! Reshape the viewport of a window of the scene.
   * It is assumed that the window context is the current context.
   * \param window_item the window to reshape
   * \param width the new width of the window
   * \param height the new height of the window
   */
  virtual void reshape_window(SGAL::Window_item* window_item,
                              SGAL::Uint width, SGAL::Uint height);

  /*! Draw into a window of the scene.
   * It is assumed that the window context is the current context.
   * \param[in] window_item the window to draw
   * \param[in] dont_accumulate indicates that no accumulation should be
   *                            performed
   */
  virtual void draw_window(SGAL::Window_item* window_item,
                           SGAL::Boolean dont_accumulate);

  /*! Handle a reshape event. */
  virtual void handle(SGAL::Reshape_event* event);

  /*! Handle a draw event. */
  virtual void handle(SGAL::Draw_event* event);

  /*! Handle a tick event. */
  virtual void handle(SGAL::Tick_event* event);

  /*! Handle a keyboard event. */
  virtual void handle(SGAL::Keyboard_event* event);

  /*! Handle a mouse event. */
  virtual void handle(SGAL::Mouse_event* event);

  /*! Handle a motion event. */
  virtual void handle(SGAL::Motion_event* event);

  /*! Handle a passive motion event. */
  virtual void handle(SGAL::Passive_motion_event* event);

  /*! Print out the name of this agent (for debugging purposes). */
  virtual void identify(void);

  /*! Set the window manager. */
  template <typename Window_manager>
  void set_window_manager(Window_manager* manager)
  { m_window_manager = manager; }

  /*! Set the option parser.
   */
  void set_option_parser(Player_option_parser* option_parser);

  /*! Obtain the scene scene-graph.
   */
  SGAL::Scene_graph* get_scene_graph() const;

  /*! Obtain the image used for snapshoting.
   */
  Shared_image get_image() const;

  /*! Set the image used for snapshoting.
   */
  void set_image(Shared_image image);

#if defined(SGAL_USE_V8)
  /*! Obtain an isolated instance of the V8 engine.
   * \return an isolated instance of the V8 engine.
   */
  v8::Isolate* get_isolate();
#endif

  /*! Determine whether the scene does simulate something. */
  SGAL::Boolean is_simulating(void) const;

  /*! Determine whether the application has a visual.
   * \return true if the application has a visual; false otherwise.
   */
  SGAL::Boolean do_have_visual() const;

  /*! Determine whether the scene is interactive. A non interactive scene
   * is being rendered at most once.
   * \return false if the user has explicitly requested for a non interactive
   *         scene, or if she has requested taking a snapshot of scene or
   *         exporting the scene.
   */
  SGAL::Boolean is_interactive() const;

  /*! Obtain the camera projection matrix.
   */
  void get_proj_mat(SGAL::Matrix4f& mat);

  /*! Obtain the camera viewing matrix.
   */
  void get_view_mat(SGAL::Matrix4f& mat);

  /*! Obtain the total volume of all polyhedrons.
   */
  float volume();

  /*! Obtain the total surface area of all polyhedrons.
   */
  float surface_area();

  /*! Obtain the attributes of all polyhedrons.
   */
  void
  process_polyhedron_attributes_array(SGAL::Polyhedron_attributes_array& array)
    const;

protected:
  /*! Initialize the scene. */
  virtual void init();

  /*! Initialize the graphics context. */
  void init_context();

  //! The window manager.
  Window_manager* m_window_manager;

  //! The window item.
  Window_item* m_window_item;

#if defined(SGAL_USE_V8)
  //! An isolated instance of the V8 engine.
  v8::Isolate* m_isolate;
#endif

  //! The scene graph.
  SGAL::Scene_graph* m_scene_graph;

  //! The context.
  SGAL::Context* m_context;

  //! Option parser.
  Player_option_parser* m_option_parser;

  //! The snapshot image.
  Shared_image m_image;

  SGAL::Boolean m_simulate;

  /*! Load the input file. */
  void load_scene_creation();

  /*! Draw guides that separate the window into 4x5 rectangles. */
  void draw_grid(SGAL::Uint width, SGAL::Uint height);

  /*! Print statistic information. */
  void print_stat();

  /*! Updates directory search. */
  void update_data_dirs();

  /*! Print geometry information of Index_face_set. */
  void print_geometry_info(SGAL::Indexed_face_set* ifs);

  /*! Print geometry information of Box. */
  void print_geometry_info(SGAL::Box* box);

  /*! Take a snapshot of the scene. */
  void snapshot_scene();

  /*! Export the scene. */
  void export_scene();

  /*! Create default nodes in the scene graph. */
  void create_defaults();

  /*! Create visual.
   */
  void create_visual();

  /*! Set preferred window attributes.
   */
  void set_preferred_window_attributes();

  /*! Set actual window attributes.
   */
  void set_actual_window_attributes();

public:
  /*! Initialize offscreen rendering.
   */
  void init_offscreen_rendering();

  /*! Clear offscreen rendering.
   */
  void clear_offscreen_rendering();

  /*! Adjust offscreen rendering.
   */
  void adjust_offscreen_rendering(size_t width, size_t height);

private:
  //! The input file full-name.
  std::string m_fullname;

  //! Frame buffer object for off-screen drawing.
  GLuint m_frame_buffer;

  //! Render buffer object for off-screen drawing.
  GLuint m_render_buffers[2];

  /*! Create the scene described in an input file given by it's name.
   */
  void create(const std::string& fullname);
};

//! \brief sets the option parser.
inline void Player_scene::set_option_parser(Player_option_parser* option_parser)
{ m_option_parser = option_parser; }

//! \brief obtains the scene scene-graph.
inline SGAL::Scene_graph* Player_scene::get_scene_graph() const
{ return m_scene_graph; }

#if defined(SGAL_USE_V8)
//! \brief obtains an isolated instance of the V8 engine.
inline v8::Isolate* Player_scene::get_isolate() { return m_isolate; }
#endif

//! \brief obtains the image used for snapshoting
inline Player_scene::Shared_image Player_scene::get_image() const
{ return m_image; }

//! \brief sets the image used for snapshoting.
inline void Player_scene::set_image(Shared_image image) { m_image = image; }

#endif
