// Copyright (c) 2004 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef PLAYER_OPTION_PARSER_HPP
#define PLAYER_OPTION_PARSER_HPP

#include <string>
#include <vector>

#include "SGAL/Main_option_parser.hpp"
#include "SGAL/Window_option_parser.hpp"

namespace po = boost::program_options;

SGAL_BEGIN_NAMESPACE

class scene_graph;

SGAL_END_NAMESPACE

class Player_option_parser : public SGAL::Main_option_parser,
                             public SGAL::Window_option_parser
{
public:
  /*! Construct. */
  Player_option_parser();

  /*! Destruct. */
  virtual ~Player_option_parser() {}

  /*! Initialize. */
  void init();

  /*! Parse the options.
   * \param[in] argc
   * \param[in] argv
   */
  void parse(int argc, char* argv[], bool allow_unregistered = false);

  /*! Apply the options.
   */
  void apply();

  /*! Configure the scene graph.
   * \param[in] scene_graph The scene graph.
   */
  void configure_scene_graph(SGAL::Configuration* conf);

  /*! Configure the window manager
   * \param[in] window_manage The window manager.
   */
  template <typename WindowManager>
  void configure_window_manager(WindowManager* window_manage);

  /*! Determine whether to display a grid.
   */
  bool get_draw_grid() const;

  /*! Determine whether to display texture information.
   */
  SGAL::Boolean get_display_texture_info() const;

  /*! Determine whether to display geometry information.
   */
  SGAL::Boolean get_display_geometry_info() const;

  /*! Determine whether to display polyhedra information.
   */
  SGAL::Boolean get_display_polyhedra_info() const;

  /*! Obtain the name-filter regular expression.
   */
  const SGAL::String& get_name_re() const;

  /*! Obtain the tag-filter regular expression.
   */
  const SGAL::String& get_tag_re() const;

  /*! Determine whether to display container names.
   */
  SGAL::Boolean get_display_names() const;

  /*! Determine whether to perform a benchmark.
   */
  SGAL::Boolean get_bench() const { return m_bench; }

  /*! Determine whether the application should render off screen.
   * \return true if the application should render off screen.
   */
  SGAL::Boolean do_render_offscreen() const;

protected:
  //! The player option description.
  boost::program_options::options_description m_player_opts;

  //! Indicate whether to draw a grid.
  SGAL::Boolean m_grid;

  //! Indicates whether to display texture information.
  SGAL::Boolean m_display_texture_info;

  //! Indicates whether to display geometry information.
  SGAL::Boolean m_display_geometry_info;

  //! Indicates whether to display polyhedra information.
  SGAL::Boolean m_display_polyhedra_info;

  //! Indicates whether to display container names.
  SGAL::Boolean m_display_names;

  //! Indicate whether to perform a benchmark.
  SGAL::Boolean m_bench;

  //! The maximum number of vertices in the index array.
  SGAL::Uint m_sub_index_buffer_size;

  //! The name-filter regular expression.
  SGAL::String m_name_re;

  //! The tag-filter regular expression.
  SGAL::String m_tag_re;

  // Default values
  static const SGAL::String s_def_name_re;
  static const SGAL::String s_def_tag_re;
};

//! \brief configures the scene graph.
inline void
Player_option_parser::configure_scene_graph(SGAL::Configuration* conf)
{ Main_option_parser::configure(conf); }

//! \brief configures the window manager.
template <typename WindowManager>
void
Player_option_parser::configure_window_manager(WindowManager* window_manager)
{
  Window_option_parser::configure(m_variable_map, window_manager);
  window_manager->set_render_offscreen(do_render_offscreen());
}

//! \brief determines whether to display a grid.
inline bool Player_option_parser::get_draw_grid() const { return m_grid; }

//! \brief determines whether to display texture information.
inline SGAL::Boolean Player_option_parser::get_display_texture_info() const
{ return m_display_texture_info; }

//! \brief determines whether to display geometry information.
inline SGAL::Boolean Player_option_parser::get_display_geometry_info() const
{ return m_display_geometry_info; }

//! \brief determines whether to display polyhedra information.
inline SGAL::Boolean Player_option_parser::get_display_polyhedra_info() const
{ return m_display_polyhedra_info; }

//! Obtain the name-filter regular expression.
inline const SGAL::String& Player_option_parser::get_name_re() const
{ return m_name_re; }

//! Obtain the tag-filter regular expression.
inline const SGAL::String& Player_option_parser::get_tag_re() const
{ return m_tag_re; }

//! Determines whether to display container names.
inline SGAL::Boolean Player_option_parser::get_display_names() const
{ return m_display_names; }

#endif
