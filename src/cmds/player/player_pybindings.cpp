// Copyright (c) 2015 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#define BOOST_BIND_GLOBAL_PLACEHOLDERS 1

#include <exception>

#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/python/implicit.hpp>
#include <boost/python/exception_translator.hpp>

#include "SGAL/sgal.hpp"
#include "SGAL/Background.hpp"
#include "SGAL/Bounding_box_3d.hpp"
#include "SGAL/Bounding_sphere.hpp"
#include "SGAL/Camera.hpp"
#include "SGAL/Color_background.hpp"
#include "SGAL/Configuration.hpp"
#include "SGAL/Color_background.hpp"
#include "SGAL/Geometry.hpp"
#include "SGAL/Indexed_face_set.hpp"
#include "SGAL/Input_output.hpp"
#include "SGAL/Frustum.hpp"
#include "SGAL/Group.hpp"
#include "SGAL/Loader_errors.hpp"
#include "SGAL/Modeling.hpp"
#include "SGAL/Navigation_info.hpp"
#include "SGAL/Node.hpp"
#include "SGAL/Scene_graph.hpp"
#include "SGAL/Shape.hpp"
#include "SGAL/Transform.hpp"
#include "SGAL/Vector3f.hpp"
#include "SGAL/Vector4f.hpp"

#include "Player.hpp"

namespace bp = boost::python;

// PyObject* exception_type = nullptr;
PyObject* exception_type = PyExc_BaseException;

//
void translate_exception(std::exception const& e)
{ PyErr_SetString(PyExc_RuntimeError, e.what()); }

//
void translate_loader_error_exception(SGAL::File_error const& e)
{
  bp::object python_exception_instance(e);
  PyErr_SetObject(exception_type, python_exception_instance.ptr());
}

//
void translate_empty_error_exception(SGAL::Empty_error const& e)
{
  bp::object python_exception_instance(e);
  PyErr_SetObject(exception_type, python_exception_instance.ptr());
}

/*! dynamic casts a Background node to a Color_background node.
 */
SGAL::Color_background* dynamic_cast_color_background(SGAL::Background* bg)
{ return dynamic_cast<SGAL::Color_background*>(bg); }

/*! @brief Adapter a non-member function that returns a unique_ptr to a python
 * function object that returns a raw pointer but explicitly passes ownership to
 * Python.
 */
template <typename T, typename ...Args>
boost::python::object adapt_unique(std::unique_ptr<T> (*fn)(Args...))
{
  return bp::make_function
    ([fn](Args... args) { return fn(args...).release(); },
     bp::return_value_policy<bp::manage_new_object>(),
     boost::mpl::vector<T*, Args...>());
}

/*! @brief Adapter a member function that returns a unique_ptr to a python
 * function object that returns a raw pointer but explicitly passes ownership to
 * Python.
 */
template <typename T, typename C, typename ...Args>
boost::python::object adapt_unique(std::unique_ptr<T> (C::*fn)(Args...))
{
  return bp::make_function
    ([fn](C& self, Args... args) { return (self.*fn)(args...).release(); },
     bp::return_value_policy<bp::manage_new_object>(),
     boost::mpl::vector<T*, C&, Args...>());
}

/*! @brief A wrapper function that converts Python bytes into char*
 */
void create_from_bytes(Player& self, bp::object py_buffer) {
  int size = bp::extract<int>(py_buffer.attr("__len__")());
  bp::stl_input_iterator<unsigned char> begin(py_buffer), end;
  // Copy the py_buffer into a local buffer with known continguous memory:
  auto* buffer = new unsigned char[size];
  size_t i(0);
  for (auto it = begin; it != end; ++it) buffer[i++] = *it;
  self.create(buffer, size);
  delete[] buffer;
}

BOOST_PYTHON_MODULE(player)
{
  typedef boost::shared_ptr<SGAL::Container>            Shared_container;
  typedef boost::shared_ptr<SGAL::Geometry>             Shared_geometry;
  typedef boost::shared_ptr<SGAL::Shape>                Shared_shape;
  typedef boost::shared_ptr<SGAL::Group>                Shared_group;
  typedef boost::shared_ptr<SGAL::Transform>            Shared_transform;
  typedef boost::shared_ptr<SGAL::Configuration>        Shared_configuration;
  typedef boost::shared_ptr<SGAL::Input_output>         Shared_input_output;
  typedef boost::shared_ptr<SGAL::Geo_set>              Shared_geo_set;
  typedef boost::shared_ptr<SGAL::Mesh_set>             Shared_mesh_set;
  typedef boost::shared_ptr<SGAL::Boundary_set>         Shared_boundary_set;
  typedef boost::shared_ptr<SGAL::Indexed_face_set>     Shared_indexed_face_set;

  typedef boost::shared_ptr<SGAL::Boolean_array>        Shared_boolean_array;
  typedef boost::shared_ptr<SGAL::Float_array>          Shared_float_array;
  typedef boost::shared_ptr<SGAL::Uint_array>           Shared_uint_array;
  typedef boost::shared_ptr<SGAL::Int32_array>          Shared_int32_array;
  typedef boost::shared_ptr<SGAL::Vector2f_array>       Shared_vector2f_array;
  typedef boost::shared_ptr<SGAL::Vector3f_array>       Shared_vector3f_array;
  typedef boost::shared_ptr<SGAL::Vector4f_array>       Shared_vector4f_array;
  typedef boost::shared_ptr<SGAL::Rotation_array>       Shared_rotation_array;
  typedef boost::shared_ptr<SGAL::String_array>         Shared_string_array;
  typedef boost::shared_ptr<SGAL::Shared_container_array>
    Shared_shared_container_array;

  // bp::register_ptr_to_python<Player::Shared_string_array>();
  // bp::register_ptr_to_python<Shared_shared_container_array>();
  bp::register_ptr_to_python<Shared_configuration>();
  bp::register_ptr_to_python<Shared_input_output>();

  // Do not use the abreviated registration of the base Container class.
  // Instead, use the class_ definition as it used as a base for all the
  // classes that derive from it.
  // bp::register_ptr_to_python<Shared_container>();
  bp::class_<SGAL::Container, Shared_container,
             boost::noncopyable>("Container", bp::no_init)
    .def("get_name", &SGAL::Container::get_name,
         bp::return_value_policy<bp::copy_const_reference>())
    .def("set_name", &SGAL::Container::set_name)
    ;

  // The following code attempts to map a C++ execption. It used to work.
  // It doesn't work with python3 (and probably with pythos >= 2.6.
  // Since version 2.6 all python exceptions must inherit from BaseException.
  // However, the mapped exception do not inherit as prescribed above, and I
  // do not know how it can be done.

  // General error
  // bp::register_exception_translator<std::exception>(&translate_exception);

  // Loader error
  // bp::class_<SGAL::Loader_error>
  //   loader_error_class("Loader_error",
  //                      bp::init<const std::string&, const std::string&>());
  // // exception_type = loader_error_class.ptr();
  // bp::register_exception_translator<SGAL::Loader_error>
  //   (translate_loader_error_exception);
  // loader_error_class
  //   .def("filename", &SGAL::Loader_error::filename,
  //        bp::return_value_policy<bp::copy_const_reference>())
  //   .def("what", &SGAL::Loader_error::what)
  //   ;

  // // Empty error
  // bp::class_<SGAL::Empty_error>
  //   empty_error_class("Empty_error", bp::init<const std::string&>());
  // // exception_type = empty_error_class.ptr();
  // bp::register_exception_translator<SGAL::Empty_error>
  //   (translate_empty_error_exception);
  // empty_error_class
  //   .def("filename", &SGAL::Empty_error::filename,
  //        bp::return_value_policy<bp::copy_const_reference>())
  //   .def("what", &SGAL::Empty_error::what)
  //   ;

  // Arrays
  // automatically wraps these special Python methods (taken from the Python reference: Emulating container types):
  bp::class_<SGAL::Boolean_array, Shared_boolean_array>("BooleanArray")
    .def(bp::vector_indexing_suite<SGAL::Boolean_array>());
  bp::class_<SGAL::Float_array, Shared_float_array>("FloatArray")
    .def(bp::vector_indexing_suite<SGAL::Float_array>());
  bp::class_<SGAL::Uint_array, Shared_uint_array>("UintArray")
    .def(bp::vector_indexing_suite<SGAL::Uint_array>());
  bp::class_<SGAL::Int32_array, Shared_int32_array>("Int32Array")
    .def(bp::vector_indexing_suite<SGAL::Int32_array>());
  bp::class_<SGAL::Vector2f_array, Shared_vector2f_array>("Vector2fArray")
    .def(bp::vector_indexing_suite<SGAL::Vector2f_array>());
  bp::class_<SGAL::Vector3f_array, Shared_vector3f_array>("Vector3fArray")
    .def(bp::vector_indexing_suite<SGAL::Vector3f_array>());
  bp::class_<SGAL::Vector4f_array, Shared_vector4f_array>("Vector4fArray")
    .def(bp::vector_indexing_suite<SGAL::Vector4f_array>());
  bp::class_<SGAL::Rotation_array, Shared_rotation_array>("RotationArray")
    .def(bp::vector_indexing_suite<SGAL::Rotation_array>());
  bp::class_<SGAL::String_array, Shared_string_array>("StringArray")
    .def(bp::vector_indexing_suite<SGAL::String_array>());

  // By default indexed elements have Python reference semantics and are
  // returned by proxy. However, in the case of a vector of shared elements,
  // this must be disabled. For example, the operator [] returns a reference to
  // a Shared_element, but we need the element vy value.
  bp::class_<SGAL::Shared_container_array, Shared_shared_container_array>
    ("SharedContainerArray")
    .def(bp::vector_indexing_suite<SGAL::Shared_container_array, true>())
    // .def<Shared_container& (SGAL::Shared_container_array::*)
    //      (const SGAL::Shared_container_array::size_type)>
    // ("__getitem__", &SGAL::Shared_container_array::operator[],
    //  bp::return_value_policy<bp::copy_non_const_reference>())
    ;

  // Player
  bp::class_<Player>("Player", bp::init<SGAL::String_array>())
    // .def("__call__", static_cast<void(Player::*)()>(&Player::operator()))
    // .def("__call__",
    //      static_cast<void(Player::*)(char*, int)>(&Player::operator()))
    .def("configure", static_cast<void(Player::*)()>(&Player::configure))
    .def("create", static_cast<void(Player::*)()>(&Player::create))
    .def("indulge", static_cast<void(Player::*)()>(&Player::indulge))

    // Scene creators
    .def<void(Player::*)(const std::string&)>("createFromFile", &Player::create)
    .def<void(Player::*)(const char*)>("createFromData", &Player::create)
    .def<void(Player::*)(const char*, const std::string&)>
    ("createFromData", &Player::create)
    .def("createFromBytes", &create_from_bytes)
    .def<void(Player::*)(const char*, const std::string&)>
    ("createFromBase64Data", &Player::create_from_encoded)
    .def<void(Player::*)(const char*, size_t size, const std::string&)>
    ("createFromBase64Bytes", &Player::create_from_encoded)
    .def<void(Player::*)(const char*, size_t size)>
    ("createFromBase64Bytes", &Player::create_from_encoded)

    .def("visualize", &Player::visualize)
    .def("init_visual", &Player::init_visual)
    .def("process_visual", &Player::process_visual)
    .def("clear_visual", &Player::clear_visual)
    .def("destroy", &Player::destroy)
    .def("clear", &Player::clear)
    .def("volume", &Player::volume)
    .def("surface_area", &Player::surface_area)
    .def("get_polyhedron_attributes_array",
         &Player::get_polyhedron_attributes_array,
         bp::return_value_policy<bp::reference_existing_object>())
    .def<void(Player::*)(const std::string&, const std::string&,
                         bool)>("export_scene", &Player::export_scene)
    .def<std::string(Player::*)(const std::string&)>("exportSceneToString",
                                                     &Player::export_scene)
    .def<void(Player::*)(const std::string&, const std::string&,
                         size_t, size_t)>("snapshot_scene",
                                          &Player::snapshot_scene)

    // Getters
    .def("get_scene_graph", &Player::get_scene_graph,
         bp::return_value_policy<bp::reference_existing_object>())
    .def("get_configuration", &Player::get_configuration,
         bp::return_value_policy<bp::reference_existing_object>())
    .def("get_background", &Player::get_background,
         bp::return_value_policy<bp::reference_existing_object>())
    .def("get_container", &Player::get_container)

    // Handle Bindable stacks
    .def("add_color_background", &Player::add_color_background,
         bp::return_value_policy<bp::reference_existing_object>())
    ;

  bp::class_<SGAL::Polyhedron_attributes_array>("Polyhedron_attributes_array")
    .def("number_of_vertices",
         &SGAL::Polyhedron_attributes_array::number_of_vertices)
    .def("number_of_edges",
         &SGAL::Polyhedron_attributes_array::number_of_edges)
    .def("number_of_facets",
         &SGAL::Polyhedron_attributes_array::number_of_facets)
    .def("number_of_connected_components",
         &SGAL::Polyhedron_attributes_array::number_of_connected_components)
    .def("volume", &SGAL::Polyhedron_attributes_array::volume)
    .def("surface_area", &SGAL::Polyhedron_attributes_array::surface_area)
    .def("bounding_box", &SGAL::Polyhedron_attributes_array::bounding_box)
    .def("bounding_sphere", &SGAL::Polyhedron_attributes_array::bounding_sphere)
    .def("is_valid", &SGAL::Polyhedron_attributes_array::is_valid)
    ;

  bp::class_<SGAL::Bounding_box_3d>("Bounding_box_3d")
    .def("xmin", &SGAL::Bounding_box_3d::xmin)
    .def("ymin", &SGAL::Bounding_box_3d::ymin)
    .def("zmin", &SGAL::Bounding_box_3d::zmin)
    .def("xmax", &SGAL::Bounding_box_3d::xmax)
    .def("ymax", &SGAL::Bounding_box_3d::ymax)
    .def("zmax", &SGAL::Bounding_box_3d::zmax)
    ;

  bp::class_<SGAL::Bounding_sphere>("Bounding_sphere")
    .def("radius", &SGAL::Bounding_sphere::get_radius)
    .def("center", &SGAL::Bounding_sphere::get_center,
         bp::return_value_policy<bp::reference_existing_object>())
    ;

  bp::class_<SGAL::Vector3f>("Vector3f", bp::init<float, float, float>())
    .def("x", &SGAL::Vector3f::x)
    .def("y", &SGAL::Vector3f::y)
    .def("z", &SGAL::Vector3f::z)
    ;

  bp::class_<SGAL::Vector4f>("Vector4f", bp::init<float, float, float, float>())
    .def("x", &SGAL::Vector4f::x)
    .def("y", &SGAL::Vector4f::y)
    .def("z", &SGAL::Vector4f::z)
    .def("w", &SGAL::Vector4f::w)
    ;

  // Geometry
  bp::class_<SGAL::Geometry, bp::bases<SGAL::Container>,
             boost::noncopyable>("Geometry", bp::no_init)
    ;

  // Navigation info
  bp::class_<SGAL::Navigation_info, bp::bases<SGAL::Container>,
             boost::noncopyable>("NavigationInfo", bp::no_init)
    ;

  // Frustum
  bp::class_<SGAL::Frustum, bp::bases<SGAL::Container>,
             boost::noncopyable>("Frustum", bp::no_init)
    .def("set_type",
         static_cast<void(SGAL::Frustum::*)(const std::string&)>
           (&SGAL::Frustum::set_type))
    ;

  // Camera
  bp::class_<SGAL::Camera, bp::bases<SGAL::Container>,
             boost::noncopyable>("Camera", bp::no_init)
    .def("get_frustum", &SGAL::Camera::get_frustum,
         bp::return_value_policy<bp::reference_existing_object>())
    .def("set_radius_scale",
         static_cast<void(SGAL::Camera::*)(float)>
           (&SGAL::Camera::set_radius_scale))
    ;

  // Background
  bp::class_<SGAL::Color_background, bp::bases<SGAL::Container>,
             boost::noncopyable>("ColorBackground", bp::no_init)
    .def("set_color",
         static_cast<void(SGAL::Color_background::*)(const SGAL::Vector4f&)>
           (&SGAL::Color_background::set_color))
    ;

  // Scene graph

  // Handling construct_container() overloading
  Shared_container
    (SGAL::Scene_graph::*construct_container)(const SGAL::String&) =
    &SGAL::Scene_graph::construct_container;

  Shared_container
    (SGAL::Scene_graph::*construct_container_with_name)
    (const SGAL::String&, const SGAL::String&) =
    &SGAL::Scene_graph::construct_container;

  // Handling get_geometries() overloading
  Shared_shared_container_array
    (SGAL::Scene_graph::*get_geometries)() const =
    &SGAL::Scene_graph::get_geometries;

  Shared_shared_container_array
    (SGAL::Scene_graph::*get_geometries_by_name)(const SGAL::String&) const =
    &SGAL::Scene_graph::get_geometries;

  bp::class_<SGAL::Scene_graph,
             boost::noncopyable>("SceneGraph", bp::init<bool>())
    .def("get_container", &SGAL::Scene_graph::get_container)
    .def("construct_container", construct_container)
    .def("construct_container_with_name", construct_container_with_name)
    .def("get_configuration",
         static_cast<SGAL::Configuration*(SGAL::Scene_graph::*)()>
           (&SGAL::Scene_graph::get_configuration),
         bp::return_value_policy<bp::reference_existing_object>())
    .def("get_active_navigation_info",
         static_cast<SGAL::Navigation_info*(SGAL::Scene_graph::*)()>
           (&SGAL::Scene_graph::get_active_navigation_info),
         bp::return_value_policy<bp::reference_existing_object>())
    .def("get_active_background",
         static_cast<SGAL::Background*(SGAL::Scene_graph::*)()>
           (&SGAL::Scene_graph::get_active_background),
         bp::return_value_policy<bp::reference_existing_object>())
    .def("get_active_camera",
         static_cast<SGAL::Camera*(SGAL::Scene_graph::*)()>
           (&SGAL::Scene_graph::get_active_camera),
         bp::return_value_policy<bp::reference_existing_object>())
    .def("centroid", &SGAL::Scene_graph::centroid,
         bp::return_value_policy<bp::return_by_value>())
    .def("get_all_names", &SGAL::Scene_graph::get_all_names)
    .def("get_geometry_names", &SGAL::Scene_graph::get_geometry_names)
    .def("get_node_names", &SGAL::Scene_graph::get_node_names)
    .def("get_geometries", get_geometries)
    .def("get_geometries_by_name", get_geometries_by_name)
    .def("get_nodes", &SGAL::Scene_graph::get_nodes)

    .def("get_navigation_root", &SGAL::Scene_graph::get_navigation_root)
    ;

  // Input_output
  bp::class_<SGAL::Input_output, bp::bases<SGAL::Container>,
             boost::noncopyable>("InputOutput", bp::no_init)
    .def("set_export_scene_root",
         static_cast<void(SGAL::Input_output::*)(const std::string&)>
         (&SGAL::Input_output::set_export_scene_root))
    ;

  // Modeling
  bp::class_<SGAL::Modeling, bp::bases<SGAL::Container>,
             boost::noncopyable>("Modeling", bp::no_init)
    ;

  // Configuration
  bp::class_<SGAL::Configuration, bp::bases<SGAL::Container>,
             boost::noncopyable>("Configuration", bp::no_init)
    .def("get_input_output", &SGAL::Configuration::get_input_output)
    .def("get_modeling", &SGAL::Configuration::get_modeling)
    ;

  // Geometry
  bp::class_<SGAL::Geometry, Shared_geometry, bp::bases<SGAL::Container>,
             boost::noncopyable>("Geometry", bp::no_init)
    ;

  // Shape
  bp::class_<SGAL::Shape, Shared_shape, bp::bases<SGAL::Container>,
             boost::noncopyable>("Shape", bp::no_init)
    .def("set_geometry", &SGAL::Shape::set_geometry)
    .def("get_geometry", &SGAL::Shape::get_geometry)
    ;

  // Group
  bp::class_<SGAL::Group, Shared_group, bp::bases<SGAL::Container>,
             boost::noncopyable>("Group", bp::no_init)
    .def("add_child", &SGAL::Group::add_child)
    ;

  // Transform
  bp::class_<SGAL::Transform, Shared_transform, bp::bases<SGAL::Group>,
             boost::noncopyable>("Transform", bp::no_init)
    ;

  // Geo_set
  bp::class_<SGAL::Geo_set, Shared_geo_set, bp::bases<SGAL::Geometry>,
             boost::noncopyable>("GeoSet", bp::no_init)
    ;

  // Mesh_set
  bp::class_<SGAL::Mesh_set, Shared_mesh_set, bp::bases<SGAL::Geo_set>,
             boost::noncopyable>("MeshSet", bp::no_init)
    ;

  // Boundary_set
  bp::class_<SGAL::Boundary_set, Shared_boundary_set, bp::bases<SGAL::Mesh_set>,
             boost::noncopyable>("BoundarySet", bp::no_init)
    ;

  // Indexed_face_set
  bp::class_<SGAL::Indexed_face_set, Shared_indexed_face_set,
             bp::bases<SGAL::Boundary_set>,
             boost::noncopyable>("IndexedFaceSet", bp::no_init)
    .def("is_polyhedron_empty",  &SGAL::Indexed_face_set::is_polyhedron_empty)
    .def("is_polyhedron_valid",  &SGAL::Indexed_face_set::is_polyhedron_valid)
    ;

  // Dynamic casts
  bp::def("dynamic_cast_color_background",
          dynamic_cast_color_background,
          bp::return_value_policy<bp::reference_existing_object>())
    ;
}
