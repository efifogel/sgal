import math

class Vrml:
  def __init__(self, file):
    self.out = file
    self.indent = 0
    self.delta = 2
    self.newline = True
    self.magic = '''#VRML V2.0 utf8'''
    self.copyright_text = '''# Copyright (c) 2023 Israel.
# All rights reserved to Efi Fogel
'''

  #! Increase the indentation level.
  def increase_indent(self):
    self.indent += self.delta

  #! Decrease the indentation level.
  def decrease_indent(self):
    self.indent -= self.delta

  #! Print a single indented line.
  def print_line(self, line, eol=True, inc=False, dec=False):
    if (dec): self.decrease_indent()
    if self.newline:
      print ('{}{}'.format(self.indent * ' ', line), file=self.out, end='\n' if eol else '')
    else:
      print ('{}'.format(line), file=self.out, end='\n' if eol else '')
    if (inc): self.increase_indent()
    self.newline = eol

  #! Print an empty line.
  def print_empty_line(self):
    self.print_line('')

  def print_vec(self, x, y, z):
    self.print_line('{} {} {},'.format(x, y, z))

  def triangle(self, a, b, c):
    self.print_line('{} {} {} -1,'.format(a, b, c))

  def quad(self, a, b, c, d, tris=True):
    if tris:
      self.triangle(a, b, c)
      self.triangle(a, c, d)
    else:
      self.print_line('{} {} {} {} -1,'.format(a, b, c, d))

  def print_poly(self, num, offset, ccw=True, tris=True):
    if tris:
      if ccw:
        for i in range(num-2):
          self.triangle(offset, i+1+offset, i+2+offset)
      else:
        for i in reversed(range(num-2)):
          self.triangle(num-1+offset, i+1+offset, i+offset)
    else:
      if ccw:
        for i in range(num):
          self.print_line(str(i+offset) + ' ', eol=False)
        self.print_line("-1,")
      else:
        for i in reversed(range(num)):
          self.print_line(str(i+offset) + ' ', eol=False)
        self.print_line("-1")

  def print_node_start(self, type, name=''):
    if name:
      self.print_line('DEF {} {} {{'.format(name, type), inc=True);
    else:
      self.print_line('{} {{'.format(type), inc=True);

  def print_node_end(self):
    self.print_line('}', dec=True)

  def print_scalar_field(self, name, value):
    self.print_line('{} {}'.format(name, value))

  def print_field(self, name, fnc, *args):
    self.print_line(name, eol=False)
    if not args:
      fnc(self.out)
    else:
      fnc(self.out, args)

  def print_field_node_start(self, field_name, node_type, node_name=''):
    self.print_line('{} '.format(field_name), eol=False)
    self.print_node_start(node_type, node_name)

  def print_header(self, name):
    self.print_line(self.magic)
    self.print_line("# Generated by " + name)
    self.print_line(self.copyright_text)

  def sphere(self, stacks, slices, radius, tris=True):
    self.print_node_start('Shape', 'SHAPE')
    self.print_field_node_start('appearance', 'Appearance')
    self.print_field_node_start('material', 'Material')
    self.print_scalar_field('diffuseColor', '0.4 0.4 0.8')
    self.print_scalar_field('specularColor', '0.1 0.1 0.1')
    self.print_node_end()
    self.print_node_end()
    self.print_field_node_start('geometry', 'IndexedFaceSet', 'GEOMETRY')
    self.print_scalar_field('colorPerVertex', 'FALSE')
    self.print_field_node_start('coord', 'Coordinate')
    self.print_line('point [', inc=True)
    gamma = 2.0 * math.pi
    delta = math.pi
    d_gamma = gamma / slices;
    d_delta = delta / stacks;
    alpha = 0
    for i in range(slices):
      sin_alpha = math.sin(alpha)
      cos_alpha = math.cos(alpha)
      beta = -0.5 * math.pi + d_delta
      for j in range(stacks-1):
        sin_beta = math.sin(beta)
        cos_beta = math.cos(beta)
        x = radius * cos_alpha * cos_beta
        y = radius * sin_alpha * cos_beta
        z = radius * sin_beta
        self.print_vec(x, y, z)
        beta += d_delta
      alpha += d_gamma
    self.print_vec(0, 0, -1)
    self.print_vec(0, 0, 1)
    self.print_line(']', dec=True)
    self.print_node_end()
    self.print_line('coordIndex [', inc=True)
    for i in range(slices-1):
      b = i*(stacks-1)
      for j in range(stacks-2):
        self.quad(b+j, b+j+stacks-1, b+j+stacks, b+j+1, tris=tris)
    # Last slice
    i = slices-1
    b = i*(stacks-1)
    for j in range(stacks-2):
      self.quad(b+j, j, j+1, b+j+1, tris=tris)
    # South poll
    p = slices*(stacks-1)
    for i in range(slices-1):
      b = i*(stacks-1)
      self.triangle(b, p, b+stacks-1)
    i = slices-1
    b = i*(stacks-1)
    self.triangle(b, p, 0)
    # North poll
    p += 1
    for i in range(slices-1):
      b = i*(stacks-1)
      self.triangle(b+stacks-2, b+stacks-1+stacks-2, p)
    i = slices-1
    b = i*(stacks-1)
    self.triangle(b+stacks-2, stacks-2, p)
    self.print_line(']', dec=True)
    self.print_node_end()
    self.print_node_end()
