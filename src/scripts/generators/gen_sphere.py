#!/usr/bin/python

import os.path
import argparse
import math
import numpy as np
import vrml

def generate(out, stacks, slices, radius, tris=True):
  v = vrml.Vrml(out)
  cx = 0
  cy = 0
  v.print_header(os.path.basename(__file__))
  v.sphere(stacks, slices, radius, tris)

def generate_from_path(output_path, basename, stacks, slices, radius, tris=True):
  filename = os.path.join(output_path, basename + str(stacks) + ".wrl")
  with open(filename, 'w') as out:
    generate(out, stacks, slices, radius, tris=tris)

def readable_dir(prospective_dir):
  if not os.path.isdir(prospective_dir):
    parser.error("readable_dir:{0} is not a valid path".format(prospective_dir))
  if os.access(prospective_dir, os.R_OK):
    return prospective_dir
  else:
    parser.error("readable_dir:{0} is not a readable dir".format(prospective_dir))

# Main function
if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Generate a sphere.')
  parser.add_argument('--output-path', type=readable_dir,
                      dest='output_path', default='.')
  parser.add_argument('basename', metavar="basename", nargs='?',
                      default='sphere', help='the basename')
  parser.add_argument('--stacks', type=int, default=16,
                      dest='stacks', help='no. of stacks')
  parser.add_argument('--slices', type=int, default=16,
                      dest='slices', help='no. of slices')
  parser.add_argument('--radius', type=float, default=1,
                      dest='radius', help='radius')
  parser.add_argument('--tris', default=False, action='store_true')
  args = parser.parse_args()
  generate_from_path(args.output_path, args.basename, args.stacks, args.slices,
                     args.radius, tris=args.tris)
  exit()
