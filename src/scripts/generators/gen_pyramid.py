#!/usr/bin/python3

import os.path
import argparse
import math
import numpy as np
import vrml

def generate(out, size, height, radius, tris=True):
  v = vrml.Vrml(out)
  cx = 0
  cy = 0
  v.print_header(os.path.basename(__file__))
  v.print_node_start('Shape', 'WORKPIECE_SHAPE')
  v.print_field_node_start('appearance', 'Appearance')
  v.print_field_node_start('material', 'Material')
  v.print_scalar_field('diffuseColor', '0.4 0.4 0.8')
  v.print_scalar_field('specularColor', '0.1 0.1 0.1')
  v.print_node_end()
  v.print_node_end()
  v.print_field_node_start('geometry', 'IndexedFaceSet', 'WORKPIECE')
  v.print_scalar_field('colorPerVertex', 'FALSE')
  v.print_field_node_start('coord', 'Coordinate')
  v.print_line('point [', inc=True)
  delta = 2.0 * math.pi / size
  for i in range(size):
    x = cx + radius * math.sin(i * delta)
    y = cy + radius * math.cos(i * delta)
    v.print_vec(x, y, 0)
  v.print_vec(cx, cy, height)
  v.print_line(']', dec=True)
  v.print_node_end()
  v.print_line('coordIndex [', inc=True)
  for i in range(size-1):
    v.triangle(i+1, i, size)
  v.triangle(0, size-1, size)
  v.print_poly(size, 0, ccw=True, tris=tris)
  v.print_line(']', dec=True)
  v.print_node_end()
  v.print_node_end()

def generate_from_path(output_path, basename, size, height, radius, tris=True):
  filename = os.path.join(output_path, basename + str(size) + ".wrl")
  with open(filename, 'w') as out:
    generate(out, size, height, radius, tris=tris)

def readable_dir(prospective_dir):
  if not os.path.isdir(prospective_dir):
    parser.error("readable_dir:{0} is not a valid path".format(prospective_dir))
  if os.access(prospective_dir, os.R_OK):
    return prospective_dir
  else:
    parser.error("readable_dir:{0} is not a readable dir".format(prospective_dir))

# Main function
if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Generate a pyramid.')
  parser.add_argument('--output-path', type=readable_dir,
                      dest='output_path', default='.')
  parser.add_argument('basename', metavar="basename", nargs='?',
                      default='pyramid',
                      help='the basename')
  parser.add_argument('--size', type=int, required=True,
                      dest='size', help='no. of corners')
  parser.add_argument('--height', type=float, default=1,
                      dest='height', help='height')
  parser.add_argument('--radius', type=float, default=1,
                      dest='radius', help='radius')
  parser.add_argument('--tris', default=False, action='store_true')
  args = parser.parse_args()
  generate_from_path(args.output_path, args.basename, args.size, args.height,
                     args.radius, tris=args.tris)
  exit()
