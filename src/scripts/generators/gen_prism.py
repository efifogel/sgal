#!/usr/bin/python3
# export PYTHONPATH=...

import os
import sys
import argparse
import pathlib
import math
import vrml

def generate(out, height, radius, num):
  v = vrml.Vrml(out)
  cx = 0
  cy = 0
  delta = 2.0 * math.pi / num

  # print("l:", l)
  v.print_header(os.path.basename(__file__))
  v.print_node_start('Shape', 'WORKPIECE_SHAPE')
  v.print_field_node_start('appearance', 'Appearance')
  v.print_field_node_start('material', 'Material')
  v.print_scalar_field('diffuseColor', '0.4 0.4 0.8')
  v.print_scalar_field('specularColor', '0.1 0.1 0.1')
  v.print_node_end()
  v.print_node_end()
  v.print_field_node_start('geometry', 'IndexedFaceSet', 'WORKPIECE')
  v.print_scalar_field('colorPerVertex', 'FALSE')
  v.print_field_node_start('coord', 'Coordinate')
  v.print_line('point [', inc=True)

  for i in range(0, num):
    angle = i * delta
    x = cx + radius * math.cos(angle)
    y = cy + radius * math.sin(angle)
    v.print_vec(x, y, 0)
  for i in range(0, num):
    angle = i * delta
    x = cx + radius * math.cos(angle)
    y = cy + radius * math.sin(angle)
    v.print_vec(x, y, height)
  v.print_line(']', dec=True)
  v.print_node_end()
  v.print_line('coordIndex [', inc=True)

  # Bottom & top:
  for i in range(1, num-1):
    v.triangle(0, i+1, i)
    v.triangle(num, num+i, num+i+1)
  # walls
  for i in range(0, num-1):
    v.triangle(i, i+1, num+i+1)
    v.triangle(i, num+i+1, num+i)
  v.triangle(num-1, 0, num)
  v.triangle(num-1, num, num+num-1)

  v.print_line(']', dec=True)
  v.print_node_end()
  v.print_node_end()

def generate_from_path(output_path, basename, height, radius, num):
  filename = os.path.join(output_path, basename + ".wrl")
  with open(filename, 'w') as out:
    generate(out, height, radius, num)

def readable_dir(prospective_dir):
  if not os.path.isdir(prospective_dir):
    parser.error("readable_dir:{0} is not a valid path".format(prospective_dir))
  if os.access(prospective_dir, os.R_OK):
    return prospective_dir
  else:
    parser.error("readable_dir:{0} is not a readable dir".format(prospective_dir))

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Generate a 3D triangular mesh of a box with holes')
  parser.add_argument('filename', metavar='filename', nargs='?', default='prism', help='the output file name')
  parser.add_argument('--output-path', type=readable_dir, dest='output_path', default='.')
  parser.add_argument('--num', type=int, default=4, help='the number of segments')
  parser.add_argument('--radius', type=float, default=1, help='the radius of a hole')
  parser.add_argument('--height', type=float, default=1.0, help='the height of object')
  args = parser.parse_args()
  generate_from_path(args.output_path, args.filename, args.height, args.radius, args.num)
