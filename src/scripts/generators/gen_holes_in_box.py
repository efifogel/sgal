#!/usr/bin/python3
# export PYTHONPATH=...

import os
import sys
import argparse
import pathlib
import math
import CGALPY_kerEpic_tri2CdelNciWiWi as CGALPY
import vrml

def mark_sub_domains(tri, start, index, border):
  if start.info() != -1: return
  queue = []
  queue.append(start)
  while queue:
    f = queue.pop(0)
    if f.info() == -1:
      f.set_info(index)
      for i in range(3):
        e = [f, i]
        n = f.neighbor(i)
        if n.info() == -1:
          if f.is_constrained(i): border.append(e)
          else: queue.append(n)

def mark_domains(tri):
  for f in tri.all_faces():
    f.set_info(-1)
  border = []
  mark_sub_domains(tri, tri.infinite_face(), 0, border)
  while border:
    e = border.pop(0)
    n = e[0].neighbor(e[1])
    if n.info() == -1:
      mark_sub_domains(tri, n, e[0].info()+1, border)

def insert_point(tri, p, l):
  v = tri.insert(p)
  v.set_info(l)
  return v, l+1

def generate(out, radius, margin, height, num, xnum, ynum):
  # print("radius:", radius)
  # print("margin:", margin)
  edge_length = (radius + margin) * 2.0
  x = xnum * edge_length
  y = ynum * edge_length
  # print("x:", x)
  # print("y:", y)
  delta = 2.0 * math.pi / num
  # print("num:", num)
  ct = CGALPY.Tri2.Triangulation_2()
  # print("ynum:", ynum)
  # print("xnum:", xnum)
  # Add the bounding square
  corner_points = [
    CGALPY.Ker.Point_2(0, 0),
    CGALPY.Ker.Point_2(x, 0),
    CGALPY.Ker.Point_2(x, y),
    CGALPY.Ker.Point_2(0, y)
  ]
  l = 0
  corner_vertices = []
  holes = []
  for i in range(4):
    v, l = insert_point(ct, corner_points[i], l)
    corner_vertices.append(v)
  ct.insert_constraint(corner_vertices[0], corner_vertices[1])
  ct.insert_constraint(corner_vertices[1], corner_vertices[2])
  ct.insert_constraint(corner_vertices[2], corner_vertices[3])
  ct.insert_constraint(corner_vertices[3], corner_vertices[0])
  # Insert the holes
  for j in range(ynum):
    cy = margin + edge_length * j + radius
    # print("cy:", cy)
    for i in range(xnum):
      cx = margin + edge_length * i + radius
      # print("cx:", cx)
      hx = cx + radius
      hy = cy
      p = CGALPY.Ker.Point_2(hx, hy)
      # print("p", p)
      hole = []
      hole.append(l)
      start_p, l = insert_point(ct, p, l)
      prev_p = start_p
      for k in range(1, num):
        angle = k * delta
        hx = cx + radius * math.cos(angle)
        hy = cy + radius * math.sin(angle)
        p = CGALPY.Ker.Point_2(hx, hy)
        # print("p", p)
        hole.append(l)
        next_p, l = insert_point(ct, p, l)
        ct.insert_constraint(prev_p, next_p)
        prev_p = next_p
      ct.insert_constraint(prev_p, start_p)
      holes.append(hole)

  mark_domains(ct)
  # print("l:", l)

  v = vrml.Vrml(out)
  v.print_header(os.path.basename(__file__))
  v.print_node_start('Shape', 'WORKPIECE_SHAPE')
  v.print_field_node_start('appearance', 'Appearance')
  v.print_field_node_start('material', 'Material')
  v.print_scalar_field('diffuseColor', '0.4 0.4 0.8')
  v.print_scalar_field('specularColor', '0.1 0.1 0.1')
  v.print_node_end()
  v.print_node_end()
  v.print_field_node_start('geometry', 'IndexedFaceSet', 'WORKPIECE')
  v.print_scalar_field('colorPerVertex', 'FALSE')
  v.print_field_node_start('coord', 'Coordinate')
  v.print_line('point [', inc=True)

  for fv in ct.finite_vertices():
    p = fv.point()
    i = fv.info()
    v.print_vec(p.x(), p.y(), 0)
  for fv in ct.finite_vertices():
    p = fv.point()
    i = fv.info()
    v.print_vec(p.x(), p.y(), height)
  v.print_line(']', dec=True)
  v.print_node_end()
  v.print_line('coordIndex [', inc=True)

  # External
  for i in range(4):
    j = (i+1) % 4
    v.triangle(i, l+j, l+i)
    v.triangle(i, j, l+j)

  # Bottom & top:
  for f in ct.finite_faces():
    nesting_level = f.info()
    if nesting_level % 2 != 1: continue;
    i = f.vertex(0).info()
    j = f.vertex(1).info()
    k = f.vertex(2).info()
    v.triangle(l+i, l+j, l+k)
    v.triangle(i, k, j)

  # Internal walls
  for h in holes:
    v.print_line("# Internal walls")
    for i, j in zip(h, h[1:]):
      v.triangle(i, l+i, l+j)
      v.triangle(i, l+j, j)
    i = h[-1]
    j = h[0]
    v.triangle(i, l+i, l+j)
    v.triangle(i,l+j,  j)

  v.print_line(']', dec=True)
  v.print_node_end()
  v.print_node_end()

def generate_from_path(output_path, basename, radius, margin, height, num, xnum, ynum):
  filename = os.path.join(output_path, basename + ".wrl")
  with open(filename, 'w') as out:
    generate(out, radius, margin, height, num, xnum, ynum)

def readable_dir(prospective_dir):
  if not os.path.isdir(prospective_dir):
    parser.error("readable_dir:{0} is not a valid path".format(prospective_dir))
  if os.access(prospective_dir, os.R_OK):
    return prospective_dir
  else:
    parser.error("readable_dir:{0} is not a readable dir".format(prospective_dir))

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Generate a 3D triangular mesh of a box with holes')
  parser.add_argument('filename', metavar='filename', nargs='?', default='holes_in_box', help='the output file name')
  parser.add_argument('--output-path', type=readable_dir, dest='output_path', default='.')
  parser.add_argument('--num', type=int, default=4, help='the number of segments')
  parser.add_argument('--radius', type=float, default=1, help='the radius of a hole')
  parser.add_argument('--margin', type=float, default=1.0, help='the margin')
  parser.add_argument('--height', type=float, default=1.0, help='the height of object')
  parser.add_argument('--ynum', type=int, default=1, help='the number of holes along the y-axis')
  parser.add_argument('--xnum', type=int, default=1, help='the number of holes along the x-axis')
  args = parser.parse_args()
  generate_from_path(args.output_path, args.filename, args.radius, args.margin,
                     args.height, args.num, args.xnum, args.ynum)
