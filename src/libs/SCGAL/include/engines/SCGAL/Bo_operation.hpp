// Copyright (c) 2022 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Author(s) : Efi Fogel         <efifogel@gmail.com>

#ifndef SCGAL_BO_OPERATION_HPP
#define SCGAL_BO_OPERATION_HPP

#include "SGAL/basic.hpp"

#include "SCGAL/basic.hpp"

SCGAL_BEGIN_NAMESPACE

//! Indicates which type of halfedges to render
enum class Bo_operation : unsigned long {
  UNION = 0,
  INTERSECTION,
  TM1_MINUS_TM2,
  TM2_MINUS_TM1,
  NUM
};

/*! Obtain the name of the operation option.
 * \return the name of the operation option.
 */
extern const char* get_operation_name(size_t id);

SCGAL_END_NAMESPACE

#endif
