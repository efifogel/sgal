// Copyright (c) 2019 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_ARR_POLYHEDRAL_SGM_POLYHEDRON_3_FACE_HPP
#define SGAL_ARR_POLYHEDRAL_SGM_POLYHEDRON_3_FACE_HPP

#include "SGAL/basic.hpp"
#include "SGAL/HalfedgeDS_face_with_id.hpp"

SGAL_BEGIN_NAMESPACE

/*! The extended Polyhedron face type */
template <typename Refs_, typename Plane_, typename Id_, typename Sgm>
class Arr_polyhedral_sgm_polyhedron_3_face :
  public HalfedgeDS_face_with_id<Refs_, Plane_, Id_>
{
private:
  typedef HalfedgeDS_face_with_id<Refs_, Plane_, Id_>   Base;
  typedef typename Sgm::Vertex_handle                   Arr_vertex_handle;

  //! The arrangement vertex handle of the projected noraml.
  Arr_vertex_handle m_vertex;

  //! Indicates whether it is a marked face.
  bool m_marked;

public:
  typedef typename  Base::Plane                        Plane;

  /*! Constructor */
  Arr_polyhedral_sgm_polyhedron_3_face() : m_vertex(NULL), m_marked(false) {}

  /*! Obtain the mutable plane. Delegate */
  Plane& plane() { return Base::plane(); }

  /*! Obtain the constant plane. Delegate */
  const Plane& plane() const { return Base::plane(); }

  /*! Obtain the vertex */
  Arr_vertex_handle vertex() { return m_vertex; }

  /*! Set the vertex */
  void set_vertex(Arr_vertex_handle vertex) { m_vertex = vertex; }

  /*! Set the "marked" flag */
  void set_marked(bool marked) { m_marked = marked; }

  /*! Obtain the "marked" flag */
  bool marked() const { return m_marked; }
};

SGAL_END_NAMESPACE

#endif
