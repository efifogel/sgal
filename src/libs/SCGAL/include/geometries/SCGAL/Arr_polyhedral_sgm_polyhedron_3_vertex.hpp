// Copyright (c) 2019 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_ARR_POLYHEDRAL_SGM_POLYHEDRON_3_VERTEX_HPP
#define SGAL_ARR_POLYHEDRAL_SGM_POLYHEDRON_3_VERTEX_HPP

#include "SGAL/basic.hpp"
#include "SGAL/HalfedgeDS_vertex_with_id.hpp"

SGAL_BEGIN_NAMESPACE

/*! The extended Polyhedron vertex type */
template <typename Refs_, typename Point_, typename Id_>
class Arr_polyhedral_sgm_polyhedron_3_vertex :
  public HalfedgeDS_vertex_with_id<Refs_, Point_, Id_>
{
private:
  typedef HalfedgeDS_vertex_with_id<Refs_, Point_, Id_> Base;

  /*! Indicates that the vertex has been processed already */
  bool m_processed;

  /*! Indicates whether it is a marked vertex */
  bool m_marked;

public:
  typedef typename  Base::Point                                  Point;

  /*! Construct. */
  Arr_polyhedral_sgm_polyhedron_3_vertex() : Base(), m_marked(false) {}

  /*! Construct. */
  Arr_polyhedral_sgm_polyhedron_3_vertex(const Point & p) :
    Base(p), m_marked(false) {}

  /*! Obtain the mutable (geometrical) point. Delegate */
  Point& point() { return Base::point(); }

  /*! Obtain the constant (geometrical) point. Delegate */
  const Point& point () const { return Base::point(); }

  /*! Set the flag */
  void set_processed(bool processed) { m_processed = processed; }

  /*! Obtain the flag */
  bool processed() const { return m_processed; }

  /*! Set the "marked" flag */
  void set_marked(bool marked) { m_marked = marked; }

  /*! Obtain the "marked" flag */
  bool marked() const { return m_marked; }
};

SGAL_END_NAMESPACE

#endif
