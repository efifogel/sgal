// Copyright (c) 2019 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_ARR_POLYHEDRAL_SGM_POLYHEDRON_3_HPP
#define SGAL_ARR_POLYHEDRAL_SGM_POLYHEDRON_3_HPP

/*! \file
 * Related definition of a Polyhedron_3 data structure, an instance of which
 * can be used to initialize a Arr_polyhedral_sgm data structure.
 * A user can construct a Arr_polyhedral_sgm data structure in two ways as follows:
 * 1. Providing a sequence of (geometric) points in three dimensions and a
 * sequence of indices into the points that indicate the facets of the
 * polyhedron. An index equal to -1 indicates the end of a facet. Internally,
 * a temporary Polyhedron_3 object is constructed from the input. Then, the
 * Arr_polyhedral_sgm object is constructed from the Polyhedron_3 object. Finally,
 * the temporary Polyhedron_3 object is destructed.
 * 2. Providing a Polyhedron_3 object directly. In this case, the type
 * is constrained. The user is free to define her/his own vertex, halfedge,
 * and face types, but they must derive from the types provided in this file,
 * namely:
 *   Arr_polyhedral_sgm_polyhedron_3_vertex,
 *   Arr_polyhedral_sgm_polyhedron_3_halfedge, and
 *   Arr_polyhedral_sgm_polyhedron_3_face.
 * Notice that the latter is parameterized with a Sgm type, the same type
 * as the type Arr_polyhedral_sgm that, and object of which the user intends to
 * construct.
 */

#include <CGAL/basic.h>
#include <CGAL/Polyhedron_3.h>

#include "SGAL/basic.hpp"

#include "SCGAL/basic.hpp"
#include "SCGAL/Arr_polyhedral_sgm_polyhedron_3_vertex.hpp"
#include "SCGAL/Arr_polyhedral_sgm_polyhedron_3_halfedge.hpp"
#include "SCGAL/Arr_polyhedral_sgm_polyhedron_3_face.hpp"

SGAL_BEGIN_NAMESPACE

/*! The "items" type. A model of the PolyhedralSgmPolyhedronItems_3 concept,
 * which is a refinment of the PolyhedronItems_3 concept. Its base class
 * Polyhedron_items_3, a model of the latter concept, provides definitions of
 * vertices with points, halfedges, and faces with normal equations. We extend
 * the definition of each one of the three items with the necessary data
 * required to construct a Arr_polyhedral_sgm object from a
 * Polyhedron_3 object, the type of which, namely Polyhedron_3, is
 * instantiated with this extended items type.
 */
template <typename Sgm_>
struct Arr_polyhedral_sgm_polyhedron_items : public CGAL::Polyhedron_items_3 {
  template <typename Refs_, typename Traits_>
  struct Vertex_wrapper {
    typedef typename Traits_::Point_3                                  Point;
    typedef Arr_polyhedral_sgm_polyhedron_3_vertex<Refs_, Point, Size> Vertex;
  };
  template <typename Refs_, typename Traits_>
  struct Halfedge_wrapper {
    typedef Arr_polyhedral_sgm_polyhedron_3_halfedge<Refs_, Size>      Halfedge;
  };
  template <typename Refs_, typename Traits_>
  struct Face_wrapper {
    typedef typename  Traits_::Plane_3                                 Plane;
    typedef Arr_polyhedral_sgm_polyhedron_3_face<Refs_, Plane, Size, Sgm_> Face;
  };
};

/*! The default polyhedron type. If the Arr_polyhedral_sgm object is indirectly
 * constructed from the points and the facets provided as indices, then a
 * temporary object of type Arr_polyhedral_sgm_default_polyhedron_3 is
 * constructed internally, and used to represent the polyhedron. Similarly, if
 * the user provides a reference to a polyhedron object as input for the
 * construction of the Arr_polyhedral_sgm object, and she/he has no need to
 * extend the polyhedron features, this type should be used to represent the
 * polyhedron. However, if the user need to extend the vertex, halfedge, or face
 * of the polyhedron, she/he must extend the appropriate type(s), define a new
 * items type that is based on the extended types, and define a new polyhedron
 * type based on the new items type.
 */
template <typename Sgm_, typename Traits_>
struct Arr_polyhedral_sgm_polyhedron_3 :
  public CGAL::Polyhedron_3<Traits_, Arr_polyhedral_sgm_polyhedron_items<Sgm_> >
{
  /*! Construct. */
  Arr_polyhedral_sgm_polyhedron_3() {}
};

SGAL_END_NAMESPACE

//! Make the polyhedron a model of FaceGraph
namespace boost {

template <typename Sgm_, typename Traits_>
struct graph_traits<SGAL::Arr_polyhedral_sgm_polyhedron_3<Sgm_, Traits_> > :
  public graph_traits<CGAL::Polyhedron_3
                      <Traits_,
                       SGAL::Arr_polyhedral_sgm_polyhedron_items<Sgm_> > >
{};

template <typename Sgm_, typename Traits_, typename Tag>
struct property_map<SGAL::Arr_polyhedral_sgm_polyhedron_3<Sgm_, Traits_>, Tag> :
  public property_map<CGAL::Polyhedron_3
                      <Traits_,
                       SGAL::Arr_polyhedral_sgm_polyhedron_items<Sgm_> >, Tag>
{};

#define PM_DT_SPEC(DTAG) \
  template <typename Sgm, typename Traits, typename T>                   \
  struct property_map<SGAL::Arr_polyhedral_sgm_polyhedron_3<Sgm, Traits>, DTAG<T> > : \
    property_map<CGAL::Polyhedron_3<Traits, SGAL::Arr_polyhedral_sgm_polyhedron_items<Sgm> >, DTAG<T> > \
{};


PM_DT_SPEC(CGAL::dynamic_vertex_property_t)
PM_DT_SPEC(CGAL::dynamic_halfedge_property_t)
PM_DT_SPEC(CGAL::dynamic_face_property_t)
PM_DT_SPEC(CGAL::dynamic_edge_property_t)

#undef PM_DT_SPEC

}

#endif
