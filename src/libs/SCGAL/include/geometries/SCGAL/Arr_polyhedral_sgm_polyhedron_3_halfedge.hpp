// Copyright (c) 2019 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_ARR_POLYHEDRAL_SGM_POLYHEDRON_3_HALFEDGE_HPP
#define SGAL_ARR_POLYHEDRAL_SGM_POLYHEDRON_3_HALFEDGE_HPP

#include "SGAL/basic.hpp"
#include "SGAL/HalfedgeDS_halfedge_with_id.hpp"

SGAL_BEGIN_NAMESPACE

/*! The extended Polyhedron halfedge type */
template <typename Refs_, typename Id_>
class Arr_polyhedral_sgm_polyhedron_3_halfedge :
  public HalfedgeDS_halfedge_with_id<Refs_, Id_>
{
private:
  /*! Indicates that the halfedge has been processed already */
  bool m_processed;

  /*! Indicates whether it is a marked vertex */
  bool m_marked;

public:
  /*! Construct. */
  Arr_polyhedral_sgm_polyhedron_3_halfedge() :
    m_processed(false), m_marked(false)
  {}

  /*! Set the flag. */
  void set_processed(bool processed) { m_processed = processed; }

  /*! Obtain the flag. */
  bool processed() const { return m_processed; }

  /*! Set the "marked" flag */
  void set_marked(bool marked) { m_marked = marked; }

  /*! Obtain the "marked" flag */
  bool marked() const { return m_marked; }
};

SGAL_END_NAMESPACE

#endif
