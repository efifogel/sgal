// Copyright (c) 2004 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// $Id: Lower_envelope_sphere.hpp 9188 2010-05-25 14:40:57Z efif $
// $Revision: 9188 $
//
// Author(s)     : Efi Fogel         <efifogel@gmail.com>

#ifndef SCGAL_LOWER_ENVELOPE_SPHERE_HPP
#define SCGAL_LOWER_ENVELOPE_SPHERE_HPP

/*! \file
 */

#include <list>

#include <CGAL/basic.h>
#include <CGAL/Arr_extended_dcel.h>
#include <CGAL/Envelope_3/Envelope_pm_dcel.h>

/*! Extend the envelope-diagram vertex. */
template <typename VertexBase, typename Data>
class Rendered_envelope_diagram_vertex :
  public CGAL::Envelope_3::Envelope_pm_vertex<VertexBase, Data> {

private:
  using Vertex_base = VertexBase;
  using Self = Rendered_envelope_diagram_vertex<Vertex_base, Data>;
  using Base = CGAL::Envelope_3::Envelope_pm_vertex<Vertex_base, Data>;
};

using Double_pair = std::pair<double, double>;
using Double_point_list = std::list<Double_pair>;
using Double_point_iter = Double_point_list::iterator;
using Double_point_const_iter = Double_point_list::const_iterator;
using Double_point_riter = Double_point_list::reverse_iterator;
using Double_point_const_riter = Double_point_list::const_reverse_iterator;

/*! Extend the planar-map halfedge. */
template <typename HalfedgeBase, typename Data>
class Rendered_envelope_diagram_halfedge :
  public CGAL::Envelope_3::Envelope_pm_halfedge<HalfedgeBase, Data> {
private:
  using Halfedge_base = HalfedgeBase;
  using Self = Rendered_envelope_diagram_halfedge<Halfedge_base, Data>;
  using Base = CGAL::Envelope_3::Envelope_pm_halfedge<Halfedge_base, Data>;

  /*! A sequence of points that approximate the x-monotone curve. */
  Double_point_list* m_points;

public:
  /*! Constructor. */
  Rendered_envelope_diagram_halfedge() : m_points(nullptr) {}

  /*! Destructor. */
  virtual ~Rendered_envelope_diagram_halfedge() { clear_points(); }

  /*! Clear the sequence of points that approximate the x-monotone curve */
  void clear_points() {
    if (! m_points) return;
    m_points->clear();
    delete m_points;
    m_points = nullptr;
  }

  /*! Assign from another halfedge.
   * \param h the other halfedge.
   */
  virtual void assign(const Self& h) {
    Base::assign(h);
    set_points(h.m_points);
  }

  /*! Set the sequence of points that approximate the x-monotone curve. */
  void set_points(Double_point_list* points) {
    clear_points();
    m_points = points;
  }

  /*! Obtain the sequence of points that approximate the x-monotone curve. */
  Double_point_list* get_points() { return m_points; }

  /*! Obtain the sequence of points that approximate the x-monotone curve. */
  const Double_point_list* get_points() const { return m_points; }
};

/*! Extend the planar-map face. */
template <typename FaceBase, typename Data>
class Rendered_envelope_diagram_face :
  public CGAL::Envelope_3::Envelope_pm_face<FaceBase, Data> {
private:
  using Face_base = FaceBase;
  using Self = Rendered_envelope_diagram_face<Face_base, Data>;
  using Base = CGAL::Envelope_3::Envelope_pm_face<Face_base, Data>;
};

template <typename Traits, typename Data>
using Rendered_envelope_diagram_dcel =
  CGAL::Arr_dcel_base<Rendered_envelope_diagram_vertex
                        <CGAL::Arr_vertex_base<typename Traits::Point_2>, Data>,
                      Rendered_envelope_diagram_halfedge
                        <CGAL::Arr_halfedge_base<typename Traits::X_monotone_curve_2>, Data>,
                      Rendered_envelope_diagram_face
                        <CGAL::Arr_face_base, Data>>;

#endif
