// Copyright (c) 2023 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Author(s) : Efi Fogel         <efifogel@gmail.com>

#include "SGAL/basic.hpp"
#include "SGAL/Tracer.hpp"
#include "SGAL/Point_set.hpp"
#include "SGAL/Indexed_line_set.hpp"
#include "SGAL/Indexed_face_set.hpp"
#include "SGAL/Field.hpp"

#include "SCGAL/Delaunay.hpp"

SCGAL_BEGIN_NAMESPACE

size_t Delaunay::s_trace_code = 0;

//! \brief adds the container to a given scene.
void Delaunay::add_to_scene(SGAL::Scene_graph* scene_graph)
{ m_scene_graph = scene_graph; }

//! \brief transforms a 3D container to a 2D container.
void Delaunay::trigger_changed(const SGAL::Field_info*) { execute(); }

//! \brief sets the trace code.
void Delaunay::set_trace_code(size_t code) { s_trace_code = code; }

//! \brief obtains the trace code.
size_t Delaunay::get_trace_code() { return s_trace_code; }

//! \brief computes the snapping fixtures.
void Delaunay::execute() {
  SGAL_TRACE_CODE(s_trace_code,
                  if (true)
                    std::cout << "Generating snapping fixture" << std::endl;);
  // Cascade the results field:
  Field* field_segments = get_field(SEGMENTS);
  if (field_segments) field_segments->cascade();
}

SCGAL_END_NAMESPACE
