// Copyright (c) 2012 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Author(s) : Efi Fogel         <efifogel@gmail.com>

#if defined(_WIN32)
#pragma warning( disable : 4503)
#endif

/*! \file
 * An engine that computes a Boolean operation.
 */

#include <bitset>

#include <CGAL/basic.h>
#include <CGAL/Polygon_mesh_processing/corefinement.h>

#include "SGAL/basic.hpp"
#include "SGAL/Indexed_face_set.hpp"
#include "SGAL/Epec_polyhedron.hpp"
#include "SGAL/Tracer.hpp"
#include "SGAL/Field.hpp"
#include "SGAL/Field_rule.hpp"
#include "SGAL/Field_infos.hpp"

#include "SCGAL/basic.hpp"
#include "SCGAL/Boolean_operation.hpp"
#include "SCGAL/Bo_operation.hpp"

namespace pmp = CGAL::Polygon_mesh_processing;
namespace Corefinement = pmp::Corefinement;

SCGAL_BEGIN_NAMESPACE

size_t Boolean_operation::s_trace_code = 0;

typedef Corefinement::Boolean_operation_type  Boolean_operation_type;

//! \brief operation tags.
const Boolean_operation_type s_operation_types[] = {
  Corefinement::UNION,
  Corefinement::INTERSECTION,
  Corefinement::TM1_MINUS_TM2,
  Corefinement::TM2_MINUS_TM1,
  Corefinement::NONE
};

//! \brief cleans the operation-options set.
void Boolean_operation::clean_operation() const {
  std::bitset<static_cast<unsigned long>(Bo_operation::NUM)> bits(m_operation_code);
  for (size_t i = 0; i < static_cast<size_t>(Bo_operation::NUM); ++i)
    if (bits[i]) m_operation.push_back(get_operation_name(i));
  m_dirty_operation = false;
}

//! \brief processes change of operation-option set.
void Boolean_operation::operation_changed(const SGAL::Field_info* field_info) {
  m_dirty_operation_code = true;
  m_dirty_operation = false;
  field_changed(field_info);
}

//! \brief cleans the compact operation-options set.
void Boolean_operation::clean_operation_code() const {
  std::bitset<static_cast<unsigned long>(Bo_operation::NUM)>
    bits(m_operation_code);
  for (size_t i = 0; i < static_cast<size_t>(Bo_operation::NUM); ++i) {
    auto it =
      std::find(m_operation.begin(), m_operation.end(), get_operation_name(i));
    bits[i] = (it != m_operation.end()) ? 1 : 0;
  }
  m_operation_code = bits.to_ulong();
  m_dirty_operation_code = false;
}

//! \brief processes change of compact operation-option set.
void
Boolean_operation::operation_code_changed(const SGAL::Field_info* field_info) {
  m_dirty_operation = true;
  m_dirty_operation_code = false;
  field_changed(field_info);
}

//! \brief obtains the operation tag.
inline Boolean_operation_type get_operation_type(size_t i)
{ return s_operation_types[i]; }

//! \brief adds the container to a given scene.
void Boolean_operation::add_to_scene(SGAL::Scene_graph* scene_graph)
{ m_scene_graph = scene_graph; }

//! \brief transforms a 3D container to a 2D container.
void Boolean_operation::trigger_changed(const SGAL::Field_info*) { execute(); }

//! \brief sets the trace code.
void Boolean_operation::set_trace_code(size_t code) { s_trace_code = code; }

//! \brief obtains the trace code.
size_t Boolean_operation::get_trace_code() { return s_trace_code; }

//! \brief computes the snapping fixtures.
void Boolean_operation::execute() {
  SGAL_TRACE_CODE(s_trace_code,
                  if (true)
                    std::cout << "Generating snapping fixture" << std::endl;);
  if (!m_operand1 || !m_operand2) return;

  m_operand1->set_polyhedron_type(Indexed_face_set::POLYHEDRON_EPEC);
  const auto& p_var1 = m_operand1->get_polyhedron();
  const Epec_polyhedron& polyhedron1 = boost::get<Epec_polyhedron>(p_var1);
  SGAL_warning_msg(polyhedron1.size_of_vertices() != 0,
                   "Operand 1 has zero vertices!");
  if (polyhedron1.size_of_vertices() == 0) return;

  m_operand2->set_polyhedron_type(Indexed_face_set::POLYHEDRON_EPEC);
  const auto& p_var2 = m_operand2->get_polyhedron();
  const Epec_polyhedron& polyhedron2 = boost::get<Epec_polyhedron>(p_var2);
  SGAL_warning_msg(polyhedron1.size_of_vertices() != 0,
                   "Operand 2 has zero vertices!");
  if (polyhedron2.size_of_vertices() == 0) return;

  m_success.resize(static_cast<size_t>(Bo_operation::NUM));
  m_results.resize(static_cast<size_t>(Bo_operation::NUM));
  for (size_t i = 0; i < static_cast<size_t>(Bo_operation::NUM); ++i) {
    m_success[i] = false;
    m_results[i].reset();
  }

#if 0
  typedef CGAL::Nef_polyhedron_3<Epec_kernel, CGAL::SNC_indexed_items>
                                                    Nef_polyhedron;

  /*! \todo Allow passing a const polyhedron to the constructor of
   * Nef_polyhedron
   */
  auto tmp1 = const_cast<Epec_polyhedron&>(polyhedron1);
  auto tmp2 = const_cast<Epec_polyhedron&>(polyhedron2);
  Nef_polyhedron nef_polyhedron1 = Nef_polyhedron(tmp1);
  Nef_polyhedron nef_polyhedron2 = Nef_polyhedron(tmp2);

  // Compute Boolean operation:
  Nef_polyhedron nef_polyhedron(nef_polyhedron1 * nef_polyhedron2);
  SGAL_assertion(nef_polyhedron.is_simple());

  Epec_polyhedron p;
  nef_polyhedron.convert_to_polyhedron(p);
  auto geometry = Shared_indexed_face_set(new Indexed_face_set);

  geometry->set_polyhedron_type(Indexed_face_set::POLYHEDRON_EPEC);
  const auto& polyhedron_var = geometry->get_polyhedron();
  const auto& p_const = boost::get<Epec_polyhedron>(polyhedron_var)
  auto& polyhedron = const_cast<Indexed_face_set::Polyhedron&>(p_const);
  polyhedron = p;
  geometry->clear_volume();
  geometry->clear_surface_area();
  geometry->clear_polyhedron_facet_normals();
  geometry->clear_normal_attributes();
  geometry->clear_coord_array();
  geometry->clear_coord_indices();
  geometry->clear_facet_coord_indices();

  m_results[get_operation_type()] = geometry;

#else
  auto tmp1 = const_cast<Epec_polyhedron&>(polyhedron1);
  auto tmp2 = const_cast<Epec_polyhedron&>(polyhedron2);

  // std::cout << "# polyhedron: " << polyhedrons.size() << std::endl;
  // if (polyhedrons.empty()) {
  //   CGAL::VRML_2_ostream vrml_out(std::cout);
  //   vrml_out << polyhedron1;
  //   vrml_out << polyhedron2;
  // }
  std::array<std::optional<Epec_polyhedron*>, 4> out;

  if (m_dirty_operation_code) clean_operation_code();
  std::bitset<static_cast<unsigned long>(Bo_operation::NUM)>
    bits(m_operation_code);
  for (size_t i = 0; i < static_cast<size_t>(Bo_operation::NUM); ++i) {
    if (! bits[i]) continue;
    auto geometry = Shared_indexed_face_set(new Indexed_face_set);
    geometry->set_polyhedron_type(Indexed_face_set::POLYHEDRON_EPEC);
    m_results[get_operation_type(i)] = geometry;

    auto& polyhedron = geometry->get_empty_epec_polyhedron();
    out[get_operation_type(i)] = std::optional<Epec_polyhedron*>(&polyhedron);
  }

  auto success = pmp::corefine_and_compute_boolean_operations(tmp1, tmp2, out);
  std::copy(success.begin(), success.end(), m_success.begin());
  for (size_t i = 0; i < static_cast<size_t>(Bo_operation::NUM); ++i) {
    if (out[i]) {
      const auto* prn_ptr = *out[i];
      const auto& prn = *prn_ptr;

      Size j(0);
      BOOST_FOREACH(auto f, faces(prn)) f->id() = j++;
      auto fcm(CGAL::get(boost::face_index, prn));
      auto num_cc = pmp::connected_components(prn, fcm);

      std::cout << "Polyhedron[" << i << "]: " << CGAL::is_valid(prn)
                << "," << prn.is_empty()
                << "," << num_cc
                << "," << prn.size_of_vertices()
                << "," << prn.size_of_halfedges()
                << "," << prn.size_of_facets() << std::endl;
    }
  }

#endif

  // Cascade the results field:
  Field* field_results = get_field(RESULTS);
  if (field_results) field_results->cascade();

  // Cascade the success field:
  Field* field_success = get_field(SUCCESS);
  if (field_success) field_success->cascade();
}

SCGAL_END_NAMESPACE
