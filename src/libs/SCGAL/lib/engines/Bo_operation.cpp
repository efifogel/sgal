// Copyright (c) 2022 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Author(s) : Efi Fogel         <efifogel@gmail.com>

#include "SGAL/basic.hpp"

#include "SCGAL/Bo_operation.hpp"

SCGAL_BEGIN_NAMESPACE

//! \brief the names of the operation options.
static const char* s_operation_names[] = {
  "union",
  "intersection",
  "tm1MinusTm2",
  "tm2MinusTm1"
};

//! \brief obtains the name of a rule.
const char* get_operation_name(size_t id) { return s_operation_names[id]; }

SCGAL_END_NAMESPACE
