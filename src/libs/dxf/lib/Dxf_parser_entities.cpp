// Copyright (c) 2018 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <string>
#include <iostream>
#include <functional>
#include <streambuf>

#include "SGAL/basic.hpp"
#include "SGAL/Types.hpp"
#include "SGAL/Tracer.hpp"

#include "dxf/basic.hpp"
#include "dxf/Dxf_parser.hpp"
#include "dxf/Dxf_data.hpp"
#include "dxf/Dxf_base_entity.hpp"

// Entities
#include "dxf/Dxf_3dface_entity.hpp"
#include "dxf/Dxf_3dsolid_entity.hpp"
#include "dxf/Dxf_acad_proxy_entity.hpp"
#include "dxf/Dxf_arc_entity.hpp"
#include "dxf/Dxf_arcalignedtext_entity.hpp"
#include "dxf/Dxf_attdef_entity.hpp"
#include "dxf/Dxf_attrib_entity.hpp"
#include "dxf/Dxf_body_entity.hpp"
#include "dxf/Dxf_circle_entity.hpp"
#include "dxf/Dxf_dimension_entity.hpp"
#include "dxf/Dxf_ellipse_entity.hpp"
#include "dxf/Dxf_hatch_entity.hpp"
#include "dxf/Dxf_image_entity.hpp"
#include "dxf/Dxf_insert_entity.hpp"
#include "dxf/Dxf_leader_entity.hpp"
#include "dxf/Dxf_line_entity.hpp"
#include "dxf/Dxf_lwpolyline_entity.hpp"
#include "dxf/Dxf_mline_entity.hpp"
#include "dxf/Dxf_mtext_entity.hpp"
#include "dxf/Dxf_oleframe_entity.hpp"
#include "dxf/Dxf_ole2frame_entity.hpp"
#include "dxf/Dxf_point_entity.hpp"
#include "dxf/Dxf_polyline_entity.hpp"
#include "dxf/Dxf_ray_entity.hpp"
#include "dxf/Dxf_region_entity.hpp"
#include "dxf/Dxf_rtext_entity.hpp"
#include "dxf/Dxf_seqend_entity.hpp"
#include "dxf/Dxf_shape_entity.hpp"
#include "dxf/Dxf_solid_entity.hpp"
#include "dxf/Dxf_spline_entity.hpp"
#include "dxf/Dxf_text_entity.hpp"
#include "dxf/Dxf_tolerance_entity.hpp"
#include "dxf/Dxf_trace_entity.hpp"
#include "dxf/Dxf_vertex_entity.hpp"
#include "dxf/Dxf_viewport_entity.hpp"
#include "dxf/Dxf_wipeout_entity.hpp"
#include "dxf/Dxf_xline_entity.hpp"
#include "dxf/Dxf_user_entity.hpp"

DXF_BEGIN_NAMESPACE

//! \brief parse ENTITIES section
void Dxf_parser::parse_entities()
{
  SGAL_TRACE_CODE(get_trace_code_parsing(),
                  if (get_verbose_level() >= 2)
                    std::cout << "Parsing ENTITIES section" << std::endl;);

  int n;
  import_code(n);
  SGAL_assertion(0 == n);

  do {
    SGAL::String str;
    import_value(str);
    if ("ENDSEC" == str) return;

    auto it = s_entities.find(str);
    if (it != s_entities.end()) {
      auto* entity = (this->*(it->second))();
      if (entity) m_data->m_entities.push_back(entity);
      continue;
    }

    // Look for user defined entities that are defined in a CLASS block
    auto cit = std::find_if(m_data->m_classes.begin(), m_data->m_classes.end(),
                            [&](const Dxf_class& my_class)
                            {
                              return ((my_class.m_record_name == str) &&
                                      my_class.m_is_entity);
                            });
    if (cit != m_data->m_classes.end()) {
      auto* user_entity = new Dxf_user_entity;
      m_data->m_entities.push_back(user_entity);
      parse_record(*user_entity);
      continue;
    }

    SGAL::String msg("Unrecognize entity \"");
    msg += str + "\", at line " + std::to_string(m_line);
    SGAL_error_msg(msg.c_str());

  } while (true);
}

// Object parsers
//! \brief parses a 3dface entity.
Dxf_base_entity* Dxf_parser::parse_3dface_entity()
{ return parse_entity("3dface", new Dxf_3dface_entity); }

//! \brief parses a 3dsolid entity.
Dxf_base_entity* Dxf_parser::parse_3dsolid_entity()
{ return parse_entity("3dsolid", new Dxf_3dsolid_entity); }

//! \brief parses an acad_proxy entity.
Dxf_base_entity* Dxf_parser::parse_acad_proxy_entity()
{ return parse_entity("acad_proxy", new Dxf_acad_proxy_entity); }

//! \brief parses an arc entity.
Dxf_base_entity* Dxf_parser::parse_arc_entity()
{ return parse_entity("arc", new Dxf_arc_entity); }

//! \brief parses an arcalignedtext entity.
Dxf_base_entity* Dxf_parser::parse_arcalignedtext_entity()
{ return parse_entity("arcalignedtext", new Dxf_arcalignedtext_entity); }

//! \brief parses an attdef entity.
Dxf_base_entity* Dxf_parser::parse_attdef_entity()
{ return parse_entity("attdef", new Dxf_attdef_entity); }

//! \brief parses an attrib entity.
Dxf_base_entity* Dxf_parser::parse_attrib_entity()
{ return parse_entity("attrib", new Dxf_attrib_entity); }

//! \brief parses a body entity.
Dxf_base_entity* Dxf_parser::parse_body_entity()
{ return parse_entity("body", new Dxf_body_entity); }

//! \brief parses a circle entity.
Dxf_base_entity* Dxf_parser::parse_circle_entity()
{ return parse_entity("circle", new Dxf_circle_entity); }

//! \brief parses a dimension entity.
Dxf_base_entity* Dxf_parser::parse_dimension_entity()
{ return parse_entity("dimension", new Dxf_dimension_entity); }

//! \brief parses an ellipse entity.
Dxf_base_entity* Dxf_parser::parse_ellipse_entity()
{ return parse_entity("ellipse", new Dxf_ellipse_entity); }

//! \brief parses a hatch entity.
Dxf_base_entity* Dxf_parser::parse_hatch_entity()
{ return parse_entity("hatch", new Dxf_hatch_entity); }

//! \brief parses an image entity.
Dxf_base_entity* Dxf_parser::parse_image_entity()
{ return parse_entity("image", new Dxf_image_entity); }

//! \brief parses an insert entity.
Dxf_base_entity* Dxf_parser::parse_insert_entity()
{ return parse_entity("insert", new Dxf_insert_entity); }

//! \brief parses a leader entity.
Dxf_base_entity* Dxf_parser::parse_leader_entity()
{ return parse_entity("leader", new Dxf_leader_entity); }

//! \brief parses a line entity.
Dxf_base_entity* Dxf_parser::parse_line_entity()
{ return parse_entity("line", new Dxf_line_entity); }

//! \brief parses a lwpolyline entity.
Dxf_base_entity* Dxf_parser::parse_lwpolyline_entity()
{ return parse_entity("lwpolyline", new Dxf_lwpolyline_entity); }

//! \brief parses am mline entity.
Dxf_base_entity* Dxf_parser::parse_mline_entity()
{ return parse_entity("mline", new Dxf_mline_entity); }

//! \brief parses an mtext entity.
Dxf_base_entity* Dxf_parser::parse_mtext_entity()
{ return parse_entity("mtext", new Dxf_mtext_entity); }

//! \brief parses an oleframe entity.
Dxf_base_entity* Dxf_parser::parse_oleframe_entity()
{ return parse_entity("oleframe", new Dxf_oleframe_entity); }

//! \brief parses an ole2frame entity.
Dxf_base_entity* Dxf_parser::parse_ole2frame_entity()
{ return parse_entity("ole2frame", new Dxf_ole2frame_entity); }

//! \brief parses a point entity.
Dxf_base_entity* Dxf_parser::parse_point_entity()
{ return parse_entity("point", new Dxf_point_entity); }

//! \brief parses a polyline entity.
Dxf_base_entity* Dxf_parser::parse_polyline_entity()
{
  auto* polyline = new Dxf_polyline_entity;
  parse_entity("polyline", polyline);
  m_active_polyline_entity = polyline;
  return polyline;
}

//! \brief parses a ray entity.
Dxf_base_entity* Dxf_parser::parse_ray_entity()
{ return parse_entity("ray", new Dxf_ray_entity); }

//! \brief parses a region entity.
Dxf_base_entity* Dxf_parser::parse_region_entity()
{ return parse_entity("region", new Dxf_region_entity); }

//! \brief parses an rtext entity.
Dxf_base_entity* Dxf_parser::parse_rtext_entity()
{ return parse_entity("rtext", new Dxf_rtext_entity); }

//! \brief parses a seqend entity.
Dxf_base_entity* Dxf_parser::parse_seqend_entity()
{
  SGAL_TRACE_CODE(get_trace_code_parsing(),
                  if (get_verbose_level() >= 4)
                    std::cout << "Parsing SEQEND entity" << std::endl;);
  Dxf_seqend_entity seqend;
  parse_record(seqend);
  m_active_polyline_entity = nullptr;
  return nullptr;
}

//! \brief parses a shape entity.
Dxf_base_entity* Dxf_parser::parse_shape_entity()
{ return parse_entity("shape", new Dxf_shape_entity); }

//! \brief parses a solid entity.
Dxf_base_entity* Dxf_parser::parse_solid_entity()
{ return parse_entity("solid", new Dxf_solid_entity); }

//! \brief parses a spline entity.
Dxf_base_entity* Dxf_parser::parse_spline_entity()
{ return parse_entity("spline", new Dxf_spline_entity); }

//! \brief parses a text entity.
Dxf_base_entity* Dxf_parser::parse_text_entity()
{ return parse_entity("text", new Dxf_text_entity); }

//! \brief parses a tolerance entity.
Dxf_base_entity* Dxf_parser::parse_tolerance_entity()
{ return parse_entity("tolerance", new Dxf_tolerance_entity); }

//! \brief parses a trace entity.
Dxf_base_entity* Dxf_parser::parse_trace_entity()
{ return parse_entity("trace", new Dxf_trace_entity); }

//! \brief parses a vertex entity.
Dxf_base_entity* Dxf_parser::parse_vertex_entity()
{
  SGAL_TRACE_CODE(get_trace_code_parsing(),
                  if (get_verbose_level() >= 4)
                    std::cout << "Parsing VERTEX entity" << std::endl;);

  SGAL_assertion(m_active_polyline_entity);
  auto& vertices = m_active_polyline_entity->m_vertex_entities;
  vertices.resize(vertices.size() + 1);
  auto& vertex = vertices.back();
  parse_record(vertex);
  return nullptr;
}

//! \brief parses a viewport entity.
Dxf_base_entity* Dxf_parser::parse_viewport_entity()
{ return parse_entity("viewport", new Dxf_viewport_entity); }

//! \brief parses a wipeout entity.
Dxf_base_entity* Dxf_parser::parse_wipeout_entity()
{ return parse_entity("wipeout", new Dxf_wipeout_entity); }

//! \brief parses an xline entity.
Dxf_base_entity* Dxf_parser::parse_xline_entity()
{ return parse_entity("xline", new Dxf_xline_entity); }

DXF_END_NAMESPACE
