// Copyright (c) 2015 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#if defined(_MSC_VER)
#pragma warning ( disable : 4512 )
#endif

#include <vector>
#include <string>

#include "SGAL/basic.hpp"
#include "SGAL/Configuration.hpp"
#include "SGAL/Vector4f.hpp"
#include "SGAL/io_vector4f.hpp"

#include "dxf/basic.hpp"
#include "dxf/Dxf_option_parser.hpp"
#include "dxf/Dxf_configuration.hpp"

DXF_BEGIN_NAMESPACE

//! \brief constructs default.
Dxf_option_parser::Dxf_option_parser() :
  m_dxf_opts("Dxf options")
{
  m_dxf_opts.add_options()
    ("dxf-palette-file",
     po::value<SGAL::String>()->
     default_value(Dxf_configuration::s_def_palette_file_name),
     "DXF color palette file name")
    ("dxf-background-color", po::value<SGAL::Vector4f>()->
     default_value(Dxf_configuration::s_def_background_color),
     "Background color")
    ("dxf-min-bulge", po::value<SGAL::Float>()->
     default_value(Dxf_configuration::s_def_min_bulge),
     "DXF Minimum bulge value (used for approximating a circular arc)")
    ("dxf-refinement-arcs-num", po::value<SGAL::Uint>()->
     default_value(Dxf_configuration::s_def_refinement_arcs_num),
     "DXF number of arcs used to represent a circle or an ellipsoid")
    ("dxf-store-data", po::value<SGAL::Boolean>()->
     default_value(Dxf_configuration::s_def_store_data),
     "Store the dxf data for later use")
    ("dxf-version", po::value<size_t>()->
     default_value(Dxf_configuration::s_def_version),
     "The DXF version number")
    ("dxf-transparency", po::value<SGAL::Float>()->
     default_value(Dxf_configuration::s_def_transparency),
     "The DXF transparency value for hatches")
    ("dxf-test-depth", po::value<SGAL::Boolean>()->
     default_value(Dxf_configuration::s_def_test_depth),
     "Enable depth testing")
   ;
}

//! \brief destructs.
Dxf_option_parser::~Dxf_option_parser() {}

//! \brief applies the options.
void Dxf_option_parser::apply() {}

//! \brief sets the Configuration node.
void Dxf_option_parser::configure(const po::variables_map& vm,
                                  Dxf_configuration* dxf_conf)
{
  if (! dxf_conf) return;

  dxf_conf->set_palette_file_name(vm["dxf-palette-file"].as<SGAL::String>());
  dxf_conf->set_background_color(vm["dxf-background-color"].
                                 as<SGAL::Vector4f>());
  dxf_conf->set_min_bulge(vm["dxf-min-bulge"].as<SGAL::Float>());
  dxf_conf->set_refinement_arcs_num(vm["dxf-refinement-arcs-num"].
                                    as<SGAL::Uint>());
  dxf_conf->set_store_data(vm["dxf-store-data"].as<SGAL::Boolean>());
  dxf_conf->set_version(vm["dxf-version"].as<size_t>());
  dxf_conf->set_transparency(vm["dxf-transparency"].as<SGAL::Float>());
  dxf_conf->set_test_depth(vm["dxf-test-depth"].as<SGAL::Boolean>());
}

DXF_END_NAMESPACE
