// Copyright (c) 2016 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <boost/lexical_cast.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/Container_proto.hpp"
#include "SGAL/Container_factory.hpp"
#include "SGAL/Element.hpp"
#include "SGAL/Utilities.hpp"
#include "SGAL/Execution_function.hpp"
#include "SGAL/Field_rule.hpp"
#include "SGAL/Field_infos.hpp"
#include "SGAL/to_boolean.hpp"

#include "dxf/basic.hpp"
#include "dxf/Dxf_configuration.hpp"

DXF_BEGIN_NAMESPACE

const std::string Dxf_configuration::s_tag = "DxfConfiguration";
SGAL::Container_proto* Dxf_configuration::s_prototype(nullptr);

// Default values
const SGAL::String Dxf_configuration::s_def_palette_file_name("dxf_palette.txt");
const SGAL::Vector4f Dxf_configuration::s_def_background_color{0.9, 0.9, 0.9, 1};
const SGAL::Float Dxf_configuration::s_def_min_bulge(0.1f);
const SGAL::Uint Dxf_configuration::s_def_refinement_arcs_num(16);
const SGAL::Boolean Dxf_configuration::s_def_store_data(false);
const SGAL::Uint Dxf_configuration::s_def_version(17);
const SGAL::Float Dxf_configuration::s_def_transparency(0.5f);
const SGAL::Boolean Dxf_configuration::s_def_test_depth(false);

//! \brief constructs.
Dxf_configuration::Dxf_configuration(SGAL::Boolean proto) :
  SGAL::Sub_configuration(proto),
  m_palette_file_name(s_def_palette_file_name),
  m_background_color(s_def_background_color),
  m_min_bulge(s_def_min_bulge),
  m_store_data(s_def_store_data),
  m_version(s_def_version),
  m_transparency(s_def_transparency),
  m_test_depth(s_def_test_depth)
{}

//! \brief creates a new container of this type (virtual copy constructor).
SGAL::Container* Dxf_configuration::create()
{
  auto* factory = SGAL::Container_factory::get_instance();
  auto dxf_cont = factory->get_prototype("DxfConfiguration");
  auto dxf_conf_proto = boost::dynamic_pointer_cast<Dxf_configuration>(dxf_cont);
  return new Dxf_configuration(*dxf_conf_proto);
}

//! \brief initializes the node prototype.
void Dxf_configuration::init_prototype()
{
  using SGAL::Field_rule;
  using SGAL::Execution_function;
  using SGAL::SF_string;
  using SGAL::SF_vector4f;
  using SGAL::SF_float;
  using SGAL::SF_bool;
  using SGAL::SF_uint;

  if (s_prototype) return;
  s_prototype =
    new SGAL::Container_proto(SGAL::Sub_configuration::get_prototype());

  // Add the object fields to the prototype
  auto exec_func =
    static_cast<Execution_function>(&Container::set_rendering_required);

  // paletteFileName
  auto palette_file_name_func =
    static_cast<SGAL::String_handle_function>
    (&Dxf_configuration::palette_file_name_handle);
  s_prototype->add_field_info(new SF_string(PALETTE_FILE_NAME, "paletteFileName",
                                            Field_rule::RULE_EXPOSED_FIELD,
                                            palette_file_name_func,
                                            s_def_palette_file_name,
                                            exec_func));

  // backgroundColor
  auto background_color_func =
    static_cast<SGAL::Vector4f_handle_function>
    (&Dxf_configuration::background_color_handle);
  s_prototype->add_field_info(new SF_vector4f(BACKGROUND_COLOR,
                                              "backgroundColor",
                                              Field_rule::RULE_EXPOSED_FIELD,
                                              background_color_func,
                                              s_def_background_color,
                                              exec_func));

  // minBulge
  auto min_bulge_func =
    static_cast<SGAL::Float_handle_function>
    (&Dxf_configuration::min_bulge_handle);
  s_prototype->add_field_info(new SF_float(MIN_BULGE,
                                           "minBulge",
                                           Field_rule::RULE_EXPOSED_FIELD,
                                           min_bulge_func,
                                           s_def_min_bulge,
                                           exec_func));

  // refinementArcsNum
  auto refinement_arcs_num_func =
    static_cast<SGAL::Uint_handle_function>
    (&Dxf_configuration::refinement_arcs_num_handle);
  s_prototype->add_field_info(new SF_uint(REFINMENT_ARCS_NUM,
                                           "refinementArcsNum",
                                           Field_rule::RULE_EXPOSED_FIELD,
                                           refinement_arcs_num_func,
                                           s_def_refinement_arcs_num,
                                           exec_func));

  // storeData
  auto store_data_func =
    static_cast<SGAL::Boolean_handle_function>
    (&Dxf_configuration::store_data_handle);
  s_prototype->add_field_info(new SF_bool(STORE_DATA, "storeData",
                                          Field_rule::RULE_EXPOSED_FIELD,
                                          store_data_func, s_def_store_data,
                                          exec_func));

  // version
  auto version_func =
    static_cast<SGAL::Uint_handle_function>(&Dxf_configuration::version_handle);
  s_prototype->add_field_info(new SF_uint(VERSION, "version",
                                          Field_rule::RULE_EXPOSED_FIELD,
                                          version_func, s_def_version,
                                          exec_func));

  // transparency
  auto transparency_func =
    static_cast<SGAL::Float_handle_function>
    (&Dxf_configuration::transparency_handle);
  s_prototype->add_field_info(new SF_float(TRANSPARENCY, "transparency",
                                           Field_rule::RULE_EXPOSED_FIELD,
                                           transparency_func, s_def_transparency,
                                           exec_func));

  // testDepth
  auto test_depth_func =
    static_cast<SGAL::Boolean_handle_function>
    (&Dxf_configuration::test_depth_handle);
  s_prototype->add_field_info(new SF_bool(TEST_DEPTH, "testDepth",
                                          Field_rule::RULE_EXPOSED_FIELD,
                                          test_depth_func, s_def_test_depth,
                                          exec_func));
}

//! \brief deletes the node prototype.
void Dxf_configuration::delete_prototype()
{
  delete s_prototype;
  s_prototype = nullptr;
}

//! \brief obtains the node prototype.
SGAL::Container_proto* Dxf_configuration::get_prototype()
{
  if (!s_prototype) Dxf_configuration::init_prototype();
  return s_prototype;
}

//! \brief sets the attributes of the object.
void Dxf_configuration::set_attributes(SGAL::Element* elem)
{
  SGAL::Sub_configuration::set_attributes(elem);

  for (auto ai = elem->str_attrs_begin(); ai != elem->str_attrs_end(); ++ai) {
    const auto& name = elem->get_name(ai);
    const auto& value = elem->get_value(ai);

    if (name == "paletteFileName") {
      set_palette_file_name(value);
      elem->mark_delete(ai);
      continue;
    }
    if (name == "backgroundColor") {
      set_background_color(value);
      elem->mark_delete(ai);
      continue;
    }
    if (name == "minBulge") {
      set_min_bulge(boost::lexical_cast<SGAL::Float>(value));
      elem->mark_delete(ai);
      continue;
    }
    if (name == "refinementArcsNum") {
      set_refinement_arcs_num(boost::lexical_cast<SGAL::Uint>(value));
      elem->mark_delete(ai);
      continue;
    }
    if (name == "storeData") {
      set_store_data(SGAL::to_boolean(value));
      elem->mark_delete(ai);
      continue;
    }
    if (name == "version") {
      set_version(boost::lexical_cast<SGAL::Uint>(value));
      elem->mark_delete(ai);
      continue;
    }
    if (name == "transparency") {
      set_transparency(boost::lexical_cast<SGAL::Float>(value));
      elem->mark_delete(ai);
      continue;
    }
    if (name == "testDepth") {
      set_test_depth(SGAL::to_boolean(value));
      elem->mark_delete(ai);
      continue;
    }
  }

  // Remove all the marked attributes:
  elem->delete_marked();
}

//! \brief sets defualt values.
void Dxf_configuration::reset()
{
  m_palette_file_name = s_def_palette_file_name;
  m_background_color = s_def_background_color;
  m_min_bulge = s_def_min_bulge;
  m_refinement_arcs_num = s_def_refinement_arcs_num;
  m_store_data = s_def_store_data;
  m_version = s_def_version;
  m_transparency = s_def_transparency;
  m_test_depth = s_def_test_depth;
}

//! \brief enables the sub-configuration---re-route all fields. */
void Dxf_configuration::enable()
{
}

DXF_END_NAMESPACE
