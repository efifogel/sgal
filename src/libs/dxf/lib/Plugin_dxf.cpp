// Copyright (c) 2018 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <boost/shared_ptr.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/Container_factory.hpp"
#include "SGAL/Loader.hpp"
#include "SGAL/Writer.hpp"
#include "SGAL/Tracer.hpp"
#include "SGAL/Geometry_format.hpp"
#include "SGAL/Plugin_api.hpp"
#include "SGAL/Main_option_parser.hpp"
#include "SGAL/Configuration.hpp"
#include "SGAL/Container_factory.hpp"
#include "SGAL/create_container.hpp"

#include "dxf/basic.hpp"
#include "dxf/Plugin_dxf.hpp"
#include "dxf/Dxf_option_parser.hpp"
#include "dxf/Dxf_parser.hpp"
#include "dxf/Dxf_writer.hpp"
#include "dxf/Dxf_configuration.hpp"

DXF_BEGIN_NAMESPACE

//! \brief constructs default
Plugin_dxf::Plugin_dxf() :
  m_dxf_option_parser(nullptr),
  m_option_parser(nullptr)
{ /* std::cout << "Constructing Plugin_dxf" << std::endl; */ }

//! \brief destructs.
Plugin_dxf::~Plugin_dxf()
{
  if (m_dxf_option_parser) {
    delete m_dxf_option_parser;
    m_dxf_option_parser = nullptr;
  }
}

//! \brief obtains the name of the plugin.
std::string Plugin_dxf::name() const { return "dxf"; }

//! \brief initalizes the plugin.
void Plugin_dxf::init(SGAL::Main_option_parser* option_parser)
{
  REGISTER_OBJECT(Dxf_configuration);

  m_option_parser = option_parser;
  m_dxf_option_parser = new Dxf_option_parser;
  const auto& dxf_opts = m_dxf_option_parser->get_dxf_opts();
  option_parser->add_visible_options(dxf_opts);
  option_parser->add_command_line_options(dxf_opts);
  option_parser->add_config_file_options(dxf_opts);
  option_parser->add_environment_options(dxf_opts);

  // const auto& vm = option_parser->get_variable_map();
  // auto it = vm.find("trace");

  // handle Tracer
  auto* tracer = SGAL::Tracer::get_instance();
  auto trace_code_parsing = tracer->register_option("dxf-parsing");
  auto trace_code_building = tracer->register_option("dxf-building");
  auto trace_code_updating = tracer->register_option("dxf-updating");

  auto* geom_format = SGAL::Geometry_format::get_instance();
  auto format_code = geom_format->register_option("dxf");

  auto* loader = SGAL::Loader::get_instance();
  auto* dxf_parser = new Dxf_parser();
  dxf_parser->set_format(format_code);
  dxf_parser->set_trace_code_parsing(trace_code_parsing);
  dxf_parser->set_trace_code_building(trace_code_building);
  loader->doregister(".dxf", dxf_parser);

  auto* writer = SGAL::Writer::get_instance();
  auto* dxf_writter = new Dxf_writer;
  dxf_writter->set_trace_code_updating(trace_code_updating);
  writer->doregister(format_code, dxf_writter);
}

/*! Configure the plugin.
 */
void Plugin_dxf::configure(SGAL::Scene_graph* scene_graph)
{
  auto dxf_conf_proto =
    SGAL::get_container_prototype<Dxf_configuration>("DxfConfiguration");
  m_dxf_option_parser->configure(m_option_parser->get_variable_map(), &*dxf_conf_proto);

  // Introduce a Dxf-configuration container:
  auto dxf_conf = SGAL::create_container<Dxf_configuration>(dxf_conf_proto, scene_graph);
  auto conf = scene_graph->get_configuration();
  SGAL_assertion(conf);
  conf->add_sub_configuration(dxf_conf);
}

//! Indulge the user, that is, apply the the command line specified options.
void Plugin_dxf::indulge(SGAL::Scene_graph* scene_graph) {}

DXF_END_NAMESPACE
