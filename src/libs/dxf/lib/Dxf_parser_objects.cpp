// Copyright (c) 2018 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <string>
#include <iostream>
#include <functional>
#include <streambuf>

#include "SGAL/basic.hpp"
#include "SGAL/Types.hpp"
#include "SGAL/Tracer.hpp"

#include "dxf/basic.hpp"
#include "dxf/Dxf_parser.hpp"
#include "dxf/Dxf_data.hpp"
#include "dxf/Dxf_base_object.hpp"

// Objects
#include "dxf/Dxf_acad_proxy_object.hpp"
#include "dxf/Dxf_acdbdictionarywdflt_object.hpp"
#include "dxf/Dxf_acdbnavisworksmodeldef_object.hpp"
#include "dxf/Dxf_acdbplaceholder_object.hpp"
#include "dxf/Dxf_datatable_object.hpp"
#include "dxf/Dxf_dictionary_object.hpp"
#include "dxf/Dxf_dictionaryvar_object.hpp"
#include "dxf/Dxf_dimassoc_object.hpp"
#include "dxf/Dxf_field_object.hpp"
#include "dxf/Dxf_geodata_object.hpp"
#include "dxf/Dxf_group_object.hpp"
#include "dxf/Dxf_idbuffer_object.hpp"
#include "dxf/Dxf_imagedef_object.hpp"
#include "dxf/Dxf_imagedef_reactor_object.hpp"
#include "dxf/Dxf_layer_filter_object.hpp"
#include "dxf/Dxf_layer_index_object.hpp"
#include "dxf/Dxf_layout_object.hpp"
#include "dxf/Dxf_lightlist_object.hpp"
#include "dxf/Dxf_material_object.hpp"
#include "dxf/Dxf_mlinestyle_object.hpp"
#include "dxf/Dxf_object_ptr_object.hpp"
#include "dxf/Dxf_plotsettings_object.hpp"
#include "dxf/Dxf_rastervariables_object.hpp"
#include "dxf/Dxf_render_object.hpp"
#include "dxf/Dxf_section_object.hpp"
#include "dxf/Dxf_sortentstable_object.hpp"
#include "dxf/Dxf_spatial_filter_object.hpp"
#include "dxf/Dxf_spatial_index_object.hpp"
#include "dxf/Dxf_sunstudy_object.hpp"
#include "dxf/Dxf_tablestyle_object.hpp"
#include "dxf/Dxf_underlaydefinition_object.hpp"
#include "dxf/Dxf_vba_project_object.hpp"
#include "dxf/Dxf_visualstyle_object.hpp"
#include "dxf/Dxf_wipeoutvariables_object.hpp"
#include "dxf/Dxf_xrecord_object.hpp"
#include "dxf/Dxf_user_object.hpp"

DXF_BEGIN_NAMESPACE

//! \brief parse OBJECTS section
void Dxf_parser::parse_objects()
{
  SGAL_TRACE_CODE(get_trace_code_parsing(),
                  if (get_verbose_level() >= 2)
                    std::cout << "Parsing OBJECTS section" << std::endl;);

  int n;
  import_code(n);
  SGAL_assertion(0 == n);

  do {
    SGAL::String str;
    import_value(str);
    if ("ENDSEC" == str) return;

    auto it = s_objects.find(str);
    if (it != s_objects.end()) {
      auto* object = (this->*(it->second))();
      if (object) m_data->m_objects.push_back(object);
      continue;
    }

    // Look for user defined objects that are defined in a CLASS block
    auto cit = std::find_if(m_data->m_classes.begin(), m_data->m_classes.end(),
                            [&](const Dxf_class& my_class)
                            {
                              return ((my_class.m_record_name == str) &&
                                      ! my_class.m_is_entity);
                            });
    if (cit != m_data->m_classes.end()) {
      auto* user_object = new Dxf_user_object;
      m_data->m_objects.push_back(user_object);
      parse_record(*user_object);
      continue;
    }

    SGAL::String msg("Unrecognize object \"");
    msg += str + "\", at line " + std::to_string(m_line);
    SGAL_error_msg(msg.c_str());

  } while (true);
}

// Object parsers
//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_acad_proxy_object()
{ return parse_object("acad_proxy", new Dxf_acad_proxy_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_acdbdictionarywdflt_object()
{
  return parse_object("acdbdictionarywdflt",
                      new Dxf_acdbdictionarywdflt_object);
}

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_acdbplaceholder_object()
{ return parse_object("acdbplaceholder", new Dxf_acdbplaceholder_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_acdbnavisworksmodeldef_object()
{
  return parse_object("acdbnavisworksmodeldef",
                      new Dxf_acdbnavisworksmodeldef_object);
}

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_datatable_object()
{ return parse_object("datatable", new Dxf_datatable_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_dictionary_object()
{ return parse_object("dictionary", new Dxf_dictionary_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_dictionaryvar_object()
{ return parse_object("dictionaryvar", new Dxf_dictionaryvar_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_dimassoc_object()
{ return parse_object("dimassoc", new Dxf_dimassoc_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_field_object()
{ return parse_object("field", new Dxf_field_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_geodata_object()
{ return parse_object("geodata", new Dxf_geodata_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_group_object()
{ return parse_object("group", new Dxf_group_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_idbuffer_object()
{ return parse_object("idbuffer", new Dxf_idbuffer_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_imagedef_object()
{ return parse_object("imagedef", new Dxf_imagedef_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_imagedef_reactor_object()
{ return parse_object("imagedef_reactor", new Dxf_imagedef_reactor_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_layer_index_object()
{ return parse_object("layer_index", new Dxf_layer_index_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_layer_filter_object()
{ return parse_object("layer_filter", new Dxf_layer_filter_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_layout_object()
{ return parse_object("layout", new Dxf_layout_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_lightlist_object()
{ return parse_object("lightlist", new Dxf_lightlist_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_material_object()
{ return parse_object("material", new Dxf_material_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_mlinestyle_object()
{ return parse_object("mlinestyle", new Dxf_mlinestyle_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_object_ptr_object()
{ return parse_object("object_ptr", new Dxf_object_ptr_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_plotsettings_object()
{ return parse_object("plotsettings", new Dxf_plotsettings_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_rastervariables_object()
{ return parse_object("rastervariables", new Dxf_rastervariables_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_render_object()
{ return parse_object("render", new Dxf_render_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_section_object()
{ return parse_object("section", new Dxf_section_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_spatial_index_object()
{ return parse_object("spatial_index", new Dxf_spatial_index_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_spatial_filter_object()
{ return parse_object("spatial_filter", new Dxf_spatial_filter_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_sortentstable_object()
{ return parse_object("sortentstable", new Dxf_sortentstable_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_sunstudy_object()
{ return parse_object("sunstudy", new Dxf_sunstudy_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_tablestyle_object()
{ return parse_object("tablestyle", new Dxf_tablestyle_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_underlaydefinition_object()
{ return parse_object("underlaydefinition", new Dxf_underlaydefinition_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_visualstyle_object()
{ return parse_object("visualstyle", new Dxf_visualstyle_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_vba_project_object()
{ return parse_object("vba_project", new Dxf_vba_project_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_wipeoutvariables_object()
{ return parse_object("wipeoutvariables", new Dxf_wipeoutvariables_object); }

//! \brief parses a object.
Dxf_base_object* Dxf_parser::parse_xrecord_object()
{ return parse_object("xrecord", new Dxf_xrecord_object); }

DXF_END_NAMESPACE
