// Copyright (c) 2018 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <vector>

#include "SGAL/basic.hpp"
#include "SGAL/Node.hpp"
#include "SGAL/Shape.hpp"
#include "SGAL/Transform.hpp"
#include "SGAL/Switch.hpp"
#include "SGAL/Group.hpp"
#include "SGAL/Indexed_line_set.hpp"
#include "SGAL/Indexed_face_set.hpp"
#include "SGAL/Coord_array_3d.hpp"
#include "SGAL/Color_array_3d.hpp"
#include "SGAL/Color_array_4d.hpp"
#include "SGAL/to_hex_string.hpp"
#include "SGAL/Tracer.hpp"
#include "SGAL/Geo_set.hpp"
#include "SGAL/Matrix4f.hpp"
#include "SGAL/Appearance.hpp"
#include "SGAL/Material.hpp"
#include "SGAL/Vector3f.hpp"
#include "SGAL/Vector4f.hpp"
#include "SGAL/Scene_graph.hpp"
#include "SGAL/Configuration.hpp"
#include "SGAL/assign_unique_name.hpp"
#include "SGAL/Input_output.hpp"

#include "dxf/basic.hpp"
#include "dxf/Dxf_data.hpp"
#include "dxf/Dxf_polyline_boundary_path.hpp"

// Tables
#include "dxf/Dxf_layer_entry.hpp"

// Entities
#include "dxf/Dxf_updater.hpp"
#include "dxf/Dxf_line_entity.hpp"
#include "dxf/Dxf_polyline_entity.hpp"
#include "dxf/Dxf_lwpolyline_entity.hpp"
#include "dxf/Dxf_hatch_entity.hpp"
#include "dxf/Dxf_insert_entity.hpp"

// Objects
#include "dxf/Dxf_material_object.hpp"

DXF_BEGIN_NAMESPACE

//! \brief constructs.
Dxf_updater::Dxf_updater(Dxf_data& data, SGAL::Scene_graph* scene_graph,
                         size_t trace_code) :
  m_data(data),
  m_scene_graph(scene_graph),
  m_trace_code(trace_code),
  m_dimension(2),
  m_cur_entities(&data.m_entities)
{
  m_matrices.emplace(SGAL::Matrix4f());
  m_transform_names.emplace("");
}

//! \brief destructs.
Dxf_updater::~Dxf_updater()
{
  m_matrices.pop();
  m_transform_names.pop();
  m_layers.clear();
}

//! \brief handles a node.
void Dxf_updater::operator()(Shared_container node)
{
  auto transform = boost::dynamic_pointer_cast<SGAL::Transform>(node);
  if (transform) {
    handle(transform);
    return;
  }
  auto swtch = boost::dynamic_pointer_cast<SGAL::Switch>(node);
  if (swtch) {
    handle(swtch);
    return;
  }

  auto group = boost::dynamic_pointer_cast<SGAL::Switch>(node);
  if (group) {
    handle(group);
    return;
  }

  auto shape = boost::dynamic_pointer_cast<SGAL::Shape>(node);
  if (shape) {
    handle(shape);
    return;
  }
}

//! \brief handles a Transform node.
void Dxf_updater::handle(Shared_transform transform)
{
  SGAL_TRACE_CODE(m_trace_code,
                  if (true)
                    std::cout << "Handling Transform" << std::endl;);

  auto* start_cur_entities = m_cur_entities;
  const auto* conf = m_scene_graph->get_configuration();
  SGAL_assertion(conf);
  auto io = conf->get_input_output();
  SGAL_assertion(io);
  if (io->get_export_flat()) {
    // Push the transform matrix
    const SGAL::Matrix4f& new_matrix = transform->get_matrix();
    const SGAL::Matrix4f& top_matrix = m_matrices.top();
    m_matrices.emplace(SGAL::Matrix4f(top_matrix, new_matrix));
  }
  else {
    // Construct an insert entity together with a layer, a block_record, a
    // block, and a blk_end records.
    // Set the entities of the newly constructed block as the current entities.

    if (transform->get_name().empty()) assign_unique_name(transform);
    const auto& name = transform->get_name();
    m_transform_names.emplace(name);
    const auto& layer_name = name;
    const auto& block_record_name = name;
    const auto& block_name = name;

    // LAYER
    auto& layer_table = m_data.m_layer_table;
    layer_table.m_entries.emplace_back();
    auto& layer_entry = layer_table.m_entries.back();
    layer_entry.m_handle = SGAL::to_hex_string(m_data.m_handle++);
    layer_entry.m_owner_handle =
      SGAL::to_hex_string(Dxf_data::s_layer_table_handle);
    layer_entry.m_name = layer_name;
    layer_entry.m_flags = 0;
    layer_entry.m_color_index = 7;
    layer_entry.m_line_type = "Continuous";
    layer_entry.m_plot_style_pointer =
      SGAL::to_hex_string(Dxf_data::s_plot_style_handle);

    // BLOCK_RECORD
    auto block_record_entry_handle = SGAL::to_hex_string(m_data.m_handle++);
    auto& block_record_table = m_data.m_block_record_table;
    block_record_table.m_entries.emplace_back();
    auto& block_record_entry = block_record_table.m_entries.back();
    block_record_entry.m_handle = block_record_entry_handle;
    block_record_entry.m_owner_handle =
      SGAL::to_hex_string(Dxf_data::s_block_record_table_handle);
    block_record_entry.m_name = block_record_name;

    // BLOCK & BLK_END
    auto& blocks = m_data.m_blocks;
    blocks.emplace_back();
    auto& full_block = blocks.back();
    auto& block = full_block.first;
    block.m_handle = SGAL::to_hex_string(m_data.m_handle++);
    block.m_owner_handle = block_record_entry_handle;
    block.m_layer_name = layer_name;
    block.m_name = block_name;
    block.m_flags = 0;
    block.m_base_point[0] = 0.0;
    block.m_base_point[1] = 0.0;
    block.m_base_point[2] = 0.0;
    block.m_name = block_name;
    block.m_xref_path_name = "";

    auto& edblk = full_block.second;
    edblk.m_handle = SGAL::to_hex_string(m_data.m_handle++);
    edblk.m_owner_handle = block_record_entry_handle;
    edblk.m_layer_name = layer_name;

    // INSERT
    auto* insert_entity = new Dxf_insert_entity;
    m_cur_entities->push_back(insert_entity);
    insert_entity->m_handle = SGAL::to_hex_string(m_data.m_handle++);
    insert_entity->m_owner_handle = block_record_entry_handle;
    insert_entity->m_layer = layer_name;
    // entity->m_color_index = ?;
    // entity->m_color = ?;

    const auto& rot = transform->get_rotation();
    insert_entity->m_name = block_name;
    const auto& translation = transform->get_translation();
    insert_entity->m_location[0] = translation[0];
    insert_entity->m_location[1] = translation[1];
    insert_entity->m_location[2] = translation[2];
    insert_entity->m_rotation = rot.get_angle();;
    const auto& axis = rot.get_axis();
    insert_entity->m_extrusion_direction[0] = axis[0];
    insert_entity->m_extrusion_direction[1] = axis[1];
    insert_entity->m_extrusion_direction[2] = axis[2];

    m_cur_entities = &(block.m_entities);
  }

  Shared_group group = transform;
  handle(group);

  // Pop the transform matrix
  if (io->get_export_flat()) m_matrices.pop();
  m_transform_names.pop();
  m_cur_entities = start_cur_entities;
}

//! \brief handles a Switch node.
void Dxf_updater::handle(Shared_switch swtch)
{
  SGAL_TRACE_CODE(m_trace_code,
                  if (true)
                    std::cout << "Handling Switch" << std::endl;);
  operator()(swtch->get_choice());
}

//! \brief handles a Group node.
void Dxf_updater::handle(Shared_group group)
{
  SGAL_TRACE_CODE(m_trace_code,
                  if (true)
                    std::cout << "Handling Group" << std::endl;);

  for (auto it = group->children_begin(); it != group->children_end(); ++it)
    operator()(*it);
}

/*! Initialize an object.
 */
void Dxf_updater::init_object(Dxf_base_object* object)
{
  object->m_handle = m_data.m_handle++;
  object->m_owner_handle =
    SGAL::to_hex_string(Dxf_data::s_block_record_model_space_handle);
}

//! \brief creates a new DXF material object.
Dxf_material_object* Dxf_updater::create_material(Shared_material mat)
{
  auto* mat_object = new Dxf_material_object;
  m_data.m_objects.push_back(mat_object);
  init_object(mat_object);
  mat_object->m_ambient_color_value = compact_color(mat->get_ambient_color());
  mat_object->m_diffuse_color_value = compact_color(mat->get_diffuse_color());
  mat_object->m_specular_color_value = compact_color(mat->get_specular_color());
  mat_object->m_opacity_factor = 1.0 - mat->get_transparency();
  return mat_object;
}

//! \brief obtains a new layer entry associated with a material.
Dxf_layer_entry& Dxf_updater::create_layer(Shared_material material)
{
  // Obtain a name for the material.
  if (material->get_name().empty()) {
    auto uuid = boost::uuids::random_generator()();
    material->set_name(boost::uuids::to_string(uuid));
  }
  const auto& name = material->get_name();

  auto* material_object = create_material(material);    // create a DXF material

  // Create a DXF layer entry.
  m_data.m_layer_table.m_entries.emplace_back();
  auto& layer_entry = m_data.m_layer_table.m_entries.back();
  m_layers.insert(std::make_pair(material, &layer_entry));

  // Populate the layer entry.
  layer_entry.m_name = name;
  layer_entry.m_material_handle = material_object->m_handle;

  return layer_entry;
}

//! \brief handles an appearance.
Dxf_layer_entry& Dxf_updater::create_layer(Shared_appearance appearance)
{
  auto material = appearance->get_material();
  auto layer_it = m_layers.find(material);

  return (layer_it == m_layers.end()) ?
    create_layer(material) : *(layer_it->second);
}

//! \brief handles a shape node.
void Dxf_updater::handle(Shared_shape shape)
{
  SGAL_TRACE_CODE(m_trace_code,
                  if (true)
                    std::cout << "Handling Shape" << std::endl;);

  bool has_color(false);
  auto appearance = shape->get_appearance();
  auto geometry = shape->get_geometry();
  auto geo_set = boost::dynamic_pointer_cast<SGAL::Geo_set>(geometry);
  if (geo_set) {
    auto colors = geo_set->get_color_array();
    if (colors && ! colors->empty()) {
      has_color = true;
    }
  }

  const std::string* layer_name = &Dxf_data::s_def_layer;
  if (! has_color && ! appearance->get_material_mode_enable() &&
      appearance->get_light_enable())
  {
    auto& layer = create_layer(appearance);
    layer_name = &layer.m_name;
  }

  auto ifs = boost::dynamic_pointer_cast<SGAL::Indexed_face_set>(geometry);
  if (ifs) {
    handle(ifs, *layer_name);
    return;
  }

  auto ils = boost::dynamic_pointer_cast<SGAL::Indexed_line_set>(geometry);
  if (ils) {
    handle(ils, *layer_name);
    return;
  }
}

//! \brief obtains a compact color.
int Dxf_updater::compact_color(Shared_color_array colors, size_t index) const
{
  auto ca3 = boost::dynamic_pointer_cast<SGAL::Color_array_3d>(colors);
  if (ca3) return compact_color(ca3, index);

  auto ca4 = boost::dynamic_pointer_cast<SGAL::Color_array_4d>(colors);
  SGAL_assertion(ca4);
  return compact_color(ca4, index);
}

/*! Initialize an entity.
 */
void Dxf_updater::init_entity(Dxf_base_entity* entity, int color,
                              SGAL::Geo_set::Attachment attach,
                              Shared_color_array colors, size_t i,
                              const std::string& layer_name)
{
  entity->m_handle = SGAL::to_hex_string(m_data.m_handle++);
  entity->m_owner_handle =
    SGAL::to_hex_string(Dxf_data::s_block_record_model_space_handle);
  entity->m_layer = layer_name;
  if (colors && ! colors->empty()) {
    if (SGAL::Geo_set::AT_PER_PRIMITIVE == attach)
      color = compact_color(colors, i);
  }
  if (-1 != color) {
    //! I don't know what should be the value of the m_color_index field,
    // but I'm certain that it should not be the default, that is, BYLAYER.
    entity->m_color_index = 0;
    entity->m_color = color;
  }
}

//! \brief handles an Indexed_face_set node.
void Dxf_updater::handle(Shared_indexed_face_set ifs,
                         const std::string& layer_name)
{
  SGAL_TRACE_CODE(m_trace_code,
                  if (true)
                    std::cout << "Handling Indexed_face_set" << std::endl;);

  auto attach = ifs->get_color_attachment();
  auto colors = ifs->get_color_array();
  int color(-1);
  if (colors && ! colors->empty()) {
    if (SGAL::Geo_set::AT_PER_MESH == attach) color = compact_color(colors, 0);
  }

  create_hatch(ifs, color, layer_name);
}

//! \brief handles an Indexed_line_set node.
void Dxf_updater::handle(Shared_indexed_line_set ils,
                         const std::string& layer_name)
{
  SGAL_TRACE_CODE(m_trace_code,
                  if (true)
                    std::cout << "Handling Indexed_line_set" << std::endl;);

  auto attach = ils->get_color_attachment();
  auto colors = ils->get_color_array();
  int color(-1);
  if (colors && ! colors->empty()) {
    if (SGAL::Geo_set::AT_PER_MESH == attach) color = compact_color(colors, 0);
  }

  if (SGAL::Geo_set::PT_LINES == ils->get_primitive_type()) {
    create_lines(ils, color, layer_name);
    return;
  }

  bool closed = SGAL::Geo_set::PT_LINE_LOOPS == ils->get_primitive_type();
  if (m_dimension == 2) {
    create_lwpolylines(ils, color, closed, layer_name);
    return;
  }

  create_polylines(ils, color, closed, layer_name);
}

//! \brief updates lines.
void Dxf_updater::create_lines(Shared_indexed_line_set ils, int color,
                               const std::string& layer_name)
{
  SGAL_TRACE_CODE(m_trace_code,
                  if (true) std::cout << "Updating lines" << std::endl;);

  auto attach = ils->get_color_attachment();
  auto colors = ils->get_color_array();
  auto cda = ils->get_coord_array();
  auto coords= boost::dynamic_pointer_cast<SGAL::Coord_array_3d>(cda);
  auto& indices = ils->line_coord_indices();

  for (size_t i = 0; i < ils->get_num_primitives(); ++ i) {
    auto* line_entity = new Dxf_line_entity;
    m_cur_entities->push_back(line_entity);
    init_entity(line_entity, color, attach, colors, i, layer_name);

    line_entity->m_thickness = ils->get_line_width();
    auto& start = line_entity->m_start;
    auto s = indices[i][0];
    start[0] = (*coords)[s][0];
    start[1] = (*coords)[s][1];
    start[2] = (*coords)[s][2];

    auto& end = line_entity->m_end;
    auto e = indices[i][1];
    end[0] = (*coords)[e][0];
    end[1] = (*coords)[e][1];
    end[2] = (*coords)[e][2];
  }
}

///! \brief updates light weight polylines.
void Dxf_updater::create_lwpolylines(Shared_indexed_line_set ils, int color,
                                     bool closed, const std::string& layer_name)
{
  SGAL_TRACE_CODE(m_trace_code,
                  if (true)
                    std::cout << "Updating lwpolylines" << std::endl;);

  auto attach = ils->get_color_attachment();
  auto colors = ils->get_color_array();
  auto cda = ils->get_coord_array();
  auto coords= boost::dynamic_pointer_cast<SGAL::Coord_array_3d>(cda);
  auto& indices = ils->polyline_coord_indices();
  const SGAL::Matrix4f& matrix = top_matrix();
  std::vector<SGAL::Vector3f> world_coords(coords->size());
  auto oit = world_coords.begin();
  for (auto it = coords->begin(); it != coords->end(); ++it)
    (*oit++).xform_pt(*it, matrix);

  for (size_t i = 0; i < ils->get_num_primitives(); ++ i) {
    auto& pindices = indices[i];
    auto* lwpolyline_entity = new Dxf_lwpolyline_entity;
    m_cur_entities->push_back(lwpolyline_entity);
    init_entity(lwpolyline_entity, color, attach, colors, i, layer_name);

    auto width = ils->get_line_width();
    if (1 != width ) lwpolyline_entity->m_thickness = ils->get_line_width();
    lwpolyline_entity->m_flags = (closed) ? 1 : 0;

    lwpolyline_entity->m_vertices.resize(pindices.size());
    std::transform(pindices.begin(), pindices.end(),
                   lwpolyline_entity->m_vertices.begin(),
                   [&](size_t j)->SGAL::Vector2f {
                     auto& v = world_coords[j];
                     return SGAL::Vector2f(v[0], v[1]);
                   });
  }
}

///! \brief creates a hatch.
Dxf_hatch_entity* Dxf_updater::create_hatch(Shared_indexed_face_set ifs,
                                            int color,
                                            const std::string& layer_name)
{
  SGAL_TRACE_CODE(m_trace_code,
                  if (true)
                    std::cout << "Creating hatch" << std::endl;);

  auto attach = ifs->get_color_attachment();
  auto colors = ifs->get_color_array();

  auto* hatch_entity = new Dxf_hatch_entity;
  m_cur_entities->push_back(hatch_entity);
  init_entity(hatch_entity, color, attach, colors, 0, layer_name);
  hatch_entity->m_pattern_name.assign("SOLID");
  hatch_entity->m_flags = Dxf_hatch_entity::SOLID_FILL;
  hatch_entity->m_boundary_paths.resize(ifs->get_num_primitives());

  auto cda = ifs->get_coord_array();
  auto coords= boost::dynamic_pointer_cast<SGAL::Coord_array_3d>(cda);
  const SGAL::Matrix4f& matrix = top_matrix();
  std::vector<SGAL::Vector3f> world_coords(coords->size());
  auto oit = world_coords.begin();
  for (auto it = coords->begin(); it != coords->end(); ++it)
    (*oit++).xform_pt(*it, matrix);

  SGAL_assertion(SGAL::Geo_set::PT_TRIANGLES == ifs->get_primitive_type());
  auto& indices = ifs->triangle_coord_indices();
  auto iit = indices.begin();
  for (auto& boundary_path : hatch_entity->m_boundary_paths) {
    auto polyline_boundary_path = new Dxf_polyline_boundary_path;
    boundary_path = polyline_boundary_path;
    polyline_boundary_path->m_flags = Dxf_base_boundary_path::POLYLINE;
    polyline_boundary_path->m_has_bulge = 0;
    polyline_boundary_path->m_is_closed = 1;
    auto& pindices = *iit++;
    polyline_boundary_path->m_locations.resize(pindices.size());
    std::transform(pindices.begin(), pindices.end(),
                   polyline_boundary_path->m_locations.begin(),
                   [&](size_t j)->SGAL::Vector2f {
                     auto& v = world_coords[j];
                     return SGAL::Vector2f(v[0], v[1]);
                   });
  }
  return hatch_entity;
}

///! \brief updates polylines.
void Dxf_updater::create_polylines(Shared_indexed_line_set ils, int color,
                                   bool closed, const std::string& layer_name)
{
  SGAL_TRACE_CODE(m_trace_code,
                  if (true)
                    std::cout << "Updating polylines" << std::endl;);
  SGAL_error_msg("Not implemented yet!");

}

DXF_END_NAMESPACE
