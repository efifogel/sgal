// Copyright (c) 2018 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef DXF_UPDATER_HPP
#define DXF_UPDATER_HPP

#include <stack>
#include <map>
#include <string>

#include <boost/shared_ptr.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/Geo_set.hpp"
#include "SGAL/Matrix4f.hpp"

#include "dxf/basic.hpp"

SGAL_BEGIN_NAMESPACE

class Container;
class Scene_graph;
class Transform;
class Switch;
class Group;
class Shape;
class Geo_set;
class Indexed_face_set;
class Indexed_line_set;
class Color_array;
class Appearance;
class Material;

SGAL_END_NAMESPACE

DXF_BEGIN_NAMESPACE

class Dxf_data;
struct Dxf_layer_entry;
struct Dxf_base_entity;
struct Dxf_base_object;
struct Dxf_hatch_entity;
struct Dxf_material_object;

class SGAL_SGAL_DECL Dxf_updater {
public:
  typedef boost::shared_ptr<SGAL::Container>            Shared_container;
  typedef boost::shared_ptr<SGAL::Transform>            Shared_transform;
  typedef boost::shared_ptr<SGAL::Switch>               Shared_switch;
  typedef boost::shared_ptr<SGAL::Group>                Shared_group;
  typedef boost::shared_ptr<SGAL::Shape>                Shared_shape;
  typedef boost::shared_ptr<SGAL::Appearance>           Shared_appearance;
  typedef boost::shared_ptr<SGAL::Material>             Shared_material;
  typedef boost::shared_ptr<SGAL::Indexed_face_set>     Shared_indexed_face_set;
  typedef boost::shared_ptr<SGAL::Indexed_line_set>     Shared_indexed_line_set;
  typedef boost::shared_ptr<SGAL::Geo_set>              Shared_geo_set;
  typedef boost::shared_ptr<SGAL::Color_array>          Shared_color_array;

  /*! Construct.
   */
  Dxf_updater(Dxf_data& data, SGAL::Scene_graph* scene_graph, size_t trace_code);

  /*! Destruct.
   */
  virtual ~Dxf_updater();

  /*! Handle a node.
   */
  void operator()(Shared_container cont);

protected:
  /*! Obtain a compact color.
   *\todo Consider moveing out from here.
   */
  template <typename SharedColorArray>
  int compact_color(SharedColorArray colors, size_t index) const;

  /*! Obtain a compact color.
   */
  int compact_color(Shared_color_array colors, size_t index) const;

  /*! Obtain a compact color.
   */
  template <typename Vector_>
  int compact_color(const Vector_& color) const;

  /*! Initialize an entity.
   */
  void init_entity(Dxf_base_entity* entity, int color,
                   SGAL::Geo_set::Attachment attach,
                   Shared_color_array colors, size_t i,
                   const std::string& layer_name);

  /*! Initialize an object.
   */
  void init_object(Dxf_base_object* object);

  /// Handlers
  //@{
  virtual void handle(Shared_transform transform);
  virtual void handle(Shared_switch swtch);
  virtual void handle(Shared_group group);
  virtual void handle(Shared_shape shape);
  virtual void handle(Shared_indexed_face_set ifs,
                      const std::string& layer_name);
  virtual void handle(Shared_indexed_line_set ils,
                      const std::string& layer_name);
  //@}

  /// Creators
  //@{
  virtual void create_lines(Shared_indexed_line_set ils, int color,
                            const std::string& layer_name);
  virtual void create_lwpolylines(Shared_indexed_line_set ils, int color,
                                  bool closed, const std::string& layer_name);
  virtual void create_polylines(Shared_indexed_line_set ils, int color,
                                bool closed, const std::string& layer_name);
  virtual Dxf_hatch_entity* create_hatch(Shared_indexed_face_set ifs, int color,
                                         const std::string& layer_name);

  virtual Dxf_material_object* create_material(Shared_material material);
  virtual Dxf_layer_entry& create_layer(Shared_material material);
  virtual Dxf_layer_entry& create_layer(Shared_appearance appearance);
  //@}

  /*! Obtain the viewing matrix at the top of the stack.
   * \return the viewing matrix at the top of the stack.
   */
  const SGAL::Matrix4f& top_matrix() const;

  //! The DXF data.
  Dxf_data& m_data;

  //! The scene graph
  SGAL::Scene_graph* m_scene_graph;

  //! The trace code.
  size_t m_trace_code;

  size_t m_dimension;

  //! Current entities. Points either to the main entities container or to
  // the entity container in a block.
  std::vector<Dxf_base_entity*>* m_cur_entities;

  //! The stack of viewing matrices.
  std::stack<SGAL::Matrix4f> m_matrices;

  //! A stack of transform names, which are used as layer names.
  std::stack<std::string> m_transform_names;

private:
  //! A mapping from material to DXF layers
  std::map<Shared_material, Dxf_layer_entry*> m_layers;
};

//! \brief obtains a compact color.
template <typename Vector_>
int Dxf_updater::compact_color(const Vector_& color) const
{
  auto r = static_cast<int>(color[0] * 255.0 + 0.5f);
  auto g = static_cast<int>(color[1] * 255.0 + 0.5f);
  auto b = static_cast<int>(color[2] * 255.0 + 0.5f);
  return (r << 16) | (g << 8) | b;
}

//! \brief obtains a compact colod.
template <typename SharedColorArray>
inline int
Dxf_updater::compact_color(SharedColorArray colors, size_t index) const
{ return compact_color((*colors)[index]); }

//! \brief obtains the viewing matrix at the top of the stack.
inline const SGAL::Matrix4f& Dxf_updater::top_matrix() const
{ return m_matrices.top(); }

DXF_END_NAMESPACE

#endif
