// Copyright (c) 2018 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef DXF_VIEW_MODE_HPP
#define DXF_VIEW_MODE_HPP

#include "SGAL/basic.hpp"

#include "dxf/basic.hpp"

DXF_BEGIN_NAMESPACE

enum class View_mode {
  VM_OFF = 0x0,                 // Off
  VM_PERSPECTIVE = 0x1,         // Perspective view
  VM_FRONT_CLIPPING = 0x2,      // Front clipping
  VM_BACK_CLIPPING = 0x4,       // Back clipping
  VM_UCS_FOLLOW = 0x8,          // UCS Follow mode
  VM_FRONTZ = 0xF               // Front clipping not at the camera (not
                                // available in AutoCAD LT)
                                // If turned on, FRONTZ determines the front
                                // clipping plane.
                                // If turned off, FRONTZ is ignored, and the
                                // front clipping plane passes through the
                                // camera point. This setting is ignored if the
                                // front-clipping bit 2 is turned off.
};

DXF_END_NAMESPACE

#endif
