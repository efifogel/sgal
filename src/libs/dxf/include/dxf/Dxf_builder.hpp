// Copyright (c) 2018 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef DXF_DXF_BUILDER_HPP
#define DXF_DXF_BUILDER_HPP

#include <list>
#include <vector>
#include <map>

#include <boost/shared_ptr.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/Vector3f.hpp"

#include "dxf/basic.hpp"

SGAL_BEGIN_NAMESPACE

class Scene_graph;
class Transform;
class Group;
class Shape;
class Appearance;
class Coord_array_3d;
class Color_array_3d;
class Color_array_4d;
class Geo_set;
class Indexed_line_set;
class Indexed_face_set;

SGAL_END_NAMESPACE

DXF_BEGIN_NAMESPACE

class Dxf_configuration;

struct Dxf_data;
struct Dxf_base_entity;
struct Dxf_polyline_boundary_path;

// Entities
struct Dxf_line_entity;
struct Dxf_circle_entity;
struct Dxf_arc_entity;
struct Dxf_hatch_entity;
struct Dxf_polyline_entity;
struct Dxf_lwpolyline_entity;
struct Dxf_spline_entity;
struct Dxf_solid_entity;
struct Dxf_insert_entity;
struct Dxf_material_object;

class SGAL_SGAL_DECL Dxf_builder {
public:
  typedef boost::shared_ptr<SGAL::Transform>          Shared_transform;
  typedef boost::shared_ptr<SGAL::Shape>              Shared_shape;
  typedef boost::shared_ptr<SGAL::Appearance>         Shared_appearance;
  typedef boost::shared_ptr<SGAL::Color_array_3d>     Shared_color_array_3d;
  typedef boost::shared_ptr<SGAL::Color_array_4d>     Shared_color_array_4d;
  typedef boost::shared_ptr<SGAL::Coord_array_3d>     Shared_coord_array_3d;
  typedef boost::shared_ptr<SGAL::Indexed_line_set>   Shared_indexed_line_set;
  typedef boost::shared_ptr<SGAL::Indexed_face_set>   Shared_indexed_face_set;
  typedef boost::shared_ptr<SGAL::Geo_set>            Shared_geo_set;
  typedef boost::shared_ptr<SGAL::Geometry>           Shared_geometry;
  typedef boost::shared_ptr<Dxf_configuration>        Shared_dxf_configuration;

  /*! Construct.
   */
  Dxf_builder(Dxf_data& data, SGAL::Scene_graph* scene_graph, size_t trace_code);

  /*! Destruct.
   */
  virtual ~Dxf_builder();

  /*! Build from the root.
   */
  void operator()(SGAL::Group* root);

protected:
  /*! Process all layers. Create a color array for each.
   */
  virtual void process_layers(SGAL::Group* root);

  /*! Initialize the pallete.
   */
  static void init_palette(const SGAL::String& file_name);

  /*! Add a default background color.
   */
  void add_background(SGAL::Group* root);

  /// \name General Processors
  //@{

  /*! Dispatch the processing of all entities.
   */
  void process_entities(std::vector<Dxf_base_entity*>& entities,
                        SGAL::Group* root);

  //@}

  /// Entity processors
  //@{

  /*! Process a line entity.
   */
  virtual void process_line_entity(const Dxf_line_entity& line,
                                   SGAL::Group* root);

  /*! Process a circle entity.
   */
  virtual void process_circle_entity(const Dxf_circle_entity& circle,
                                     SGAL::Group* root);

  /*! Process an arc entity.
   */
  virtual void process_arc_entity(const Dxf_arc_entity& arc, SGAL::Group* root);

  /*! Process a hatch entity.
   */
  virtual void process_hatch_entity(const Dxf_hatch_entity& hatch,
                                    SGAL::Group* root);

  /*! Process a polyline entity.
   */
  virtual void process_polyline_entity(const Dxf_polyline_entity& polyline,
                                       SGAL::Group* root);

  /*! Process a light weight polyline entity.
   */
  virtual void process_lwpolyline_entity(const Dxf_lwpolyline_entity& polyline,
                                         SGAL::Group* root);

  /*! Process a spline entity.
   */
  virtual void process_spline_entity(const Dxf_spline_entity& spline,
                                     SGAL::Group* root);

  /*! Process an insert entity.
   */
  virtual void process_insert_entity(const Dxf_insert_entity& insert,
                                     SGAL::Group* root);

  /*! Process a solid entity.
   */
  virtual void process_solid_entity(const Dxf_solid_entity& solid,
                                    SGAL::Group* root);

  //@}

  /// Creators
  //@{

  /*! Create a transform node.
   */
  virtual Shared_transform create_transform(const SGAL::Vector3f& translation,
                                            const SGAL::Rotation& rotation,
                                            const SGAL::Vector3f& scale);

  /*! Create a shape node.
   */
  virtual Shared_shape create_shape(Shared_geometry geometry,
                                    Shared_appearance appearance,
                                    const std::string* name = nullptr);

  /*! Create a shape node and add it as a new child of a given group node.
   */
  virtual void add_shape(Shared_geometry geometry, Shared_appearance appearance,
                         SGAL::Group* root);

  /*! Create a geometry node of type Geo_set from a 3D polyface mesh.
   */
  virtual Shared_geo_set
    create_geo_set_from_polyface_mesh(const Dxf_polyline_entity& polyline);

  /*! Create a geometry node of type Geo_set from a 3D polygon mesh.
   */
  virtual Shared_geo_set
    create_geo_set_from_polygon_mesh(const Dxf_polyline_entity& polyline);

  /*! Create a geometry node of type Geo_set from a light-weight polyline.
   */
  virtual Shared_geo_set
    create_geo_set_from_lwpolyline(const Dxf_lwpolyline_entity& polyline);

  /*! Create a geometry node of type Geo_set from a polyline.
   */
  virtual Shared_geo_set
    create_geo_set_from_polyline(const Dxf_polyline_entity& polyline);

  /*! Create coordinates in 2D
   */
  virtual Shared_coord_array_3d
    create_coord_array_3d_from_2d(const Dxf_polyline_entity& polyline);

  /*! Create coordinates in 3D
   */
  virtual Shared_coord_array_3d
    create_coord_array_3d_from_3d(const Dxf_polyline_entity& polyline);

  //@}

  /*! Obtain the n-component color array of an entity.
   */
  template <typename ColorArray>
  boost::shared_ptr<ColorArray>
  get_color_array(std::map<size_t, boost::shared_ptr<ColorArray> >&
                    index_arrays,
                  std::map<SGAL::String, boost::shared_ptr<ColorArray> >&
                    color_arrays,
                  int32_t color, int16_t color_index,
                  const SGAL::String& layer_name);

  /*! Find the material object the handle of which is given.
   */
  Dxf_material_object* find_material_object(const SGAL::String& handle) const;

  /*! Obtain the light-disabled, depth-disabled appearance.
   */
  Shared_appearance get_2d_appearance();

  /*! Obtain a light-disabled pattern appearance.
   */
  Shared_appearance get_2d_pattern_appearance();

  /*! Obtain the appearance of a layer.
   */
  Shared_appearance get_layer_appearance(const SGAL::String layer_name) const;

  /*! Add polylines (without bulge).
   * \param[in] polylines
   * \param[in] root
   * \param[in] closed
   */
  void process_polyline_boundaries
    (const Dxf_hatch_entity& hatch_entity,
     const std::list<Dxf_polyline_boundary_path*>& polylines,
     SGAL::Group* root);

  /*! Print out hatch information.
   */
  void print_hatch_information(const Dxf_hatch_entity& hatch);

  /*! Obtain the transparency value.
   */
  SGAL::Float get_transparency() const;

  /*! Determine whether to mirror the coordinates about the X-axis.
   */
  bool do_mirror(const double extrusion_direction[3]);

  /*! Construct a shape container.
   */
  Shared_shape construct_shape(const std::string* name = nullptr);

  /*! Construct an Indexed_face_set geometry container.
   */
  Shared_indexed_face_set construct_indexed_face_set();

  /*! Construct an Indexed_line_set geometry container.
   */
  Shared_indexed_line_set construct_indexed_line_set();

  /*! Construct a Coord_array_3d container.
   */
  Shared_coord_array_3d construct_coord_array_3d();

  /*! Obtain the min bulge.
   */
  float get_min_bulge() const;

  /*! Expand an int32_t to a Vector3d representing an RGB color.
   */
  void expand(int32_t compact, SGAL::Vector3f& out);

  //! The DXF data
  Dxf_data& m_data;

  //! The scene graph
  SGAL::Scene_graph* m_scene_graph;

  //! The scene to export.
  Shared_dxf_configuration m_dxf_configuration;

  //! The trace code.
  size_t m_trace_code;

  //! A light-disabled appearance.
  Shared_appearance m_fill_appearance;

  //! A light-disabled pattern enabled appearance.
  std::list<Shared_appearance> m_pattern_appearances;

  //! A mapping from a layer name to an appearance.
  std::map<SGAL::String, Shared_appearance> m_layer_appearances;

  //! A mapping from a color index to a color array.
  std::map<size_t, Shared_color_array_3d> m_color_3d_arrays;

  //! A mapping from a color index to a color array.
  std::map<size_t, Shared_color_array_4d> m_color_4d_arrays;

  //! A mapping from a layer name to a 3-components color array.
  std::map<SGAL::String, Shared_color_array_3d> m_layer_color_3d_arrays;

  //! A mapping from a layer name to a 4-components color array.
  std::map<SGAL::String, Shared_color_array_4d> m_layer_color_4d_arrays;

  /// Counters for statistics. \todo Move out with Builder.
  //@{
  size_t m_lines_num;
  size_t m_polylines_num;
  size_t m_lwpolylines_num;
  size_t m_circles_num;
  size_t m_arcs_num;
  size_t m_hatches_num;
  size_t m_splines_num;
  size_t m_solids_num;

  size_t m_inserts_num;
  //@}

private:
  //! Default color palette
  static std::vector<SGAL::Vector3f> s_palette;
};

//! \brief expand an Int32 to an rGB floating color vector.
inline void Dxf_builder::expand(int32_t compact, SGAL::Vector3f& color)
{
  auto r = static_cast<float>((compact >> 16) & 0xff) / 255.0f;
  auto g = static_cast<float>((compact >> 8) & 0xff) / 255.0f;
  auto b = static_cast<float>((compact >> 0) & 0xff) / 255.0f;
  color.set(r, g, b);
}

DXF_END_NAMESPACE

#endif
