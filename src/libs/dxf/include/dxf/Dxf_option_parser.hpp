// Copyright (c) 2018 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef DXF_DXF_OPTION_PARSER_HPP
#define DXF_DXF_OPTION_PARSER_HPP

#include <boost/program_options.hpp>

#include "SGAL/basic.hpp"

#include "dxf/basic.hpp"

namespace po = boost::program_options;

SGAL_BEGIN_NAMESPACE

class Configuration;

SGAL_END_NAMESPACE

DXF_BEGIN_NAMESPACE

class Dxf_configuration;

class SGAL_SGAL_DECL Dxf_option_parser {
public:
  typedef boost::shared_ptr<Dxf_configuration>         Shared_dxf_configuration;

  /*! Construct default.
   */
  Dxf_option_parser();

  /*! Destruct.
   */
  ~Dxf_option_parser();

  /*! Apply the options
   */
  void apply();

  /*! Obtain the dxf-option description.
   * \return the dxf-option description.
   */
  const po::options_description& get_dxf_opts() const;

  /*! Configure. */
  void configure(const po::variables_map& vars_map, Dxf_configuration* dxf_conf);

protected:
  //! The dxf options.
  po::options_description m_dxf_opts;
};

//! obtains the dxf-option description.
inline const po::options_description& Dxf_option_parser::get_dxf_opts() const
{ return m_dxf_opts; }

DXF_END_NAMESPACE

#endif
