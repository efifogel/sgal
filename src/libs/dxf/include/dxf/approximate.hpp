// Copyright (c) 2018 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef DXF_APPROXIMATE_HPP
#define DXF_APPROXIMATE_HPP

#include <list>
#include <iterator>

#include "SGAL/basic.hpp"
#include "SGAL/Vector2f.hpp"
#include "SGAL/approximate_circular_arc.hpp"

#include "dxf/basic.hpp"

DXF_BEGIN_NAMESPACE

/*! Approximate a polyline with buldges in-place
 * \param[in,out] the sequence of points.
 * \param[in] bulges_begin the begin iterator of the bulges
 * \param[in] min_bulge the minimum bulge (used for approximating a circular
 *            arc).
 * \param[in] closed indicates whether the polyline is closed.
 */
template <typename BulgeInputIterator>
void approximate(std::list<SGAL::Vector2f>& points,
                 BulgeInputIterator bulges_begin,
                 double min_bulge, bool closed = true)
{
  typedef std::list<SGAL::Vector2f>             Point_list;
  typedef std::insert_iterator<Point_list>      Insert_point_iterator;

  auto bit = bulges_begin;
  auto vit2 = points.begin();
  auto vit1 = vit2;

  for (++vit2; vit2 != points.end(); ++vit2, ++bit) {
    SGAL::approximate_circular_arc(*vit1, *vit2, *bit, min_bulge,
                                   Insert_point_iterator(points, vit2));
    vit1 = vit2;
  }

  if (! closed) return;

  SGAL::approximate_circular_arc(*vit1, *(points.begin()), *bit, min_bulge,
                                 Insert_point_iterator(points, vit2));
}

DXF_END_NAMESPACE

#endif
