// Copyright (c) 2018 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <boost/shared_ptr.hpp>
#include <boost/dll/alias.hpp>

#include "dxf/basic.hpp"

DXF_BEGIN_NAMESPACE

class Dxf_option_parser;

class Plugin_dxf : public SGAL::Plugin_api {
public:
  /*! Destruct. */
  virtual ~Plugin_dxf();

  /*! Obtain the name of the plugin.
   */
  virtual std::string name() const;

  /*! Initalize the plugin.
   */
  virtual void init(SGAL::Main_option_parser* option_parser);

  /*! Configure the plugin.
   */
  virtual void configure(SGAL::Scene_graph* scene_graph);

  /*! Indulge the user, that is, apply the the command line specified options.
   */
  virtual void indulge(SGAL::Scene_graph* scene_graph);

  /*! Factory method
   */
  static boost::shared_ptr<Plugin_dxf> create()
  { return boost::shared_ptr<Plugin_dxf>(new Plugin_dxf()); }

private:
  /*! Construct default. */
  Plugin_dxf();

  //! The option parser.
  SGAL::Main_option_parser* m_option_parser;

  //! The dxf option parser.
  Dxf_option_parser* m_dxf_option_parser;
};

BOOST_DLL_ALIAS(dxf::Plugin_dxf::create, // <-- this function is exported with...
                create_plugin)           // <-- ...this alias name

DXF_END_NAMESPACE
