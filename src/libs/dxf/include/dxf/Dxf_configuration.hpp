// Copyright (c) 2016 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef DXF_DXF_CONFIGURATION_HPP
#define DXF_DXF_CONFIGURATION_HPP

#include <string>

#include "SGAL/basic.hpp"
#include "SGAL/Types.hpp"
#include "SGAL/Sub_configuration.hpp"
#include "SGAL/Vector4f.hpp"

#include "dxf/basic.hpp"

SGAL_BEGIN_NAMESPACE

class Container_proto;

SGAL_END_NAMESPACE

DXF_BEGIN_NAMESPACE

class SGAL_SGAL_DECL Dxf_configuration : public SGAL::Sub_configuration {
  friend class Dxf_option_parser;

public:

  enum {
    FIRST = SGAL::Sub_configuration::LAST - 1,
    PALETTE_FILE_NAME,
    BACKGROUND_COLOR,
    MIN_BULGE,
    REFINMENT_ARCS_NUM,
    STORE_DATA,
    VERSION,
    TRANSPARENCY,
    TEST_DEPTH,
    LAST
  };

  /*! Construct. */
  Dxf_configuration(SGAL::Boolean proto = false);

  /*! Destruct. */
  virtual ~Dxf_configuration() {}

  /*! Construct the prototype. */
  static Dxf_configuration* prototype();

  /*! Create a new container of this type (virtual copy constructor).
   * \return a new container of this type.
   */
  virtual SGAL::Container* create();

  /*! Initialize the node prototype. */
  virtual void init_prototype();

  /*! Delete the node prototype. */
  virtual void delete_prototype();

  /*! Obtain the node prototype. */
  virtual SGAL::Container_proto* get_prototype();

  /// \name field handlers
  //@{
  SGAL::String* palette_file_name_handle(const SGAL::Field_info*)
  { return &m_palette_file_name; }
  SGAL::Vector4f* background_color_handle(const SGAL::Field_info*)
  { return &m_background_color; }
  SGAL::Float* min_bulge_handle(const SGAL::Field_info*)
  { return &m_min_bulge; }
  SGAL::Uint* refinement_arcs_num_handle(const SGAL::Field_info*)
  { return &m_refinement_arcs_num; }
  SGAL::Boolean* store_data_handle(const SGAL::Field_info*)
  { return &m_store_data; }
  SGAL::Uint* version_handle(const SGAL::Field_info*) { return &m_version; }
  SGAL::Float* transparency_handle(const SGAL::Field_info*)
  { return &m_transparency; }
  SGAL::Boolean* test_depth_handle(const SGAL::Field_info*)
  { return &m_test_depth; }
  //@}

  /*! Set the attributes of this node. */
  virtual void set_attributes(SGAL::Element* elem);

  /*! Set the palette filename.
   * \param[in] filename the palette filename.
   */
  void set_palette_file_name(const SGAL::String& filename);

  /*! Obtain the palette filename.
   * \return the palette filename.
   */
  const SGAL::String& get_palette_file_name() const;

  /*! Set the background color.
   */
  void set_background_color(const SGAL::Vector4f& color);

  /*! Obatin the background color.
   */
  const SGAL::Vector4f& get_background_color() const;

  /*! Set the min-bulge value.
   */
  void set_min_bulge(SGAL::Float min_bulge);

  /*! Obatin the min-bulge value.
   */
  SGAL::Float get_min_bulge() const;

  /*! Set the efinement arcs number.
   */
  void set_refinement_arcs_num(SGAL::Uint num);

  /*! Obatin the efinement arcs number.
   */
  SGAL::Uint get_refinement_arcs_num() const;

  /*! Set the flag that indicates whether to store the dxf data.
   */
  void set_store_data(SGAL::Boolean flag);

  /*! Obatin the flag that indicates whether to store the dxf data.
   */
  SGAL::Boolean get_store_data() const;

  /*! Set the DXF version number.
   */
  void set_version(SGAL::Uint version);

  /*! Obatin the DXF version number.
   */
  SGAL::Uint get_version() const;

  /*! Set the DXF transparency value.
   */
  void set_transparency(SGAL::Float transparency);

  /*! Obatin the DXF transparency value.
   */
  SGAL::Float get_transparency() const;

  /*! Set the flag that indicates whether to enable depth testing.
   */
  void set_test_depth(SGAL::Boolean flag);

  /*! Obatin the flag that indicates whether to enable depth testing.
   */
  SGAL::Boolean get_test_depth() const;

  /*! Reset to factory settings. */
  void reset();

  /*! Enable the sub-configuration---re-route all fields. */
  virtual void enable();

protected:
  /*! Obtain the tag (type) of the container. */
  virtual const std::string& get_tag() const;

private:
  //! The tag that identifies this container type.
  static const std::string s_tag;

  //! The node prototype.
  static SGAL::Container_proto* s_prototype;

  //! The palette file-name.
  SGAL::String m_palette_file_name;

  //! The default background color.
  SGAL::Vector4f m_background_color;

  //! The minimum bulge value.
  SGAL::Float m_min_bulge;

  //! The number of arcs used to represent a circle or an ellipsoid.
  SGAL::Uint m_refinement_arcs_num;

  //! Indicates whether to store the dxf data for later use.
  SGAL::Boolean m_store_data;

  //! The DXF version number.
  SGAL::Uint m_version;

  //! The DXF transparency value.
  SGAL::Float m_transparency;

  //! Indicates whether depth testing is enabled or not.
  SGAL::Boolean m_test_depth;

  // default values
  static const SGAL::String s_def_palette_file_name;
  static const SGAL::Vector4f s_def_background_color;
  static const SGAL::Float s_def_min_bulge;
  static const SGAL::Uint s_def_refinement_arcs_num;
  static const SGAL::Boolean s_def_store_data;
  static const SGAL::Uint s_def_version;
  static const SGAL::Float s_def_transparency;
  static const SGAL::Boolean s_def_test_depth;
};

//! \brief constructs the prototype.
inline Dxf_configuration* Dxf_configuration::prototype()
{ return new Dxf_configuration(true); }

//! \brief obtains the tag (type) of the container.
inline const std::string& Dxf_configuration::get_tag() const { return s_tag; }

//! \brief sets the palette file-name.
inline void
Dxf_configuration::set_palette_file_name(const SGAL::String& filename)
{ m_palette_file_name = filename; }

//! \brief obtains the palette file-name.
inline const SGAL::String& Dxf_configuration::get_palette_file_name() const
{ return m_palette_file_name; }

//! \brief sets the background color.
inline void Dxf_configuration::set_background_color(const SGAL::Vector4f& color)
{ m_background_color = color; }

//! \brief gets the background color.
inline const SGAL::Vector4f& Dxf_configuration::get_background_color() const
{ return m_background_color; }

//! \brief sets the min-bulge value.
inline void Dxf_configuration::set_min_bulge(SGAL::Float min_bulge)
{ m_min_bulge = min_bulge; }

//! \brief obtains the min-bulge value.
inline SGAL::Float Dxf_configuration::get_min_bulge() const
{ return m_min_bulge; }

//! \brief sets the efinement arcs number.
inline void Dxf_configuration::set_refinement_arcs_num(SGAL::Uint num)
{ m_refinement_arcs_num = num; }

//! \brief obtains the efinement arcs number.
inline SGAL::Uint Dxf_configuration::get_refinement_arcs_num() const
{ return m_refinement_arcs_num; }

//! \brief sets the flag that indicates whether to store the dxf data.
inline void Dxf_configuration::set_store_data(SGAL::Boolean flag)
{ m_store_data = flag; }

//! \brief obatins the flag that indicates whether to store the dxf data.
inline SGAL::Boolean Dxf_configuration::get_store_data() const
{ return m_store_data; }

//! \brief sets the DXF version number.
inline void Dxf_configuration::set_version(SGAL::Uint version)
{ m_version = version; }

//! \brief obatins the DXF version number.
inline SGAL::Uint Dxf_configuration::get_version() const { return m_version; }

//! \brief sets the DXF transparency value.
inline void Dxf_configuration::set_transparency(SGAL::Float transparency)
{ m_transparency = transparency; }

//! \brief obatins the DXF transparency value.
inline SGAL::Float Dxf_configuration::get_transparency() const
{ return m_transparency; }

//! \brief sets the flag that indicates whether to enable depth testing.
inline void Dxf_configuration::set_test_depth(SGAL::Boolean flag)
{ m_test_depth = flag; }

//! \brief obatins the flag that indicates whether to enable depth testing.
inline SGAL::Boolean Dxf_configuration::get_test_depth() const
{ return m_test_depth; }

SGAL_END_NAMESPACE

#endif
