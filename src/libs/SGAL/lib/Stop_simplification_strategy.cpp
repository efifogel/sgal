// Copyright (c) 2020 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Author(s): Efi Fogel        <efifogel@gmail.com>

#include <cstring>

#include "SGAL/basic.hpp"
#include "SGAL/Stop_simplification_strategy.hpp"

SGAL_BEGIN_NAMESPACE

//! \brief the names of the stop_simplification_strategy options.
static const char* s_stop_simplification_strategy_names[] = {
  "count",
  "countRatio",
  "edgeLength"
};

//! \brief obtains the name of a stop_simplification_strategy given by its code.
const char* get_stop_simplification_strategy_name(size_t id)
{ return s_stop_simplification_strategy_names[id]; }

//! \brief finds the code of a stop_simplification_strategy given by its name.
size_t find_stop_simplification_strategy_code(const char* name)
{
  auto size = sizeof(s_stop_simplification_strategy_names) / sizeof(char*);
  for (size_t i = 0; i < size; ++i) {
    if (strcmp(s_stop_simplification_strategy_names[i], name) == 0) return i;
  }
  return static_cast<size_t>(Stop_simplification_strategy::COUNT_RATIO);
}

SGAL_END_NAMESPACE
