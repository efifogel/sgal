// Copyright (c) 2014 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include "SGAL/basic.hpp"
#include "SGAL/Types.hpp"
#include "SGAL/Bounding_box_2d.hpp"

SGAL_BEGIN_NAMESPACE

//! \brief obtains a bounding box of the current bounding box and a given one.
Bounding_box_2d Bounding_box_2d::operator+(const Bounding_box_2d& bb) const
{
  Bounding_box_2d bbox(std::min(xmin(), bb.xmin()),
                       std::min(ymin(), bb.ymin()),
                       std::max(xmax(), bb.xmax()),
                       std::max(ymax(), bb.ymax()));
  return bbox;
}

//! \brief updates the bounding box with a given one.
Bounding_box_2d& Bounding_box_2d::operator+=(const Bounding_box_2d& bb)
{
  if (bb.xmin() < xmin()) m_x_min = bb.xmin();
  if (bb.ymin() < ymin()) m_y_min = bb.ymin();
  if (bb.xmax() > xmax()) m_x_max = bb.xmax();
  if (bb.ymax() > ymax()) m_y_max = bb.ymax();
  return *this;
}

//! \brief obtains the dimensions in order of their length.
boost::tuple<Uint, Uint> Bounding_box_2d::get_longest_dimensions() const
{
  auto x_range = xmax() - xmin();
  auto y_range = ymax() - ymin();
  Uint i(0), j(1), k(2);
  if (x_range >= y_range) return boost::make_tuple(0, 1);
  return boost::make_tuple(1, 0);
}

/*! Insert a point.
 */
void Bounding_box_2d::insert(float x, float y)
{
  if (x < m_x_min) m_x_min = x;
  if (x > m_x_max) m_x_max = x;
  if (y < m_y_min) m_y_min = y;
  if (y > m_y_max) m_y_max = y;
}

//! \brief expands the bounding box by a scale factor in all dimensions.
void Bounding_box_2d::expand_scale(float factor)
{
  auto dx = 0.5f * (m_x_max - m_x_min) * factor;
  auto dy = 0.5f * (m_y_max - m_y_min) * factor;
  m_x_min -= dx;
  m_y_min -= dy;
  m_x_max += dx;
  m_y_max += dy;
}

//! \brief determines whether a given bounding box is approximately the same.
bool Bounding_box_2d::is_approx_same(const Bounding_box_2d& other,
                                     float epsilon) const
{
  if ((std::abs(m_x_min - other.m_x_min) > epsilon) ||
      (std::abs(m_y_min - other.m_y_min) > epsilon) ||
      (std::abs(m_x_max - other.m_x_max) > epsilon) ||
      (std::abs(m_y_max - other.m_y_max) > epsilon))
    return false;
  return true;
}

SGAL_END_NAMESPACE
