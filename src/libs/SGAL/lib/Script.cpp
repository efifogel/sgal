// Copyright (c) 2014 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <algorithm>
#include <iterator>
#include <string>
#include <regex>

#include <boost/lexical_cast.hpp>

#include <v8.h>

#include "SGAL/basic.hpp"
#include "SGAL/Element.hpp"
#include "SGAL/Container_proto.hpp"
#include "SGAL/Container_factory.hpp"
#include "SGAL/Element.hpp"
#include "SGAL/Script.hpp"
#include "SGAL/Field_rule.hpp"
#include "SGAL/Field_info.hpp"
#include "SGAL/Field_infos.hpp"
#include "SGAL/Scene_graph.hpp"
#include "SGAL/Field.hpp"
#include "SGAL/Tracer.hpp"
#include "SGAL/Vrml_formatter.hpp"
#include "SGAL/Utilities.hpp"
#include "SGAL/multi_istream_iterator.hpp"
#include "SGAL/io_vector2f.hpp"
#include "SGAL/io_vector3f.hpp"
#include "SGAL/io_rotation.hpp"
#include "SGAL/to_boolean.hpp"
#include "SGAL/Scene_graph.hpp"

SGAL_BEGIN_NAMESPACE

const std::string Script::s_tag = "Script";
std::vector<Container_proto*> Script::s_prototypes;

REGISTER_TO_FACTORY(Script, "Script");

#define CHECK(maybe) if ((maybe).IsNothing()) SGAL_error();

//! \brief construct from prototype.
Script::Script(Boolean proto) :
  Script_base(proto),
  m_isolate(nullptr),
  m_scene_graph(nullptr)
{
  if (proto) return;
  auto id = s_prototypes.size();
  m_id = id;
  s_prototypes.resize(id+1);
  s_prototypes[id] = nullptr;
}

//! \brief destructor.
Script::~Script() {
  m_context.Reset();
  while (! m_assigned.empty()) m_assigned.pop();
  for (auto* sc : m_persistent_containers) delete sc;
  m_persistent_containers.clear();
}

//! \brief initializes the container prototype.
void Script::init_prototype() {
  if (s_prototypes[m_id]) return;
  s_prototypes[m_id] = new Container_proto(Script_base::get_prototype());
}

//! \brief deletes the container prototype.
void Script::delete_prototype() {
  if (!s_prototypes[m_id]) return;
  delete s_prototypes[m_id];
  s_prototypes[m_id] = nullptr;
}

//! \brief obtains the container prototype.
Container_proto* Script::get_prototype() {
  if (!s_prototypes[m_id]) Script::init_prototype();
  return s_prototypes[m_id];
}

//! \brief sets the attributes of the object extracted from the input file.
void Script::set_attributes(Element* elem) {
  Script_base::set_attributes(elem);
  for (auto fi = elem->field_attrs_begin(); fi != elem->field_attrs_end(); ++fi)
  {
    auto rule = elem->get_rule(fi);
    auto type = elem->get_type(fi);
    const auto& name = elem->get_name(fi);
    const auto& value = elem->get_value(fi);
    add_field_info(rule, type, name, value);
  }

  // Remove all the marked attributes:
  elem->delete_marked();
}

//! \brief intercepts getting scalars.
void Script::getter(v8::Local<v8::String> property,
                    const v8::PropertyCallbackInfo<v8::Value>& info) {
  auto isolate = info.GetIsolate();

#if !defined(NDEBUG) || defined(SGAL_TRACE)
  if (SGAL::TRACE(Tracer::SCRIPT)) {
    v8::String::Utf8Value utf8_property(isolate, property);
    std::cout << "getter() " << *utf8_property << std::endl;
  }
#endif

  // Obtain the Script node:
  v8::Handle<v8::Object> obj = info.Holder();
  SGAL_assertion(obj->InternalFieldCount() == 1);
  v8::Handle<v8::External> internal_field =
    v8::Handle<v8::External>::Cast(obj->GetInternalField(0));
  SGAL_assertion(internal_field->Value() != nullptr);
  auto* script_node = static_cast<Script*>(internal_field->Value());
  SGAL_TRACE_MSG(Tracer::SCRIPT, "  script: " + script_node->get_name() + "\n");

  // Obtain the field information record:
  v8::Local<v8::Value> data = info.Data();
  v8::Handle<v8::External> ext = v8::Handle<v8::External>::Cast(data);
  SGAL_assertion(ext->Value() != nullptr);
  auto* field_info = static_cast<Field_info*>(ext->Value());
  SGAL_TRACE_MSG(Tracer::SCRIPT, "  field: " + field_info->get_name() + "\n");

  auto type_id = field_info->get_type_id();
  auto id = field_info->get_id();
  switch (type_id) {
   case Field_type::SF_BOOL:
    {
     Boolean* tmp =
       (id == DIRECT_OUTPUT) ? script_node->direct_output_handle(field_info) :
       (id == MUST_EVALUATE) ? script_node->must_evaluate_handle(field_info) :
       script_node->field_handle<Boolean>(field_info);
     info.GetReturnValue().Set(*tmp);
     SGAL_TRACE_MSG(Tracer::SCRIPT, "  value: " + std::to_string(*tmp) + "\n");
    }
    break;

   case Field_type::SF_FLOAT:
    {
     auto* tmp = script_node->field_handle<Float>(field_info);
     info.GetReturnValue().Set(*tmp);
     SGAL_TRACE_MSG(Tracer::SCRIPT, "  value: " + std::to_string(*tmp) + "\n");
    }
    break;

   case Field_type::SF_TIME:
    {
     auto* tmp = script_node->field_handle<Scene_time>(field_info);
     info.GetReturnValue().Set(*tmp);
     SGAL_TRACE_MSG(Tracer::SCRIPT, "  value: " + std::to_string(*tmp) + "\n");
    }
    break;

   case Field_type::SF_INT32:
    {
     auto* tmp = script_node->field_handle<Int32>(field_info);
     info.GetReturnValue().Set(*tmp);
     SGAL_TRACE_MSG(Tracer::SCRIPT, "  value: " + std::to_string(*tmp) + "\n");
    }
    break;

   case Field_type::SF_STR:
    {
     auto* tmp = script_node->field_handle<std::string>(field_info);
     v8::Handle<v8::String> str =
       v8::String::NewFromUtf8(isolate, tmp->c_str());
     info.GetReturnValue().Set(str);
     SGAL_TRACE_MSG(Tracer::SCRIPT, "  value: " + *tmp + "\n");
    }
    break;

   case Field_type::SF_SHARED_CONTAINER:
    {
     // Set (the GetReturnValue) a pointer to the Shared_container object.
     auto* tmp = script_node->field_handle<Shared_container>(field_info);
     info.GetReturnValue().Set(v8::External::New(isolate, tmp));
    }
    break;

   case Field_type::SF_VEC2F:
   case Field_type::SF_VEC3F:
   case Field_type::SF_COLOR:
   case Field_type::SF_ROTATION:
   case Field_type::SF_IMAGE:
   case Field_type::MF_BOOL:
   case Field_type::MF_FLOAT:
   case Field_type::MF_TIME:
   case Field_type::MF_INT32:
   case Field_type::MF_VEC2F:
   case Field_type::MF_VEC3F:
   case Field_type::MF_COLOR:
   case Field_type::MF_STR:
   case Field_type::MF_ROTATION:
   case Field_type::MF_SHARED_CONTAINER:
   default:
    SGAL_error();
    return;
  }
}

//! \brief intercepts setting scalars.
void Script::setter(v8::Local<v8::String> property,
                    v8::Local<v8::Value> value,
                    const v8::PropertyCallbackInfo<void>& info) {
  auto isolate = info.GetIsolate();

#if !defined(NDEBUG) || defined(SGAL_TRACE)
  if (SGAL::TRACE(Tracer::SCRIPT)) {
    v8::String::Utf8Value utf8_property(isolate, property);
    std::cout << "setter() " << *utf8_property << std::endl;
  }
#endif

  // Obtain the Script node:
  v8::Handle<v8::Object> obj = info.Holder();
  SGAL_assertion(obj->InternalFieldCount() == 1);
  v8::Handle<v8::External> internal_field =
    v8::Handle<v8::External>::Cast(obj->GetInternalField(0));
  SGAL_assertion(internal_field->Value() != nullptr);
  auto* script_node = static_cast<Script*>(internal_field->Value());
  SGAL_TRACE_MSG(Tracer::SCRIPT, "  script: " + script_node->get_name() + "\n");

  // Obtain the field information record:
  v8::Local<v8::Value> data = info.Data();
  v8::Handle<v8::External> ext = v8::Handle<v8::External>::Cast(data);
  SGAL_assertion(ext->Value() != nullptr);
  auto* field_info = static_cast<Field_info*>(ext->Value());
  SGAL_TRACE_MSG(Tracer::SCRIPT, "  field: " + field_info->get_name() + "\n");

  // Apply the setting
  v8::Local<v8::Context> context(isolate->GetCurrentContext());
  auto type_id = field_info->get_type_id();
  auto id = field_info->get_id();
  switch (type_id) {
   case Field_type::SF_BOOL:
    {
     Boolean* tmp =
       (id == DIRECT_OUTPUT) ? script_node->direct_output_handle(field_info) :
       (id == MUST_EVALUATE) ? script_node->must_evaluate_handle(field_info) :
       script_node->field_handle<Boolean>(field_info);
     *tmp = value->BooleanValue();
     SGAL_TRACE_MSG(Tracer::SCRIPT, "  value: " + std::to_string(*tmp) + "\n");
    }
    break;

   case Field_type::SF_FLOAT:
    {
     auto* tmp = script_node->field_handle<Float>(field_info);
     *tmp = static_cast<Float>(value->NumberValue(context).FromJust());
     SGAL_TRACE_MSG(Tracer::SCRIPT, "  value: " + std::to_string(*tmp) + "\n");
    }
    break;

   case Field_type::SF_TIME:
    {
     auto* tmp = script_node->field_handle<Scene_time>(field_info);
     *tmp = static_cast<Scene_time>(value->NumberValue(context).FromJust());
     SGAL_TRACE_MSG(Tracer::SCRIPT, "  value: " + std::to_string(*tmp) + "\n");
    }
    break;

   case Field_type::SF_INT32:
    {
     auto* tmp = script_node->field_handle<Int32>(field_info);
     *tmp = value->Int32Value(context).FromJust();
     SGAL_TRACE_MSG(Tracer::SCRIPT, "  value: " + std::to_string(*tmp) + "\n");
    }
    break;

   case Field_type::SF_STR:
    {
     auto* tmp = script_node->field_handle<std::string>(field_info);
     v8::Local<v8::String> str = value->ToString(context).ToLocalChecked();
     v8::String::Utf8Value utf8(isolate, str);
     tmp->assign(*utf8);
     SGAL_TRACE_MSG(Tracer::SCRIPT, "  value: " + *tmp + "\n");
    }
    break;

   case Field_type::SF_SHARED_CONTAINER:
    {
     // The value is an V8::External that holds a void*, which actually points
     // at a Shared_container.
     auto external(v8::External::Cast(*value));
     auto* tmp = script_node->field_handle<Shared_container>(field_info);
     *tmp = *((Shared_container*)(external->Value()));
    }
    break;

   case Field_type::SF_VEC2F:
   case Field_type::SF_VEC3F:
   case Field_type::SF_COLOR:
   case Field_type::SF_ROTATION:
   case Field_type::SF_IMAGE:
   case Field_type::MF_BOOL:
   case Field_type::MF_FLOAT:
   case Field_type::MF_TIME:
   case Field_type::MF_INT32:
   case Field_type::MF_VEC2F:
   case Field_type::MF_VEC3F:
   case Field_type::MF_COLOR:
   case Field_type::MF_STR:
   case Field_type::MF_ROTATION:
   case Field_type::MF_SHARED_CONTAINER:
   default:
    SGAL_error();
    return;
  }

  if (script_node->is_direct_output()) {
    Field* field = script_node->get_field(field_info);
    if (field) field->cascade();
  }
  else script_node->insert_assigned(field_info);
}

/*! Obtain an External object that holds an array.
 */
template <typename ArrayType>
v8::Handle<v8::External> get_external(v8::Isolate* isolate, Script* script_node,
                                      Field_info* field_info) {
  auto* tmp = script_node->field_handle<ArrayType>(field_info);
  return v8::External::New(isolate, tmp);
}

//! \brief intercepts getting arrays.
void Script::array_getter(v8::Local<v8::String> property,
                          const v8::PropertyCallbackInfo<v8::Value>& info) {
  auto isolate = info.GetIsolate();

#if !defined(NDEBUG) || defined(SGAL_TRACE)
  if (SGAL::TRACE(Tracer::SCRIPT)) {
    v8::String::Utf8Value utf8_property(isolate, property);
    std::cout << "array_getter() " << *utf8_property << std::endl;
  }
#endif

  v8::HandleScope handle_scope(isolate);

  // Obtain the Script node:
  v8::Handle<v8::Object> obj = info.Holder();
  SGAL_assertion(obj->InternalFieldCount() == 1);
  v8::Handle<v8::External> internal_field =
    v8::Handle<v8::External>::Cast(obj->GetInternalField(0));
  SGAL_assertion(internal_field->Value() != nullptr);
  auto* script_node = static_cast<Script*>(internal_field->Value());
  SGAL_TRACE_MSG(Tracer::SCRIPT, "  script: " + script_node->get_name() + "\n");

  // Obtain the field information record:
  v8::Local<v8::Value> data = info.Data();
  v8::Handle<v8::External> ext = v8::Handle<v8::External>::Cast(data);
  SGAL_assertion(ext->Value() != nullptr);
  auto* field_info = static_cast<Field_info*>(ext->Value());
  SGAL_TRACE_MSG(Tracer::SCRIPT, "  field: " + field_info->get_name() + "\n");

  // Prepare for indexing and interception
  v8::Handle<v8::ObjectTemplate> tmpl = v8::ObjectTemplate::New(isolate);
  tmpl->SetInternalFieldCount(2);
  tmpl->SetHandler(v8::IndexedPropertyHandlerConfiguration(indexed_getter,
                                                           indexed_setter,
                                                           0, 0, 0, ext));
  tmpl->SetHandler(v8::NamedPropertyHandlerConfiguration(property_getter,
                                                         property_setter,
                                                         0, 0, 0, ext));

  v8::Local<v8::Context> context(isolate->GetCurrentContext());
  auto inst = tmpl->NewInstance(context).ToLocalChecked();
  inst->SetInternalField(0, v8::External::New(isolate, script_node));
  info.GetReturnValue().Set(inst);

  // Add the field information record:
  switch (field_info->get_type_id()) {
   case Field_type::SF_BOOL:
   case Field_type::SF_FLOAT:
   case Field_type::SF_TIME:
   case Field_type::SF_INT32:
   case Field_type::SF_STR:
    SGAL_error();
    break;

   case Field_type::SF_VEC2F:
    inst->SetInternalField(1, get_external<Vector2f>(isolate, script_node,
                                                     field_info));
    break;

   case Field_type::SF_VEC3F:
   case Field_type::SF_COLOR:
    inst->SetInternalField(1, get_external<Vector3f>(isolate, script_node,
                                                     field_info));
    break;

   case Field_type::SF_ROTATION:
    inst->SetInternalField(1, get_external<Rotation>(isolate, script_node,
                                                     field_info));
    break;

   case Field_type::SF_IMAGE:
    std::cerr << "Not supported yet!" << std::endl;
    break;

   case Field_type::SF_SHARED_CONTAINER:
    SGAL_error();
    break;

   case Field_type::MF_BOOL:
    inst->SetInternalField(1, get_external<Boolean_array>(isolate, script_node,
                                                          field_info));
    break;

   case Field_type::MF_FLOAT:
    inst->SetInternalField(1, get_external<Float_array>(isolate, script_node,
                                                        field_info));
    break;

   case Field_type::MF_TIME:
    inst->SetInternalField(1, get_external<Scene_time_array>(isolate,
                                                             script_node,
                                                             field_info));
    break;

   case Field_type::MF_INT32:
    inst->SetInternalField(1, get_external<Int32_array>(isolate, script_node,
                                                        field_info));
    break;

   case Field_type::MF_VEC2F:
    inst->SetInternalField(1, get_external<Vector2f_array>(isolate, script_node,
                                                           field_info));
    break;

   case Field_type::MF_VEC3F:
   case Field_type::MF_COLOR:
    inst->SetInternalField(1, get_external<Vector3f_array>(isolate, script_node,
                                                           field_info));
    break;

   case Field_type::MF_STR:
    inst->SetInternalField(1, get_external<String_array>(isolate, script_node,
                                                         field_info));
    break;

   case Field_type::MF_ROTATION:
    inst->SetInternalField(1, get_external<Rotation_array>(isolate, script_node,
                                                           field_info));
    break;

   case Field_type::MF_SHARED_CONTAINER:
    inst->SetInternalField(1, get_external<Shared_container_array>(isolate,
                                                                   script_node,
                                                                   field_info));
    break;

   default:
    std::cerr << "Unsupported type!" << std::endl;
    return;
  }
}

/*! Copy an array.
 */
template <typename Type_>
void my_copy(v8::Local<v8::Value> value, v8::Local<v8::Context> context,
             Type_* target) {
  typedef Type_         Type;
  auto obj = value->ToObject(context).ToLocalChecked();
  v8::Handle<v8::External> internal_field =
    v8::Handle<v8::External>::Cast(obj->GetInternalField(1));
  auto* src = static_cast<Type*>(internal_field->Value());
  *target = *src;
}

//! \brief intercepts setting arrays.
void Script::array_setter(v8::Local<v8::String> property,
                          v8::Local<v8::Value> value,
                          const v8::PropertyCallbackInfo<void>& info) {
  auto isolate = info.GetIsolate();

#if !defined(NDEBUG) || defined(SGAL_TRACE)
  if (SGAL::TRACE(Tracer::SCRIPT)) {
    v8::String::Utf8Value utf8_property(isolate, property);
    std::cout << "array_setter() " << *utf8_property << std::endl;
  }
#endif

  // Obtain the Script node:
  v8::Handle<v8::Object> obj = info.Holder();
  SGAL_assertion(obj->InternalFieldCount() == 1);
  v8::Handle<v8::External> internal_field =
    v8::Handle<v8::External>::Cast(obj->GetInternalField(0));
  SGAL_assertion(internal_field->Value() != nullptr);
  auto* script_node = static_cast<Script*>(internal_field->Value());
  SGAL_TRACE_MSG(Tracer::SCRIPT, "  script: " + script_node->get_name() + "\n");

  // Obtain the field handle and apply the setting:
  v8::Local<v8::Value> data = info.Data();
  v8::Handle<v8::External> ext = v8::Handle<v8::External>::Cast(data);
  SGAL_assertion(ext->Value() != nullptr);
  auto* field_info = static_cast<Field_info*>(ext->Value());
  SGAL_TRACE_MSG(Tracer::SCRIPT, "  field: " + field_info->get_name() + "\n");

  v8::Local<v8::Context> context(isolate->GetCurrentContext());
  v8::HandleScope scope(isolate);
  switch (field_info->get_type_id()) {
   case Field_type::SF_BOOL:
   case Field_type::SF_FLOAT:
   case Field_type::SF_TIME:
   case Field_type::SF_INT32:
   case Field_type::SF_STR:
    SGAL_error();
    break;

   case Field_type::SF_VEC2F:
    if (value->IsArray()) {
      auto* target = script_node->field_handle<Vector2f>(field_info);
      v8::Local<v8::Array> array = v8::Handle<v8::Array>::Cast(value);
      target->set(static_cast<Float>(array->Get(context, 0).ToLocalChecked()->NumberValue(context).FromJust()),
                  static_cast<Float>(array->Get(context, 1).ToLocalChecked()->NumberValue(context).FromJust()));
      SGAL_assertion(! std::isnan((*target)[0]));
      SGAL_assertion(! std::isnan((*target)[1]));
      break;
    }
    my_copy(value, context, script_node->field_handle<Vector2f>(field_info));
    break;

   case Field_type::SF_VEC3F:
   case Field_type::SF_COLOR:
    if (value->IsArray()) {
      auto* target = script_node->field_handle<Vector3f>(field_info);
      v8::Local<v8::Array> array = v8::Handle<v8::Array>::Cast(value);
      target->set(static_cast<Float>(array->Get(context, 0).ToLocalChecked()->NumberValue(context).FromJust()),
                  static_cast<Float>(array->Get(context, 1).ToLocalChecked()->NumberValue(context).FromJust()),
                  static_cast<Float>(array->Get(context, 2).ToLocalChecked()->NumberValue(context).FromJust()));
      SGAL_assertion(! std::isnan((*target)[0]));
      SGAL_assertion(! std::isnan((*target)[1]));
      SGAL_assertion(! std::isnan((*target)[2]));
      // std::cout << "  value: " << *target << std::endl;
      break;
    }
    my_copy(value, context, script_node->field_handle<Vector3f>(field_info));
    break;

   case Field_type::SF_ROTATION:
    if (value->IsArray()) {
      break;
    }
    break;

   case Field_type::SF_IMAGE:
    std::cerr << "Not supported yet!" << std::endl;
    break;

   case Field_type::SF_SHARED_CONTAINER:
    SGAL_error();
    break;

   case Field_type::MF_BOOL:
    if (value->IsArray()) {
      auto* target = script_node->field_handle<Boolean_array>(field_info);
      v8::Local<v8::Array> array = v8::Handle<v8::Array>::Cast(value);
      target->resize(array->Length());
      for (size_t i = 0; i < array->Length(); ++i)
        (*target)[i] = array->Get(context, i).ToLocalChecked()->BooleanValue();
      // std::cout << "  ";
      // std::copy(target->begin(), target->end(),
      //           std::ostream_iterator<bool>(std::cout, " "));
      // std::cout << std::endl;
      break;
    }
    my_copy(value, context,
            script_node->field_handle<Boolean_array>(field_info));
    break;

   case Field_type::MF_FLOAT:
    if (value->IsArray()) {
      auto* target = script_node->field_handle<Float_array>(field_info);
      v8::Local<v8::Array> array = v8::Handle<v8::Array>::Cast(value);
      target->resize(array->Length());
      for (size_t i = 0; i < array->Length(); ++i)
        (*target)[i] = static_cast<Float>(array->Get(context, i).ToLocalChecked()->NumberValue(context).FromJust());
      // std::cout << "  ";
      // std::copy(target->begin(), target->end(),
      //           std::ostream_iterator<bool>(std::cout, " "));
      // std::cout << std::endl;
      break;
    }
    my_copy(value, context, script_node->field_handle<Float_array>(field_info));
    break;

   case Field_type::MF_TIME:
    if (value->IsArray()) {
      auto* target = script_node->field_handle<Scene_time_array>(field_info);
      v8::Local<v8::Array> array = v8::Handle<v8::Array>::Cast(value);
      target->resize(array->Length());
      for (size_t i = 0; i < array->Length(); ++i)
        (*target)[i] = static_cast<Scene_time>(array->Get(context, i).ToLocalChecked()->NumberValue(context).FromJust());
      break;
    }
    my_copy(value, context,
            script_node->field_handle<Scene_time_array>(field_info));
    break;

   case Field_type::MF_INT32:
    if (value->IsArray()) {
      auto* target = script_node->field_handle<Int32_array>(field_info);
      v8::Local<v8::Array> array = v8::Handle<v8::Array>::Cast(value);
      target->resize(array->Length());
      for (size_t i = 0; i < array->Length(); ++i)
        (*target)[i] = array->Get(context, i).ToLocalChecked()->Int32Value(context).FromJust();
      break;
    }
    my_copy(value, context, script_node->field_handle<Int32_array>(field_info));
    break;

   case Field_type::MF_VEC2F:
    if (value->IsArray()) {
      auto* target = script_node->field_handle<Vector2f_array>(field_info);
      v8::Local<v8::Array> array = v8::Handle<v8::Array>::Cast(value);
      target->resize(array->Length());
      for (size_t i = 0; i < array->Length(); ++i) {
        v8::Local<v8::Array> vec2f =
          v8::Handle<v8::Array>::Cast(array->Get(context, i).ToLocalChecked());
        ((*target)[i])[0] = static_cast<Float>(vec2f->Get(context, 0).ToLocalChecked()->NumberValue(context).FromJust());
        ((*target)[i])[1] = static_cast<Float>(vec2f->Get(context, 1).ToLocalChecked()->NumberValue(context).FromJust());
      }
      // std::cout << "  ";
      // std::copy(target->begin(), target->end(),
      //           std::ostream_iterator<Vector2f>(std::cout, " "));
      // std::cout << std::endl;
      break;
    }
    my_copy(value, context,
            script_node->field_handle<Vector2f_array>(field_info));
    break;

   case Field_type::MF_VEC3F:
   case Field_type::MF_COLOR:
    if (value->IsArray()) {
      auto* target = script_node->field_handle<Vector3f_array>(field_info);
      v8::Local<v8::Array> array = v8::Handle<v8::Array>::Cast(value);
      target->resize(array->Length());
      for (size_t i = 0; i < array->Length(); ++i) {
        v8::Local<v8::Array> vec3f =
          v8::Handle<v8::Array>::Cast(array->Get(context, i).ToLocalChecked());
        ((*target)[i])[0] = static_cast<Float>(vec3f->Get(context, 0).ToLocalChecked()->NumberValue(context).FromJust());
        ((*target)[i])[1] = static_cast<Float>(vec3f->Get(context, 1).ToLocalChecked()->NumberValue(context).FromJust());
        ((*target)[i])[2] = static_cast<Float>(vec3f->Get(context, 2).ToLocalChecked()->NumberValue(context).FromJust());
      }
      // std::cout << "  ";
      // std::copy(target->begin(), target->end(),
      //           std::ostream_iterator<Vector3f>(std::cout, " "));
      // std::cout << std::endl;
      break;
    }
    my_copy(value, context,
            script_node->field_handle<Vector3f_array>(field_info));
    break;

   case Field_type::MF_STR:
    if (value->IsArray()) {
      auto* target = script_node->field_handle<String_array>(field_info);
      v8::Local<v8::Array> array = v8::Handle<v8::Array>::Cast(value);
      target->resize(array->Length());
      for (size_t i = 0; i < array->Length(); ++i) {
        v8::Local<v8::String> str = array->Get(context, i).ToLocalChecked()->ToString(context).ToLocalChecked();
        v8::String::Utf8Value utf8(isolate, str);
        (*target)[i].assign(*utf8);
      }
      break;
    }
    my_copy(value, context,
            script_node->field_handle<String_array>(field_info));
    break;

   case Field_type::MF_ROTATION:
    if (value->IsArray()) {
      std::cerr << "Not supported yet!" << std::endl;
      break;
    }
    my_copy(value, context,
            script_node->field_handle<Rotation_array>(field_info));
    break;

   case Field_type::MF_SHARED_CONTAINER:
    if (value->IsArray()) {
      auto* target =
        script_node->field_handle<Shared_container_array>(field_info);
      v8::Local<v8::Array> array = v8::Handle<v8::Array>::Cast(value);
      target->resize(array->Length());
      // Extract the pointers to the Shared_container objects and set the
      // shared-container array with the corresponing pointed values.
      for (size_t i = 0; i < array->Length(); ++i) {
        auto external(v8::External::Cast(*(array->Get(context, i).ToLocalChecked())));
        (*target)[i] = *((Shared_container*)(external->Value()));
      }
      break;
    }
    my_copy(value, context,
            script_node->field_handle<Shared_container_array>(field_info));
    break;

   default:
    std::cerr << "Unsupported type!" << std::endl;
    return;
  }

  if (script_node->is_direct_output()) {
    auto* field = script_node->get_field(field_info);
    if (field) field->cascade();
  }
  else script_node->insert_assigned(field_info);
}

/*! Obtain the size of an array.
 */
template <typename ArrayType>
Int32 get_size(Script* script_node, Field_info* field_info) {
  auto* tmp = script_node->field_handle<ArrayType>(field_info);
  SGAL_TRACE_MSG(Tracer::SCRIPT,
                 "  value: " + std::to_string(tmp->size()) + "\n");
  return static_cast<Int32>(tmp->size());
}

//! \brief intercepts getting an array property.
void Script::property_getter(v8::Local<v8::Name> name,
                             const v8::PropertyCallbackInfo<v8::Value>& info) {
  auto isolate = info.GetIsolate();

#if !defined(NDEBUG) || defined(SGAL_TRACE)
  if (SGAL::TRACE(Tracer::SCRIPT)) {
    v8::String::Utf8Value utf8_name(isolate, name);
    std::cout << "property_getter() " << *utf8_name << std::endl;
  }
#endif

  // Obtain the Script node:
  v8::Handle<v8::Object> obj = info.Holder();
  SGAL_assertion(obj->InternalFieldCount() == 1);
  v8::Handle<v8::External> internal_field =
    v8::Handle<v8::External>::Cast(obj->GetInternalField(0));
  SGAL_assertion(internal_field->Value() != nullptr);
  auto* script_node = static_cast<Script*>(internal_field->Value());
  SGAL_TRACE_MSG(Tracer::SCRIPT, "  script: " + script_node->get_name() + "\n");

  // Obtain the field information record:
  v8::Local<v8::Value> data = info.Data();
  v8::Handle<v8::External> ext = v8::Handle<v8::External>::Cast(data);
  SGAL_assertion(ext->Value() != nullptr);
  auto* field_info = static_cast<Field_info*>(ext->Value());
  SGAL_TRACE_MSG(Tracer::SCRIPT, "  field: " + field_info->get_name() + "\n");

  // Convert the JavaScript string to a std::string.
  v8::String::Utf8Value utf8_name(isolate, name);
  std::string name_str(*utf8_name);

  if (name_str != "length") {
    std::string msg;
    msg.append("Unknown property \"").append(name_str).append("\"!");
    SGAL_warning_msg(0, msg.c_str());
    return;
  }

  // Obtain
  v8::HandleScope scope(isolate);
  switch (field_info->get_type_id()) {
   case Field_type::SF_BOOL:
   case Field_type::SF_FLOAT:
   case Field_type::SF_TIME:
   case Field_type::SF_INT32:
   case Field_type::SF_STR:
   case Field_type::SF_SHARED_CONTAINER:
    SGAL_error();
    break;

   case Field_type::SF_VEC2F:
    info.GetReturnValue().Set(2);
    break;

   case Field_type::SF_VEC3F:
   case Field_type::SF_COLOR:
    info.GetReturnValue().Set(3);
    break;

   case Field_type::SF_ROTATION:
    info.GetReturnValue().Set(4);
    break;

   case Field_type::SF_IMAGE:
    std::cerr << "Not supported yet!" << std::endl;
    break;

   case Field_type::MF_BOOL:
    info.GetReturnValue().Set(get_size<Boolean_array>(script_node, field_info));
    break;

   case Field_type::MF_FLOAT:
    info.GetReturnValue().Set(get_size<Float_array>(script_node, field_info));
    break;

   case Field_type::MF_TIME:
    info.GetReturnValue().Set(get_size<Scene_time_array>(script_node,
                                                         field_info));
    break;

   case Field_type::MF_INT32:
    info.GetReturnValue().Set(get_size<Int32_array>(script_node, field_info));
    break;

   case Field_type::MF_VEC2F:
    info.GetReturnValue().Set(get_size<Vector2f_array>(script_node, field_info));
    break;

   case Field_type::MF_VEC3F:
   case Field_type::MF_COLOR:
    info.GetReturnValue().Set(get_size<Vector3f_array>(script_node, field_info));
    break;

   case Field_type::MF_STR:
    info.GetReturnValue().Set(get_size<String_array>(script_node, field_info));
    break;

   case Field_type::MF_ROTATION:
    info.GetReturnValue().Set(get_size<Rotation_array>(script_node, field_info));
    break;

   case Field_type::MF_SHARED_CONTAINER:
    info.GetReturnValue().Set(get_size<Shared_container_array>(script_node,
                                                               field_info));
    break;

   default:
    std::cerr << "Unsupported type!" << std::endl;
    return;
  }
}

//! \brief intercepts setting an array property.
void Script::property_setter(v8::Local<v8::Name> name,
                             v8::Local<v8::Value> value,
                             const v8::PropertyCallbackInfo<v8::Value>& info)
{
  auto isolate = info.GetIsolate();

#if !defined(NDEBUG) || defined(SGAL_TRACE)
  if (SGAL::TRACE(Tracer::SCRIPT)) {
    v8::String::Utf8Value utf8_name(isolate, name);
    std::cout << "property_setter() " << *utf8_name << std::endl;
  }
#endif

  // Obtain the Script node:
  v8::Handle<v8::Object> obj = info.Holder();
  SGAL_assertion(obj->InternalFieldCount() == 1);
  v8::Handle<v8::External> internal_field =
    v8::Handle<v8::External>::Cast(obj->GetInternalField(0));
  SGAL_assertion(internal_field->Value() != nullptr);
  auto* script_node = static_cast<Script*>(internal_field->Value());
  SGAL_TRACE_MSG(Tracer::SCRIPT, "  script: " + script_node->get_name() + "\n");

  // Obtain the field information record:
  v8::Local<v8::Value> data = info.Data();
  v8::Handle<v8::External> ext = v8::Handle<v8::External>::Cast(data);
  SGAL_assertion(ext->Value() != nullptr);
  auto* field_info = static_cast<Field_info*>(ext->Value());
  SGAL_TRACE_MSG(Tracer::SCRIPT, "  field: " + field_info->get_name() + "\n");

  SGAL_error_msg("\"property_setter()\" Not supported yet!");
}

//! \brief intercepts getting an array element by index.
void Script::indexed_getter(uint32_t index,
                            const v8::PropertyCallbackInfo<v8::Value>& info) {
  SGAL_TRACE_CODE(Tracer::SCRIPT,
                  std::cout << "indexed_getter() " << index << std::endl;);

  auto isolate = info.GetIsolate();

  // Obtain the Script node:
  v8::Handle<v8::Object> obj = info.Holder();
  SGAL_assertion(obj->InternalFieldCount() >= 1);
  v8::Handle<v8::External> internal_field =
    v8::Handle<v8::External>::Cast(obj->GetInternalField(0));
  SGAL_assertion(internal_field->Value() != nullptr);
  auto* script_node = static_cast<Script*>(internal_field->Value());
  SGAL_TRACE_MSG(Tracer::SCRIPT, "  script: " + script_node->get_name() + "\n");

  // Obtain the field information record:
  v8::Local<v8::Value> data = info.Data();
  v8::Handle<v8::External> ext = v8::Handle<v8::External>::Cast(data);
  SGAL_assertion(ext->Value() != nullptr);
  auto* field_info = static_cast<Field_info*>(ext->Value());
  SGAL_TRACE_MSG(Tracer::SCRIPT, "  field: " + field_info->get_name() + "\n");

  // Obtain
  v8::Local<v8::Context> context(isolate->GetCurrentContext());
  v8::HandleScope scope(isolate);
  switch (field_info->get_type_id()) {
   case Field_type::SF_BOOL:
   case Field_type::SF_FLOAT:
   case Field_type::SF_TIME:
   case Field_type::SF_INT32:
   case Field_type::SF_STR:
    SGAL_error();
    break;

   case Field_type::SF_VEC2F:
    {
     SGAL_assertion(index < 2);
     auto* tmp = script_node->field_handle<Vector2f>(field_info);
     info.GetReturnValue().Set((*tmp)[index]);
    }
    break;

   case Field_type::SF_VEC3F:
   case Field_type::SF_COLOR:
    {
     SGAL_assertion(index < 3);
     auto* tmp = script_node->field_handle<Vector3f>(field_info);
     SGAL_TRACE_MSG(Tracer::SCRIPT,
                    "  value: " + std::to_string((*tmp)[index]) + "\n");
     info.GetReturnValue().Set((*tmp)[index]);
    }
    break;

   case Field_type::SF_ROTATION:
   case Field_type::SF_IMAGE:
    std::cerr << "Not supported yet!" << std::endl;
    break;

   case Field_type::SF_SHARED_CONTAINER:
    SGAL_error();
    break;

   case Field_type::MF_BOOL:
    {
     auto* tmp = script_node->field_handle<Boolean_array>(field_info);
     SGAL_assertion(index < tmp->size());
     info.GetReturnValue().Set((*tmp)[index]);
    }
    break;

   case Field_type::MF_FLOAT:
    {
     auto* tmp = script_node->field_handle<Float_array>(field_info);
     SGAL_assertion(index < tmp->size());
     info.GetReturnValue().Set((*tmp)[index]);
    }
    break;

   case Field_type::MF_TIME:
    {
     auto* tmp = script_node->field_handle<Scene_time_array>(field_info);
     SGAL_assertion(index < tmp->size());
     info.GetReturnValue().Set((*tmp)[index]);
    }
    break;

   case Field_type::MF_INT32:
    {
      auto* tmp = script_node->field_handle<Int32_array>(field_info);
      SGAL_assertion(index < tmp->size());
      info.GetReturnValue().Set((*tmp)[index]);
    }
    break;

   case Field_type::MF_VEC2F:
    {
     auto* tmp = script_node->field_handle<Vector2f_array>(field_info);
     SGAL_assertion(index < tmp->size());
     v8::Handle<v8::Array> vec2f = v8::Array::New(isolate, 2);
     if (vec2f.IsEmpty()) {
       std::cerr << "failed to allocate v8 Array!" << std::endl;
     }
     CHECK(vec2f->Set(context, 0, v8::Number::New(isolate, ((*tmp)[index])[0])));
     CHECK(vec2f->Set(context, 1, v8::Number::New(isolate, ((*tmp)[index])[1])));
     info.GetReturnValue().Set(vec2f);
    }
    break;

   case Field_type::MF_VEC3F:
   case Field_type::MF_COLOR:
    {
     auto* tmp = script_node->field_handle<Vector2f_array>(field_info);
     SGAL_assertion(index < tmp->size());
     v8::Handle<v8::Array> vec3f = v8::Array::New(isolate, 3);
     if (vec3f.IsEmpty()) {
       std::cerr << "failed to allocate v8 Array!" << std::endl;
     }
     CHECK(vec3f->Set(context, 0, v8::Number::New(isolate, ((*tmp)[index])[0])));
     CHECK(vec3f->Set(context, 1, v8::Number::New(isolate, ((*tmp)[index])[1])));
     CHECK(vec3f->Set(context, 2, v8::Number::New(isolate, ((*tmp)[index])[2])));
     info.GetReturnValue().Set(vec3f);
    }
    break;

   case Field_type::MF_STR:
    {
     auto* tmp = script_node->field_handle<String_array>(field_info);
     SGAL_assertion(index < tmp->size());
     v8::Handle<v8::String> str =
       v8::String::NewFromUtf8(isolate, (*tmp)[index].c_str());
     info.GetReturnValue().Set(str);
    }
    break;

   case Field_type::MF_ROTATION:
    std::cerr << "Not supported yet!" << std::endl;
    break;

   case Field_type::MF_SHARED_CONTAINER:
    {
     // Set (in GetReturnValue) a pointer to the Shared_container object
     // indexed by 'index'.
     auto* tmp = script_node->field_handle<Shared_container_array>(field_info);
     SGAL_assertion(index < tmp->size());
     info.GetReturnValue().Set(v8::External::New(isolate, &((*tmp)[index])));
    }
    break;

   default:
    std::cerr << "Unsupported type!" << std::endl;
    return;
  }
}

//! \brief intercepts setting an array element by index.
void Script::indexed_setter(uint32_t index,
                            v8::Local<v8::Value> value,
                            const v8::PropertyCallbackInfo<v8::Value>& info) {
  SGAL_TRACE_CODE(Tracer::SCRIPT,
                  std::cout << "indexed_setter() " << index << std::endl;);

  auto isolate = info.GetIsolate();

  // Obtain the Script node:
  v8::Handle<v8::Object> obj = info.Holder();
  SGAL_assertion(obj->InternalFieldCount() >= 1);
  v8::Handle<v8::External> internal_field =
    v8::Handle<v8::External>::Cast(obj->GetInternalField(0));
  SGAL_assertion(internal_field->Value() != nullptr);
  auto* script_node = static_cast<Script*>(internal_field->Value());
  SGAL_TRACE_MSG(Tracer::SCRIPT, "  script: " + script_node->get_name() + "\n");

  // Obtain the field information record:
  v8::Local<v8::Value> data = info.Data();
  v8::Handle<v8::External> ext = v8::Handle<v8::External>::Cast(data);
  SGAL_assertion(ext->Value() != nullptr);
  auto* field_info = static_cast<Field_info*>(ext->Value());
  SGAL_TRACE_MSG(Tracer::SCRIPT, "  field: " + field_info->get_name() + "\n");
  v8::Local<v8::Context> context(isolate->GetCurrentContext());

  // Assign
  switch (field_info->get_type_id()) {
   case Field_type::SF_BOOL:
   case Field_type::SF_FLOAT:
   case Field_type::SF_TIME:
   case Field_type::SF_INT32:
   case Field_type::SF_STR:
    SGAL_error();
    break;

   case Field_type::SF_VEC2F:
    {
     v8::HandleScope scope(isolate);
     SGAL_assertion(index < 2);
     auto* tmp = script_node->field_handle<Vector2f>(field_info);
     (*tmp)[index] = static_cast<Float>(value->NumberValue(context).FromJust());
    }
    break;

   case Field_type::SF_VEC3F:
   case Field_type::SF_COLOR:
    {
     v8::HandleScope scope(isolate);
     SGAL_assertion(index < 3);
     auto* tmp = script_node->field_handle<Vector3f>(field_info);
     (*tmp)[index] = static_cast<Float>(value->NumberValue(context).FromJust());
    }
    break;

   case Field_type::SF_ROTATION:
   case Field_type::SF_IMAGE:
    std::cerr << "Not supported yet!" << std::endl;
    break;

   case Field_type::SF_SHARED_CONTAINER:
    SGAL_error();
    break;

   case Field_type::MF_BOOL:
    {
     v8::HandleScope scope(isolate);
     auto* tmp = script_node->field_handle<Boolean_array>(field_info);
     tmp->resize(index+1);
     (*tmp)[index] = value->BooleanValue();
    }
    break;

   case Field_type::MF_FLOAT:
    {
     v8::HandleScope scope(isolate);
     auto* tmp = script_node->field_handle<Float_array>(field_info);
     tmp->resize(index+1);
     (*tmp)[index] =
       static_cast<Float>(value->NumberValue(context).FromJust());
    }
    break;

   case Field_type::MF_TIME:
    {
     v8::HandleScope scope(isolate);
     auto* tmp = script_node->field_handle<Scene_time_array>(field_info);
     tmp->resize(index+1);
     (*tmp)[index] =
       static_cast<Scene_time>(value->NumberValue(context).FromJust());
    }
    break;

   case Field_type::MF_INT32:
    {
     v8::HandleScope scope(isolate);
     auto* tmp = script_node->field_handle<Int32_array>(field_info);
     tmp->resize(index+1);
     (*tmp)[index] = value->Int32Value(context).FromJust();
    }
    break;

   case Field_type::MF_VEC2F:
    {
     v8::HandleScope scope(isolate);
     auto* tmp = script_node->field_handle<Vector2f_array>(field_info);
     tmp->resize(index+1);
     v8::Local<v8::Array> vec2f = v8::Handle<v8::Array>::Cast(value);
     ((*tmp)[index])[0] = static_cast<Float>(vec2f->Get(context, 0).ToLocalChecked()->NumberValue(context).FromJust());
     ((*tmp)[index])[1] = static_cast<Float>(vec2f->Get(context, 1).ToLocalChecked()->NumberValue(context).FromJust());
    }
    break;

   case Field_type::MF_VEC3F:
   case Field_type::MF_COLOR:
    {
     v8::HandleScope scope(isolate);
     auto* tmp = script_node->field_handle<Vector3f_array>(field_info);
     tmp->resize(index+1);
     v8::Local<v8::Array> vec3f = v8::Handle<v8::Array>::Cast(value);
     ((*tmp)[index])[0] = static_cast<Float>(vec3f->Get(context, 0).ToLocalChecked()->NumberValue(context).FromJust());
     ((*tmp)[index])[1] = static_cast<Float>(vec3f->Get(context, 1).ToLocalChecked()->NumberValue(context).FromJust());
     ((*tmp)[index])[2] = static_cast<Float>(vec3f->Get(context, 2).ToLocalChecked()->NumberValue(context).FromJust());
    }
    break;

   case Field_type::MF_STR:
   case Field_type::MF_ROTATION:
    std::cerr << "Not supported yet!" << std::endl;
    break;

   case Field_type::MF_SHARED_CONTAINER:
    {
     // Observe that the value of the external object stores a pointer to
     // a shared-container. Set the shared-container item indexed by 'index'
     // to the value pointed by the external value.
     v8::HandleScope scope(isolate);
     auto* tmp = script_node->field_handle<Shared_container_array>(field_info);
     tmp->resize(index+1);
     auto external(v8::External::Cast(*value));
     (*tmp)[index] = *((Shared_container*)(external->Value()));
    }
    break;

   default:
    std::cerr << "Unsupported type!" << std::endl;
    return;
  }

  if (script_node->is_direct_output()) {
    auto* field = script_node->get_field(field_info);
    if (field) field->cascade();
  }
  else script_node->insert_assigned(field_info);
}

//! \brief records the scene graph.
void Script::add_to_scene(Scene_graph* scene_graph) {
  m_scene_graph = scene_graph;

  // Get the Isolate instance of the V8 engine from the scene graph.
  m_isolate = scene_graph->get_isolate();
  v8::Isolate::Scope isolate_scope(m_isolate);
  v8::HandleScope handle_scope(m_isolate);      // stack-allocated handle scope

  // Create a new ObjectTemplate
  v8::Handle<v8::ObjectTemplate> global_tmpl =
    v8::ObjectTemplate::New(m_isolate);
  global_tmpl->SetInternalFieldCount(1);
  add_callbacks(global_tmpl); // add callbacks for every field and output field.

  v8::Local<v8::Context> context =
    v8::Context::New(m_isolate, NULL, global_tmpl);
  m_context.Reset(m_isolate, context);

  v8::Context::Scope context_scope(context);    // enter the contex
  v8::Handle<v8::Object> global = context->Global();
  v8::Local<v8::Object> prototype =
    v8::Local<v8::Object>::Cast(global->GetPrototype());
  prototype->SetInternalField(0, v8::External::New(m_isolate, this));
  bound_script();           // bound the script to the context
}

//! \brief adds a field info record to the script node.
void Script::add_field_info(Field_rule rule, Field_type type,
                            const String& name, const String& value)
{
#if !defined(NDEBUG) || defined(SGAL_TRACE)
  if (SGAL::TRACE(Tracer::SCRIPT)) {
    std::cout << "add_field_info() " << name << ", " << value << std::endl;
  }
#endif

  auto* prototype = get_prototype();

  Execution_function exec_func = (rule == Field_rule::RULE_IN) ?
    static_cast<Execution_function>(&Script::execute) : nullptr;

  Variant_field variant_field;
  Uint id = LAST + m_fields.size();
  switch (type) {
   case Field_type::SF_BOOL:
    {
      auto initial_value = value.empty() ? false : to_boolean(value);
      variant_field = initial_value;
      add_fi<Field_type::SF_BOOL>(id, name, rule, initial_value, exec_func,
                                  prototype);
    }
    break;

   case Field_type::SF_FLOAT:
    {
      auto initial_value = value.empty() ? 0 : boost::lexical_cast<Float>(value);
      variant_field = initial_value;
      add_fi<Field_type::SF_FLOAT>(id, name, rule, initial_value, exec_func,
                                   prototype);
    }
    break;

   case Field_type::SF_TIME:
    {
      auto initial_value =
        value.empty() ? 0 : boost::lexical_cast<Scene_time>(value);
      variant_field = initial_value;
      add_fi<Field_type::SF_TIME>(id, name, rule, initial_value, exec_func,
                                  prototype);
    }
    break;

   case Field_type::SF_INT32:
    {
      auto initial_value = value.empty() ? 0 : boost::lexical_cast<Int32>(value);
      variant_field = initial_value;
      add_fi<Field_type::SF_INT32>(id, name, rule, initial_value, exec_func,
                                   prototype);
    }
    break;

   case Field_type::SF_VEC2F:
    {
      Vector2f initial_value;
      if (! value.empty()) initial_value = boost::lexical_cast<Vector2f>(value);
      variant_field = initial_value;
      add_fi<Field_type::SF_VEC2F>(id, name, rule, initial_value, exec_func,
                                   prototype);
    }
    break;

   case Field_type::SF_VEC3F:
    {
      Vector3f initial_value;
      if (! value.empty()) initial_value = boost::lexical_cast<Vector3f>(value);
      variant_field = initial_value;
      add_fi<Field_type::SF_VEC3F>(id, name, rule, initial_value, exec_func,
                                   prototype);
    }
    break;

   case Field_type::SF_COLOR:
    {
      Vector3f initial_value;
      if (! value.empty()) initial_value = boost::lexical_cast<Vector3f>(value);
      variant_field = initial_value;
      add_fi<Field_type::SF_COLOR>(id, name, rule, initial_value, exec_func,
                                   prototype);
    }
    break;

   case Field_type::SF_ROTATION:
    {
      Rotation initial_value;
      if (! value.empty()) initial_value = boost::lexical_cast<Rotation>(value);
      variant_field = initial_value;
      add_fi<Field_type::SF_ROTATION>(id, name, rule, initial_value, exec_func,
                                      prototype);
    }
    break;

   case Field_type::SF_STR:
    {
      variant_field = value;
      add_fi<Field_type::SF_STR>(id, name, rule, value, exec_func, prototype);
    }
    break;

   case Field_type::SF_IMAGE:
    std::cerr << "Not supported yet!" << std::endl;
    break;

   case Field_type::SF_SHARED_CONTAINER:
    {
      Shared_container initial_value;
      variant_field = initial_value;
      add_fi<Field_type::SF_SHARED_CONTAINER>(id, name, rule, initial_value,
                                              exec_func, prototype);
    }
    break;

   case Field_type::MF_BOOL:
    {
      Boolean_array initial_value;
      std::stringstream ss(value);
      std::transform(std::istream_iterator<std::string>(ss),
                     std::istream_iterator<std::string>(),
                     std::back_inserter(initial_value), to_boolean);
      variant_field = initial_value;
      add_fi<Field_type::MF_BOOL>(id, name, rule, initial_value, exec_func,
                                  prototype);
    }
    break;

   case Field_type::MF_FLOAT:
    {
      Float_array initial_value;
      std::stringstream ss(value);
      std::copy(std::istream_iterator<Float>(ss),
                std::istream_iterator<Float>(),
                std::back_inserter(initial_value));
      variant_field = initial_value;
      add_fi<Field_type::MF_FLOAT>(id, name, rule, initial_value, exec_func,
                                   prototype);
    }
    break;

   case Field_type::MF_TIME:
    {
      Scene_time_array initial_value;
      std::stringstream ss(value);
      std::copy(std::istream_iterator<Scene_time>(ss),
                std::istream_iterator<Scene_time>(),
                std::back_inserter(initial_value));
      variant_field = initial_value;
      add_fi<Field_type::MF_TIME>(id, name, rule, initial_value, exec_func,
                                  prototype);
    }
    break;

   case Field_type::MF_INT32:
    {
      Int32_array initial_value;
      std::stringstream ss(value);
      std::copy(std::istream_iterator<Int32>(ss),
                std::istream_iterator<Int32>(),
                std::back_inserter(initial_value));
      variant_field = initial_value;
      add_fi<Field_type::MF_INT32>(id, name, rule, initial_value, exec_func,
                                  prototype);
    }
     break;

   case Field_type::MF_VEC2F:
    {
     Vector2f_array initial_value;
     std::stringstream ss(value);
     std::transform(multi_istream_iterator<2>(ss),
                    multi_istream_iterator<2>(),
                    std::back_inserter(initial_value),
                    &boost::lexical_cast<Vector2f, String>);
     variant_field = initial_value;
     add_fi<Field_type::MF_VEC2F>(id, name, rule, initial_value, exec_func,
                                  prototype);
    }
    break;

   case Field_type::MF_VEC3F:
   case Field_type::MF_COLOR:
    {
     Vector3f_array initial_value;
     std::stringstream ss(value);
     std::transform(multi_istream_iterator<3>(ss),
                    multi_istream_iterator<3>(),
                    std::back_inserter(initial_value),
                    &boost::lexical_cast<Vector3f, String>);
     variant_field = initial_value;
     add_fi<Field_type::MF_VEC3F>(id, name, rule, initial_value, exec_func,
                                  prototype);
    }
    break;

   case Field_type::MF_ROTATION:
    {
     Rotation_array initial_value;
     std::stringstream ss(value);
     std::transform(multi_istream_iterator<4>(ss),
                    multi_istream_iterator<4>(),
                    std::back_inserter(initial_value),
                    &boost::lexical_cast<Rotation, String>);
     variant_field = initial_value;
     add_fi<Field_type::MF_ROTATION>(id, name, rule, initial_value, exec_func,
                                     prototype);
    }
    break;

   case Field_type::MF_STR:
    {
     String_array initial_value;
      std::stringstream ss(value);
      std::copy(std::istream_iterator<std::string>(ss),
                std::istream_iterator<std::string>(),
                std::back_inserter(initial_value));
     variant_field = initial_value;
     add_fi<Field_type::MF_STR>(id, name, rule, initial_value, exec_func,
                                prototype);
    }
    break;

   case Field_type::MF_SHARED_CONTAINER:
    {
     Shared_container_array initial_value;
     variant_field = initial_value;
     add_fi<Field_type::MF_SHARED_CONTAINER>(id, name, rule, initial_value,
                                             exec_func, prototype);
    }
    break;

   default:
    std::cerr << "Unrecognized type (" << get_field_type_name(type) << ")!"
              << std::endl;
    break;
  }

  m_fields.push_back(variant_field);
}

//! \brief converts a single Boolean field value to a v8 engine Boolean.
v8::Handle<v8::Value> Script::get_single_boolean(const Field_info* field_info)
{
  auto id = field_info->get_id();
  Boolean* tmp =
    (id == DIRECT_OUTPUT) ? direct_output_handle(field_info) :
    (id == MUST_EVALUATE) ? must_evaluate_handle(field_info) :
    field_handle<Boolean>(field_info);
  return v8::Boolean::New(m_isolate, *tmp);
}

//! \brief converts a single string field value to a v8 engine string.
v8::Handle<v8::Value> Script::get_single_string(const Field_info* field_info)
{
  auto id = field_info->get_id();
  std::string* tmp = field_handle<std::string>(field_info);
  return v8::String::NewFromUtf8(m_isolate, tmp->c_str());
}

/*! \brief converts a single Vector2f field value to a v8 engine array of 2
 * floats.
 */
v8::Handle<v8::Value> Script::get_single_vector2f(const Field_info* field_info)
{
  auto* tmp = field_handle<Vector2f>(field_info);
  v8::Handle<v8::Array> array = v8::Array::New(m_isolate, 2);
  if (array.IsEmpty()) {
    std::cerr << "failed to allocate v8 Array!" << std::endl;
    return v8::Null(m_isolate);
  }
  v8::Local<v8::Context> context(m_isolate->GetCurrentContext());
  CHECK(array->Set(context, 0, v8::Number::New(m_isolate, (*tmp)[0])));
  CHECK(array->Set(context, 1, v8::Number::New(m_isolate, (*tmp)[1])));
  return array;
}

/*! \brief converts a single Vector3f field value to a v8 engine array of 3
 * floats.
 */
v8::Handle<v8::Value> Script::get_single_vector3f(const Field_info* field_info)
{
  auto* tmp = field_handle<Vector3f>(field_info);
  v8::Handle<v8::Array> array = v8::Array::New(m_isolate, 3);
  if (array.IsEmpty()) {
    std::cerr << "failed to allocate v8 Array!" << std::endl;
    return v8::Null(m_isolate);
  }
  v8::Local<v8::Context> context(m_isolate->GetCurrentContext());
  CHECK(array->Set(context, 0, v8::Number::New(m_isolate, (*tmp)[0])));
  CHECK(array->Set(context, 1, v8::Number::New(m_isolate, (*tmp)[1])));
  CHECK(array->Set(context, 2, v8::Number::New(m_isolate, (*tmp)[2])));
  return array;
}

/*! \brief converts a single Shared_container field value to a v8 engine
 * external (which holds a void* that actually points to a persistent
 * Shared_container object).
 */
v8::Handle<v8::Value> Script::get_single_external(const Field_info* field_info)
{
  auto* cont = field_handle<Shared_container>(field_info);
  auto* persistent_cont = m_scene_graph->find_container(*cont);
  if (! persistent_cont) {
    persistent_cont = new Shared_container(*cont);
    m_persistent_containers.push_back(persistent_cont);
  }
  return v8::External::New(m_isolate, (void*)(persistent_cont));
}

/*! \brief converts a multi Boolean field value to a v8 engine array of
 * Booleans.
 */
v8::Handle<v8::Value> Script::get_multi_boolean(const Field_info* field_info)
{
  auto* tmp = field_handle<Boolean_array>(field_info);
  v8::Handle<v8::Array> array = v8::Array::New(m_isolate, tmp->size());
  if (array.IsEmpty()) {
    std::cerr << "failed to allocate v8 Array!" << std::endl;
    return v8::Null(m_isolate);
  }
  v8::Local<v8::Context> context(m_isolate->GetCurrentContext());
  for (size_t i = 0; i < tmp->size(); ++i) {
    CHECK(array->Set(context, i, v8::Boolean::New(m_isolate, (*tmp)[i])));
  }
  return array;
}

//! \brief converts a multi float field value to a v8 engine array of floats.
v8::Handle<v8::Value> Script::get_multi_float(const Field_info* field_info)
{
  auto* tmp = field_handle<Float_array>(field_info);
  v8::Handle<v8::Array> array = v8::Array::New(m_isolate, tmp->size());
  if (array.IsEmpty()) {
    std::cerr << "failed to allocate v8 Array!" << std::endl;
    return v8::Null(m_isolate);
  }
  v8::Local<v8::Context> context(m_isolate->GetCurrentContext());
  for (size_t i = 0; i < tmp->size(); ++i) {
    CHECK(array->Set(context, i, v8::Number::New(m_isolate, (*tmp)[i])));
  }
  return array;
}

//! \brief converts a multi time field value to a v8 engine array of floats.
v8::Handle<v8::Value> Script::get_multi_time(const Field_info* field_info)
{
  auto* tmp = field_handle<Scene_time_array>(field_info);
  v8::Handle<v8::Array> array = v8::Array::New(m_isolate, tmp->size());
  if (array.IsEmpty()) {
    std::cerr << "failed to allocate v8 Array!" << std::endl;
    return v8::Null(m_isolate);
  }
  v8::Local<v8::Context> context(m_isolate->GetCurrentContext());
  for (size_t i = 0; i < tmp->size(); ++i) {
    CHECK(array->Set(context, i, v8::Number::New(m_isolate, (*tmp)[i])));
  }
  return array;
}

//! \brief converts a multi int32 field value to a v8 engine array of int32.
v8::Handle<v8::Value> Script::get_multi_int32(const Field_info* field_info)
{
  auto* tmp = field_handle<Int32_array>(field_info);
  v8::Handle<v8::Array> array = v8::Array::New(m_isolate, tmp->size());
  if (array.IsEmpty()) {
    std::cerr << "failed to allocate v8 Array!" << std::endl;
    return v8::Null(m_isolate);
  }
  v8::Local<v8::Context> context(m_isolate->GetCurrentContext());
  for (size_t i = 0; i < tmp->size(); ++i) {
    CHECK(array->Set(context, i, v8::Int32::New(m_isolate, (*tmp)[i])));
  }
  return array;
}

//! \brief converts a multi string field value to a v8 engine array of strings.
v8::Handle<v8::Value> Script::get_multi_string(const Field_info* field_info)
{
  auto* tmp = field_handle<String_array>(field_info);
  v8::Handle<v8::Array> array = v8::Array::New(m_isolate, tmp->size());
  if (array.IsEmpty()) {
    std::cerr << "failed to allocate v8 Array!" << std::endl;
    return v8::Null(m_isolate);
  }
  v8::Local<v8::Context> context(m_isolate->GetCurrentContext());
  for (size_t i = 0; i < tmp->size(); ++i) {
    v8::Handle<v8::String> str =
      v8::String::NewFromUtf8(m_isolate, (*tmp)[i].c_str());
    CHECK(array->Set(context, i, str));
  }
  return array;
}

/*! \brief converts a multi Vector2f field value to a v8 engine array of
 * arrays of 2 floats.
 */
v8::Handle<v8::Value> Script::get_multi_vector2f(const Field_info* field_info)
{
  auto* tmp = field_handle<Vector2f_array>(field_info);
  v8::Handle<v8::Array> array = v8::Array::New(m_isolate, tmp->size());
  if (array.IsEmpty()) {
    std::cerr << "failed to allocate v8 Array!" << std::endl;
    return v8::Null(m_isolate);
  }
  v8::Local<v8::Context> context(m_isolate->GetCurrentContext());
  for (size_t i = 0; i < tmp->size(); ++i) {
    auto& arr = (*tmp)[i];
    v8::Handle<v8::Array> vec2f = v8::Array::New(m_isolate, 2);
    CHECK(vec2f->Set(context, 0, v8::Number::New(m_isolate, arr[0])));
    CHECK(vec2f->Set(context, 1, v8::Number::New(m_isolate, arr[1])));
    CHECK(array->Set(context, i, vec2f));
  }
  return array;
}

/*! \brief converts a multi Vector3f field value to a v8 engine array of
 * arrays of 3 floats.
 */
v8::Handle<v8::Value> Script::get_multi_vector3f(const Field_info* field_info)
{
  auto* tmp = field_handle<Vector3f_array>(field_info);
  v8::Handle<v8::Array> array = v8::Array::New(m_isolate, tmp->size());
  if (array.IsEmpty()) {
    std::cerr << "failed to allocate v8 Array!" << std::endl;
    return v8::Null(m_isolate);
  }
  v8::Local<v8::Context> context(m_isolate->GetCurrentContext());
  for (size_t i = 0; i < tmp->size(); ++i) {
    auto& arr = (*tmp)[i];
    v8::Handle<v8::Array> vec3f = v8::Array::New(m_isolate, 3);
    CHECK(vec3f->Set(context, 0, v8::Number::New(m_isolate, arr[0])));
    CHECK(vec3f->Set(context, 1, v8::Number::New(m_isolate, arr[1])));
    CHECK(vec3f->Set(context, 2, v8::Number::New(m_isolate, arr[2])));
    CHECK(array->Set(context, i, vec3f));
  }
  return array;
}

/*! \brief converts a multi shared-container field value to a v8 engine array of
 * exernals.
   */
v8::Handle<v8::Value> Script::get_multi_external(const Field_info* field_info)
{
  auto* tmp = field_handle<Shared_container_array>(field_info);
  v8::Handle<v8::Array> array = v8::Array::New(m_isolate, tmp->size());
  if (array.IsEmpty()) {
    std::cerr << "failed to allocate v8 Array!" << std::endl;
    return v8::Null(m_isolate);
  }
  v8::Local<v8::Context> context(m_isolate->GetCurrentContext());
  for (size_t i = 0; i < tmp->size(); ++i)
    CHECK(array->Set(context, i, v8::External::New(m_isolate, (void*)&((*tmp)[i]))));
  return array;
}

/*! \brief adds setter and getter callbacks to the engine for every field and
 * every output field.
 */
void Script::add_callbacks(v8::Local<v8::ObjectTemplate> tmpl)
{
  auto* proto = get_prototype();
  for (auto it = proto->ids_begin(proto); it != proto->ids_end(proto); ++it) {
    const auto* field_info = (*it).second;
    if ((field_info->get_rule() != Field_rule::RULE_FIELD) &&
        (field_info->get_rule() != Field_rule::RULE_OUT))
      continue;

    v8::Handle<v8::String> field_name =
      v8::String::NewFromUtf8(m_isolate, field_info->get_name().c_str(),
                              v8::NewStringType::kInternalized).ToLocalChecked();
    // std::cout << "In name: " << field_info->get_name() << std::endl;
    v8::Handle<v8::External> ext =
      v8::External::New(m_isolate, const_cast<Field_info*>(field_info));
    if (field_info->is_scalar())
      tmpl->SetAccessor(field_name, getter, setter, ext);
    else {
      tmpl->SetAccessor(field_name, array_getter, array_setter, ext);
    }
  }
}

//! \brief pushes a new set of altered field-information records onto the stack.
void Script::push_assigned()
{
  if (m_direct_output) return;
  m_assigned.emplace();
}

/*! \brief inserts a field-information into the set of altered field-information
 * records.
 */
void Script::insert_assigned(const Field_info* field_info)
{
  auto& assigned = m_assigned.top();
  assigned.insert(field_info);
}

/*! \brief cascades all fields that have been altered during the execution of
 * the script.
 */
void Script::cascade_assigned()
{
  if (m_direct_output) return;

  auto& assigned = m_assigned.top();
  for (auto it = assigned.begin(); it != assigned.end(); ++it) {
    auto* field = get_field(*it);
    if (field) field->cascade();
  }
  assigned.clear();
  m_assigned.pop();
}

//! \brief bounds the script.
void Script::bound_script() {
  for (const auto& url : m_url) {
    // Extract the script
    auto pos = url.find(':');
    if (std::string::npos == pos) {
      std::cerr << "Invalid script!" << std::endl;
      continue;
    }
    //! \todo m_protocol is overriden each iteration
    if (url.compare(0, pos, "javascript") == 0)
      m_protocol = PROTOCOL_CUSTOM_ECMASCRIPT;
    else if (std::regex_match(url, std::regex("http://.*\\.js")))
      m_protocol = PROTOCOL_ECMASCRIPT;
    else if (std::regex_match(url, std::regex("http://.*\\.class")))
      m_protocol = PROTOCOL_JAVA_BYTECODE;
    else {
      std::cerr << "Invalid script!" << std::endl;
      continue;
    }

    pos = url.find_first_not_of(" \t\r\n", pos + 1);
    std::string source_str = url.substr(pos);

    // std::cout << "source_str.c_str(): " << source_str.c_str() << std::endl;

    // Create a string containing the JavaScript source code.
    v8::Handle<v8::String> source =
      v8::String::NewFromUtf8(m_isolate, source_str.c_str());

    // set up an error handler to catch any exceptions the script might throw.
    v8::TryCatch try_catch(m_isolate);

    v8::Local<v8::Context> context(m_isolate->GetCurrentContext());
    v8::Handle<v8::Script> script;
    if (! v8::Script::Compile(context, source).ToLocal(&script)) {
      // The script failed to compile; bail out.
      v8::String::Utf8Value error(m_isolate, try_catch.Exception());
      std::cerr << *error << std::endl;
      return;
    }

    v8::Handle<v8::Value> result;
    if (! script->Run(context).ToLocal(&result)) {
      // The TryCatch above is still in effect and will have caught the error.
      v8::String::Utf8Value error(m_isolate, try_catch.Exception());
      std::cerr << *error << std::endl;
      return;
    }
  }
}

//! \brief obtains the first argument for the call.
v8::Handle<v8::Value> Script::get_argument(const Field_info* field_info) {
  // std::cout << field_info->get_name() << std::endl;
  switch (field_info->get_type_id()) {
   case Field_type::SF_BOOL: return get_single_boolean(field_info);
   case Field_type::SF_FLOAT: return get_single_float(field_info);
   case Field_type::SF_TIME: return get_single_time(field_info);
   case Field_type::SF_INT32: return get_single_int32(field_info);
   case Field_type::SF_STR: return get_single_string(field_info);
   case Field_type::SF_VEC2F: return get_single_vector2f(field_info);

   case Field_type::SF_VEC3F:
   case Field_type::SF_COLOR: return get_single_vector3f(field_info);

   case Field_type::SF_ROTATION:
    std::cerr << "Not supported yet!" << std::endl;
    return v8::Null(m_isolate);

   case Field_type::SF_IMAGE:
    std::cerr << "Not supported yet!" << std::endl;
    return v8::Null(m_isolate);

   case Field_type::SF_SHARED_CONTAINER: return get_single_external(field_info);

   case Field_type::MF_BOOL: return get_multi_boolean(field_info);
   case Field_type::MF_FLOAT: return get_multi_float(field_info);
   case Field_type::MF_TIME: return get_multi_time(field_info);
   case Field_type::MF_INT32: return get_multi_int32(field_info);
   case Field_type::MF_STR: return get_multi_string(field_info);
   case Field_type::MF_VEC2F: return get_multi_vector2f(field_info);

   case Field_type::MF_VEC3F:
   case Field_type::MF_COLOR: return get_multi_vector3f(field_info);

   case Field_type::MF_ROTATION:
    SGAL_error_msg("Not supported yet!");
    return v8::Null(m_isolate);

   case Field_type::MF_SHARED_CONTAINER: return get_multi_external(field_info);

   default:
    SGAL_error_msg("Unsupported type!");
    return v8::Null(m_isolate);
  }
  SGAL_error_msg("Should never get here!");
  return v8::Null(m_isolate);
}

// \brief extracts a C string from a V8 Utf8Value.
const char* Script::to_c_string(const v8::String::Utf8Value& value)
{ return *value ? *value : "<string conversion failed>"; }

//
void Script::log(const v8::FunctionCallbackInfo<v8::Value>& args) {
  v8::String::Utf8Value str(args[0]);
  const char* cstr = to_c_string(str);
  printf("%s\n", cstr);
}

//
void Script::error(const v8::FunctionCallbackInfo<v8::Value>& args) {
  v8::String::Utf8Value str(args[0]);
  const char* cstr = to_c_string(str);
  fprintf(stderr,"%s\n", cstr);
}

//
v8::Local<v8::Object> Script::wrap_object(Console* c) {
  v8::EscapableHandleScope handle_scope(get_isolate());

  v8::Local<v8::ObjectTemplate> class_t;
  v8::Local<v8::ObjectTemplate> raw_t = v8::ObjectTemplate::New(get_isolate());
  raw_t->SetInternalFieldCount(1);
  raw_t->
    Set(v8::String::NewFromUtf8(get_isolate(), "log",
                                v8::NewStringType::kNormal).ToLocalChecked(),
        v8::FunctionTemplate::New(get_isolate(), log));
  raw_t->
    Set(v8::String::NewFromUtf8(get_isolate(), "error",
                                v8::NewStringType::kNormal).ToLocalChecked(),
        v8::FunctionTemplate::New(get_isolate(), error));
  class_t = v8::Local<v8::ObjectTemplate>::New(get_isolate(),raw_t);
  //create instance
  v8::Local<v8::Object> result =
    class_t->NewInstance(get_isolate()->GetCurrentContext()).ToLocalChecked();
  //create wrapper
  v8::Local<v8::External> ptr = v8::External::New(get_isolate(),c);
  result->SetInternalField(0,ptr);
  return handle_scope.Escape(result);
}

//! \brief executes the suitable script function according to the event.
void Script::execute(const Field_info* field_info) {
  v8::Isolate::Scope isolate_scope(m_isolate);
  v8::HandleScope handle_scope(m_isolate);      // stack-allocated handle scope
  v8::Local<v8::Context> context =
      v8::Local<v8::Context>::New(m_isolate, m_context);
  v8::Context::Scope context_scope(context);    // enter the contex
  v8::Handle<v8::Object> global = context->Global();

  Console* c = new Console();
  v8::Local<v8::Object> con = wrap_object(c);
  context->Global()->
    Set(v8::String::NewFromUtf8(m_isolate, "console",
                                v8::NewStringType::kNormal).ToLocalChecked(),
        con);


  v8::Local<v8::Object> prototype =
    v8::Local<v8::Object>::Cast(global->GetPrototype());
  prototype->SetInternalField(0, v8::External::New(m_isolate, this));

  const std::string& str = field_info->get_name();
  v8::Handle<v8::String> name =
    v8::String::NewFromUtf8(m_isolate, str.c_str());
  v8::Handle<v8::Value> value = global->Get(context, name).ToLocalChecked();

  // If there is no such function, bail out.
  if (! value->IsFunction()) {
    std::cerr << "A function named " << str.c_str() << " does not exist!"
              << std::endl;
    return;
  }

  v8::Handle<v8::Function> func = v8::Handle<v8::Function>::Cast(value);
  v8::Handle<v8::Value> args[2];
  args[0] = get_argument(field_info);
  args[1] = v8::Number::New(m_isolate, m_time);

  push_assigned();

  // When an object is assigned, the id of the associate field is inserted into
  // the set. A field, the id of which is not found in the set is not cascaded.
  // When a field is cascaded, it is removed from the assigned set.
  v8::TryCatch try_catch(m_isolate);
  v8::Handle<v8::Value> func_result;
  if (! func->Call(context, global, 2, args).ToLocal(&func_result)) {
    v8::String::Utf8Value error(m_isolate, try_catch.Exception());
    std::cerr << *error << std::endl;
    return;
  }

  cascade_assigned();
}

//! \brief writes all fields of this container.
void Script::write_fields(Formatter* formatter) {
  auto* vrml_formatter = dynamic_cast<Vrml_formatter*>(formatter);
  if (!vrml_formatter) return;

  auto* proto = get_prototype();
  for (auto it = proto->ids_begin(proto); it != proto->ids_end(proto); ++it) {
    const Field_info* field_info = (*it).second;
    Boolean definition = ((URL == field_info->get_id()) ||
                          (DIRECT_OUTPUT == field_info->get_id()) ||
                          (MUST_EVALUATE == field_info->get_id()));
    field_info->write(this, formatter, !definition);
  }
}

SGAL_END_NAMESPACE
