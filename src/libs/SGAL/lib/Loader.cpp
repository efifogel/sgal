// Copyright (c) 2004 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <list>

#include <boost/regex.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/interprocess/streams/bufferstream.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/Appearance.hpp"
#include "SGAL/Base_loader.hpp"
#include "SGAL/Coord_array_3d.hpp"
#include "SGAL/Epec_coord_array_3d.hpp"
#include "SGAL/Color_array_3d.hpp"
#include "SGAL/File_errors.hpp"
#include "SGAL/find_file.hpp"
#include "SGAL/Geometry_format.hpp"
#include "SGAL/Group.hpp"
#include "SGAL/Indexed_face_set.hpp"
#include "SGAL/Loader.hpp"
#include "SGAL/Shape.hpp"
#include "SGAL/Scene_graph.hpp"
#include "SGAL/Material.hpp"
#include "SGAL/Normal_array.hpp"
#include "SGAL/Tex_coord_array_2d.hpp"
#include "SGAL/Types.hpp"

#include "Vrml_scanner.hpp"

namespace fi = boost::filesystem;

SGAL_BEGIN_NAMESPACE

//! The Loader singleton.
Loader* Loader::s_instance(nullptr);

//! \brief constructs.
Loader::Loader() : m_multiple_shapes(false) {}

//! \brief destructs.
Loader::~Loader() { m_loaders.clear(); }

//! \brief obtains the loader singleton.
Loader* Loader::get_instance()
{
  if (!s_instance) s_instance = new Loader();
  return s_instance;
}

//! \brief creates a new Material container.
Loader::Shared_material Loader::create_material(Scene_graph* sg) const
{
  Shared_material mat(new Material);
  SGAL_assertion(mat);
  sg->add_container(mat);
  return mat;
}

//! \brief creates a new appearance container.
Loader::Shared_appearance Loader::create_appearance(Scene_graph* sg) const
{
  Shared_appearance app(new Appearance);
  SGAL_assertion(app);
  sg->add_container(app);
  return app;
}

//! \brief creates a new appearance container.
Loader::Shared_appearance
Loader::create_appearance(Scene_graph* sg, const std::string& name) const
{
  Shared_appearance app(new Appearance);
  SGAL_assertion(app);
  sg->add_container(app, name);
  return app;
}

//! \brief creates a new Coord_array_3d container.
Loader::Shared_coord_array_3d
Loader::create_coord_array_3d(Scene_graph* sg, size_t num) const {
  auto coords = (num != 0) ?
    Shared_coord_array_3d(new Coord_array_3d(num)) :
    Shared_coord_array_3d(new Coord_array_3d);
  SGAL_assertion(coords);
  coords->add_to_scene(sg);
  sg->add_container(coords);
  return coords;
}

//! \brief creates a new Color_array_3d container.
Loader::Shared_color_array_3d
Loader::create_color_array_3d(Scene_graph* sg, size_t num) const {
  auto colors = (num != 0) ?
    Shared_color_array_3d(new Color_array_3d(num)) :
    Shared_color_array_3d(new Color_array_3d);
  SGAL_assertion(colors);
  colors->add_to_scene(sg);
  sg->add_container(colors);
  return colors;
}

//! \brief creates a new Normal_array_3d container.
Loader::Shared_normal_array
Loader::create_normal_array(Scene_graph* sg, size_t num) const {
  auto normals = (num != 0) ?
    Shared_normal_array(new Normal_array(num)) :
    Shared_normal_array(new Normal_array);
  SGAL_assertion(normals);
  normals->add_to_scene(sg);
  sg->add_container(normals);
  return normals;
}

//! \brief creates a new Tex_coord_array_2d container.
Loader::Shared_tex_coord_array_2d
Loader::create_tex_coord_array_2d(Scene_graph* sg, size_t num) const {
  auto tex_coords = (num != 0) ?
    Shared_tex_coord_array_2d(new Tex_coord_array_2d(num)) :
    Shared_tex_coord_array_2d(new Tex_coord_array_2d);
  SGAL_assertion(tex_coords);
  tex_coords->add_to_scene(sg);
  sg->add_container(tex_coords);
  return tex_coords;
}

//! \brief computes a new Indexed Face Set container.
Loader::Shared_indexed_face_set
Loader::create_ifs(Scene_graph* scene_graph, size_t count,
                   std::list<Triangle>::const_iterator begin,
                   std::list<Triangle>::const_iterator end) {
  Shared_indexed_face_set ifs(new Indexed_face_set);
  SGAL_assertion(ifs);
  scene_graph->add_container(ifs);
  ifs->set_primitive_type(Geo_set::PT_TRIANGLES);
  ifs->set_num_primitives(count);
  ifs->set_make_consistent(true);

  auto num_vertices = count * 3;
  auto* coords = new Coord_array_3d(num_vertices);
  Shared_coord_array_3d shared_coords(coords);
  ifs->set_coord_array(shared_coords);
  auto& indices = ifs->get_empty_triangle_coord_indices();
  indices.resize(count);
  size_t i(0);
  Uint coord_index(0);
  for (auto it = begin; it != end; ++it) {
    auto& triangle = *it;
    (*coords)[coord_index].set(triangle.v0);
    indices[i][0] = coord_index++;
    (*coords)[coord_index].set(triangle.v1);
    indices[i][1] = coord_index++;
    (*coords)[coord_index].set(triangle.v2);
    indices[i][2] = coord_index++;
    ++i;
  }
  ifs->facet_coord_indices_changed();
  ifs->collapse_identical_coordinates();
  ifs->add_to_scene(scene_graph);

  return ifs;
}

//! \brief creates a new IndexedFaceSet container.
Loader::Shared_indexed_face_set Loader::create_ifs(Scene_graph* sg) const {
  Shared_indexed_face_set ifs(new Indexed_face_set);
  SGAL_assertion(ifs);
  ifs->add_to_scene(sg);
  sg->add_container(ifs);
  return ifs;
}

//! \brief creates a new shape node.
Loader::Shared_shape Loader::create_shape(Scene_graph* sg) const {
  Shared_shape shape(new Shape);
  SGAL_assertion(shape);
  shape->add_to_scene(sg);
  sg->add_container(shape);
  return shape;
}

//! \brief adds a shape node to the scene.
Loader::Shared_shape
Loader::create_shape(Scene_graph* scene_graph, const Vector3f& color) {
  auto shape = create_shape(scene_graph);
  Shared_appearance appearance(new Appearance);
  SGAL_assertion(appearance);
  shape->set_appearance(appearance);
  Shared_material material(new Material);
  SGAL_assertion(material);
  appearance->set_material(material);
  material->set_emissive_color(color);
  material->set_ambient_intensity(0.0f);
  return shape;
}

//! \brief loads a scene graph from a file.
Loader_code
Loader::operator()(const std::string& filename, Scene_graph* sg, Group* root) {
  SGAL_assertion(! filename.empty());
  m_filename = filename;        // record the filename

  // Open source file.
  std::ifstream is(m_filename.c_str(), std::ifstream::binary);
  if (! is.good()) {
    throw Open_file_error(m_filename);
    return Loader_code::FAILURE;
  }

  auto rc = operator()(is, sg, root);
  is.close();
  return rc;
}

//! \brief loads a scene graph from a stream.
Loader_code Loader::operator()(std::istream& is, Scene_graph* sg, Group* root) {
  // Verify that the file is not empty.
  if (is.peek() == std::char_traits<char>::eof()) {
    throw Empty_error(m_filename);
    return Loader_code::FAILURE;
  }

  // Obtain the file extension.
  auto file_extension = fi::path(m_filename).extension().string();

  // We do not rely on the file extension neither on the magic string (if
  // exists at all). We don't know whether the file is binary or text.
  // We read the first 80 characters into a buffer and treat it as string.
  // (We rewind the input stream indicater afterwards.)
  // We terminate the buffer with the end-of-string character replacing the
  // end-of-line character, if exists, otherwise, at the end of the buffer.
  // Obtain the first line and and examine the magic string
  char line[81];
  is.read(line, 80);
  if (! is) {
    if (is.eof()) is.clear();
    else {
      throw Read_error(m_filename.c_str());
      return Loader_code::FAILURE;
    }
  }
  is.seekg(0, is.beg);          // rewind
  auto it = line;
  for (; it != &line[80]; ++it) if (*it == '\n') break;
  *it = '\0';
  std::string magic(line);

  // If the first line starts with "OFF", assume that the file is in the off
  // format. If the return code of the loader is positive, the file might be
  // in a different format. In this case, continue trying matching.
  boost::smatch what;
  boost::regex re("(ST)?(C)?(N)?(4)?(n)?OFF\\s*(\\s#.*)?");
  if (boost::regex_match(magic, what, re)) {
    // If the first line starts with "OFF" and the file extension is off,
    // assume that the file format is OFF.
    auto rc = load_off(is, what, sg, root);
    if (static_cast<int>(rc) <= 0) {
      if ((rc == Loader_code::SUCCESS) &&
          ! boost::iequals(file_extension, ".off"))
        std::cerr << "Warning: File extension " << file_extension
                  << " does not match file format (OFF)" << std::endl;
      return rc;
    }
  }

  // If the first line starts with "OFF", assume that the file is in the off
  // format. If the return code of the loader is positive, the file might be
  // in a different format. In this case, continue trying matching.
  re = boost::regex("(ST)?(C)?(N)?(4)?(n)?EOFF\\s*(\\s#.*)?");
  if (boost::regex_match(magic, what, re)) {
    // If the first line starts with "EOFF" and the file extension is eoff,
    // assume that the file format is EOFF.
    auto rc = load_eoff(is, what, sg, root);
    if (static_cast<int>(rc) <= 0) {
      if ((rc == Loader_code::SUCCESS) &&
          ! boost::iequals(file_extension, ".eoff"))
        std::cerr << "Warning: File extension " << file_extension
                  << " does not match file format (EOFF)" << std::endl;
      return rc;
    }
  }

  // If the magic string is either "solid" or #VRL, assume that the file is in
  // the VRML or the text STL format, respectively.
  // If the return code of the parser is positive, the file might be in a
  // different format. In this case, continue trying matching.
  else if ((0 == magic.compare(0, 5, "solid")) ||
           (0 == magic.compare(0, 5, "#VRML")))
  {
    auto rc = parse(is, sg, root);
    if (static_cast<int>(rc) <= 0) return rc;
    is.seekg(0, is.beg);          // rewind
  }

  // If the extension is .obj, assume that the file is in the obj format.
  // If the return code of the loader is positive, the file might be in a
  // different format. In this case, continue trying matching.
  if (boost::iequals(file_extension, ".obj")) {
    auto rc = parse_obj(is, sg, root);
    if (static_cast<int>(rc) <= 0) {
      if (static_cast<int>(rc) < 0) throw Parse_error(m_filename.c_str());
      return rc;
    }
  }

  // If the extension is .stl, assume that the file is in the binary stl format.
  // If the return code of the loader is positive, the file might be in a
  // different format. In this case, continue trying matching.
  else if (boost::iequals(file_extension, ".stl")) {
    auto rc = load_stl(is, sg, root);
    if (static_cast<int>(rc) <= 0) {
      if (rc == Loader_code::SUCCESS) {
        if (0 == magic.compare(0, 5, "solid")) {
          std::cerr << "Warning: The file magic string " << "\"solid\""
                    << " does not match the file format (binary STL)"
                    << std::endl;
        }
      }
      return rc;
    }
  }

  // Try registered loaders
  else {
    // If the extension matches the extension of the registered loader, use it.
    auto it = m_loaders.find(file_extension);
    if (it != m_loaders.end()) {
      auto& loader = *(it->second);
      auto rc = loader(is, m_filename, sg, root);
      if (rc == Loader_code::SUCCESS) return rc;
      if (rc == Loader_code::FAILURE) {
        throw Parse_error(m_filename.c_str());
        return rc;
      }
    }
  }

  // Assume that the file is in the binary STL format.
  is.seekg(0, is.beg);
  auto rc = load_stl(is, sg, root);
  if (rc == Loader_code::SUCCESS) {
    if (!boost::iequals(file_extension, ".stl"))
      std::cerr << "Warning: The file extension " << file_extension
                << " does not match the file format (binary STL)" << std::endl;
    if (0 == magic.compare(0, 5, "solid")) {
      std::cerr << "Warning: The file magic string " << "\"solid\""
                << " does not match the file format (binary STL)" << std::endl;
    }
  }
  return rc;
}

//! \brief reads the title from a stream in the STL binary format.
Loader_code Loader::read_stl_title(std::istream& is, Vector3f& color) {
  char str[81];
  is.read(str, 80);
  if (! is) {
    throw Read_error(m_filename.c_str());
    return Loader_code::FAILURE;
  }

  std::string title(str);
  // std::cout << "title: " << title << std::endl;
  auto pos = title.find("COLOR=");
  if ((pos != std::string::npos) && (pos < 70)) {
    pos += 6;
    Float red = static_cast<Float>(static_cast<Uchar>(str[pos])) / 255.0f;
    Float green = static_cast<Float>(static_cast<Uchar>(str[pos+1])) / 255.0f;
    Float blue = static_cast<Float>(static_cast<Uchar>(str[pos+2])) / 255.0f;
    Float alpha = static_cast<Float>(static_cast<Uchar>(str[pos+3])) / 255.0f;
    color.set(red, green, blue);
  }
  return Loader_code::SUCCESS;
}

//! \brief reads a scene graph from a stream in the STL binary format.
Loader_code Loader::read_stl_body(std::istream& is, Scene_graph* scene_graph,
                                  Group* root, const Vector3f& color)
{
  Int32 total_num_tris;
  is.read((char*)&total_num_tris, sizeof(Int32));

  // Read all triangles while discarding collinear triangles.
  std::list<Triangle> triangles;
  auto rc = read_triangles(is, triangles, total_num_tris);
  if (static_cast<int>(rc) < 0) return rc;

  // Construct shape nodes
  if (m_multiple_shapes) add_shapes(scene_graph, root, triangles, color);
  else {
    bool use_colors((color[0] != .0f) || (color[1] != .0f) ||(color[2] != .0f));
    if (use_colors) add_colored_shape(scene_graph, root, triangles, color);
    else add_shape(scene_graph, root, triangles);
  }

  return Loader_code::SUCCESS;
}

//! \brief loads a scene graph from an input stream.
Loader_code Loader::load_stl(std::istream& is, Scene_graph* sg, Group* root) {
  sg->push_geometry_format(Geometry_format::STL);
  Vector3f color;
  auto rc = read_stl_title(is, color);
  if (static_cast<int>(rc) < 0) {
    sg->pop_geometry_format();
    return rc;
  }
  rc = read_stl_body(is, sg, root, color);
  sg->pop_geometry_format();
  if (static_cast<int>(rc) < 0) return rc;
  return Loader_code::SUCCESS;
}

//! \brief reads the title from a stream in the STL binary format.
Loader_code
Loader::read_stl_title(const unsigned char* data, size_t size, Vector3f& color) {
  std::string title(&data[0], &data[80]);
  auto pos = title.find("COLOR=");
  if ((pos != std::string::npos) && (pos < 70)) {
    pos += 6;
    Float red = static_cast<Float>(data[pos]) / 255.0f;
    Float green = static_cast<Float>(data[pos+1]) / 255.0f;
    Float blue = static_cast<Float>(data[pos+2]) / 255.0f;
    Float alpha = static_cast<Float>(data[pos+3]) / 255.0f;
    color.set(red, green, blue);
  }
  return Loader_code::SUCCESS;
}

//! \brief reads a scene graph from a stream in the STL binary format.
Loader_code Loader::read_stl_body(const unsigned char* data, size_t size,
                                  Scene_graph* scene_graph, Group* root,
                                  const Vector3f& color)
{
  // std::cout << "# size: " << size << std::endl;
  Int32 total_num_tris(*(reinterpret_cast<const Int32*>(data)));
  data += sizeof(Int32);
  // std::cout << "# triangles: " << total_num_tris << std::endl;
  // Header---80
  // # triangles---4
  // Triangle---50
  //   normal,v0,v1,v2---12*4
  //   spacer--2
  if (size != 84 + total_num_tris * 50) {
    throw Inconsistent_error(m_filename.c_str());
    return Loader_code::FAILURE;
  }

  // Read all triangles while discarding collinear triangles.
  std::list<Triangle> triangles;
  auto rc = read_triangles(data, size, triangles, total_num_tris);
  if (static_cast<int>(rc) < 0) return rc;

  // Construct shape nodes
  if (m_multiple_shapes) add_shapes(scene_graph, root, triangles, color);
  else {
    bool use_colors((color[0] != .0f) || (color[1] != .0f) ||(color[2] != .0f));
    if (use_colors) add_colored_shape(scene_graph, root, triangles, color);
    else add_shape(scene_graph, root, triangles);
  }

  return Loader_code::SUCCESS;
}

//! \brief loads a scene graph from an stl buffer.
Loader_code
Loader::load_stl(const unsigned char* data, size_t size, Scene_graph* sg,
                 Group* root) {
  sg->push_geometry_format(Geometry_format::STL);
  Vector3f color;
  auto rc = read_stl_title(data, size, color);
  if (static_cast<int>(rc) < 0) {
    sg->pop_geometry_format();
    return rc;
  }
  rc = read_stl_body(data+80, size, sg, root, color);
  sg->pop_geometry_format();
  if (static_cast<int>(rc) < 0) return rc;
  return Loader_code::SUCCESS;
}

//! \brief loads a scene graph from an stl buffer.
Loader_code Loader::load_stl(const unsigned char* data, size_t size,
                             const std::string& filename,
                             Scene_graph* scene_graph, Group* root)
{
  SGAL_assertion(! filename.empty());
  m_filename = filename;        // record the filename
  auto rc = load_stl(data, size, scene_graph, root);
  return rc;
}

//! \brief parses a scene from a stream.
Loader_code Loader::parse(std::istream& its, Scene_graph* sg, Group* root) {
  Vrml_scanner scanner(&its);
  scanner.set_filename(m_filename.c_str());
  // scanner.set_debug(1);

  // Parse & export:
  bool maybe_binary_stl(false);
  Vrml_parser parser(scanner, sg, root, maybe_binary_stl);
  auto rc = parser.parse();
  if (0 != rc) {
    if (maybe_binary_stl) return Loader_code::RETRY;
    throw Parse_error(m_filename.c_str());
    return Loader_code::FAILURE;
  }
  return Loader_code::SUCCESS;
}

//! \brief loads a scene from a string.
Loader_code Loader::operator()(const char* str, const std::string& filename,
                               Scene_graph* sg, Group* root) {
  SGAL_assertion(! filename.empty());
  m_filename = filename;        // record the filename

  boost::interprocess::bufferstream is(const_cast<char*>(str), std::strlen(str));
  if (! is.good()) {
    throw Overflow_error(m_filename.c_str());
    return Loader_code::FAILURE;
  }

  auto rc = operator()(is, sg, root);
  return rc;
}

//! \brief loads a scene from a string.
Loader_code Loader::operator()(const char* str, Scene_graph* sg, Group* root) {
  boost::interprocess::bufferstream is(const_cast<char*>(str), std::strlen(str));
  if (! is.good()) {
    throw Overflow_error(m_filename.c_str());
    return Loader_code::FAILURE;
  }

  auto rc = operator()(is, sg, root);
  return rc;
}

//! \brief loads a scene from binary data.
Loader_code Loader::operator()(const unsigned char* data, size_t size,
                               Scene_graph* scene_graph, Group* root) {
  const char* cdata = reinterpret_cast<const char*>(data);
  boost::interprocess::bufferstream is(const_cast<char*>(cdata), size);
  return operator()(is, scene_graph, root);
}

//! \brief loads a scene from binary data.
Loader_code Loader::operator()(const unsigned char* data, size_t size,
                               const std::string& filename,
                               Scene_graph* scene_graph, Group* root) {
  SGAL_assertion(! filename.empty());
  m_filename = filename;        // record the filename
  return operator()(data, size, scene_graph, root);
}

//! \bried read a traingle (1 normal and 3 vertices)
Loader_code Loader::read_triangle(std::istream& is, Triangle& triangle) {
  Float x, y, z;

  // Read normal
  is.read((char*)&x, sizeof(Float));
  is.read((char*)&y, sizeof(Float));
  is.read((char*)&z, sizeof(Float));

  // Read vertex 1:
  is.read((char*)&x, sizeof(Float));
  is.read((char*)&y, sizeof(Float));
  is.read((char*)&z, sizeof(Float));
  triangle.v0.set(x, y, z);

  // Read vertex 2:
  is.read((char*)&x, sizeof(Float));
  is.read((char*)&y, sizeof(Float));
  is.read((char*)&z, sizeof(Float));
  triangle.v1.set(x, y, z);

  // Read vertex 3:
  is.read((char*)&x, sizeof(Float));
  is.read((char*)&y, sizeof(Float));
  is.read((char*)&z, sizeof(Float));
  triangle.v2.set(x, y, z);

  is.read((char*)&(triangle.spacer), sizeof(unsigned short));
  // std::cout << std::hex << triangle.spacer << std::endl;

  if (!is) {
    throw Read_error(m_filename.c_str());
    return Loader_code::FAILURE;
  }
  return Loader_code::SUCCESS;
}

//! \brief reads all traingles.
Loader_code Loader::read_triangles(std::istream& is,
                                   std::list<Triangle>& triangles,
                                   size_t num_tris) {
  size_t i(0);
  while (i != num_tris) {
    Triangle triangle;
    do {
      auto rc = read_triangle(is, triangle);
      if (static_cast<int>(rc) < 0) return rc;
      ++i;
    } while (Vector3f::collinear(triangle.v0, triangle.v1, triangle.v2));
    triangles.push_back(triangle);
  }

  // Check end condition:
  if (! is) {
    throw Read_error(m_filename.c_str());
    return Loader_code::FAILURE;
  }
  return Loader_code::SUCCESS;
}

//! \bried read a traingle (1 normal and 3 vertices)
Loader_code
Loader::read_triangle(const unsigned char* data, size_t size,
                      Triangle& triangle) {
  // Read normal
  Float x(*(reinterpret_cast<const Float*>(data))); data += sizeof(Float);
  Float y(*(reinterpret_cast<const Float*>(data))); data += sizeof(Float);
  Float z(*(reinterpret_cast<const Float*>(data))); data += sizeof(Float);

  // Read vertex 1:
  x = *(reinterpret_cast<const Float*>(data)); data += sizeof(Float);
  y = *(reinterpret_cast<const Float*>(data)); data += sizeof(Float);
  z = *(reinterpret_cast<const Float*>(data)); data += sizeof(Float);
  triangle.v0.set(x, y, z);

  // Read vertex 2:
  x = *(reinterpret_cast<const Float*>(data)); data += sizeof(Float);
  y = *(reinterpret_cast<const Float*>(data)); data += sizeof(Float);
  z = *(reinterpret_cast<const Float*>(data)); data += sizeof(Float);
  triangle.v1.set(x, y, z);

  // Read vertex 3:
  x = *(reinterpret_cast<const Float*>(data)); data += sizeof(Float);
  y = *(reinterpret_cast<const Float*>(data)); data += sizeof(Float);
  z = *(reinterpret_cast<const Float*>(data)); data += sizeof(Float);
  triangle.v2.set(x, y, z);

  triangle.spacer = *(reinterpret_cast<const unsigned short*>(data));
  // std::cout << std::hex << triangle.spacer << std::endl;

  return Loader_code::SUCCESS;
}

//! \brief reads all traingles.
Loader_code Loader::read_triangles(const unsigned char* data, size_t size,
                                   std::list<Triangle>& triangles,
                                   size_t num_tris) {
  size_t i(0);
  while (i != num_tris) {
    Triangle triangle;
    do {
      auto rc = read_triangle(data, size, triangle);
      data += sizeof(Float) * 12 + sizeof(unsigned short);
      if (static_cast<int>(rc) < 0) return rc;
      ++i;
    } while (Vector3f::collinear(triangle.v0, triangle.v1, triangle.v2));
    triangles.push_back(triangle);
  }

  return Loader_code::SUCCESS;
}

//! \brief adds a single shape for all triangles.
void Loader::add_shape(Scene_graph* scene_graph, Group* group,
                       std::list<Triangle>& tris)
{
  Vector3f color;       // Place holder
  auto shape = create_shape(scene_graph, color);
  group->add_child(shape);
  auto ifs = create_ifs(scene_graph, tris.size(), tris.begin(), tris.end());
  shape->set_geometry(ifs);
}

/*! \brief adds a shape for every sub sequence of triangles with a distinguish
 * color.
 */
void Loader::add_shapes(Scene_graph* scene_graph, Group* group,
                        std::list<Triangle>& triangles, const Vector3f& color)
{
  auto it = triangles.begin();
  auto first = it;
  const Triangle& triangle = *it++;
  size_t count(1);

  Vector3f last_color(color);
  if (triangle.is_colored()) triangle.set_color(last_color);

  while (it != triangles.end()) {
    const Triangle& triangle = *it;

    Vector3f tmp_color(color);
    if (triangle.is_colored()) triangle.set_color(tmp_color);

    if (tmp_color != last_color) {
      auto shape = create_shape(scene_graph, last_color);
      group->add_child(shape);
      auto ifs = create_ifs(scene_graph, count, first, it);
      shape->set_geometry(ifs);

      first = it;
      count = 0;
      last_color.set(tmp_color);
    }
    ++it;
    ++count;
  }
  auto shape = create_shape(scene_graph, last_color);
  group->add_child(shape);
  auto ifs = create_ifs(scene_graph, count, first, triangles.end());
  shape->set_geometry(ifs);
}

//! \brief adds a colored shape.
void Loader::add_colored_shape(Scene_graph* scene_graph, Group* group,
                               std::list<Triangle>& triangles,
                               const Vector3f& color)
{
  auto count = triangles.size();

  Shared_indexed_face_set ifs(new Indexed_face_set);
  SGAL_assertion(ifs);
  ifs->set_primitive_type(Geo_set::PT_TRIANGLES);
  ifs->set_num_primitives(count);

  auto num_vertices = count * 3;
  Coord_array_3d* coords = new Coord_array_3d(num_vertices);
  Shared_coord_array_3d shared_coords(coords);
  ifs->set_coord_array(shared_coords);
  auto& indices = ifs->get_empty_triangle_coord_indices();
  indices.resize(count);

  auto* colors = new Color_array_3d(count);
  Shared_color_array_3d shared_colors(colors);
  ifs->set_color_array(shared_colors);
  Flat_indices color_indices(count);
  ifs->set_color_attachment(Mesh_set::AT_PER_PRIMITIVE);

  size_t i(0);
  Uint j(0);

  for (auto& triangle : triangles) {
    Vector3f use_color(color);
    if (triangle.is_colored()) triangle.set_color(use_color);
    (*colors)[i].set(use_color);
    color_indices[i] = i;

    (*coords)[j].set(triangle.v0);
    indices[i][0] = j++;

    (*coords)[j].set(triangle.v1);
    indices[i][1] = j++;

    (*coords)[j].set(triangle.v2);
    indices[i][2] = j++;

    ++i;
  }
  auto shape = create_shape(scene_graph, color);
  group->add_child(shape);
  ifs->facet_coord_indices_changed();
  ifs->set_facet_color_indices(std::move(color_indices));
  ifs->set_color_attachment(Geo_set::AT_PER_PRIMITIVE);
  ifs->collapse_identical_coordinates();
  ifs->add_to_scene(scene_graph);
  shape->set_geometry(ifs);
}

/*! Convert polygons to triangles and clear the polygons.
 */
void to_tri_indices(Polygon_indices& indices, Triangle_indices& tri_indices)
{
  tri_indices.resize(indices.size());
  for (auto i = 0; i < tri_indices.size(); ++i) {
    tri_indices[i][0] = indices[i][0];
    tri_indices[i][1] = indices[i][1];
    tri_indices[i][2] = indices[i][2];
    indices[i].clear();
  }
  indices.clear();
}

/*! Convert polygons to quadrilaterals and clear the polygons.
 */
void to_quad_indices(Polygon_indices& indices, Quad_indices& quad_indices)
{
  quad_indices.resize(indices.size());
  for (auto i = 0; i < quad_indices.size(); ++i) {
    quad_indices[i][0] = indices[i][0];
    quad_indices[i][1] = indices[i][1];
    quad_indices[i][2] = indices[i][2];
    quad_indices[i][3] = indices[i][3];
    indices[i].clear();
  }
  indices.clear();
}

// istream modifier skips chars until end of line.
std::istream& skip_until_eol(std::istream& in) {
  if (in.eof()) return in;
  char c;
  while (in.get(c) && (c != '\n'));
  return in;
}

// istream modifier that checks for OFF comments and removes them.
std::istream& skip_comment_off(std::istream& in) {
  char c;
  while ((in >> c) && (c == '#')) in >> skip_until_eol;
  in.putback(c);
  return in;
}

//! \brief loads a face from an OFF file.
Loader_code Loader::load_off_facet(std::istream& is,
                                   Polygon_indices& coord_ids,
                                   Index_type id) const {
  is >> skip_comment_off;
  size_t n;
  is >> n;
  SGAL_assertion(2 != n);
  coord_ids[id].resize(n);
  for (auto j = 0; j < n; ++j) is >> coord_ids[id][j];
  return Loader_code::SUCCESS;
}

//! \brief loads a color from an OFF file.
//! \todo Handle the following color options
//  Line breaks are significant here: the color description begins after
// the index of the last vertex and ends with the end of the line (or the
// next # comment).
// nothing---the default color
// 1 integer---index into "the" colormap
// 3 or 4 integers---RGB and possibly alpha values in the range 0..255
// 3 or 4 floating-point numbers
//   RGB and possibly alpha values in the range 0..1
// Import into strings. Search for '.'....
Loader_code Loader::load_off_color(std::istream& is, Vector3f& color) const {
  float r, g, b;
  is >> r >> g >> b;
  color.set(r, g, b);
  return Loader_code::SUCCESS;
}

/*! \brief loads a scene graph represented in the off file format from a stream.
 * Observe that the magic string has been consumed.
 * \param what
 *    what[0]---entire string
 *    what[1]---texture coordinate
 *    what[2]---color
 *    what[3]---normal
 *    what[4]---4 components including a final homogeneous component
 *    what[5]---n components
 */
Loader_code Loader::load_off(std::istream& is, const boost::smatch& what,
                             Scene_graph* sg, Group* root) {
  sg->push_geometry_format(Geometry_format::OFF);

  // Consume the magic string
  std::string line;
  std::getline(is, line);
  bool has_vcolors = what.length(2);

  // Add Shape
  auto shape = create_shape(sg);
  root->add_child(shape);

  // Add Appearance
  auto app = create_appearance(sg);
  shape->set_appearance(app);

  // Add IndexedFaceSet
  auto ifs = create_ifs(sg);
  shape->set_geometry(ifs);

  size_t num_vertices;
  size_t num_facets;
  size_t num_edges;
  is >> skip_comment_off;
  is >> num_vertices >> num_facets >> num_edges;
  // std::cout << num_vertices << std::endl;
  // std::cout << num_facets << std::endl;
  // std::cout << num_edges << std::endl;

  auto coords = create_coord_array_3d(sg, num_vertices);

  Shared_color_array_3d colors(nullptr);
  // float scale(1.0 / 255.0);
  if (has_vcolors) colors = create_color_array_3d(sg, num_vertices);

  for (auto i = 0; i < num_vertices; ++i) {
    float x, y, z;
    is >> skip_comment_off;
    is >> x >> y >> z;
    (*coords)[i].set(x, y, z);

    if (has_vcolors) load_off_color(is, (*colors)[i]);
  }

  bool has_fcolors(false);

  Polygon_indices coord_ids;
  coord_ids.resize(num_facets);
  load_off_facet(is, coord_ids, 0);             // read first facet

  // Check whether colors exist
  std::string col;
  std::getline(is, col);
  std::istringstream iss(col);
  char ci;
  //! Fix the test for facet colors.
  // It's possible that the succeeding character is a white space or the
  // beginning of a comment ('#'); see explanation bellow.
  if (iss >> ci) {
    has_fcolors = true;
    colors = create_color_array_3d(sg, num_facets);
    load_off_color(is, (*colors)[0]);
  }

  // Read remaining facets
  for (auto i = 1; i < num_facets; ++i) {
    load_off_facet(is, coord_ids, i);
    if (has_fcolors) load_off_color(is, (*colors)[i]);
  }

  ifs->set_coord_array(coords);
  if (colors && ! colors->empty()) ifs->set_color_array(colors);
  auto rc = update_ifs(sg, ifs, coord_ids);

  sg->pop_geometry_format();
  if (static_cast<int>(rc) < 0) return Loader_code::FAILURE;
  return Loader_code::SUCCESS;
}

//! \brief creates a new Coord_array_3d container.
Loader::Shared_epec_coord_array_3d
Loader::create_epec_coord_array_3d(Scene_graph* sg, size_t num) const {
  auto coords = (num != 0) ?
    Shared_epec_coord_array_3d(new Epec_coord_array_3d(num)) :
    Shared_epec_coord_array_3d(new Epec_coord_array_3d);
  SGAL_assertion(coords);
  coords->add_to_scene(sg);
  sg->add_container(coords);
  return coords;
}

/*! \brief loads a scene graph represented in the eoff file format from a
 * stream.
 * Observe that the magic string has been consumed.
 * \param what
 *    what[0]---entire string
 *    what[1]---texture coordinate
 *    what[2]---color
 *    what[3]---normal
 *    what[4]---4 components including a final homogeneous component
 *    what[5]---n components
 */
Loader_code Loader::load_eoff(std::istream& is, const boost::smatch& what,
                              Scene_graph* sg, Group* root) {
  sg->push_geometry_format(Geometry_format::OFF);

  // Consume the magic string
  std::string line;
  std::getline(is, line);
  bool has_vcolors = what.length(2);

  // Add Shape
  auto shape = create_shape(sg);
  root->add_child(shape);

  // Add Appearance
  auto app = create_appearance(sg);
  shape->set_appearance(app);

  // Add IndexedFaceSet
  auto ifs = create_ifs(sg);
  shape->set_geometry(ifs);

  size_t num_vertices;
  size_t num_facets;
  size_t num_edges;
  is >> skip_comment_off;
  is >> num_vertices >> num_facets >> num_edges;
  // std::cout << num_vertices << std::endl;
  // std::cout << num_facets << std::endl;
  // std::cout << num_edges << std::endl;

  auto coords = create_epec_coord_array_3d(sg, num_vertices);

  Shared_color_array_3d colors(nullptr);
  // float scale(1.0 / 255.0);
  if (has_vcolors) colors = create_color_array_3d(sg, num_vertices);

  for (auto i = 0; i < num_vertices; ++i) {
    is >> skip_comment_off;
    Epec_RT nom, den;
    CGAL::Rational_traits<Epec_FT> tr;
    is >> nom >> den;
    Epec_FT x = tr.make_rational(nom, den);
    is >> nom >> den;
    Epec_FT y = tr.make_rational(nom, den);
    is >> nom >> den;
    Epec_FT z = tr.make_rational(nom, den);
    (*coords)[i] = Epec_point_3(x, y, z);

    if (has_vcolors) load_off_color(is, (*colors)[i]);
  }

  bool has_fcolors(false);

  Polygon_indices coord_ids;
  coord_ids.resize(num_facets);
  load_off_facet(is, coord_ids, 0);             // read first facet

  // Check whether colors exist
  std::string col;
  std::getline(is, col);
  std::istringstream iss(col);
  char ci;
  //! Fix the test for facet colors.
  // It's possible that the succeeding character is a white space or the
  // beginning of a comment ('#'); see explanation bellow.
  if (iss >> ci) {
    has_fcolors = true;
    colors = create_color_array_3d(sg, num_facets);
    load_off_color(is, (*colors)[0]);
  }

  // Read remaining facets
  for (auto i = 1; i < num_facets; ++i) {
    load_off_facet(is, coord_ids, i);
    if (has_fcolors) load_off_color(is, (*colors)[i]);
  }

  ifs->set_coord_array(coords);
  if (colors && ! colors->empty()) ifs->set_color_array(colors);
  auto rc = update_ifs(sg, ifs, coord_ids);

  sg->pop_geometry_format();
  if (static_cast<int>(rc) < 0) return Loader_code::FAILURE;
  return Loader_code::SUCCESS;
}

/*! Obtain the number represented by a symbol.
 */
template <typename Number_type>
bool get_number(const Vrml_parser::symbol_type& symbol, Number_type& number) {
  if (symbol.type_get() != Vrml_parser::symbol_kind::S_NUMBER) {
    std::cerr << "Error at " << symbol.location << ": "
              << "invalid symbol" << std::endl;
    return false;
  }
  Shared_string number_str(symbol.value.as<Shared_string>());
  number = boost::lexical_cast<Number_type>(*number_str);
  return true;
}

/*! Read a number from a scanner.
 */
template <typename Number_type>
bool read_number(Vrml_scanner& scanner, Number_type& number)
{
  Vrml_parser::symbol_type symbol(scanner.mylex());
  return get_number(symbol, number);
}

/*! Obtain the string represented by a symbol.
 */
bool get_string(const Vrml_parser::symbol_type& symbol,
                Shared_string& str)
{
  if (symbol.type_get() != Vrml_parser::symbol_kind::S_IDENTIFIER) {
    std::cerr << "Error at " << symbol.location << ": "
              << "invalid string" << std::endl;
    return false;
  }
  str = symbol.value.as<Shared_string>();
  return true;
}

/*! Read a string from a scanner.
 */
bool read_string(Vrml_scanner& scanner, Shared_string& str)
{
  Vrml_parser::symbol_type symbol(scanner.mylex());
  return get_string(symbol, str);
}

/*! Read a line.
 */
bool read_obj_line(Vrml_scanner& scanner)
{
  while (true) {
    Vrml_parser::symbol_type symbol(scanner.mylex());
    if (symbol.type_get() == Vrml_parser::symbol_kind::S_K_LINE_END) break;
  }
  return true;
}

/*! Read coords and perhaps colors from an obj file.
 */
bool read_obj_coords_and_colors(Vrml_scanner& scanner, Coord_array_3d* coords,
                                Color_array_3d* colors)
{
  Vrml_parser::location_type loc;
  std::array<float, 6> nums;
  size_t j(0);
  while (true) {
    Vrml_parser::symbol_type num_symbol(scanner.mylex());
    if (num_symbol.type_get() == Vrml_parser::symbol_kind::S_K_LINE_END) break;
    loc = num_symbol.location;
    if (j == 6) {
      std::cerr << "Warning: more than 6 coordinates at " << loc
                << ". Remaining coordinates are ignored!" << std::endl;
      break;
    }
    if (! get_number(num_symbol, nums[j++])) return false;
  }

  if ((j < 3) || (j == 5)) {
    std::cerr << "Error at " << loc << ": "
              << "wrong number of coordinates" << std::endl;
    return false;
  }
  if (j == 4) {
    nums[0] /= nums[3];
    nums[1] /= nums[3];
    nums[2] /= nums[3];
  }
  Vector3f v(nums[0], nums[1], nums[2]);
  coords->push_back(v);
  if (j == 6) {
    Vector3f c(nums[3], nums[4], nums[5]);
    colors->push_back(c);
  }
  return true;
}

/*! Read texture coordinates from an obj file.
 */
bool read_obj_tex_coords(Vrml_scanner& scanner, Tex_coord_array_2d* tex_coords)
{
  Vrml_parser::location_type loc;
  std::array<float, 6> nums;
  size_t j(0);
  while (true) {
    Vrml_parser::symbol_type num_symbol(scanner.mylex());
    if (num_symbol.type_get() == Vrml_parser::symbol_kind::S_K_LINE_END) break;
    loc = num_symbol.location;
    if (j == 4) {
      std::cerr << "Warning: more than 4 texture coordinates at " << loc
                << ". Remaining coordinates are ignored!" << std::endl;
      break;
    }
    if (! get_number(num_symbol, nums[j++])) return false;
  }

  if (j < 2) {
    std::cerr << "Error at " << loc << ": "
              << "insufficient number of texture coordinates" << std::endl;
    return false;
  }
  if (j == 3) {
    nums[0] /= nums[2];
    nums[1] /= nums[2];
  }
  Vector2f v(nums[0], nums[1]);
  tex_coords->push_back(v);
  return true;
}

/*! Read normals from an obj file.
 */
bool read_obj_normals(Vrml_scanner& scanner, Normal_array* normals)
{
  Vrml_parser::location_type loc;
  std::array<float, 3> nums;
  size_t j(0);
  while (true) {
    Vrml_parser::symbol_type num_symbol(scanner.mylex());
    if (num_symbol.type_get() == Vrml_parser::symbol_kind::S_K_LINE_END) break;
    loc = num_symbol.location;
    if (j == 4) {
      std::cerr << "Warning: more than 4 normal coordinates at " << loc
                << ". Remaining normals coordinates are ignored!" << std::endl;
      break;
    }
    if (! get_number(num_symbol, nums[j++])) return false;
  }

  if (j < 3) {
    std::cerr << "Error at " << loc << ": "
              << "insufficient number of normal coordinates" << std::endl;
    return false;
  }
  Vector3f n(nums[0], nums[1], nums[2]);
  normals->push_back(n);
  return true;
}

/*! Read indices of coordinates from an obj file.
 */
bool read_obj_facet(Vrml_scanner& scanner,
                    Polygon_indices& coord_indices,
                    Polygon_indices& normal_indices,
                    Polygon_indices& tex_coord_indices,
                    size_t num_coords)
{
  coord_indices.resize(coord_indices.size()+1);
  auto& facet_coord_indices = coord_indices.back();
  facet_coord_indices.reserve(3);

  Index_array facet_normal_indices;
  facet_normal_indices.reserve(3);
  Index_array facet_tex_coord_indices;
  facet_tex_coord_indices.reserve(3);
  size_t num_slashes(0);
  while (true) {
    Vrml_parser::symbol_type v_symbol(scanner.mylex());
    if (v_symbol.type_get() == Vrml_parser::symbol_kind::S_K_LINE_END) break;
    if (v_symbol.type_get() == Vrml_parser::symbol_kind::S_SLASH) {
      ++num_slashes;
      continue;
    }
    auto loc = v_symbol.location;
    int i;
    if (! get_number(v_symbol, i)) return false;
    if (0 == i) {
      std::cerr << "Error at " << v_symbol.location << ": "
                << "zero index is illegal" << std::endl;
      return false;
    }
    size_t index = (i > 0) ? i - 1 : num_coords + i;
    if (num_slashes == 0) facet_coord_indices.push_back(index);
    else if (num_slashes == 1) facet_tex_coord_indices.push_back(index);
    else if (num_slashes == 2) {
      facet_normal_indices.push_back(index);
      num_slashes = 0;
    }
    else {
      SGAL_error();
      return false;
    }
  }
  if (!facet_normal_indices.empty())
    normal_indices.push_back(std::move(facet_normal_indices));
  if (!facet_tex_coord_indices.empty())
    tex_coord_indices.push_back(std::move(facet_tex_coord_indices));
  return true;
}

/*! Read material library.
 */
Loader_code Loader::parse_mtl(const std::string& filename, Scene_graph* sg) {
  std::string fullname;
  std::list<fi::path> dirs;
  dirs.push_back(".");
  fi::path dir(m_filename);
  dirs.push_back(dir.parent_path());
  find_file(filename, dirs.begin(), dirs.end(), fullname);
  if (fullname.empty()) {
    throw Find_file_error(filename);
    return Loader_code::FAILURE;
  }
  // Open source file.
  std::ifstream mtl_is(fullname);
  if (!mtl_is.good()) {
    throw Open_file_error(fullname);
    return Loader_code::FAILURE;
  }

  Vrml_scanner scanner(&mtl_is);
  scanner.push_state(6);
  Shared_appearance app;
  Shared_material mat;
  bool done(false);
  while (!done) {
    Vrml_parser::symbol_type symbol(scanner.mylex());
    switch (symbol.type_get()) {
     case Vrml_parser::token::TOK_END: done = true; break;
     case Vrml_parser::token::TOK_K_NEW_MATERIAL:
      {
        Shared_string name;
        if (! read_string(scanner, name)) return Loader_code::FAILURE;
        app = create_appearance(sg, *name);
        mat = create_material(sg);
        app->set_material(mat);
      }
      break;

     case Vrml_parser::symbol_kind::S_K_SPECULAR_EXPONENT:
      {
        float number;
        if (! read_number(scanner, number)) return Loader_code::FAILURE;
        std::cerr << "Specular Exponent not supported yet!" << std::endl;
      }
      break;

     case Vrml_parser::symbol_kind::S_K_AMBIENT_COLOR:
      {
        float r, g, b;
        if (! read_number(scanner, r)) return Loader_code::FAILURE;
        if (! read_number(scanner, g)) return Loader_code::FAILURE;
        if (! read_number(scanner, b)) return Loader_code::FAILURE;
        if ((r != 1) && (g != 1) & (b != 1)) mat->set_ambient_color(r, g, b);
      }
      break;

     case Vrml_parser::symbol_kind::S_K_DIFFUSE_COLOR:
      {
        float r, g, b;
        if (! read_number(scanner, r)) return Loader_code::FAILURE;
        if (! read_number(scanner, g)) return Loader_code::FAILURE;
        if (! read_number(scanner, b)) return Loader_code::FAILURE;
        mat->set_diffuse_color(r, g, b);
      }
      break;

     case Vrml_parser::symbol_kind::S_K_SPECULAR_COLOR:
      {
        float r, g, b;
        if (! read_number(scanner, r)) return Loader_code::FAILURE;
        if (! read_number(scanner, g)) return Loader_code::FAILURE;
        if (! read_number(scanner, b)) return Loader_code::FAILURE;
        mat->set_specular_color(r, g, b);
      }
      break;

     case Vrml_parser::symbol_kind::S_K_EMISSIVE_COLOR:
      {
        float r, g, b;
        if (! read_number(scanner, r)) return Loader_code::FAILURE;
        if (! read_number(scanner, g)) return Loader_code::FAILURE;
        if (! read_number(scanner, b)) return Loader_code::FAILURE;
        mat->set_emissive_color(r, g, b);
      }
      break;

     case Vrml_parser::symbol_kind::S_K_OPTICAL_DENSITY:
      {
        float number;
        if (! read_number(scanner, number)) return Loader_code::FAILURE;
        std::cerr << "Optical Density not supported yet!" << std::endl;
      }
      break;

     case Vrml_parser::symbol_kind::S_K_DISSOLVE:
      {
        float number;
        if (! read_number(scanner, number)) return Loader_code::FAILURE;
        mat->set_transparency(1.0f - number);
      }
      break;

     case Vrml_parser::symbol_kind::S_K_TRANSPARENCY:
      {
        float number;
        if (! read_number(scanner, number)) return Loader_code::FAILURE;
        mat->set_transparency(number);
      }
      break;

     case Vrml_parser::symbol_kind::S_K_ILLUMINATION:
      {
        float number;
        if (! read_number(scanner, number)) return Loader_code::FAILURE;
        std::cerr << "illumination not supported yet!" << std::endl;
      }
      break;

     default: break;
    }
  }
  scanner.pop_state();
  mtl_is.close();
  return Loader_code::SUCCESS;
}

/*! Update an IndexedFaceSet container.
 */
Loader_code Loader::update_ifs(Scene_graph* sg,
                               Shared_indexed_face_set ifs,
                               Polygon_indices& coord_indices,
                               Polygon_indices& color_indices,
                               Polygon_indices& normal_indices,
                               Polygon_indices& tex_coord_indices) const {
  auto num_primitives = coord_indices.size();

  if (! normal_indices.empty()) ifs->set_normal_per_vertex(true);
  else if (! color_indices.empty()) ifs->set_color_per_vertex(true);

  // Convert to triangles or quads if possible.
  Boolean tris(true), quads(true);
  for (const auto& polygon : coord_indices) {
    auto n = polygon.size();
    SGAL_assertion(2 != n);
    if (3 != n) tris = false;
    if (4 != n) quads = false;
  }

  if (tris) {
    ifs->set_primitive_type(Geo_set::PT_TRIANGLES);
    auto& tri_coord_indices = ifs->get_empty_triangle_coord_indices();
    to_tri_indices(coord_indices, tri_coord_indices);
    if (! normal_indices.empty()) {
      auto& tri_normal_indices = ifs->get_empty_triangle_normal_indices();
      to_tri_indices(normal_indices, tri_normal_indices);
    }
    if (! color_indices.empty()) {
      auto& tri_color_indices = ifs->get_empty_triangle_color_indices();
      to_tri_indices(color_indices, tri_color_indices);
    }
    if (! tex_coord_indices.empty()) {
      auto& tri_tex_coord_indices = ifs->get_empty_triangle_tex_coord_indices();
      to_tri_indices(tex_coord_indices, tri_tex_coord_indices);
    }
  }
  else if (quads) {
    ifs->set_primitive_type(Geo_set::PT_QUADS);
    auto& quad_coord_indices = ifs->get_empty_quad_coord_indices();
    to_quad_indices(coord_indices, quad_coord_indices);
    if (! normal_indices.empty()) {
      auto& quad_normal_indices = ifs->get_empty_quad_normal_indices();
      to_quad_indices(normal_indices, quad_normal_indices);
    }
    if (! color_indices.empty()) {
      auto& quad_color_indices = ifs->get_empty_quad_color_indices();
      to_quad_indices(color_indices, quad_color_indices);
    }
    if (! tex_coord_indices.empty()) {
      auto& quad_tex_coord_indices = ifs->get_empty_quad_tex_coord_indices();
      to_quad_indices(tex_coord_indices, quad_tex_coord_indices);
    }
  }
  else {
    ifs->set_primitive_type(Geo_set::PT_POLYGONS);
    ifs->set_facet_coord_indices(std::move(coord_indices));
    if (! normal_indices.empty())
      ifs->set_facet_normal_indices(std::move(normal_indices));
    if (! color_indices.empty())
      ifs->set_facet_color_indices(std::move(color_indices));
    if (! tex_coord_indices.empty())
      ifs->set_facet_tex_coord_indices(std::move(tex_coord_indices));
  }

  ifs->set_num_primitives(num_primitives);
  ifs->facet_coord_indices_changed();

  return Loader_code::SUCCESS;
}

//! \brief updates an IndexedFaceSet container.
Loader_code Loader::update_ifs(Scene_graph* sg, Shared_indexed_face_set ifs,
                               Polygon_indices& coord_ids) const {
  Polygon_indices color_ids;
  Polygon_indices normal_ids;
  Polygon_indices tex_coord_ids;
  return update_ifs(sg, ifs, coord_ids, color_ids, normal_ids, tex_coord_ids);
}

// \brief loads a scene graph represented in the obj file format from a stream.
Loader_code Loader::parse_obj(std::istream& is, Scene_graph* sg, Group* root) {
  sg->push_geometry_format(Geometry_format::OBJ);

  // Construct arrays
  auto coords = create_coord_array_3d(sg);
  auto normals = create_normal_array(sg);
  auto colors = create_color_array_3d(sg);
  auto tex_coords = create_tex_coord_array_2d(sg);

  // Start scanning
  Vrml_scanner scanner(&is);
  scanner.push_state(4);
  bool done(false);
  Shared_appearance app;                        // define a place holder
  Loader_code rc;
  while (! done) {
    bool done_shape(false);

    // Create new containers
    auto shape = create_shape(sg);              // create a new Shape node
    root->add_child(shape);                     // add the new Shape node
    auto ifs = create_ifs(sg);                  // create a new Ifs container
    Polygon_indices coord_ids;
    Polygon_indices color_ids;
    Polygon_indices normal_ids;
    Polygon_indices tex_coord_ids;

    while (! done && ! done_shape) {
      Vrml_parser::symbol_type symbol(scanner.mylex());
      switch (symbol.type_get()) {
       case Vrml_parser::token::TOK_END:
        if (app) shape->set_appearance(app);
        done = true;
        break;

       case Vrml_parser::token::TOK_K_VERTEX:
        if (! read_obj_coords_and_colors(scanner, &*coords, &*colors))
          return Loader_code::FAILURE;
        break;

       case Vrml_parser::token::TOK_K_TEXTURE_COORDINATE:
        if (! read_obj_tex_coords(scanner, &*tex_coords))
          return Loader_code::FAILURE;
        break;

       case Vrml_parser::token::TOK_K_NORMAL:
        if (! read_obj_normals(scanner, &*normals)) return Loader_code::FAILURE;
        break;

       case Vrml_parser::token::TOK_K_FACET:
        if (! read_obj_facet(scanner, coord_ids, normal_ids,
                             tex_coord_ids, coords->size()))
          return Loader_code::FAILURE;
        break;

       case Vrml_parser::token::TOK_K_SMOOTH:
        if (! read_obj_line(scanner)) return Loader_code::FAILURE;
        break;

       case Vrml_parser::token::TOK_K_MATERIAL_LIB:
        {
          Shared_string name;
          if (! read_string(scanner, name)) return Loader_code::FAILURE;
          auto rc = parse_mtl(*name, sg);
          if (static_cast<int>(rc) < 0) return Loader_code::FAILURE;
        }
        break;

       case Vrml_parser::token::TOK_K_USE_MATERIAL:
        {
          // If an appearance exists, set it as the appearance of the current
          // shape, and mark it done.
          if (app) {
            shape->set_appearance(app);
            done_shape = true;
          }
          // Record the new appearance.
          Shared_string name;
          if (! read_string(scanner, name)) return Loader_code::FAILURE;
          auto cont = sg->get_container(*name);
          app = boost::dynamic_pointer_cast<Appearance>(cont);
          if (!app) {
            std::cerr << "Warning: Appearance " << "\"" << *name << "\""
                      << " is not defined" << std::endl;
          }
          break;
        }
        break;

       default: break;
      }
    }
    // Finsh creating a Shape node
    ifs->set_coord_array(coords);
    if (normals && ! normals->empty()) ifs->set_normal_array(normals);
    if (colors && ! colors->empty()) ifs->set_color_array(colors);
    if (tex_coords && ! tex_coords->empty())
      ifs->set_tex_coord_array(tex_coords);
    rc = update_ifs(sg, ifs, coord_ids, color_ids, normal_ids, tex_coord_ids);
    if (static_cast<int>(rc) < 0) break;

    SGAL_assertion(normal_ids.empty() || (normals && ! normals->empty()));
    SGAL_assertion(color_ids.empty() || (colors && ! colors->empty()));

    if (! app) {
      app = create_appearance(sg);
      shape->set_appearance(app);
    }
    shape->set_geometry(ifs);
    if (done) break;
  }
  scanner.pop_state();

  sg->pop_geometry_format();

  if (static_cast<int>(rc) < 0) return Loader_code::FAILURE;
  return Loader_code::SUCCESS;
}

//! \brief registers a loader.
void Loader::doregister(const String& extension, Base_loader* loader)
{ m_loaders[extension] = loader; }

//! \brief unregisters a registered loader.
Base_loader* Loader::unregister(const String& extension)
{
  auto it = m_loaders.find(extension);
  if (it == m_loaders.end()) return nullptr;
  auto* base_loader = it->second;
  m_loaders.erase(it);
  return base_loader;
}

SGAL_END_NAMESPACE
