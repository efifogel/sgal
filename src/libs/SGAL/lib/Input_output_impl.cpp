// Copyright (c) 2020 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel        <efifogel@gmail.com>

#include "SGAL/basic.hpp"
#include "SGAL/Input_output.hpp"

SGAL_BEGIN_NAMESPACE

//! \brief adds the container to a given scene.
void Input_output::add_to_scene(Scene_graph* sg)
{ Container::add_to_scene(sg); }

void Input_output::reset(const String& export_scene_root,
                         Uint export_scene_level,
                         Boolean export_non_visible,
                         Boolean override_export_non_visible,
                         Boolean export_flat)
{
  m_export_scene_root = export_scene_root;
  m_export_scene_level = export_scene_level;
  m_export_non_visible = export_non_visible;
  m_override_export_non_visible = override_export_non_visible;
  m_export_flat = export_flat;
}

SGAL_END_NAMESPACE
