// Copyright (c) 2021 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Author(s): Efi Fogel        <efifogel@gmail.com>

#include "SGAL/basic.hpp"
#include "SGAL/Context.hpp"
#include "SGAL/Coord_array.hpp"
#include "SGAL/Coord_array_3d.hpp"
#include "SGAL/Draw_action.hpp"
#include "SGAL/Point_set.hpp"
#include "SGAL/Indexed_point_set_mask.hpp"

SGAL_BEGIN_NAMESPACE

//! \brief initializes
void Point_set::init(Boolean proto) {
  m_primitive_type = PT_POINTS;
  if (get_color_attachment() == AT_PER_PRIMITIVE) set_color_attachment(AT_PER_VERTEX);
  if (! proto) return;

  for (auto i = 0; i < PO_NUM_DRAWS; ++i)
    s_draws[i] = &Point_set::draw_invalid;

  s_draws[SGAL_CINO_FANO_TENO_VANO] = &Point_set::draw_CINO_FANO_TENO_VANO;
  s_draws[SGAL_CINO_FANO_TEYE_TINO_VANO] = &Point_set::draw_CINO_FANO_TEYE_TINO_VANO;
  s_draws[SGAL_CINO_FANO_TEYE_TIYE_VANO] = &Point_set::draw_CINO_FANO_TEYE_TIYE_VANO;
  s_draws[SGAL_CIYE_FANO_TENO_VANO] = &Point_set::draw_CIYE_FANO_TENO_VANO;
  s_draws[SGAL_CIYE_FANO_TEYE_TINO_VANO] = &Point_set::draw_CIYE_FANO_TEYE_TINO_VANO;
  s_draws[SGAL_CIYE_FANO_TEYE_TIYE_VANO] = &Point_set::draw_CIYE_FANO_TEYE_TIYE_VANO;
  s_draws[SGAL_CINO_FSNO_FINO_FAPV_TENO_VANO] = &Point_set::draw_CINO_FSNO_FINO_FAPV_TENO_VANO;
  s_draws[SGAL_CINO_FSNO_FINO_FAPV_TEYE_TINO_VANO] = &Point_set::draw_CINO_FSNO_FINO_FAPV_TEYE_TINO_VANO;
  s_draws[SGAL_CINO_FSNO_FINO_FAPV_TEYE_TIYE_VANO] = &Point_set::draw_CINO_FSNO_FINO_FAPV_TEYE_TIYE_VANO;
  s_draws[SGAL_CINO_FSNO_FINO_FAPS_TENO_VANO] = &Point_set::draw_CINO_FSNO_FINO_FAPS_TENO_VANO;
  s_draws[SGAL_CINO_FSNO_FINO_FAPS_TEYE_TINO_VANO] = &Point_set::draw_CINO_FSNO_FINO_FAPS_TEYE_TINO_VANO;
  s_draws[SGAL_CINO_FSNO_FINO_FAPS_TEYE_TIYE_VANO] = &Point_set::draw_CINO_FSNO_FINO_FAPS_TEYE_TIYE_VANO;
  s_draws[SGAL_CINO_FSNO_FIYE_FAPV_TENO_VANO] = &Point_set::draw_CINO_FSNO_FIYE_FAPV_TENO_VANO;
  s_draws[SGAL_CINO_FSNO_FIYE_FAPV_TEYE_TINO_VANO] = &Point_set::draw_CINO_FSNO_FIYE_FAPV_TEYE_TINO_VANO;
  s_draws[SGAL_CINO_FSNO_FIYE_FAPV_TEYE_TIYE_VANO] = &Point_set::draw_CINO_FSNO_FIYE_FAPV_TEYE_TIYE_VANO;
  s_draws[SGAL_CINO_FSNO_FIYE_FAPS_TENO_VANO] = &Point_set::draw_CINO_FSNO_FIYE_FAPS_TENO_VANO;
  s_draws[SGAL_CINO_FSNO_FIYE_FAPS_TEYE_TINO_VANO] = &Point_set::draw_CINO_FSNO_FIYE_FAPS_TEYE_TINO_VANO;
  s_draws[SGAL_CINO_FSNO_FIYE_FAPS_TEYE_TIYE_VANO] = &Point_set::draw_CINO_FSNO_FIYE_FAPS_TEYE_TIYE_VANO;
  s_draws[SGAL_CINO_FSCO_FINO_FAPV_TENO_VANO] = &Point_set::draw_CINO_FSCO_FINO_FAPV_TENO_VANO;
  s_draws[SGAL_CINO_FSCO_FINO_FAPV_TEYE_TINO_VANO] = &Point_set::draw_CINO_FSCO_FINO_FAPV_TEYE_TINO_VANO;
  s_draws[SGAL_CINO_FSCO_FINO_FAPV_TEYE_TIYE_VANO] = &Point_set::draw_CINO_FSCO_FINO_FAPV_TEYE_TIYE_VANO;
  s_draws[SGAL_CINO_FSCO_FINO_FAPS_TENO_VANO] = &Point_set::draw_CINO_FSCO_FINO_FAPS_TENO_VANO;
  s_draws[SGAL_CINO_FSCO_FINO_FAPS_TEYE_TINO_VANO] = &Point_set::draw_CINO_FSCO_FINO_FAPS_TEYE_TINO_VANO;
  s_draws[SGAL_CINO_FSCO_FINO_FAPS_TEYE_TIYE_VANO] = &Point_set::draw_CINO_FSCO_FINO_FAPS_TEYE_TIYE_VANO;
  s_draws[SGAL_CINO_FSCO_FIYE_FAPV_TENO_VANO] = &Point_set::draw_CINO_FSCO_FIYE_FAPV_TENO_VANO;
  s_draws[SGAL_CINO_FSCO_FIYE_FAPV_TEYE_TINO_VANO] = &Point_set::draw_CINO_FSCO_FIYE_FAPV_TEYE_TINO_VANO;
  s_draws[SGAL_CINO_FSCO_FIYE_FAPV_TEYE_TIYE_VANO] = &Point_set::draw_CINO_FSCO_FIYE_FAPV_TEYE_TIYE_VANO;
  s_draws[SGAL_CINO_FSCO_FIYE_FAPS_TENO_VANO] = &Point_set::draw_CINO_FSCO_FIYE_FAPS_TENO_VANO;
  s_draws[SGAL_CINO_FSCO_FIYE_FAPS_TEYE_TINO_VANO] = &Point_set::draw_CINO_FSCO_FIYE_FAPS_TEYE_TINO_VANO;
  s_draws[SGAL_CINO_FSCO_FIYE_FAPS_TEYE_TIYE_VANO] = &Point_set::draw_CINO_FSCO_FIYE_FAPS_TEYE_TIYE_VANO;
  s_draws[SGAL_CIYE_FSNO_FINO_FAPV_TENO_VANO] = &Point_set::draw_CIYE_FSNO_FINO_FAPV_TENO_VANO;
  s_draws[SGAL_CIYE_FSNO_FINO_FAPV_TEYE_TINO_VANO] = &Point_set::draw_CIYE_FSNO_FINO_FAPV_TEYE_TINO_VANO;
  s_draws[SGAL_CIYE_FSNO_FINO_FAPV_TEYE_TIYE_VANO] = &Point_set::draw_CIYE_FSNO_FINO_FAPV_TEYE_TIYE_VANO;
  s_draws[SGAL_CIYE_FSNO_FINO_FAPS_TENO_VANO] = &Point_set::draw_CIYE_FSNO_FINO_FAPS_TENO_VANO;
  s_draws[SGAL_CIYE_FSNO_FINO_FAPS_TEYE_TINO_VANO] = &Point_set::draw_CIYE_FSNO_FINO_FAPS_TEYE_TINO_VANO;
  s_draws[SGAL_CIYE_FSNO_FINO_FAPS_TEYE_TIYE_VANO] = &Point_set::draw_CIYE_FSNO_FINO_FAPS_TEYE_TIYE_VANO;
  s_draws[SGAL_CIYE_FSNO_FIYE_FAPV_TENO_VANO] = &Point_set::draw_CIYE_FSNO_FIYE_FAPV_TENO_VANO;
  s_draws[SGAL_CIYE_FSNO_FIYE_FAPV_TEYE_TINO_VANO] = &Point_set::draw_CIYE_FSNO_FIYE_FAPV_TEYE_TINO_VANO;
  s_draws[SGAL_CIYE_FSNO_FIYE_FAPV_TEYE_TIYE_VANO] = &Point_set::draw_CIYE_FSNO_FIYE_FAPV_TEYE_TIYE_VANO;
  s_draws[SGAL_CIYE_FSNO_FIYE_FAPS_TENO_VANO] = &Point_set::draw_CIYE_FSNO_FIYE_FAPS_TENO_VANO;
  s_draws[SGAL_CIYE_FSNO_FIYE_FAPS_TEYE_TINO_VANO] = &Point_set::draw_CIYE_FSNO_FIYE_FAPS_TEYE_TINO_VANO;
  s_draws[SGAL_CIYE_FSNO_FIYE_FAPS_TEYE_TIYE_VANO] = &Point_set::draw_CIYE_FSNO_FIYE_FAPS_TEYE_TIYE_VANO;
  s_draws[SGAL_CIYE_FSCO_FINO_FAPV_TENO_VANO] = &Point_set::draw_CIYE_FSCO_FINO_FAPV_TENO_VANO;
  s_draws[SGAL_CIYE_FSCO_FINO_FAPV_TEYE_TINO_VANO] = &Point_set::draw_CIYE_FSCO_FINO_FAPV_TEYE_TINO_VANO;
  s_draws[SGAL_CIYE_FSCO_FINO_FAPV_TEYE_TIYE_VANO] = &Point_set::draw_CIYE_FSCO_FINO_FAPV_TEYE_TIYE_VANO;
  s_draws[SGAL_CIYE_FSCO_FINO_FAPS_TENO_VANO] = &Point_set::draw_CIYE_FSCO_FINO_FAPS_TENO_VANO;
  s_draws[SGAL_CIYE_FSCO_FINO_FAPS_TEYE_TINO_VANO] = &Point_set::draw_CIYE_FSCO_FINO_FAPS_TEYE_TINO_VANO;
  s_draws[SGAL_CIYE_FSCO_FINO_FAPS_TEYE_TIYE_VANO] = &Point_set::draw_CIYE_FSCO_FINO_FAPS_TEYE_TIYE_VANO;
  s_draws[SGAL_CIYE_FSCO_FIYE_FAPV_TENO_VANO] = &Point_set::draw_CIYE_FSCO_FIYE_FAPV_TENO_VANO;
  s_draws[SGAL_CIYE_FSCO_FIYE_FAPV_TEYE_TINO_VANO] = &Point_set::draw_CIYE_FSCO_FIYE_FAPV_TEYE_TINO_VANO;
  s_draws[SGAL_CIYE_FSCO_FIYE_FAPV_TEYE_TIYE_VANO] = &Point_set::draw_CIYE_FSCO_FIYE_FAPV_TEYE_TIYE_VANO;
  s_draws[SGAL_CIYE_FSCO_FIYE_FAPS_TENO_VANO] = &Point_set::draw_CIYE_FSCO_FIYE_FAPS_TENO_VANO;
  s_draws[SGAL_CIYE_FSCO_FIYE_FAPS_TEYE_TINO_VANO] = &Point_set::draw_CIYE_FSCO_FIYE_FAPS_TEYE_TINO_VANO;
  s_draws[SGAL_CIYE_FSCO_FIYE_FAPS_TEYE_TIYE_VANO] = &Point_set::draw_CIYE_FSCO_FIYE_FAPS_TEYE_TIYE_VANO;
}

//! \brief adds the container to a given scene.
void Point_set::add_to_scene(Scene_graph* sg) {
  Container::add_to_scene(sg);
  Boolean coord_indexed = ! is_coord_indices_empty();
  auto num = (coord_indexed) ? get_coord_indices().size() : get_coord_array()->size();
  set_num_primitives(num);
}

//! \brief draws the points.
void Point_set::draw(Draw_action* action) {
  if (! m_coord_array || m_coord_array->empty()) return;
  auto* context = action->get_context();
  context->draw_point_size(m_point_size);

  //! \todo use vertex array
  // Boolean va = use_vertex_array();
  auto va = false;
  Boolean coord_indexed = ! m_coord_indices.empty();
  Fragment_source fragment_source = resolve_fragment_source();
  Boolean fragment_indexed = va ? false :
    ((fragment_source == FS_NORMAL) ?
     (m_normal_indices.empty() ? false : true) :
     (m_color_indices.empty() ? false : true));
  Attachment fragment_attached = (fragment_source == FS_NORMAL) ?
    (m_normal_array ? m_normal_attachment : AT_NONE) :
    (m_color_array ? m_color_attachment : AT_NONE);
  Boolean texture_enabled = m_tex_coord_array ? true : false;
  Boolean texture_indexed = va ? false : ! m_tex_coord_indices.empty();

  // std::cout << "coord_index: " << coord_indexed << std::endl;
  // std::cout << "fragment_source: " << fragment_source << std::endl;
  // std::cout << "fragment_attached: " << fragment_attached << std::endl;
  // std::cout << "fragment_indexed: " << fragment_indexed << std::endl;
  // std::cout << "texture_enabled: " << texture_enabled << std::endl;
  // std::cout << "texture_indexed: " << texture_indexed << std::endl;
  // std::cout << "va: " << va << std::endl;

  auto mask =
    set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,coord_indexed,
      set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,fragment_source,
        set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,fragment_indexed,
          set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,fragment_attached,
            set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,texture_enabled,
              set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,texture_indexed,
                set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,va,0x0)))))));

  s_draws[mask](*this);

  context->draw_point_size(1.0f);
}

//! \brief projects the points onto a framebuffer."
void Point_set::isect(Isect_action* action) {
  std::cerr << "isect() not implemented yet!\n";
}

//! \brief cleans the bounding sphere of the points."
void Point_set::clean_bounding_sphere() {
  Boolean fragment_indexed = is_coord_indices_empty();
  if (! fragment_indexed) {
    Geo_set::clean_bounding_sphere();
    return;
  }
  std::cerr << "clean_bounding_sphere() not implemented yet!\n";
  if (! m_bb_is_pre_set && m_coord_array) {
    auto coords = boost::static_pointer_cast<Coord_array_3d>(m_coord_array);
    SGAL_assertion(coords);
    m_bounding_sphere.set_around(coords->begin(), coords->end());
  }
  m_dirty_bounding_sphere = false;

}

SGAL_END_NAMESPACE
