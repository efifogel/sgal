// Copyright (c) 2004 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <iostream>
#include <sstream>
#include <algorithm>
#include <time.h>
#include <vector>

#if (defined _MSC_VER)
#define NOMINMAX 1
#include <windows.h>
#endif
#include <GL/gl.h>
#include <GL/glext.h>

#include <CGAL/basic.h>
#include <CGAL/Triangulation_3.h>
#include <CGAL/enum.h>
#include <CGAL/Projection_traits_3.h>

#include "SGAL/basic.hpp"
#include "SGAL/Inexact_polyhedron.hpp"
#include "SGAL/Epic_polyhedron.hpp"
#include "SGAL/Epec_polyhedron.hpp"
#include "SGAL/Indexed_face_set.hpp"
#include "SGAL/Coord_array_3d.hpp"
#include "SGAL/Normal_array.hpp"
#include "SGAL/Color_array.hpp"
#include "SGAL/Tex_coord_array_2d.hpp"
#include "SGAL/Tex_coord_array_3d.hpp"
#include "SGAL/Vector3f.hpp"
#include "SGAL/Vector2f.hpp"
#include "SGAL/Container_factory.hpp"
#include "SGAL/Element.hpp"
#include "SGAL/Container_proto.hpp"
#include "SGAL/Tracer.hpp"
#include "SGAL/Gl_wrapper.hpp"
#include "SGAL/GL_error.hpp"
#include "SGAL/calculate_multiple_normals_per_vertex.hpp"
#include "SGAL/Field_rule.hpp"
#include "SGAL/Field_info.hpp"
#include "SGAL/Field_infos.hpp"
#include "SGAL/Configuration.hpp"
#include "SGAL/Modeling.hpp"
#include "SGAL/Scene_graph.hpp"
#include "SGAL/Utilities.hpp"
#include "SGAL/Constrained_triangulation.hpp"
#include "SGAL/construct_triangulation.hpp"
#include "SGAL/Coord_iterator.hpp"
#include "SGAL/calculate_epic_normal.hpp"
#include "SGAL/Stop_simplification_strategy.hpp"

#include "SGAL/Hole_filler_visitor.hpp"
#include "SGAL/Clean_facet_indices_from_polyhedron_visitor.hpp"
#include "SGAL/Orient_polygon_soup_visitor.hpp"
#include "SGAL/Delegate_surface_visitor.hpp"
#include "SGAL/Connected_components_counter_visitor.hpp"
#include "SGAL/Reverse_facet_indices_visitor.hpp"
#include "SGAL/Clean_vertices_visitor.hpp"
#include "SGAL/Convex_hull_visitor.hpp"
#include "SGAL/Compute_coords_visitor.hpp"
#include "SGAL/Polyhedron_vertices_counter_visitor.hpp"
#include "SGAL/Polyhedron_halfedges_counter_visitor.hpp"
#include "SGAL/Polyhedron_facets_counter_visitor.hpp"
#include "SGAL/Polyhedron_facets_type_visitor.hpp"
#include "SGAL/Faces_counter_visitor.hpp"
#include "SGAL/Border_edges_counter_visitor.hpp"
#include "SGAL/Empty_polyhedron_visitor.hpp"
#include "SGAL/Valid_polyhedron_visitor.hpp"
#include "SGAL/Clear_polyhedron_visitor.hpp"
#include "SGAL/Border_normalizer_visitor.hpp"
#include "SGAL/Volume_calculator_visitor.hpp"
#include "SGAL/Surface_area_calculator_visitor.hpp"
#include "SGAL/Coplanar_facets_merger_visitor.hpp"
#include "SGAL/Polyhedron_facet_triangulator_visitor.hpp"
#include "SGAL/Type_polyhedron_visitor.hpp"
#include "SGAL/Clean_facet_normals_visitor.hpp"
#include "SGAL/Clean_planes_visitor.hpp"
#include "SGAL/Polyhedron_simplifier_visitor.hpp"
#include "SGAL/Remove_isolated_vertices_visitor.hpp"

SGAL_BEGIN_NAMESPACE

/*!
 * -----------------------------------------------------------------------------
 * Handling Fields
 * -----------------------------------------------------------------------------
 * We use a deferred response mechanism to handle the change of fields.
 * For every field f we define at least the following functions:
 * 1. set_f(value) -          sets a new value for f.
 * 2. value get_f() -         obtains the value of f.
 * 3. clean_f() -             cleans (or validates) the value of f.
 * 4. clear_f() -             clears (or invalidates) the value of f.
 * 5. f_changed(field-info) - responds to a change of the value of f.
 * 6. bool is_dirty_f() -     determines whether the field has been cleared.
 *
 * set_f() accepts a new value of f and stores it. Then, it calls f_changed().
 *
 * f_changed() propagates the change to other fields. It also reflects the
 *             change at derived classes if exist and perhaps even at the
 *             parent Shape node (which, in turn, reflects it further up in
 *             parent via polymorphism. (Notice that this function is virtual.)
 *
 * clear_f() clears the data structure storing f, and sets the corresponding
 *           m_dirty_f flag to indicate that the f is no longer valid. Then,
 *           it calls f_changed() of the base class if exists. It is imperative
 *           to call the base class function, so that the call to f_changed()
 *           is not captured by the derived classes if exist.
 *
 * f_clean() validates the value of f. Then, it calls f_changed() of the base
 *           class if exists; see clear_f() above.
 *
 * get_f() calls clean_f() if the m_dirty_f flag is set. Only then, it returns
 *         the current value of f.
 *
 * is_dirty_f() simply returns the value of the m_dirty_f flag.
 *
 * The above, essencially implies that the call to clear_f() clears all
 * dependent data structures of base classes and the call to f_changed() clears
 * all dependent data structures of derived classes.
 *
 * -----------------------------------------------------------------------------
 * Restructing
 * -----------------------------------------------------------------------------
 * Restructing refers to the following operations:
 * 1. consistency repairing,
 * 2. hole triangulation,
 * 3. orientation repairing,
 * 4. coplanar facat merging, and
 * 5. triangulation (not implemented yet).
 * The first 3 collectively are referred to as repairing; operations 4 and 5
 * are the opposite of each other.
 * It is assumed that the geometry represented by this data structure does
 * not need repairing at all, that is, it is consistent, has no singular
 * vertices, does not have holes, and has the correct orientation. This is
 * the default of the configuration.
 * Alter the default configuration to enable the desire operation.
 * In order to alter the defaults, e.g., in the case of polygon soup read from
 * an stl file, call the following corespnding member functions:
 *   set_make_consistent(true)---enables consistency enforcing.
 *   set_triangulate_holes(true)---enables hole-triangulation repairing.
 *   set_repair_orientation(true)---enables orientation-repairing.
 * \todo if (!closed) set_solid(false);
 */

const std::string Indexed_face_set::s_tag = "IndexedFaceSet";
Container_proto* Indexed_face_set::s_prototype(nullptr);

REGISTER_TO_FACTORY(Indexed_face_set, "Indexed_face_set");

//! \brief constructs from the prototype.
// By default, we assume that the mesh is inconsistent.
// If the mesh is consistent, there is no need to re-orient the polygon soup
Indexed_face_set::Indexed_face_set(Boolean proto) :
  Boundary_set(proto),
  m_consistent(false),
  m_has_singular_vertices(false),
  m_holes_triangulated(false),
  m_isolated_vertices_removed(false),
  m_orientation_repaired(false),
  m_coplanar_facets_merged(false),
  m_facets_triangulated(false),
  m_normals_repaired(false),
  m_simplified(false),
  m_dirty_volume(true),
  m_dirty_surface_area(true),
  m_convex_hull(false),
  m_volume(0),
  m_surface_area(0),
  m_polyhedron_type(POLYHEDRON_INEXACT),
  m_dirty_polyhedron(true),
  m_dirty_polyhedron_planes(true),
  m_dirty_polyhedron_facet_normals(true),
  m_dirty_normal_attributes(true),
  m_make_consistent(Modeling::s_def_make_consistent),
  m_triangulate_holes(Modeling::s_def_triangulate_holes),
  m_refine(Modeling::s_def_refine_hole_triangulation),
  m_fair(Modeling::s_def_fair_hole_triangulation),
  m_triangulate_facets(Modeling::s_def_triangulate_facets),
  m_remove_isolated_vertices(Modeling::s_def_remove_isolated_vertices),
  m_repair_orientation(Modeling::s_def_repair_orientation),
  m_merge_coplanar_facets(Modeling::s_def_merge_coplanar_facets),
  m_repair_normals(Modeling::s_def_repair_normals),
  m_simplify(Modeling::s_def_simplify),
  m_stop_simplification_strategy
  (Modeling::s_def_stop_simplification_strategy_code),
  m_simplification_count_stop(Modeling::s_def_simplification_count_stop),
  m_simplification_count_ratio_stop
  (Modeling::s_def_simplification_count_ratio_stop),
  m_simplification_edge_length_stop
  (Modeling::s_def_simplification_edge_length_stop)
{
  if (proto) return;
  set_crease_angle(0);
  set_normal_per_vertex(true);
  set_color_per_vertex(true);
}

//! \brief destructs.
Indexed_face_set::~Indexed_face_set(){}

//! \brief sets the attributes of the object.
void Indexed_face_set::set_attributes(Element* elem) {
  Boundary_set::set_attributes(elem);

  for (auto ai = elem->str_attrs_begin(); ai != elem->str_attrs_end(); ++ai) {
    const auto& name = elem->get_name(ai);
    const auto& value = elem->get_value(ai);
    if (name == "convexHull") {
      set_convex_hull(compare_to_true(value));
      elem->mark_delete(ai);
      continue;
    }
  }

  // Remove all the deleted attributes:
  elem->delete_marked();
}

//! \brief initializes the container prototype.
void Indexed_face_set::init_prototype() {
  if (s_prototype) return;
  s_prototype = new Container_proto(Boundary_set::get_prototype());

  // volume
  auto volume_func = static_cast<Float_handle_function>
    (&Indexed_face_set::volume_handle);
  s_prototype->add_field_info(new SF_float(VOLUME, "volume",
                                           Field_rule::RULE_OUT, volume_func));

  // surfaceArea
  auto surface_area_func = static_cast<Float_handle_function>
    (&Indexed_face_set::surface_area_handle);
  s_prototype->add_field_info(new SF_float(SURFACE_AREA, "surfaceArea",
                                           Field_rule::RULE_OUT,
                                           surface_area_func));
}

//! \brief deletes the container prototype.
void Indexed_face_set::delete_prototype() {
  delete s_prototype;
  s_prototype = nullptr;
}

//! \brief obtains the container prototype.
Container_proto* Indexed_face_set::get_prototype() {
  if (s_prototype == nullptr) Indexed_face_set::init_prototype();
  return s_prototype;
}

//! \brief restructs the data structure.
void Indexed_face_set::restruct() {
  if (is_dirty_coord_array()) clean_coords();
  if (is_dirty_facet_coord_indices()) clean_facet_coord_indices();

  if (! m_isolated_vertices_removed && m_remove_isolated_vertices) {
    if (m_dirty_polyhedron) clean_polyhedron();
    remove_isolated_vertices();
  }

  if (! m_holes_triangulated && m_triangulate_holes) {
    if (m_dirty_polyhedron) clean_polyhedron();
    auto closed =
      boost::apply_visitor(Is_closed_polyhedron_visitor(), m_polyhedron);
    if (! closed) triangulate_holes();
  }
  if (! m_orientation_repaired && m_repair_orientation) {
    if (m_dirty_polyhedron) clean_polyhedron();
    repair_orientation();
  }

  if (m_simplify && ! m_simplified) {
    if (m_dirty_polyhedron) clean_polyhedron();
    // First triangulate
    if ((PT_TRIANGLES != m_primitive_type) && ! m_facets_triangulated) {
      if (m_dirty_polyhedron_planes) clean_polyhedron_planes();
      triangulate_polyhedron_facets();
    }
    simplify_polyhedron();
  }

  if (! m_coplanar_facets_merged && m_merge_coplanar_facets) {
    if (m_dirty_polyhedron) clean_polyhedron();
    if (m_dirty_polyhedron_planes) clean_polyhedron_planes();
    merge_coplanar_facets();
  }

  if ((PT_TRIANGLES != m_primitive_type) && ! m_facets_triangulated &&
      m_triangulate_facets)
  {
    if (m_dirty_polyhedron) clean_polyhedron();
    if (m_dirty_polyhedron_planes) clean_polyhedron_planes();
    triangulate_polyhedron_facets();
    // if (is_dirty_coord_array()) clean_coords();
    // if (is_dirty_facet_coord_indices()) clean_facet_coord_indices();
    // triangulate_facets();
  }
}

//! \brief simplify the polyhedron.
void Indexed_face_set::simplify_polyhedron() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::simplify_polyhedron(): "
                  << get_name() << std::endl;);

  SGAL_assertion(PT_TRIANGLES == m_primitive_type);
  auto sss =
    static_cast<Stop_simplification_strategy>(m_stop_simplification_strategy);
  Polyhedron_simplifier_visitor visitor(sss,
                                        m_simplification_count_stop,
                                        m_simplification_count_ratio_stop,
                                        m_simplification_edge_length_stop);
  auto count = boost::apply_visitor(visitor, m_polyhedron);
  std::cout << "# collapsed edges: " << count << std::endl;

  m_simplified = true;
  m_dirty_polyhedron_planes = true;
  clear_coord_array();
  clear_coord_indices();
  clear_facet_coord_indices();
}

//! \brief draws the polygons.
void Indexed_face_set::draw(Draw_action* action) {
  restruct();
  if (is_dirty_coord_array()) clean_coords();
  if (is_dirty_facet_coord_indices()) clean_facet_coord_indices();
  if (! m_normals_repaired && m_repair_normals) repair_normals();
  Boundary_set::draw(action);
}

//! \brief draws the polygons for selection.
void Indexed_face_set::isect(Isect_action* action) {
  restruct();
  if (is_dirty_coord_array()) clean_coords();
  if (is_dirty_facet_coord_indices()) clean_facet_coord_indices();
  Boundary_set::isect(action);
}

//! \brief cleans the sphere bound.
void Indexed_face_set::clean_bounding_sphere() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::clean_bounding_sphere(): "
                  << "name: " << get_name() << std::endl;);

  restruct();
  if (is_dirty_coord_array()) clean_coords();
  if (!m_coord_array) return;

  auto coords = boost::dynamic_pointer_cast<Coord_array_3d>(m_coord_array);
  if (coords) {
    m_bounding_sphere.set_around(coords->begin(), coords->end());
    return;
  }

  auto epic_coords =
    boost::dynamic_pointer_cast<Epic_coord_array_3d>(m_coord_array);
  if (epic_coords) {
    const auto& vecs = epic_coords->get_inexact_coords();
    m_bounding_sphere.set_around(vecs.begin(), vecs.end());
    return;
  }

  auto epec_coords =
    boost::dynamic_pointer_cast<Epec_coord_array_3d>(m_coord_array);
  if (epec_coords) {
    const auto& vecs = epec_coords->get_inexact_coords();
    m_bounding_sphere.set_around(vecs.begin(), vecs.end());
    return;
  }

  SGAL_error();
}

//! \brief cleans the coordinate array and coordinate indices.
void Indexed_face_set::clean_coords() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout
                  << "Indexed_face_set::clean_coords(): "
                  << "name: " << get_name() << std::endl;);

  // Prevent re-entering the cleaning function in case m_polyhedron is empty.
  m_dirty_coord_array = false;

  // If the polyhedron is empty, return.
  if (boost::apply_visitor(Empty_polyhedron_visitor(), m_polyhedron)) return;

  Polyhedron_vertices_counter_visitor vertices_visitor;
  auto num_vertices = boost::apply_visitor(vertices_visitor, m_polyhedron);

  // If the polyhedron type does not match the coord array type or the latter
  // does not exist at all, allocate a new one.
  switch (m_polyhedron_type) {
   case POLYHEDRON_EPEC:
    if (! m_coord_array ||
        ! boost::dynamic_pointer_cast<Epec_coord_array_3d>(m_coord_array)) {
      m_coord_array.reset(new Epec_coord_array_3d(num_vertices));
      SGAL_assertion(m_coord_array);
      break;
    }
    m_coord_array->resize(num_vertices);
    break;

   case POLYHEDRON_EPIC:
    if (! m_coord_array ||
        ! boost::dynamic_pointer_cast<Epic_coord_array_3d>(m_coord_array)) {
      m_coord_array.reset(new Epic_coord_array_3d(num_vertices));
      SGAL_assertion(m_coord_array);
      break;
    }
    m_coord_array->resize(num_vertices);
    break;

   case POLYHEDRON_INEXACT:
    if (! m_coord_array ||
        ! boost::dynamic_pointer_cast<Coord_array_3d>(m_coord_array)) {
      m_coord_array.reset(new Coord_array_3d(num_vertices));
      SGAL_assertion(m_coord_array);
      break;
    }
    m_coord_array->resize(num_vertices);
    break;

   default: SGAL_error();
  }

  /* Generate the coordinate array and assign the index into the coordinate
   * array of the vertex to the vertex.
   */
  Compute_coords_visitor coords_visitor(m_coord_array);
  boost::apply_visitor(coords_visitor, m_polyhedron);

  clean_facet_coord_indices_from_polyhedron();

  /* Notice that we call the function of the base calss.
   * In general when the coordinates change, we must invalidate the polyhedron
   * to force cleaning of the polyhedron, so that the change to the coordinates
   * is reflected in the polyhedron. However, clean_coords() could have beeen
   * invoked as a consequence of an update of the polyhedron. Naturally, in this
   * case we do not want to invalidate the polyhedron.
   */
  Boundary_set::coord_content_changed(get_field_info(COORD_ARRAY));
}

//! \brief obtains the coordinate array.
Indexed_face_set::Shared_coord_array Indexed_face_set::get_coord_array() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::get_coord_array(): "
                  << "name: " << get_name() << std::endl;);

  restruct();
  if (is_dirty_coord_array()) clean_coords();
  return m_coord_array;
}

//! \brief cleans the coordinate indices.
void Indexed_face_set::clean_facet_coord_indices_from_polyhedron() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::clean_facet_coord_indices_from_polyhedron(): "
                  << "name: " << get_name() << std::endl;);

  // If the polyhedron is empty, return.
  if (boost::apply_visitor(Empty_polyhedron_visitor(), m_polyhedron)) {
    m_dirty_coord_indices = false;
    m_dirty_facet_coord_indices = false;
    return;
  }

  auto num_primitives =
    boost::apply_visitor(Polyhedron_facets_counter_visitor(), m_polyhedron);
  set_num_primitives(num_primitives);

  auto primitive_type =
    boost::apply_visitor(Type_polyhedron_visitor(), m_polyhedron);
  set_primitive_type(primitive_type);

  init_facet_coord_indices();
  Clean_facet_indices_from_polyhedron_visitor visitor(num_primitives);
  boost::apply_visitor(visitor, m_polyhedron, m_facet_coord_indices);

  m_dirty_coord_indices = true;
  m_dirty_facet_coord_indices = false;

  /* Notice that we call the function of the base calss.
   * In general when the coordinates change, we must invalidate the polyhedron
   * to force cleaning of the polyhedron, so that the change to the coordinates
   * is reflected in the polyhedron. However, clean_coords() could have beeen
   * invoked as a consequence of an update of the polyhedron. Naturally, in this
   * case we do not want to invalidate the polyhedron.
   */
  Boundary_set::coord_content_changed(get_field_info(COORD_INDEX_ARRAY));
}

//! \brief cleans (validate) the facet coordinate indices.
void Indexed_face_set::clean_facet_coord_indices() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::clean_facet_coord_indices(): "
                  << "name: " << get_name() << std::endl;);

  if (! is_dirty_polyhedron()) clean_facet_coord_indices_from_polyhedron();
  else Boundary_set::clean_facet_coord_indices();
}

//! \brief cleans the normal array and the normal indices.
void Indexed_face_set::clean_normals() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::clean_normals(): "
                  << "name: " << get_name() << std::endl;);

  if ((0 < m_crease_angle) && (m_crease_angle < SGAL_PI)) {
    if (m_dirty_polyhedron) clean_polyhedron();
    if (m_dirty_polyhedron_planes) clean_polyhedron_planes();
    if (m_dirty_polyhedron_facet_normals) clean_polyhedron_facet_normals();
    if (m_dirty_normal_attributes) clean_normal_attributes();

    if (m_smooth) calculate_single_normal_per_vertex();
    else if (m_creased) calculate_normal_per_facet();
    else calculate_multiple_normals_per_vertex();
  }
  else if (m_crease_angle >= SGAL_PI) calculate_single_normal_per_vertex();
  else if (m_crease_angle == 0) calculate_normal_per_facet();
  else SGAL_error();
  m_dirty_normal_array = false;
  m_normal_array_cleaned = true;
  m_dirty_normal_buffer = true;
}

//! \brief creates a polyhedron.
void Indexed_face_set::create_polyhedron() {
  init_polyhedron_type();
  Delegate_surface_visitor visitor(m_primitive_type, m_num_primitives,
                                   m_coord_array, m_facet_coord_indices);
  m_consistent = boost::apply_visitor(visitor, m_polyhedron);

  // Normalize the border of the polyhedron.
  boost::apply_visitor(Border_normalizer_visitor(), m_polyhedron);

  m_dirty_polyhedron = false;
  m_dirty_polyhedron_facet_normals = true;
  m_dirty_volume = true;
  m_dirty_surface_area = true;
}

//! \brief makes consistent.
void Indexed_face_set::make_consistent() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::make_consistent(): "
                  << "name: " << get_name() << std::endl;);

  Orient_polygon_soup_visitor visitor(m_coord_array);
  m_has_singular_vertices =
    boost::apply_visitor(visitor, m_facet_coord_indices);
  clear_coord_indices();

  auto num_primitives =
    boost::apply_visitor(Faces_counter_visitor(), m_facet_coord_indices);
  set_num_primitives(num_primitives);
}

//! \brief triangulates holes.
void Indexed_face_set::triangulate_holes() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::triangulate_holes(): "
                  << "name: " << get_name() << std::endl;);

  Hole_filler_visitor visitor(m_refine, m_fair, m_coord_array);
  boost::apply_visitor(visitor, m_polyhedron, m_facet_coord_indices);

  // Border_edges_counter_visitor bec_visitor;
  // auto num_border_edges = boost::apply_visitor(bec_visitor, m_polyhedron);
  // std::cout << "# border edge: " << num_border_edges << std::endl;

  m_holes_triangulated = true;

  //! \todo instead of brutally clearing the arrays,
  // update these arrays based on the results of the hole-filler visitor.
  // auto new_coord_array = visitor.get_new_coord_array();
  // if (new_coord_array) std::swap(new_coord_array, m_coord_array);
  clear_coord_array();
  clear_coord_indices();
  clear_facet_coord_indices();

  m_dirty_polyhedron_facet_normals = true;
  m_dirty_volume = true;
  m_dirty_surface_area = true;
}

//! \brief removes isolated vertices
size_t Indexed_face_set::remove_isolated_vertices() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::remove_isolated_vertices(): "
                  << get_name() << std::endl;);
  Remove_isolated_vertices_visitor visitor;
  auto num_removed = boost::apply_visitor(visitor, m_polyhedron);
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Removed " << num_removed
                  << " isolated vertices" << std::endl;);
  m_isolated_vertices_removed = true;      // disable repetative invocations
  return num_removed;
}

//! \brief repairs the orientation of the polyhedron data structures.
void Indexed_face_set::repair_orientation()
{
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::repair_orientation(): "
                  << get_name() << std::endl;);

  Connected_components_counter_visitor ccs_counter_visitor;
  auto num_ccs = boost::apply_visitor(ccs_counter_visitor, m_polyhedron);
  Is_closed_polyhedron_visitor is_closed_visitor;
  auto closed = boost::apply_visitor(is_closed_visitor, m_polyhedron);
  if ((num_ccs != 1) || ! closed || ! m_repair_orientation) {

    m_orientation_repaired = true;      // disable repetative invocations
    return;
  }

  clean_volume();
  // If the volume of the closed polyhedron is negative, all facets are
  // oriented clockwise. That is, the normal of every facet is directed
  // towards the interior of the polyhedron instead of the exterior.
  if (m_volume >= 0) {
    m_orientation_repaired = true;      // disable the repetative invocation
    return;
  }

  // If the facet coord indices is dirty (invalid), clean (recompute) it.
  //! \todo Do the cleaning and reversing at once as an optimization.
  if (is_dirty_coord_array() || is_dirty_facet_coord_indices())
    clean_coords();

  // Do not call reverse_facet_coord_indices() of the base class.
  // Instead, reverse and set the appropriate flags.
  // Do not alter the following flags:
  // m_consistent, m_has_singular_vertices, and m_dirty_repaired_polyhedron
  Reverse_facet_indices_visitor visitor;
  boost::apply_visitor(visitor, m_facet_coord_indices);

  m_dirty_facet_coord_indices = false;
  Boundary_set::facet_coord_indices_changed();
  m_dirty_coord_indices = true;
  boost::apply_visitor(Clear_polyhedron_visitor(), m_polyhedron);
  m_dirty_polyhedron = true;

  //! \todo Instead of brutally clearing the normals, colors, and texture
  // coordinat, reorder them appropriately.
  m_dirty_polyhedron_facet_normals = true;
  m_dirty_normal_attributes = true;
  clear_normal_indices();
  clear_facet_normal_indices();
  set_normal_array(Shared_normal_array());

  // Clean the colors
  clear_color_indices();
  clear_facet_color_indices();
  set_color_array(Shared_color_array());

  // Clean the texture coordinate
  clear_tex_coord_indices();
  clear_facet_tex_coord_indices();
  set_tex_coord_array(Shared_tex_coord_array());

  m_dirty_volume = true;
  m_dirty_surface_area = true;
  m_orientation_repaired = true;
}

//! \brief merges coplanar facets.
void Indexed_face_set::merge_coplanar_facets()
{
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::merge_coplanar_facets(): "
                  << get_name() << std::endl;);

  Coplanar_facets_merger_visitor visitor;
  auto changed = boost::apply_visitor(visitor, m_polyhedron);
  m_dirty_polyhedron_facet_normals = false;
  m_coplanar_facets_merged = true;
  if (changed) {
    clear_coord_array();
    clear_coord_indices();
    clear_facet_coord_indices();
  }
}

//! \brief triangulate facets.
void Indexed_face_set::triangulate_polyhedron_facets()
{
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::triangulate_polyhedron_facets(): "
                  << get_name() << std::endl;);

  SGAL_assertion(PT_TRIANGLES != m_primitive_type);

  Polyhedron_facets_counter_visitor counter_visitor;
  auto num_before = boost::apply_visitor(counter_visitor, m_polyhedron);

  Polyhedron_facet_triangulator_visitor visitor;
  boost::apply_visitor(visitor, m_polyhedron);

  auto num_after = boost::apply_visitor(counter_visitor, m_polyhedron);
  // std::cout << get_name() << ": Triangulate facets: "
  //           << num_before << ", " << num_after << std::endl;
  m_facets_triangulated = true;

  if (num_before != num_after) {
    m_dirty_polyhedron_planes = true;
    clear_coord_array();
    clear_coord_indices();
    clear_facet_coord_indices();
  }
}

template <typename OutputIterator, typename CDT>
OutputIterator finish_triangulate_facet(const CDT& tri,
                                        const std::vector<Index_type>& facet,
                                        bool is_ccw, OutputIterator oi)
{
  mark_domains(tri);
  bool consistent;
  auto it = tri.finite_faces_begin();
  for (; it != tri.finite_faces_end(); ++it) {
    if (! it->info().in_domain()) continue;
    auto i0 = it->vertex(0)->info().m_index;
    auto i1 = it->vertex(1)->info().m_index;
    auto i2 = it->vertex(2)->info().m_index;
    bool k1 = i0 < i1;
    bool k2 = i1 < i2;
    bool k3 = i2 < i0;
    consistent = (k1 && k2) || (k2 && k3) || (k3 && k1);
    break;
  }

  bool flip = ((consistent && is_ccw) || (! consistent && ! is_ccw));
  for (; it != tri.finite_faces_end(); ++it) {
    if (! it->info().in_domain()) continue;
    auto i0 = it->vertex(0)->info().m_index;
    auto i1 = it->vertex(1)->info().m_index;
    auto i2 = it->vertex(2)->info().m_index;
    *oi++ = (flip) ?
      std::array<Index_type, 3>{{facet[i0], facet[i1], facet[i2]}} :
      std::array<Index_type, 3>{{facet[i2], facet[i1], facet[i0]}};
  }
  return oi;
}

/*! Triangulate a facet.
 */
template <typename OutputIterator>
OutputIterator Indexed_face_set::
triangulate_facet(const std::vector<Index_type>& facet, OutputIterator oi) {
  if (facet.size() == 3) {
    *oi++ = (is_ccw()) ?
      std::array<Index_type, 3>{{facet[0], facet[1], facet[2]}} :
      std::array<Index_type, 3>{{facet[2], facet[1], facet[0]}};
    return oi;
  }
  if (facet.size() == 4) {
    if (is_ccw()) {
      *oi++ = std::array<Index_type, 3>{{facet[0], facet[1], facet[2]}};
      *oi++ = std::array<Index_type, 3>{{facet[0], facet[2], facet[3]}};
      return oi;
    }
    *oi++ = std::array<Index_type, 3>{{facet[0], facet[1], facet[3]}};
    *oi++ = std::array<Index_type, 3>{{facet[1], facet[2], facet[3]}};
    return oi;
  }

  using Index_const_iter = std::vector<Index_type>::const_iterator;

  Epic_kernel kernel;
  auto coords = boost::dynamic_pointer_cast<Coord_array_3d>(m_coord_array);
  if (coords) {
    Epic_vector_3 normal;
    auto res = calculate_epic_normal(facet, coords, normal, is_ccw(), kernel);
    if (! res) {
      std::cerr << "ERROR: All vertices are collinear!\n";
      return oi;
    }
    using P_traits = CGAL::Projection_traits_3<Epic_kernel>;
    P_traits cdt_traits(normal);
    Constrained_triangulation<P_traits>::CDT tri(cdt_traits);
    using Shared_coords_3d = boost::shared_ptr<SGAL::Coord_array_3d>;
    using Iter = Coord_iterator<Shared_coords_3d, Index_const_iter, Vector3f>;
    Iter cbegin(coords, facet.begin());
    Iter cend(coords, facet.end());
    construct_triangulation(tri, cbegin, cend, 0, false);
    finish_triangulate_facet(tri, facet, is_ccw(), oi);
    return oi;
  }

  auto epic_coords =
    boost::dynamic_pointer_cast<Epic_coord_array_3d>(m_coord_array);
  if (epic_coords) {
    Epic_vector_3 normal;
    auto res =
      calculate_epic_normal(facet, epic_coords, normal, is_ccw(), kernel);
    if (! res) {
      std::cerr << "ERROR: All vertices are collinear!\n";
      return oi;
    }
    using P_traits = CGAL::Projection_traits_3<Epic_kernel>;
    P_traits cdt_traits(normal);
    Constrained_triangulation<P_traits>::CDT tri(cdt_traits);
    using Shared_epic_coords_3d = boost::shared_ptr<SGAL::Epic_coord_array_3d>;
    using Iter =
      Coord_iterator<Shared_epic_coords_3d, Index_const_iter, Epic_point_3>;
    Iter cbegin(epic_coords, facet.begin());
    Iter cend(epic_coords, facet.end());
    construct_triangulation(tri, cbegin, cend, 0, false);
    finish_triangulate_facet(tri, facet, is_ccw(), oi);
    return oi;
  }

  auto epec_coords =
    boost::dynamic_pointer_cast<Epec_coord_array_3d>(m_coord_array);
  if (epec_coords) {
    CGAL_error_msg("Not implemented yet!");
    return oi;
  }

  CGAL_error();                         // should never reach here
  return oi;
}

//! \brief triangulate facets.
void Indexed_face_set::triangulate_facets() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::triangulate_facets(): "
                  << get_name() << std::endl;);

  SGAL_assertion(PT_POLYGONS == m_primitive_type);

  auto num_before = m_num_primitives;
  Triangle_indices tri_indices;
  const auto& indices = polygon_coord_indices();
  for (const auto& facet : indices) {
    triangulate_facet(facet, std::back_inserter(tri_indices));
  }
  set_facet_coord_indices(std::move(Facet_indices(tri_indices)));
  m_primitive_type = PT_TRIANGLES;
  m_num_primitives = tri_indices.size();
  std::cout << "facets: " << num_before << ", " << m_num_primitives << std::endl;
  m_facets_triangulated = true;

  if (num_before != m_num_primitives) {
    clear_polyhedron();
    clear_facet_normal_indices();
    clear_facet_tex_coord_indices();
  }
}

//! \brief repairs the data structures
void Indexed_face_set::repair_normals() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::repair_normals(): "
                  << get_name() << std::endl;);

  clear_normal_indices();
  clear_facet_normal_indices();
  set_normal_array(Shared_normal_array());
  set_normal_per_vertex(false);
  m_normals_repaired = true;
}

//! \brief initializes the polyhedron type.
void Indexed_face_set::init_polyhedron_type() {
  switch (m_polyhedron_type) {
   case POLYHEDRON_INEXACT: m_polyhedron = Inexact_polyhedron(); break;
   case POLYHEDRON_EPIC: m_polyhedron = Epic_polyhedron(); break;
   case POLYHEDRON_EPEC: m_polyhedron = Epec_polyhedron(); break;
   default: SGAL_error();
  }
}

//! \brief responds to a change in the polyhedron.
void Indexed_face_set::polyhedron_changed() {
  m_dirty_polyhedron = false;
  m_dirty_volume = true;
  m_dirty_surface_area = true;

  // Assume that the incoming polyhedron
  //   (i) the facets normal of which are cleaned,
  //  (ii) has no singular vertices,
  // (iii) is consistent, and
  // ((iv) repaired.
  m_dirty_polyhedron_facet_normals = false;

  m_consistent = true;
  m_isolated_vertices_removed = true;
  m_orientation_repaired = true;
  m_coplanar_facets_merged = false;
  m_has_singular_vertices = false;
  m_holes_triangulated = false;
  m_facets_triangulated = false;
  m_simplified = false;

  clear_coord_array();
  clear_coord_indices();
  clear_facet_coord_indices();
}

//! \brief computes the convex hull of the coordinate set.
void Indexed_face_set::convex_hull() {
  if (!m_coord_array || m_coord_array->empty()) return;

  if (m_polyhedron_type != POLYHEDRON_EPEC) m_polyhedron_type = POLYHEDRON_EPEC;
  init_polyhedron_type();

  auto exact_coords =
    boost::dynamic_pointer_cast<Epec_coord_array_3d>(m_coord_array);
  if (exact_coords) {
    if (exact_coords->size() > 0) {
      Convex_hull_visitor<Epec_coord_array_3d::Epec_point_const_iter>
        ch_visitor(exact_coords->begin(), exact_coords->end());
      boost::apply_visitor(ch_visitor, m_polyhedron);

      /* Compute the index of the vertex into the coordinate array and assign it
       * to the polyhedron-vertex record.
       * \todo make more efficient.
       */
      Clean_vertices_visitor<Epec_coord_array_3d::Epec_point_const_iter>
        cv_visitor(exact_coords->begin(), exact_coords->end());
      boost::apply_visitor(cv_visitor, m_polyhedron);
    }
  }
  else {
    auto coords = boost::dynamic_pointer_cast<Coord_array_3d>(m_coord_array);
    if (coords) {
      if (coords->size() > 0) {
        std::vector<Epec_point_3> points;
        points.resize(coords->size());
        std::transform(coords->begin(), coords->end(),
                       points.begin(), Vector_to_point());

        // std::copy(points.begin(), points.end(),
        //           std::ostream_iterator<Epec_point_3>(std::cout, "\n"));

        Convex_hull_visitor<std::vector<Epec_point_3>::const_iterator>
          ch_visitor(points.begin(), points.end());
        boost::apply_visitor(ch_visitor, m_polyhedron);

        /* Compute the index of the vertex into the coordinate array and assign
         * it to the polyhedron-vertex record.
         * \todo make more efficient.
         */
        Clean_vertices_visitor<std::vector<Epec_point_3>::const_iterator>
          cv_visitor(points.begin(), points.end());
        boost::apply_visitor(cv_visitor, m_polyhedron);
      }
    }
    else SGAL_error();
  }

  m_dirty_polyhedron = false;
  m_dirty_facet_coord_indices = true;
  m_dirty_polyhedron_facet_normals = true;
  m_dirty_coord_indices = true;
  m_dirty_volume = true;
  m_dirty_surface_area = true;

  m_consistent = true;
  m_holes_triangulated = true;
  m_isolated_vertices_removed = true;
  m_orientation_repaired = true;
  m_coplanar_facets_merged = false;
}

//! \brief cleans the polyhedron data structure.
// Set (or reset) the dirty flags at the end.
// Bail out if the indices or the coordinate indices are empty and the
// m_dirty_polyhedron must retain its (true) value; this is imperative as
// explained next.  When we need the clean (valid) polyhedron (e.g., when we
// need to determine whether the polyhedron is closed) we first attempt to clean
// the coordinates & coordinate indices and immediately after we attempt to
// clean the polyhedron. If, on the other hand, we need the clean (valid)
// coordinates & coordinate indices, we attemp the cleaning in reverse order;
// that is, we first attempt to clean the polyhedron, and immediately after we
// attempt to clean the coordinates & coordinate indices. In this case, when the
// polyhedron cleaning function is invoked, and indices are not cleaned, we must
// enable the re-invocation of the polyhedron cleaning function at a later point
// after the indices have been cleaned.
void Indexed_face_set::clean_polyhedron() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::clean_polyhedron(): "
                  << "name: " << get_name() << std::endl;);

  // If there are no coordinates bail out.
  if (!m_coord_array || m_coord_array->empty()) return;

  if (m_convex_hull) {
    clock_t start_time = clock();
    convex_hull();
    clock_t end_time = clock();
    m_time = (float) (end_time - start_time) / (float) CLOCKS_PER_SEC;
    return;
  }

  // If there are no coordinate indices bail out.
  if (empty_facet_indices(m_facet_coord_indices)) return;

  // We do not remove collinear facets now, cause it may create T verticees.

  // Make consistent.
  if (! m_consistent && m_make_consistent) make_consistent();

  // Create the polyhedral surface
  if ((m_polyhedron_type == POLYHEDRON_INEXACT) &&
      (m_triangulate_holes || m_merge_coplanar_facets || m_triangulate_facets))

    m_polyhedron_type =
      (boost::dynamic_pointer_cast<Epec_coord_array_3d>(m_coord_array)) ?
      POLYHEDRON_EPEC : POLYHEDRON_EPIC;
  create_polyhedron();

  auto valid = boost::apply_visitor(Valid_polyhedron_visitor(false), m_polyhedron);
  if (! valid) std::cerr << "The polyhedron is invalid!\n";
}

//! \brief clears the polyhedron.
void Indexed_face_set::clear_polyhedron() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::clear_polyhedron(): "
                  << "name: " << get_name() << std::endl;);

  m_dirty_polyhedron = true;
  m_dirty_polyhedron_facet_normals = true;
  m_dirty_volume = true;
  m_dirty_surface_area = true;

  // Reset the state flags:
  m_consistent = false;
  m_has_singular_vertices = false;
  m_holes_triangulated = false;
  m_isolated_vertices_removed = false;
  m_orientation_repaired = false;
  m_coplanar_facets_merged = false;

  boost::apply_visitor(Clear_polyhedron_visitor(), m_polyhedron);
}

//! \brief sets the polyhedron data-structure.
void Indexed_face_set::set_polyhedron(const Polyhedron& polyhedron) {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::set_polyhedron(): "
                  << "name: " << get_name() << std::endl;);

  Polyhedron_facets_type_visitor facet_type_visitor;
  set_primitive_type(boost::apply_visitor(facet_type_visitor, polyhedron));
  Polyhedron_facets_counter_visitor facet_counter_visitor;
  set_num_primitives(boost::apply_visitor(facet_counter_visitor, polyhedron));

  m_polyhedron = polyhedron;
  polyhedron_changed();
}

//! \brief sets the polyhedron data-structure.
void Indexed_face_set::set_polyhedron(const Inexact_polyhedron& polyhedron) {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::set_polyhedron(): "
                  << "name: " << get_name() << std::endl;);


  auto type = (polyhedron.is_pure_triangle()) ? Geo_set::PT_TRIANGLES :
    (polyhedron.is_pure_quad()) ? Geo_set::PT_QUADS : Geo_set::PT_POLYGONS;
  set_primitive_type(type);
  set_num_primitives(polyhedron.size_of_facets());

  m_polyhedron = polyhedron;
  polyhedron_changed();
}

//! \brief sets the polyhedron data-structure.
void Indexed_face_set::set_polyhedron(const Epic_polyhedron& polyhedron) {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::set_polyhedron(): "
                  << "name: " << get_name() << std::endl;);

  auto type = (polyhedron.is_pure_triangle()) ? Geo_set::PT_TRIANGLES :
    (polyhedron.is_pure_quad()) ? Geo_set::PT_QUADS : Geo_set::PT_POLYGONS;
  set_primitive_type(type);
  set_num_primitives(polyhedron.size_of_facets());

  m_polyhedron = polyhedron;
  polyhedron_changed();
}

//! \brief sets the polyhedron data-structure.
void Indexed_face_set::set_polyhedron(const Epec_polyhedron& polyhedron) {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::set_polyhedron(): "
                  << "name: " << get_name() << std::endl;);

  auto type = (polyhedron.is_pure_triangle()) ? Geo_set::PT_TRIANGLES :
    (polyhedron.is_pure_quad()) ? Geo_set::PT_QUADS : Geo_set::PT_POLYGONS;
  set_primitive_type(type);
  set_num_primitives(polyhedron.size_of_facets());

  m_polyhedron = polyhedron;
  polyhedron_changed();
}

//! \brief obtains the representation mode.
Geo_set::Primitive_type Indexed_face_set::get_primitive_type() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::get_primitive_type(): "
                  << "name: " << get_name() << std::endl;);

  restruct();
  if (is_dirty_coord_array()) clean_coords();
  if (is_dirty_facet_coord_indices()) clean_facet_coord_indices();
  return Boundary_set::get_primitive_type();
}

//! \brief obtains the number of primitives.
Uint Indexed_face_set::get_num_primitives() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::get_num_primitives(): "
                  << "name: " << get_name() << std::endl;);

  restruct();
  if (is_dirty_coord_array()) clean_coords();
  if (is_dirty_facet_coord_indices()) clean_facet_coord_indices();
  return Boundary_set::get_num_primitives();
}

//! \brief obtains the polyhedron data-structure.
const Indexed_face_set::Polyhedron&
Indexed_face_set::get_polyhedron(Boolean clean_facet_normals) {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::get_polyhedron(): "
                  << "name: " << get_name() << std::endl;);

  restruct();
  if (m_dirty_polyhedron) clean_polyhedron();
  if (clean_facet_normals) {
    if (m_dirty_polyhedron_planes) clean_polyhedron_planes();
    if (m_dirty_polyhedron_facet_normals) clean_polyhedron_facet_normals();
  }
  return m_polyhedron;
}

//! \brief responds to a change in the coordinate array.
void Indexed_face_set::coord_content_changed(const Field_info* field_info) {
  clear_polyhedron();
  Boundary_set::coord_content_changed(field_info);
}

//! \brief determines whether the surface is smooth.
Boolean Indexed_face_set::is_smooth(const Vector3f& normal1,
                                    const Vector3f& normal2) const {
  float angle = acosf(normal1.dot(normal2));
  return (angle > m_crease_angle);
}

//! \brief obtains the ith 3D coordinate.
const Vector3f& Indexed_face_set::get_coord_3d(Uint i) const {
  auto coords = boost::dynamic_pointer_cast<Coord_array_3d>(m_coord_array);
  if (coords) return (*coords)[i];

  auto epic_coords =
    boost::dynamic_pointer_cast<Epic_coord_array_3d>(m_coord_array);
  if (epic_coords) return epic_coords->get_inexact_coord(i);

  auto epec_coords =
    boost::dynamic_pointer_cast<Epec_coord_array_3d>(m_coord_array);
  SGAL_assertion(epec_coords);
  return epec_coords->get_inexact_coord(i);
}

//! \brief calculates multiple normals per vertex for all vertices.
void Indexed_face_set::calculate_multiple_normals_per_vertex() {
  resize_facet_indices(m_facet_normal_indices, m_facet_coord_indices);
  if (!m_normal_array) {
    m_normal_array.reset(new Normal_array());
    SGAL_assertion(m_normal_array);
  }
  else m_normal_array->clear();
  Calculate_multiple_normals_per_vertex_visitor visitor(m_normal_array,
                                                        m_facet_normal_indices);
  boost::apply_visitor(visitor, m_polyhedron);
  set_normal_attachment(AT_PER_VERTEX);
  m_dirty_normal_indices = true;
  m_dirty_facet_normal_indices = false;
}

//! \brief prints statistics.
void Indexed_face_set::print_stat() {
  restruct();
  if (is_dirty_coord_array()) clean_coords();
  if (is_dirty_facet_coord_indices()) clean_facet_coord_indices();

  std::cout << "Container name: " << get_name() << std::endl;
  std::cout << "Container tag: " << get_tag() << std::endl;
  std::cout << "Is closed: " << is_closed() << std::endl;
  // std::cout << "# vertices: " << m_polyhedron.size_of_vertices()
  //           << ", # edges: " << m_polyhedron.size_of_halfedges() / 2
  //           << ", # facets: " << m_polyhedron.size_of_facets()
  //           << std::endl;
  std::cout << "Volume of convex hull: " << volume_of_convex_hull()
            << std::endl;

  if (m_convex_hull)
    std::cout << "Convex hull took " << m_time << " seconds to compute"
              << std::endl;
}

//! Write this container.
void Indexed_face_set::write(Formatter* formatter) {
  SGAL_TRACE_CODE(Tracer::EXPORT,
                  std::cout << "Indexed_face_set::write(): "
                  << "Tag: " << get_tag()
                  << ", name: " << get_name()
                  << std::endl;);

  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::write(): "
                  << "Tag: " << get_tag()
                  << ", name: " << get_name() << std::endl;);

  restruct();
  if (is_dirty_coord_array()) clean_coords();
  if (is_dirty_facet_coord_indices()) clean_facet_coord_indices();
  if (! m_normals_repaired && m_repair_normals) repair_normals();
  Boundary_set::write(formatter);
}

//! \brief cleans (compute) the volume.
void Indexed_face_set::clean_volume() {
  m_dirty_volume = false;

  m_volume = 0;
  if (boost::apply_visitor(Empty_polyhedron_visitor(), m_polyhedron)) return;
  m_volume = boost::apply_visitor(Volume_calculator_visitor(), m_polyhedron);
}

//! \brief cleans (compute) the surface area.
void Indexed_face_set::clean_surface_area() {
  m_dirty_surface_area = false;

  m_surface_area = 0;
  if (boost::apply_visitor(Empty_polyhedron_visitor(), m_polyhedron)) return;
  Surface_area_calculator_visitor visitor(m_primitive_type);
  m_surface_area = boost::apply_visitor(visitor, m_polyhedron);
}

//! \brief computes the volume of the polyhedron.
Float Indexed_face_set::volume() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::volume(): "
                  << "name: " << get_name() << std::endl;);

  restruct();
  if (m_dirty_polyhedron) clean_polyhedron();
  if (m_dirty_volume) clean_volume();
  return m_volume;
}

//! \brief computes the surface area of the polyhedron.
Float Indexed_face_set::surface_area() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::surface_area(): "
                  << "name: " << get_name() << std::endl;);

  restruct();
  if (m_dirty_polyhedron) clean_polyhedron();
  if (m_dirty_surface_area) clean_surface_area();
  return m_surface_area;
}

//! \brief determines wheather the mesh is consistent.
Boolean Indexed_face_set::is_consistent() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::is_consistent(): "
                  << "name: " << get_name() << std::endl;);

  restruct();
  if (m_dirty_polyhedron) clean_polyhedron();
  return m_consistent;
}

//! \brief determines whether the mesh has singular vertices.
Boolean Indexed_face_set::has_singular_vertices() {
  restruct();
  if (m_dirty_polyhedron) clean_polyhedron();
  return m_has_singular_vertices;
}

//! \brief determines whether the polyhedron representation is empty.
bool Indexed_face_set::is_polyhedron_empty() {
  restruct();
  if (m_dirty_polyhedron) clean_polyhedron();
  return boost::apply_visitor(Empty_polyhedron_visitor(), m_polyhedron);
}

//! \brief determine whether the polyhedron representation is valid.
bool Indexed_face_set::is_polyhedron_valid() {
  restruct();
  if (m_dirty_polyhedron) clean_polyhedron();
  return boost::apply_visitor(Valid_polyhedron_visitor(), m_polyhedron);
}

//! \brief determines whether there are no border edges.
Boolean Indexed_face_set::is_closed() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::is_closed(): "
                  << "name: " << get_name() << std::endl;);

  restruct();
  if (m_dirty_polyhedron) clean_polyhedron();
  return boost::apply_visitor(Is_closed_polyhedron_visitor(), m_polyhedron);
}

//! \brief obtains the number of border edges.
size_t Indexed_face_set::get_number_of_border_edges() {
  restruct();
  if (m_dirty_polyhedron) clean_polyhedron();
  Border_edges_counter_visitor visitor;
  return boost::apply_visitor(visitor, m_polyhedron);
}

//! \bried obtains the number of connected components.
Size Indexed_face_set::get_number_of_connected_components() {
  restruct();
  if (m_dirty_polyhedron) clean_polyhedron();
  Connected_components_counter_visitor visitor;
  return boost::apply_visitor(visitor, m_polyhedron);
}

//! \brief obtains the number of vertices.
Size Indexed_face_set::get_number_of_vertices() {
  restruct();
  if (m_dirty_polyhedron) clean_polyhedron();
  Polyhedron_vertices_counter_visitor visitor;
  return boost::apply_visitor(visitor, m_polyhedron);
}

//! \brief obtains the number of edges.
Size Indexed_face_set::get_number_of_edges() {
  restruct();
  if (m_dirty_polyhedron) clean_polyhedron();
  Polyhedron_halfedges_counter_visitor visitor;
  return boost::apply_visitor(visitor, m_polyhedron) / 2;
}

//! \brief obtains the number of facets.
Size Indexed_face_set::get_number_of_facets() {
  restruct();
  if (m_dirty_polyhedron) clean_polyhedron();
  Polyhedron_facets_counter_visitor visitor;
  return boost::apply_visitor(visitor, m_polyhedron);
}

//! \brief cleans the polyhedron planes.
void Indexed_face_set::clean_polyhedron_planes() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::clean_polyhedron_planes(): "
                  << "name: " << get_name() << std::endl;);

  m_dirty_polyhedron_planes = false;
  bool is_convex = m_primitive_type == Geo_set::PT_TRIANGLES;

  Clean_planes_visitor visitor(is_convex);
  boost::apply_visitor(visitor, m_polyhedron);
  m_dirty_polyhedron_facet_normals = true;
  m_dirty_normal_attributes = true;
}

//! \brief cleans the polyhedron facets.
void Indexed_face_set::clean_polyhedron_facet_normals() {
  SGAL_TRACE_CODE(Tracer::INDEXED_FACE_SET,
                  std::cout << "Indexed_face_set::clean_polyhedron_facet_normals(): "
                  << "name: " << get_name() << std::endl;);

  m_dirty_polyhedron_facet_normals = false;
  Clean_facet_normals_visitor visitor;
  boost::apply_visitor(visitor, m_polyhedron);
  m_dirty_normal_attributes = true;
}

//! \brief cleans the polyhedron edges.
void Indexed_face_set::clean_normal_attributes() {
  m_dirty_normal_attributes = false;
  Clean_normal_attributes_visitor visitor(get_crease_angle());
  std::pair<Boolean, Boolean> res = boost::apply_visitor(visitor, m_polyhedron);
  m_smooth = res.first;
  m_creased = res.second;
}

//! brief sets the polyhedron type.
void Indexed_face_set::set_polyhedron_type(Polyhedron_type type) {
  if (m_polyhedron_type == type) return;
  clear_polyhedron();
  m_polyhedron_type = type;
  init_polyhedron_type();
}

//! \brief adds the container to a given scene.
void Indexed_face_set::add_to_scene(Scene_graph* sg) {
  Boundary_set::add_to_scene(sg);
  const auto* conf = sg->get_configuration();

  if (!conf) return;

  auto modeling = conf->get_modeling();
  if (!modeling) return;

  m_make_consistent = modeling->get_make_consistent();
  m_triangulate_holes = modeling->get_triangulate_holes();
  m_refine = modeling->get_refine_hole_triangulation();
  m_fair = modeling->get_fair_hole_triangulation();
  m_triangulate_facets = modeling->get_triangulate_facets();
  m_remove_isolated_vertices = modeling->get_remove_isolated_vertices();
  m_repair_orientation = modeling->get_repair_orientation();
  m_merge_coplanar_facets = modeling->get_merge_coplanar_facets();
  m_repair_normals = modeling->get_repair_normals();
  m_simplify = modeling->get_simplify();
  m_stop_simplification_strategy =
    modeling->get_stop_simplification_strategy_code();
  m_simplification_count_stop = modeling->get_simplification_count_stop();
  m_simplification_count_ratio_stop =
    modeling->get_simplification_count_ratio_stop();
  m_simplification_edge_length_stop =
    modeling->get_simplification_edge_length_stop();

  // if (m_triangulate_holes) m_holes_triangulated = false;
  // if (m_remove_isolated_vertices) m_isolated_vertices_removed = false;
  // if (m_repair_orientation) m_orientation_repaired = false;
  // if (m_repair_normals) m_normals_repaired = false;
}

//! \brief responds to a change in the coordinate-index array.
void Indexed_face_set::coord_indices_changed(const Field_info* field_info) {
  clear_polyhedron();
  Boundary_set::coord_indices_changed(field_info);
}

//! \brief responds to a change in the normal-index array.
void Indexed_face_set::normal_indices_changed(const Field_info* field_info)
{ Boundary_set::normal_indices_changed(field_info); }

//! \brief responds to a change in the color-index array.
void Indexed_face_set::color_indices_changed(const Field_info* field_info)
{ Boundary_set::color_indices_changed(field_info); }

//! \brief responds to a change in the texture-coordinate index array.
void Indexed_face_set::tex_coord_indices_changed(const Field_info* field_info)
{ Boundary_set::tex_coord_indices_changed(field_info); }

//! \brief responds to a change in the facet coordinate-index array.
void Indexed_face_set::facet_coord_indices_changed() {
  clear_polyhedron();
  Boundary_set::facet_coord_indices_changed();
}

//! \brief responds to a change in the facet normal-index array.
void Indexed_face_set::facet_normal_indices_changed()
{ Boundary_set::facet_normal_indices_changed(); }

//! \brief responds to a change in the facet color-index array.
void Indexed_face_set::facet_color_indices_changed()
{ Boundary_set::facet_color_indices_changed(); }

//! \brief responds to a change in the facet texture-coordinate index array.
void Indexed_face_set::facet_tex_coord_indices_changed()
{ Boundary_set::facet_tex_coord_indices_changed(); }

//! \brief obtains the empty polyhedron.
Indexed_face_set::Polyhedron& Indexed_face_set::get_empty_polyhedron() {
  init_polyhedron_type();
  polyhedron_changed();
  return m_polyhedron;
}

//! \brief obtains the empty inexact polyhedron.
Inexact_polyhedron& Indexed_face_set::get_empty_inexact_polyhedron() {
  m_polyhedron = Inexact_polyhedron();
  m_polyhedron_type = POLYHEDRON_INEXACT;
  polyhedron_changed();
  return boost::get<Inexact_polyhedron>(m_polyhedron);
}

//! \brief obtains the empty epic  polyhedron.
Epic_polyhedron& Indexed_face_set::get_empty_epic_polyhedron() {
  m_polyhedron = Epic_polyhedron();
  m_polyhedron_type = POLYHEDRON_EPIC;
  polyhedron_changed();
  return boost::get<Epic_polyhedron>(m_polyhedron);
}

//! \brief obtains the empty epec  polyhedron.
Epec_polyhedron& Indexed_face_set::get_empty_epec_polyhedron() {
  m_polyhedron = Epec_polyhedron();
  m_polyhedron_type = POLYHEDRON_EPEC;
  polyhedron_changed();
  return boost::get<Epec_polyhedron>(m_polyhedron);
}

/*! \brief Sets the flag that indicates whether to compute the convex hull
 * of the coordinate set.
 */
void Indexed_face_set::set_convex_hull(Boolean flag) {
  if (m_convex_hull == flag) return;
  m_convex_hull = flag;
  clear_polyhedron();
}

//! \brief computes the volume of the convex hull of the polyhedron.
Float Indexed_face_set::volume_of_convex_hull() {
  restruct();
  if (m_dirty_polyhedron) clean_polyhedron();

  Float volume = 0.0f;

  // If the polyhedron is empty, return.
  if (boost::apply_visitor(Empty_polyhedron_visitor(), m_polyhedron))
    return volume;

  //! \todo replace with a visitor.
  const auto& polyhedron = boost::get<Epec_polyhedron>(m_polyhedron);

  if (is_convex_hull()) {
    using Triangulation = CGAL::Triangulation_3<Epec_kernel>;
    Triangulation tri(polyhedron.points_begin(), polyhedron.points_end());
    for (auto it = tri.finite_cells_begin(); it != tri.finite_cells_end(); ++it)
    {
      auto tetr = tri.tetrahedron(it);
      volume += CGAL::to_double(tetr.volume());
    }
  }
  else {
    Epec_polyhedron ch;
    CGAL::convex_hull_3(polyhedron.points_begin(), polyhedron.points_end(), ch);

    using Triangulation = CGAL::Triangulation_3<Epec_kernel>;
    Triangulation tri(ch.points_begin(), ch.points_end());
    for (auto it = tri.finite_cells_begin(); it != tri.finite_cells_end(); ++it)
    {
      auto tetr = tri.tetrahedron(it);
      volume += CGAL::to_double(tetr.volume());
    }
  }

  return volume;
}

//! \brief clones the container (virtual constructor) with deep-copy.
Container* Indexed_face_set::clone() {
  restruct();
  if (is_dirty_coord_array()) clean_coords();
  if (is_dirty_facet_coord_indices()) clean_facet_coord_indices();
  if (!m_normals_repaired && m_repair_normals) repair_normals();
  return Container::clone();
}

SGAL_END_NAMESPACE
