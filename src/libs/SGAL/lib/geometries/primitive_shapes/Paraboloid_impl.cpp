// Copyright (c) 2023 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Author(s): Efi Fogel        <efifogel@gmail.com>

#include <CGAL/Homogeneous.h>
#include <CGAL/Random.h>
#include <CGAL/Min_sphere_annulus_d_traits_3.h>
#include <CGAL/Min_sphere_d.h>

#include "SGAL/basic.hpp"
#include "SGAL/Paraboloid.hpp"

SGAL_BEGIN_NAMESPACE

class Field_info;

//! \brief cleans (recalculate) the bounding sphere of the paraboloid.
void Paraboloid::clean_bounding_sphere() {
  using Kernel = CGAL::Homogeneous<Float>;
  using Traits = CGAL::Min_sphere_annulus_d_traits_3<Kernel>;
  using Min_sphere = CGAL::Min_sphere_d<Traits>;
  using Point = Kernel::Point_3;

  auto height = m_radius*m_radius;
  Point points[] = {
    Point(0, 0, 0),
    Point(m_radius, 0, height),
    Point(-m_radius, 0, height),
    Point(0, m_radius, height)
  };
  Min_sphere ms(points, points+4);
  const auto& center = ms.center();
  auto sr = ms.squared_radius();
  auto radius = sqrtf(CGAL::to_double(sr));
  m_bounding_sphere.set_radius(radius);
  auto x = CGAL::to_double(center.x());
  auto y = CGAL::to_double(center.y());
  auto z = CGAL::to_double(center.z());
  m_bounding_sphere.set_center(Vector3f(x, y, z));
  m_dirty_bounding_sphere = false;
}

//! \brief cleans the paraboloid coordinate array and coordinate indices.
void Paraboloid::clean_coords() {
  m_dirty_coord_array = false;

  // Clear internal representation:
  if (!m_coord_array) m_coord_array.reset(new Coord_array_3d);

  // Generate points:
  auto size = 2 + m_slices * (m_stacks - 1);
  m_coord_array->resize(size);

  auto coord_array = boost::static_pointer_cast<Coord_array_3d>(m_coord_array);
  SGAL_assertion(coord_array);

  // size_t k(0);
  // for (size_t i = 0; i <= m_stacks; ++i) {
  //   if (i == 0) {
  //     Float x = m_width;
  //     Float y = 0;
  //     Float z = 0;
  //     (*coord_array)[k++] = Vector3f(x, y, z);
  //     continue;
  //   }
  //   if (i == m_stacks) {
  //     Float x = -m_width;
  //     Float y = 0;
  //     Float z = 0;
  //     (*coord_array)[k++] = Vector3f(x, y, z);
  //     continue;
  //   }
  //   Float theta = SGAL_PI * i / m_stacks;
  //   for (size_t j =  0; j < m_slices; ++j) {
  //     Float phi = SGAL_PI * j * 2 / m_slices;
  //     Float x = m_width * cosf(theta);
  //     Float y = m_height * sinf(theta) * cosf(phi);
  //     Float z = m_depth * sinf(theta) * sinf(phi);
  //     (*coord_array)[k++] = Vector3f(x, y, z);
  //   }
  // }

  coord_content_changed(get_field_info(COORD_ARRAY));
}

//! \brief cleans the facet coordinate indices.
void Paraboloid::clean_coord_indices() {
  m_dirty_coord_indices = false;

  // Generate start:
  // m_num_primitives = m_slices * m_stacks;
  // auto size = 5 * m_stacks * m_slices - 2 * m_slices;
  // m_coord_indices.resize(size);

  // size_t k(0);
  // size_t start(0);
  // auto end = m_slices;
  // for (size_t i = 1; i <= end; ++i) {
  //   auto j = i + 1;
  //   if (j == end + 1) j = 1;
  //   m_coord_indices[k++] = 0;
  //   m_coord_indices[k++] = i;
  //   m_coord_indices[k++] = j;
  //   m_coord_indices[k++] = static_cast<Uint>(-1);
  // }

  // // Gen middle
  // for (size_t l = 1; l < m_stacks - 1; ++l) {
  //   auto start = m_slices * (l - 1) + 1;
  //   auto end = start + m_slices - 1;
  //   for (size_t i = start; i <= end; ++i) {
  //     auto a = i;
  //     auto b = i + 1;
  //     if (b == end + 1) b = start;
  //     auto c = b + m_slices;
  //     auto d = a + m_slices;
  //     m_coord_indices[k++] = b;
  //     m_coord_indices[k++] = a;
  //     m_coord_indices[k++] = d;
  //     m_coord_indices[k++] = c;
  //     m_coord_indices[k++] = static_cast<Uint>(-1);
  //   }
  // }

  // // Gen end
  // start = m_slices * (m_stacks - 2) + 1;
  // end = m_slices * (m_stacks - 1);
  // for (size_t i  = start; i <= end; ++i) {
  //   auto j = i + 1;
  //   if (j == end + 1) j = start;
  //   m_coord_indices[k++] = end + 1;
  //   m_coord_indices[k++] = j;
  //   m_coord_indices[k++] = i;
  //   m_coord_indices[k++] = static_cast<Uint>(-1);
  // }
}

//! \brief processes change of structure.
void Paraboloid::structure_changed(const Field_info* field_info) {
  clear_coord_array();
  clear_coord_indices();
  field_changed(field_info);
}

SGAL_END_NAMESPACE
