// Copyright (c) 2021 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <iostream>

#if (defined _MSC_VER)
#define NOMINMAX 1
#include <windows.h>
#endif
#include <GL/gl.h>
#include <GL/glext.h>

#include "SGAL/basic.hpp"
#include "SGAL/Point_set.hpp"
#include "SGAL/Coord_array.hpp"
#include "SGAL/Color_array.hpp"
#include "SGAL/Normal_array.hpp"
#include "SGAL/Tex_coord_array.hpp"
#include "SGAL/Tracer.hpp"
#include "SGAL/Gl_wrapper.hpp"

SGAL_BEGIN_NAMESPACE

void Point_set::draw_invalid(Point_set& s)
{ SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "invalid\n"); }

// Coord index: no
// Fragment attachment: none
// Texture mapping: disabled
void Point_set::draw_CINO_FANO_TENO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FANO_TENO_VANO()\n");

  SGAL_assertion(ps.get_coord_array());

  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i)
    glVertex3fv(ps.get_coord_array()->datum(i));
  glEnd();
}

// Coord index: no
// Fragment attachment: none
// Texture mapping: enabled
// Texture index: no
void Point_set::draw_CINO_FANO_TEYE_TINO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FANO_TEYE_TINO_VANO()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_tex_coord_array());

  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment attachment: none
// Texture mapping: enabled
// Texture index: yes
void Point_set::draw_CINO_FANO_TEYE_TIYE_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FANO_TEYE_TIYE_VANO()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& tex_coords = ps.get_tex_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(tex_coords[i]));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: yes
// Fragment attachment: none
// Texture mapping: disabled
void Point_set::draw_CIYE_FANO_TENO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FANO_TENO_VANO()\n");

  SGAL_assertion(ps.get_coord_array());

  const auto& coords = ps.get_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i)
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  glEnd();
}

// Coord index: yes
// Fragment attachment: none
// Texture mapping: enabled
// Texture index: no
void Point_set::draw_CIYE_FANO_TEYE_TINO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FANO_TEYE_TINO_VANO()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& coords = ps.get_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment attachment: none
// Texture mapping: enabled
// Texture index: yes
void Point_set::draw_CIYE_FANO_TEYE_TIYE_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FANO_TEYE_TIYE_VANO()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& coords = ps.get_coord_indices();
  const auto& tex_coords = ps.get_tex_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(tex_coords[i]));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: no
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: disabled
void Point_set::draw_CINO_FSNO_FINO_FAPV_TENO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSNO_FINO_FAPV_TENO_VANO()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());
  SGAL_assertion(ps.get_tex_coord_array());

  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glNormal3fv(ps.get_normal_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: no
void Point_set::draw_CINO_FSNO_FINO_FAPV_TEYE_TINO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSNO_FINO_FAPV_TEYE_TINO_VANO()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());
  SGAL_assertion(ps.get_tex_coord_array());

  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glNormal3fv(ps.get_normal_array()->datum(i));
    glTexCoord2fv(ps.get_tex_coord_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: yes
void Point_set::draw_CINO_FSNO_FINO_FAPV_TEYE_TIYE_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSNO_FINO_FAPV_TEYE_TIYE_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& tex_coords = ps.get_tex_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glNormal3fv(ps.get_normal_array()->datum(i));
    glTexCoord2fv(ps.get_tex_coord_array()->datum(tex_coords[i]));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: disabled
void Point_set::draw_CINO_FSNO_FINO_FAPS_TENO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSNO_FINO_FAPS_TENO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());

  glNormal3fv(ps.get_normal_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i)
    glVertex3fv(ps.get_coord_array()->datum(i));
  glEnd();
}

// Coord index: no
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: no
void Point_set::draw_CINO_FSNO_FINO_FAPS_TEYE_TINO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSNO_FINO_FAPS_TEYE_TINO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());
  SGAL_assertion(ps.get_tex_coord_array());

  glNormal3fv(ps.get_normal_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: yes
void Point_set::draw_CINO_FSNO_FINO_FAPS_TEYE_TIYE_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSNO_FINO_FAPS_TEYE_TIYE_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& tex_coords = ps.get_tex_coord_indices();
  glNormal3fv(ps.get_normal_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(tex_coords[i]));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: disabled
void Point_set::draw_CINO_FSNO_FIYE_FAPV_TENO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSNO_FIYE_FAPV_TENO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());

  const auto& normals = ps.get_normal_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glNormal3fv(ps.get_normal_array()->datum(normals[i]));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: no
void Point_set::draw_CINO_FSNO_FIYE_FAPV_TEYE_TINO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSNO_FIYE_FAPV_TEYE_TINO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& normals = ps.get_normal_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glNormal3fv(ps.get_normal_array()->datum(normals[i]));
    glTexCoord2fv(ps.get_tex_coord_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: yes
void Point_set::draw_CINO_FSNO_FIYE_FAPV_TEYE_TIYE_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSNO_FIYE_FAPV_TEYE_TIYE_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& normals = ps.get_normal_indices();
  const auto& tex_coords = ps.get_tex_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glNormal3fv(ps.get_normal_array()->datum(normals[i]));
    glTexCoord2fv(ps.get_tex_coord_array()->datum(tex_coords[i]));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: disabled
void Point_set::draw_CINO_FSNO_FIYE_FAPS_TENO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSNO_FIYE_FAPS_TENO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());

  const auto& normals = ps.get_normal_indices();
  glNormal3fv(ps.get_normal_array()->datum(normals[0]));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i)
    glVertex3fv(ps.get_coord_array()->datum(i));
  glEnd();
}

// Coord index: no
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: no
void Point_set::draw_CINO_FSNO_FIYE_FAPS_TEYE_TINO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSNO_FIYE_FAPS_TEYE_TINO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& normals = ps.get_normal_indices();
  glNormal3fv(ps.get_normal_array()->datum(normals[0]));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: yes
void Point_set::draw_CINO_FSNO_FIYE_FAPS_TEYE_TIYE_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSNO_FIYE_FAPS_TEYE_TIYE_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& normals = ps.get_normal_indices();
  const auto& tex_coords = ps.get_tex_coord_indices();
  glNormal3fv(ps.get_normal_array()->datum(normals[0]));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(tex_coords[i]));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: color
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: disabled
void Point_set::draw_CINO_FSCO_FINO_FAPV_TENO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSCO_FINO_FAPV_TENO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());

  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glColor3fv(ps.get_color_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: color
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: no
void Point_set::draw_CINO_FSCO_FINO_FAPV_TEYE_TINO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSCO_FINO_FAPV_TEYE_TINO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());
  SGAL_assertion(ps.get_tex_coord_array());

  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glColor3fv(ps.get_color_array()->datum(i));
    glTexCoord2fv(ps.get_tex_coord_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: color
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: yes
void Point_set::draw_CINO_FSCO_FINO_FAPV_TEYE_TIYE_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSCO_FINO_FAPV_TEYE_TIYE_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& tex_coords = ps.get_tex_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glColor3fv(ps.get_color_array()->datum(i));
    glTexCoord2fv(ps.get_tex_coord_array()->datum(tex_coords[i]));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: color
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: disabled
void Point_set::draw_CINO_FSCO_FINO_FAPS_TENO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSCO_FINO_FAPS_TENO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());

  glColor3fv(ps.get_color_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i)
    glVertex3fv(ps.get_coord_array()->datum(i));
  glEnd();
}

// Coord index: no
// Fragment source: color
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: no
void Point_set::draw_CINO_FSCO_FINO_FAPS_TEYE_TINO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSCO_FINO_FAPS_TEYE_TINO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());
  SGAL_assertion(ps.get_tex_coord_array());

  glColor3fv(ps.get_color_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: color
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: yes
void Point_set::draw_CINO_FSCO_FINO_FAPS_TEYE_TIYE_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSCO_FINO_FAPS_TEYE_TIYE_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& tex_coords = ps.get_tex_coord_indices();
  glColor3fv(ps.get_color_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(tex_coords[i]));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: disabled
void Point_set::draw_CINO_FSCO_FIYE_FAPV_TENO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSCO_FIYE_FAPV_TENO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());

  const auto& colors = ps.get_color_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glColor3fv(ps.get_color_array()->datum(colors[i]));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: no
void Point_set::draw_CINO_FSCO_FIYE_FAPV_TEYE_TINO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSCO_FIYE_FAPV_TEYE_TINO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& colors = ps.get_color_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glColor3fv(ps.get_color_array()->datum(colors[i]));
    glTexCoord2fv(ps.get_tex_coord_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: yes
void Point_set::draw_CINO_FSCO_FIYE_FAPV_TEYE_TIYE_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSCO_FIYE_FAPV_TEYE_TIYE_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& colors = ps.get_color_indices();
  const auto& tex_coords = ps.get_tex_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glColor3fv(ps.get_color_array()->datum(colors[i]));
    glTexCoord2fv(ps.get_tex_coord_array()->datum(tex_coords[i]));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: disabled
void Point_set::draw_CINO_FSCO_FIYE_FAPS_TENO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSCO_FIYE_FAPS_TENO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());

  const auto& colors = ps.get_color_indices();
  glColor3fv(ps.get_color_array()->datum(colors[0]));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i)
    glVertex3fv(ps.get_coord_array()->datum(i));
  glEnd();
}

// Coord index: no
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: no
void Point_set::draw_CINO_FSCO_FIYE_FAPS_TEYE_TINO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSCO_FIYE_FAPS_TEYE_TINO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& colors = ps.get_color_indices();
  glColor3fv(ps.get_color_array()->datum(colors[0]));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: no
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: yes
void Point_set::draw_CINO_FSCO_FIYE_FAPS_TEYE_TIYE_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CINO_FSCO_FIYE_FAPS_TEYE_TIYE_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& colors = ps.get_color_indices();
  const auto& tex_coords = ps.get_tex_coord_indices();
  glColor3fv(ps.get_color_array()->datum(colors[0]));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(tex_coords[i]));
    glVertex3fv(ps.get_coord_array()->datum(i));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: disabled
void Point_set::draw_CIYE_FSNO_FINO_FAPV_TENO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSNO_FINO_FAPV_TENO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());

  const auto& coords = ps.get_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glNormal3fv(ps.get_normal_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: no
void Point_set::draw_CIYE_FSNO_FINO_FAPV_TEYE_TINO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSNO_FINO_FAPV_TEYE_TINO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& coords = ps.get_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glNormal3fv(ps.get_normal_array()->datum(i));
    glTexCoord2fv(ps.get_tex_coord_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: yes
void Point_set::draw_CIYE_FSNO_FINO_FAPV_TEYE_TIYE_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSNO_FINO_FAPV_TEYE_TIYE_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& coords = ps.get_coord_indices();
  const auto& tex_coords = ps.get_tex_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glNormal3fv(ps.get_normal_array()->datum(i));
    glTexCoord2fv(ps.get_tex_coord_array()->datum(tex_coords[i]));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: disabled
void Point_set::draw_CIYE_FSNO_FINO_FAPS_TENO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSNO_FINO_FAPS_TENO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());

  const auto& coords = ps.get_coord_indices();
  glNormal3fv(ps.get_normal_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i)
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  glEnd();
}

// Coord index: yes
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: no
void Point_set::draw_CIYE_FSNO_FINO_FAPS_TEYE_TINO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSNO_FINO_FAPS_TEYE_TINO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& coords = ps.get_coord_indices();
  glNormal3fv(ps.get_normal_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: yes
void Point_set::draw_CIYE_FSNO_FINO_FAPS_TEYE_TIYE_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSNO_FINO_FAPS_TEYE_TIYE_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& coords = ps.get_coord_indices();
  const auto& tex_coords = ps.get_tex_coord_indices();
  glNormal3fv(ps.get_normal_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(tex_coords[i]));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: disabled
void Point_set::draw_CIYE_FSNO_FIYE_FAPV_TENO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSNO_FIYE_FAPV_TENO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());

  const auto& coords = ps.get_coord_indices();
  glNormal3fv(ps.get_normal_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i)
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  glEnd();
}

// Coord index: yes
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: no
void Point_set::draw_CIYE_FSNO_FIYE_FAPV_TEYE_TINO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSNO_FIYE_FAPV_TEYE_TINO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& coords = ps.get_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glNormal3fv(ps.get_normal_array()->datum(i));
    glTexCoord2fv(ps.get_tex_coord_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: yes
void Point_set::draw_CIYE_FSNO_FIYE_FAPV_TEYE_TIYE_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSNO_FIYE_FAPV_TEYE_TIYE_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& coords = ps.get_coord_indices();
  const auto& tex_coords = ps.get_tex_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glNormal3fv(ps.get_normal_array()->datum(i));
    glTexCoord2fv(ps.get_tex_coord_array()->datum(tex_coords[i]));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: disabled
void Point_set::draw_CIYE_FSNO_FIYE_FAPS_TENO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSNO_FIYE_FAPS_TENO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());

  const auto& coords = ps.get_coord_indices();
  glNormal3fv(ps.get_normal_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i)
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  glEnd();
}

// Coord index: yes
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: no
void Point_set::draw_CIYE_FSNO_FIYE_FAPS_TEYE_TINO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSNO_FIYE_FAPS_TEYE_TINO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& coords = ps.get_coord_indices();
  glNormal3fv(ps.get_normal_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: yes
void Point_set::draw_CIYE_FSNO_FIYE_FAPS_TEYE_TIYE_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSNO_FIYE_FAPS_TEYE_TIYE_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_normal_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& coords = ps.get_coord_indices();
  const auto& tex_coords = ps.get_tex_coord_indices();
  glNormal3fv(ps.get_normal_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(tex_coords[i]));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
}
  glEnd();
}

// Coord index: yes
// Fragment source: color
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: disabled
void Point_set::draw_CIYE_FSCO_FINO_FAPV_TENO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSCO_FINO_FAPV_TENO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());

  const auto& coords = ps.get_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glColor3fv(ps.get_color_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: color
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: no
void Point_set::draw_CIYE_FSCO_FINO_FAPV_TEYE_TINO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSCO_FINO_FAPV_TEYE_TINO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& coords = ps.get_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glColor3fv(ps.get_color_array()->datum(i));
    glTexCoord2fv(ps.get_tex_coord_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: color
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: yes
void Point_set::draw_CIYE_FSCO_FINO_FAPV_TEYE_TIYE_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSCO_FINO_FAPV_TEYE_TIYE_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& coords = ps.get_coord_indices();
  const auto& tex_coords = ps.get_tex_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glColor3fv(ps.get_color_array()->datum(i));
    glTexCoord2fv(ps.get_tex_coord_array()->datum(tex_coords[i]));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: color
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: disabled
void Point_set::draw_CIYE_FSCO_FINO_FAPS_TENO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSCO_FINO_FAPS_TENO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());

  const auto& coords = ps.get_coord_indices();
  glColor3fv(ps.get_color_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i)
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  glEnd();
}

// Coord index: yes
// Fragment source: color
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: no
void Point_set::draw_CIYE_FSCO_FINO_FAPS_TEYE_TINO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSCO_FINO_FAPS_TEYE_TINO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& coords = ps.get_coord_indices();
  glColor3fv(ps.get_color_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: color
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: yes
void Point_set::draw_CIYE_FSCO_FINO_FAPS_TEYE_TIYE_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSCO_FINO_FAPS_TEYE_TIYE_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& coords = ps.get_coord_indices();
  const auto& tex_coords = ps.get_tex_coord_indices();
  glColor3fv(ps.get_color_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(tex_coords[i]));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: disabled
void Point_set::draw_CIYE_FSCO_FIYE_FAPV_TENO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSCO_FIYE_FAPV_TENO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());

  const auto& coords = ps.get_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glColor3fv(ps.get_color_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: no
void Point_set::draw_CIYE_FSCO_FIYE_FAPV_TEYE_TINO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSCO_FIYE_FAPV_TEYE_TINO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& coords = ps.get_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glColor3fv(ps.get_color_array()->datum(i));
    glTexCoord2fv(ps.get_tex_coord_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: yes
void Point_set::draw_CIYE_FSCO_FIYE_FAPV_TEYE_TIYE_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSCO_FIYE_FAPV_TEYE_TIYE_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& coords = ps.get_coord_indices();
  const auto& tex_coords = ps.get_tex_coord_indices();
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glColor3fv(ps.get_color_array()->datum(i));
    glTexCoord2fv(ps.get_tex_coord_array()->datum(tex_coords[i]));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: disabled
void Point_set::draw_CIYE_FSCO_FIYE_FAPS_TENO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSCO_FIYE_FAPS_TENO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());

  const auto& coords = ps.get_coord_indices();
  glColor3fv(ps.get_color_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: no
void Point_set::draw_CIYE_FSCO_FIYE_FAPS_TEYE_TINO_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSCO_FIYE_FAPS_TEYE_TINO_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& coords = ps.get_coord_indices();
  glColor3fv(ps.get_color_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(i));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

// Coord index: yes
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: yes
void Point_set::draw_CIYE_FSCO_FIYE_FAPS_TEYE_TIYE_VANO(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "draw_CIYE_FSCO_FIYE_FAPS_TEYE_TIYE_VANO(Point_set& ps()\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_assertion(ps.get_color_array());
  SGAL_assertion(ps.get_tex_coord_array());

  const auto& coords = ps.get_coord_indices();
  const auto& tex_coords = ps.get_tex_coord_indices();
  glColor3fv(ps.get_color_array()->datum(0));
  glBegin(GL_POINTS);
  for (size_t i = 0; i < ps.get_num_primitives(); ++i) {
    glTexCoord2fv(ps.get_tex_coord_array()->datum(tex_coords[i]));
    glVertex3fv(ps.get_coord_array()->datum(coords[i]));
  }
  glEnd();
}

void Point_set::draw_FAPV_VAYE(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "FAPV_VAYE\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_error_msg("Not implemented yet!");
}

void Point_set::draw_FAPL_VAYE(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "FAPL_VAYE\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_error_msg("Not implemented yet!");
}

void Point_set::draw_FAPM_VAYE(Point_set& ps) {
  SGAL_TRACE_MSG(Tracer::INDEXED_POINT_SET, "FAPM_VAYE\n");

  SGAL_assertion(ps.get_coord_array());
  SGAL_error_msg("Not implemented yet!");
}

SGAL_END_NAMESPACE
