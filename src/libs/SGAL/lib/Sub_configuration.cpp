// Copyright (c) 2020 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include "SGAL/basic.hpp"
#include "SGAL/Sub_configuration.hpp"
#include "SGAL/Container_proto.hpp"

SGAL_BEGIN_NAMESPACE

Container_proto* Sub_configuration::s_prototype(nullptr);

//! \brief constructs.
Sub_configuration::Sub_configuration(Boolean proto) : Node(proto) {}

//! \brief destruct.
Sub_configuration::~Sub_configuration() {}

//! \brief initializes the node prototype.
void Sub_configuration::init_prototype()
{
  if (s_prototype) return;
  s_prototype = new Container_proto(Node::get_prototype());
}

//! \brief deletes the node prototype.
void Sub_configuration::delete_prototype()
{
  delete s_prototype;
  s_prototype = nullptr;
}

//! \brief obtains the node prototype.
Container_proto* Sub_configuration::get_prototype()
{
  if (!s_prototype) Node::init_prototype();
  return s_prototype;
}

//! \brief sets the attributes of this node.
void Sub_configuration::set_attributes(Element* elem)
{
  Node::set_attributes(elem);
}

SGAL_END_NAMESPACE
