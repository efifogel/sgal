// Copyright (c) 2004 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <boost/lexical_cast.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/Configuration.hpp"
#include "SGAL/create_container.hpp"
#include "SGAL/Scene_graph.hpp"
#include "SGAL/Field_rule.hpp"
#include "SGAL/Field_infos.hpp"
#include "SGAL/Field.hpp"
#include "SGAL/Element.hpp"
#include "SGAL/Container_factory.hpp"
#include "SGAL/Container_proto.hpp"
#include "SGAL/Execution_function.hpp"
#include "SGAL/Accumulation.hpp"
#include "SGAL/Multisample.hpp"
#include "SGAL/Modeling.hpp"
#include "SGAL/Window_item.hpp"
#include "SGAL/Utilities.hpp"
#include "SGAL/to_boolean.hpp"
#include "SGAL/Sub_configuration.hpp"
#include "SGAL/Input_output.hpp"

SGAL_BEGIN_NAMESPACE

const std::string Configuration::s_tag = "Configuration";
Container_proto* Configuration::s_prototype(nullptr);

// Default values:
const Configuration::Viewpoint_mode
  Configuration::s_def_viewpoint_mode(VM_VIEWING);
const Configuration::Geometry_drawing_mode
  Configuration::s_def_geometry_drawing_mode(Configuration::GDM_VERTEX_ARRAY);
const Boolean Configuration::s_def_use_vertex_buffer_object(true);
const Boolean Configuration::s_def_are_global_lights_stationary(false);
const Boolean Configuration::s_def_texture_map(true);
const Boolean Configuration::s_def_is_fixed_head_light(true);
const Uint Configuration::s_def_stencil_bits(SGAL_DEF_WINDOW_STENCIL_BITS);
const Uint Configuration::s_def_depth_bits(SGAL_DEF_WINDOW_DEPTH_BITS);
const Float Configuration::s_def_min_frame_rate(15);
const Gfx::Poly_mode Configuration::s_def_poly_mode(Gfx::FILL_PMODE);
const Boolean Configuration::s_def_display_fps(false);
const Float Configuration::s_def_min_zoom_distance(0);
const Float Configuration::s_def_speed_factor(100);
const Uint Configuration::s_def_verbose_level(0);
const Boolean Configuration::s_def_seamless_cube_map(true);
const Boolean Configuration::s_def_override_material(true);
const Boolean Configuration::s_def_override_tex_enable(true);
const Boolean Configuration::s_def_override_tex_env(false);
const Boolean Configuration::s_def_override_blend_func(true);
const Boolean Configuration::s_def_override_light_model(true);
const Boolean Configuration::s_def_override_tex_gen(true);
const Boolean Configuration::s_def_override_light_enable(true);

const Char* Configuration::s_geometry_drawing_mode_names[] =
  { "direct", "displayList", "vertexArray" };

const Char* Configuration::s_viewpoint_mode_names[] =
  { "viewing", "modeling" };

REGISTER_TO_FACTORY(Configuration, "Configuration");

//! \brief constructs.
Configuration::Configuration(Boolean proto) :
  Bindable_node(proto),
  m_scene_graph(nullptr),
  m_viewpoint_mode(s_def_viewpoint_mode),
  m_geometry_drawing_mode(s_def_geometry_drawing_mode),
  m_use_vertex_buffer_object(s_def_use_vertex_buffer_object),
  m_are_global_lights_stationary(s_def_are_global_lights_stationary),
  m_texture_map(s_def_texture_map),
  m_is_fixed_head_light(s_def_is_fixed_head_light),
  m_stencil_bits(s_def_stencil_bits),
  m_depth_bits(s_def_depth_bits),
  m_min_frame_rate(s_def_min_frame_rate),
  m_poly_mode(s_def_poly_mode),
  m_display_fps(s_def_display_fps),
  m_min_zoom_distance(s_def_min_zoom_distance),
  m_speed_factor(s_def_speed_factor),
  m_verbose_level(s_def_verbose_level),
  m_seamless_cube_map(s_def_seamless_cube_map),
  m_override_material(s_def_override_material),
  m_override_tex_enable(s_def_override_tex_enable),
  m_override_tex_env(s_def_override_tex_env),
  m_override_blend_func(s_def_override_blend_func),
  m_override_light_model(s_def_override_light_model),
  m_override_tex_gen(s_def_override_tex_gen),
  m_override_light_enable(s_def_override_light_enable)
{}

//! \brief destructs.
Configuration::~Configuration() {}

//! \brief creates a new container of this type (virtual copy constructor).
Container* Configuration::create()
{ return new Configuration(*this); }

//! \brief resets to factory settings.
void Configuration::reset()
{
  if (m_accumulation) m_accumulation->reset();
  if (m_modeling) m_modeling->reset();

  m_viewpoint_mode = s_def_viewpoint_mode;
  m_geometry_drawing_mode = s_def_geometry_drawing_mode;
  m_are_global_lights_stationary = s_def_are_global_lights_stationary;
  m_texture_map = s_def_texture_map;
  m_is_fixed_head_light = s_def_is_fixed_head_light;
  m_stencil_bits = s_def_stencil_bits;
  m_depth_bits = s_def_depth_bits;
  m_min_frame_rate = s_def_min_frame_rate;
  m_poly_mode = s_def_poly_mode;
  m_display_fps = s_def_display_fps;
  m_min_zoom_distance = s_def_min_zoom_distance;
  m_speed_factor = s_def_speed_factor;
  m_seamless_cube_map = s_def_seamless_cube_map;
}

//! \brief initializess the node prototype.
void Configuration::init_prototype()
{
  if (s_prototype) return;
  s_prototype = new Container_proto(Bindable_node::get_prototype());

  // Add the object fields to the prototype
  Execution_function exec_func;

  // polyMode
  exec_func =
    static_cast<Execution_function>(&Container::set_rendering_required);
  auto poly_mode_func =
    reinterpret_cast<Uint_handle_function>(&Configuration::poly_mode_handle);
  s_prototype->add_field_info(new SF_uint(POLY_MODE, "polyMode",
                                          Field_rule::RULE_EXPOSED_FIELD,
                                          poly_mode_func,
                                          s_def_poly_mode, exec_func));

  // displayFPS
  auto display_fps_func =
    static_cast<Boolean_handle_function>(&Configuration::display_fps_handle);
  s_prototype->add_field_info(new SF_bool(DISPLAY_FPS, "displayFPS",
                                          Field_rule::RULE_EXPOSED_FIELD,
                                          display_fps_func,
                                          s_def_display_fps, exec_func));

  // fixedHeadLight
  auto is_fixed_head_light_func =
    static_cast<Boolean_handle_function>
    (&Configuration::is_fixed_head_light_handle);
  s_prototype->add_field_info(new SF_bool(FIXED_HEADLIGHT, "fixedHeadLight",
                                          Field_rule::RULE_EXPOSED_FIELD,
                                          is_fixed_head_light_func,
                                          s_def_is_fixed_head_light,
                                          exec_func));

  // stencilBits
  auto stencil_bits_func =
    static_cast<Uint_handle_function>(&Configuration::stencil_bits_handle);
  s_prototype->add_field_info(new SF_uint(STENCIL_BITS, "stencilBits",
                                          Field_rule::RULE_EXPOSED_FIELD,
                                          stencil_bits_func,
                                          s_def_stencil_bits));

  // depthBits
  auto depth_bits_func =
    static_cast<Uint_handle_function>(&Configuration::depth_bits_handle);
  s_prototype->add_field_info(new SF_uint(DEPTH_BITS, "depthBits",
                                          Field_rule::RULE_EXPOSED_FIELD,
                                          depth_bits_func, s_def_depth_bits));

  // minFrameRate
  auto min_frame_rate_func =
    static_cast<Float_handle_function>(&Configuration::min_frame_rate_handle);
  s_prototype->add_field_info(new SF_float(MIN_FRAME_RATE, "minFrameRate",
                                           Field_rule::RULE_EXPOSED_FIELD,
                                           min_frame_rate_func,
                                           s_def_min_frame_rate));

  // minZoomDistance
  auto min_zoom_distance_func = static_cast<Float_handle_function>
    (&Configuration::min_zoom_distance_handle);
  s_prototype->add_field_info(new SF_float(MIN_ZOOM_DISTANCE,
                                           "minZoomDistance",
                                           Field_rule::RULE_EXPOSED_FIELD,
                                           min_zoom_distance_func,
                                           s_def_min_zoom_distance));

  // speedFacotor
  auto speed_factor_func =
    static_cast<Float_handle_function>(&Configuration::speed_factor_handle);
  s_prototype->add_field_info(new SF_float(SPEED_FACTOR, "speedFacotor",
                                           Field_rule::RULE_EXPOSED_FIELD,
                                           speed_factor_func,
                                           s_def_speed_factor));

  // textureMap
  auto texture_map_func =
    static_cast<Boolean_handle_function>(&Configuration::texture_map_handle);
  s_prototype->add_field_info(new SF_bool(TEXTURE_MAP, "textureMap",
                                          Field_rule::RULE_EXPOSED_FIELD,
                                          texture_map_func,
                                          s_def_texture_map));

  // verbosityLevel
  auto verbose_level_func =
    static_cast<Uint_handle_function>(&Configuration::verbose_level_handle);
  s_prototype->add_field_info(new SF_uint(VERBOSITY_LEVEL, "verbosityLevel",
                                          Field_rule::RULE_EXPOSED_FIELD,
                                          verbose_level_func,
                                          s_def_verbose_level));

  // seamlessCubeMap
  auto seamless_cube_map_func =
    static_cast<Boolean_handle_function>
    (&Configuration::seamless_cube_map_handle);
  s_prototype->add_field_info(new SF_bool(SEAMLESS_CUBE_MAP, "seamlessCubeMap",
                                          Field_rule::RULE_EXPOSED_FIELD,
                                          seamless_cube_map_func,
                                          s_def_seamless_cube_map));

  // Accumulation
  auto accumulation_func =
    reinterpret_cast<Shared_container_handle_function>
    (&Configuration::accumulation_handle);
  s_prototype->add_field_info(new SF_shared_container(ACCUMULATION,
                                                      "accumulation",
                                                      Field_rule::RULE_EXPOSED_FIELD,
                                                      accumulation_func,
                                                      exec_func));

  // Modeling
  auto modeling_func =
    reinterpret_cast<Shared_container_handle_function>
    (&Configuration::modeling_handle);
  s_prototype->add_field_info(new SF_shared_container(MODELING,
                                                      "modeling",
                                                      Field_rule::RULE_EXPOSED_FIELD,
                                                      modeling_func,
                                                      exec_func));

  // Sub-configurations
  auto sub_configurations_func =
    reinterpret_cast<Shared_container_array_handle_function>
    (&Configuration::sub_configurations_handle);
  s_prototype->add_field_info(new MF_shared_container(SUB_CONFIGURATIONS,
                                                      "subConfigurations",
                                                      Field_rule::RULE_EXPOSED_FIELD,
                                                      sub_configurations_func,
                                                      exec_func));
}

//! \brief deletes the node prototype.
void Configuration::delete_prototype()
{
  delete s_prototype;
  s_prototype = nullptr;
}

//! \brief obtains the node prototype.
Container_proto* Configuration::get_prototype()
{
  if (!s_prototype) Configuration::init_prototype();
  return s_prototype;
}

//! \brief sets the attributes of the object.
void Configuration::set_attributes(Element* elem)
{
  Bindable_node::set_attributes(elem);

  for (auto ai = elem->str_attrs_begin(); ai != elem->str_attrs_end(); ++ai) {
    const auto& name = elem->get_name(ai);
    const auto& value = elem->get_value(ai);
    if (name == "stencilBits") {
      set_number_of_stencil_bits(boost::lexical_cast<Uint>(value));
      elem->mark_delete(ai);
      continue;
    }
    if (name == "depthBits") {
      set_number_of_depth_bits(boost::lexical_cast<Uint>(value));
      elem->mark_delete(ai);
      continue;
    }
    if (name == "minFrameRate") {
      set_min_frame_rate(boost::lexical_cast<Float>(value));
      elem->mark_delete(ai);
      continue;
    }
    if (name == "globalLightsStationary") {
      set_global_lights_stationary(compare_to_true(value));
      elem->mark_delete(ai);
      continue;
    }
    if (name == "viewpointMode") {
      auto num = sizeof(s_viewpoint_mode_names) / sizeof(char *);
      const auto** found =
        std::find(s_viewpoint_mode_names, &s_viewpoint_mode_names[num], value);
      auto index = found - s_viewpoint_mode_names;
      if (index < num) m_viewpoint_mode = static_cast<Viewpoint_mode>(index);
      elem->mark_delete(ai);
      continue;
    }
    if (name == "geometryDrawingMode") {
      auto num = sizeof(s_geometry_drawing_mode_names) / sizeof(char *);
      const auto** found = std::find(s_geometry_drawing_mode_names,
                                     &s_geometry_drawing_mode_names[num],
                                     value);
      auto index = found - s_geometry_drawing_mode_names;
      if (index < num)
        m_geometry_drawing_mode = static_cast<Geometry_drawing_mode>(index);
      elem->mark_delete(ai);
      continue;
    }
    if (name == "fixedHeadLight") {
      set_fixed_head_light(compare_to_true(value));
      elem->mark_delete(ai);
      continue;
    }
    if (name == "polyMode") {
      if (value == "line") set_poly_mode(Gfx::LINE_PMODE);
      else if (value == "point") set_poly_mode(Gfx::POINT_PMODE);
      else std::cerr << "Unrecognized polygon mode \"" << value << "\"!"
                     << std::endl;
      elem->mark_delete(ai);
      continue;
    }
    if (name == "displayFPS") {
      set_display_fps(compare_to_true(value));
      elem->mark_delete(ai);
      continue;
    }
    if (name == "minZoomDistance") {
      set_min_zoom_distance(boost::lexical_cast<Float>(value));
      elem->mark_delete(ai);
      continue;
    }
    if (name == "maxModelName") {
      set_max_model_name(value);
      elem->mark_delete(ai);
      continue;
    }
    if (name == "textureMap") {
      m_texture_map = compare_to_true(value);
      elem->mark_delete(ai);
      continue;
    }
    if (name == "verbosityLevel") {
      set_verbose_level(boost::lexical_cast<Uint>(value));
      elem->mark_delete(ai);
      continue;
    }
    if (name == "seamlessCubeMap") {
      set_seamless_cube_map(compare_to_true(value));
      elem->mark_delete(ai);
      continue;
    }
    if (name == "overrideMaterial") {
      m_override_material = compare_to_true(value);
      elem->mark_delete(ai);
      continue;
    }
    if (name == "overrideTexEnable") {
      m_override_tex_enable = compare_to_true(value);
      elem->mark_delete(ai);
      continue;
    }
    if (name == "overrideTexEnv") {
      m_override_tex_env = compare_to_true(value);
      elem->mark_delete(ai);
      continue;
    }
    if (name == "overrideBlendFunc") {
      m_override_blend_func = compare_to_true(value);
      elem->mark_delete(ai);
      continue;
    }
    if (name == "overrideLightModel") {
      m_override_light_model = compare_to_true(value);
      elem->mark_delete(ai);
      continue;
    }
    if (name == "overrideTexGen") {
      m_override_tex_gen = compare_to_true(value);
      elem->mark_delete(ai);
      continue;
    }
    if (name == "overrideLightEnable") {
      m_override_light_enable = compare_to_true(value);
      elem->mark_delete(ai);
      continue;
    }
  }

  for (auto cai = elem->cont_attrs_begin(); cai != elem->cont_attrs_end();
       ++cai)
  {
    const auto& name = elem->get_name(cai);
    auto cont = elem->get_value(cai);
    if (name == "accumulation") {
      auto acc = boost::dynamic_pointer_cast<Accumulation>(cont);
      set_accumulation(acc);
      elem->mark_delete(cai);
      continue;
    }
    if (name == "multisample") {
      auto ms = boost::dynamic_pointer_cast<Multisample>(cont);
      set_multisample(ms);
      elem->mark_delete(cai);
      continue;
    }
    if (name == "inputOutput") {
      auto io = boost::dynamic_pointer_cast<Input_output>(cont);
      set_input_output(io);
      elem->mark_delete(cai);
      continue;
    }
    if (name == "modeling") {
      auto modeling = boost::dynamic_pointer_cast<Modeling>(cont);
      set_modeling(modeling);
      elem->mark_delete(cai);
      continue;
    }
  }

  for (auto mcai = elem->multi_cont_attrs_begin();
       mcai != elem->multi_cont_attrs_end(); ++mcai)
  {
    const auto& name = elem->get_name(mcai);
    auto& cont_list = elem->get_value(mcai);
    if (name == "subConfigurations") {
      for (auto& cont : cont_list)
        add_sub_configuration(boost::dynamic_pointer_cast<Sub_configuration>(cont));
      elem->mark_delete(mcai);
      continue;
    }
  }

  // Remove all the marked attributes:
  elem->delete_marked();
}

/*! \brief adds the container to a given scene.
 */
void Configuration::add_to_scene(Scene_graph* sg)
{
  typedef boost::shared_ptr<SGAL::Container> Shared_container;
  auto* factory = SGAL::Container_factory::get_instance();

  // Introduce an Input_output and modeling configuration containers if not
  // present:
  if (! get_input_output()) {
    auto io = create_container<Input_output>("InputOutput", sg);
    set_input_output(io);
  }
  if (! get_modeling()) {
    auto modeling = create_container<Modeling>("Modeling", sg);
    set_modeling(modeling);
  }

  // Introduce sub-configuration containers if not present. However, we do not
  // know their exact type, so we simply use the sub-configuration containers
  // present in the previous configuration. This is awkward, because the
  // previous configuration may not be identical to the configuration prototype.
  //! \todo somehow collect prototypes of all sub-configurations and introduce
  //        from each a new sub-configuration container.
  auto prev_conf = sg->get_configuration();
  if (prev_conf) {
    if (get_sub_configurations().empty())
      set_sub_configurations(prev_conf->get_sub_configurations());
  }
  set_scene_graph(sg);
  push_stack(this);
}

//! \brief sets the verbosity level.
void Configuration::set_verbose_level(Uint level)
{
  m_verbose_level = level;
  SGAL::Field* field = get_field(VERBOSITY_LEVEL);
  if (field) field->cascade();
}

//! \brief enables the camera---called when the camera is bound.
void Configuration::enable() {}

//! \brief adds a sub-configuration to the array of sub-configurations.
void Configuration::add_sub_configuration(Shared_sub_configuration sub_conf)
{ m_sub_configurations.push_back(sub_conf); }

//! \brief obtains the (const) bindable stack.
const Bindable_stack& Configuration::get_stack() const
{ return m_scene_graph->get_configuration_stack(); }

//! \brief obtains the (non-const) bindable stack.
Bindable_stack& Configuration::get_stack()
{ return m_scene_graph->get_configuration_stack(); }

SGAL_END_NAMESPACE
