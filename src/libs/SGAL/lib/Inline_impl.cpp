// Copyright (c) 2018 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel        <efifogel@gmail.com>

#include <iostream>
#include <fstream>

#include "SGAL/basic.hpp"
#include "SGAL/Configuration.hpp"
#include "SGAL/Field_info.hpp"
#include "SGAL/find_file.hpp"
#include "SGAL/Inline.hpp"
#include "SGAL/Input_output.hpp"
#include "SGAL/Loader.hpp"
#include "SGAL/Loader_errors.hpp"
#include "SGAL/Scene_graph.hpp"

SGAL_BEGIN_NAMESPACE

/*! Process change of url field.
 */
void Inline::url_changed(const Field_info* field_info)
{
  m_dirty_childs = true;
  field_changed(field_info);
}

//! \brief adds the container to a given scene.
void Inline::add_to_scene(Scene_graph* sg)
{
  Group::add_to_scene(sg);
  if (m_dirty_childs) clean_childs();
}

//! \brief cleans (generate) the children.
void Inline::clean_childs()
{
  Group::clean_childs();
  const auto& filename = m_url[0];
  auto& loader = *(SGAL::Loader::get_instance());
  const auto* conf = m_scene_graph->get_configuration();
  std::string fullname;
  if (! conf) fullname = filename;
  else {
    auto io = conf->get_input_output();
    if (! io) fullname = filename;
    else {
      const auto& input_paths = io->get_input_paths();
      find_file(filename, input_paths.begin(), input_paths.end(), fullname);
      if (fullname.empty()) fullname = filename;
    }
  }
  loader(fullname, m_scene_graph, this);
}

//! \brief exports a field of this container.
void Inline::write_field(const Field_info* field_info, Formatter* formatter)
{
  // We can either export the URL field or export the CHILDREN field of the base
  // Group node, implying that we explicitly inline the entire hierarchy of
  // inligned files. The default is the latter option. We support only the
  // default. Supporting the alternative, that is, exporting the URL field
  // instead of the CHILDREN field blindly may cause an error, where a node is
  // defined twice, once inside the inlined file and once in the remaining
  // scene.
  if (URL == field_info->get_id()) return;
  Group::write_field(field_info, formatter);
}

SGAL_END_NAMESPACE
