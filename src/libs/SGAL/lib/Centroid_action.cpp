// Copyright (c) 2019 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <set>

#include <boost/shared_ptr.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/Centroid_action.hpp"
#include "SGAL/Shape.hpp"
#include "SGAL/Matrix4f.hpp"
#include "SGAL/Transform.hpp"

#include "SGAL/Mesh_set.hpp"
#include "SGAL/Lines_set.hpp"
#include "SGAL/Coord_array.hpp"
#include "SGAL/Coord_array_2d.hpp"
#include "SGAL/Coord_array_3d.hpp"
#include "SGAL/Arc.hpp"
#include "SGAL/Arc.hpp"
#include "SGAL/Box.hpp"
#include "SGAL/Cone.hpp"
#include "SGAL/Cylinder.hpp"
#include "SGAL/Cylindrical_patch.hpp"
#include "SGAL/Ellipsoid.hpp"
#include "SGAL/Sphere.hpp"

SGAL_BEGIN_NAMESPACE

/*! Coordinate pusher. */
class Visitor : public boost::static_visitor<> {
public:
  /*! Construct. */
  Visitor(const std::vector<Vector3f>& coords, std::list<Vector3f>& points) :
    m_coords(coords),
    m_points(points)
  {}

  /*! Destruct. */
  ~Visitor() { m_indices.clear(); }

  /*! Push all coordinates to the output list of points.
   */
  void operator()(const Flat_indices& indices) { SGAL_error(); }

  /*! Push all coordinates to the output list of points.
   */
  void operator()(const Line_indices& indices) { push_all(indices); }

  /*! Push all coordinates to the output list of points.
   */
  void operator()(const Triangle_indices& indices) { push_all(indices); }

  /*! Push all coordinates to the output list of points.
   */
  void operator()(const Quad_indices& indices) { push_all(indices); }

  /*! Push all coordinates to the output list of points.
   */
  void operator()(const Polygon_indices& indices) { push_all(indices); }

  /*! Push all coordinates into the point list.
   */
  template <typename PrimitiveIndices>
  void push_all(const PrimitiveIndices& indices)
  {
    for (const auto& ptimitive : indices) {
      for (auto index : ptimitive) m_indices.insert(index);
    }
    for (auto index : m_indices) m_points.push_back(m_coords[index]);
  }

private:
  //! The input coordinates.
  const std::vector<Vector3f>& m_coords;

  //! The output points.
  std::list<Vector3f>& m_points;

  //! The set of indices.
  std::set<size_t> m_indices;
};

//! \brief constructs default.
Centroid_action::Centroid_action() : m_dirty(true)
{
  Shared_matrix4f mat(new Matrix4f);
  m_matrices.push(mat);
}

//! \brief destructs.
Centroid_action::~Centroid_action()
{
  m_points.clear();
  m_matrices.pop();
}

//! \brief applies the volume action to a given node.
Action::Trav_directive Centroid_action::apply(Shared_node node)
{
  if (!node) return TRAV_CONT;

  auto transform = boost::dynamic_pointer_cast<Transform>(node);
  if (transform) {
    // Push the transform matrix
    const auto& curr_mat = transform->get_matrix();
    auto last_mat = m_matrices.top();
    Shared_matrix4f next_mat(new Matrix4f);
    next_mat->mult(*last_mat, curr_mat);
    m_matrices.push(next_mat);

    // Process the children.
    auto group = boost::dynamic_pointer_cast<Group>(transform);
    group->traverse(this);

    // Pop the transform matrix
    m_matrices.pop();
    return TRAV_CONT;
  }

  auto group = boost::dynamic_pointer_cast<Group>(node);
  if (group) {
    group->traverse(this);
    return TRAV_CONT;
  }

  auto shape = boost::dynamic_pointer_cast<Shape>(node);
  if (shape) {
    auto matrix = m_matrices.top();

    if (! shape->is_visible()) return TRAV_CONT;
    auto geometry = shape->get_geometry();
    if (!geometry) return TRAV_CONT;

    auto mesh = boost::dynamic_pointer_cast<Mesh_set>(geometry);
    if (mesh) {
      auto coords = mesh->get_coord_array();
      if (!coords || (coords->size() == 0)) return TRAV_CONT;

      std::vector<Vector3f> world_coords(coords->size());
      size_t i(0);
      for (auto it = world_coords.begin(); it != world_coords.end(); ++it)
        it->xform_pt(mesh->get_coord_3d(i++), *matrix);
      const auto& indices = mesh->get_facet_coord_indices();
      Visitor visitor(world_coords, m_points);
      boost::apply_visitor(visitor, indices);

      return TRAV_CONT;
    }

    auto lines_set = boost::dynamic_pointer_cast<Lines_set>(geometry);
    if (lines_set) {
      auto coords = lines_set->get_coord_array();
      if (!coords || (coords->size() == 0)) return TRAV_CONT;

      std::vector<Vector3f> world_coords(coords->size());
      size_t i(0);
      for (auto it = world_coords.begin(); it != world_coords.end(); ++it)
        it->xform_pt(lines_set->get_coord_3d(i++), *matrix);
      const auto& indices = lines_set->get_lines_coord_indices();
      Visitor visitor(world_coords, m_points);
      boost::apply_visitor(visitor, indices);

      return TRAV_CONT;
    }

    // Primitive types
    // Arc

    // Box
    auto box = boost::dynamic_pointer_cast<Box>(geometry);
    if (box) {
      Vector3f size;
      box->get_size(size);

      float w = size[0] * 0.5f;
      float h = size[1] * 0.5f;
      float d = size[2] * 0.5f;

      std::vector<Vector3f> points(8);
      points[0].set(-w, -h, -d);
      points[1].set( w, -h, -d);
      points[2].set(-w,  h, -d);
      points[3].set( w,  h, -d);
      points[4].set(-w, -h,  d);
      points[5].set( w, -h,  d);
      points[6].set(-w,  h,  d);
      points[7].set( w,  h,  d);
      auto matrix = m_matrices.top();
      for (const auto& v : points) {
        Vector3f p;
        p.xform_pt(v, *matrix);
        m_points.push_back(p);
      }
      points.clear();
      return TRAV_CONT;
    }

    // Cone
    // Cylinder
    // Cylindrical_patch
    // Ellipsoid
    // Sphere

    return TRAV_CONT;
  }

  return TRAV_CONT;
}

//! \brief obtains the centroid.
const Vector3f& Centroid_action::centroid()
{
  if (m_dirty) clean();
  return m_centroid;
}

//! \brief computes the centroid from the points.
void Centroid_action::clean()
{
  m_centroid.set(0, 0, 0);
  for (const auto& p : m_points) m_centroid.add(p);
  m_centroid.scale(1.0f/m_points.size());
  m_dirty = false;
}

SGAL_END_NAMESPACE
