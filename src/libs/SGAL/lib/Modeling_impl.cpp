// Copyright (c) 2020 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Author(s): Efi Fogel        <efifogel@gmail.com>

#include "SGAL/basic.hpp"
#include "SGAL/Modeling.hpp"

SGAL_BEGIN_NAMESPACE

//! \brief adds the container to a given scene.
void Modeling::add_to_scene(Scene_graph* sg)
{
  Container::add_to_scene(sg);
}

//! \brief processes change of stop_simplification-strategy.
void Modeling::
stop_simplification_strategy_changed(const SGAL::Field_info* field_info)
{
  m_dirty_stop_simplification_strategy_code = true;
  m_dirty_stop_simplification_strategy = false;
  field_changed(field_info);
}

//! \brief clean the stop_simplification-strategy options set.
void Modeling::clean_stop_simplification_strategy() const
{
  auto code = get_stop_simplification_strategy_code();
  m_stop_simplification_strategy = get_stop_simplification_strategy_name(code);
  m_dirty_stop_simplification_strategy = false;
}

//! \brief processes change of stop_simplification_code-strategy.
void Modeling::
stop_simplification_strategy_code_changed(const SGAL::Field_info* field_info)
{
  m_dirty_stop_simplification_strategy = true;
  m_dirty_stop_simplification_strategy_code = false;
  field_changed(field_info);
}

//! \brief clean the stop_simplification_code-strategy options set.
void Modeling::clean_stop_simplification_strategy_code() const
{
  const auto& name = get_stop_simplification_strategy();
  m_stop_simplification_strategy_code =
    find_stop_simplification_strategy_code(name.c_str());
  m_dirty_stop_simplification_strategy_code = false;
}

//! \brief sets defualt values.
void Modeling::reset(Boolean def_make_consistent,
                     Boolean def_triangulate_holes,
                     Boolean def_refine,
                     Boolean def_fair,
                     Boolean def_triangulate_facets,
                     Boolean def_split_ccs,
                     Boolean def_remove_degeneracies,
                     Boolean def_remove_isolated_vertices,
                     Boolean def_repair_orientation,
                     Boolean def_repair_normals,
                     Boolean def_merge_coplanar_facets,
                     Boolean def_simplify,
                     String stop_simplification_strategy,
		     Uint stop_simplification_strategy_code,
		     Uint simplification_count_stop,
		     Float simplification_count_ratio_stop,
                     Float simplification_edge_length_stop)
{
  m_make_consistent = def_make_consistent;
  m_triangulate_holes = def_triangulate_holes;
  m_refine_hole_triangulation = def_refine;
  m_fair_hole_triangulation = def_fair;
  m_triangulate_facets = def_triangulate_facets;
  m_split_ccs = def_split_ccs;
  m_remove_degeneracies = def_remove_degeneracies;
  m_repair_orientation = def_repair_orientation;
  m_repair_normals = def_repair_normals;
  m_merge_coplanar_facets = def_merge_coplanar_facets;
  m_simplify = def_simplify;
  m_stop_simplification_strategy = stop_simplification_strategy;
  m_stop_simplification_strategy_code = stop_simplification_strategy_code;
  m_simplification_count_stop = simplification_count_stop;
  m_simplification_count_ratio_stop = simplification_count_ratio_stop;
  m_simplification_edge_length_stop = simplification_edge_length_stop;
}

SGAL_END_NAMESPACE
