// Copyright (c) 2015 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#if defined(_MSC_VER)
#pragma warning ( disable : 4512 )
#endif

#include <vector>
#include <string>

#include "SGAL/basic.hpp"
#include "SGAL/Modeling_option_parser.hpp"
#include "SGAL/Configuration.hpp"
#include "SGAL/Modeling.hpp"

SGAL_BEGIN_NAMESPACE

//! \brief constructs default.
Modeling_option_parser::Modeling_option_parser() :
  m_modeling_opts("Modeling options"),
  m_multiple_shapes(false)
{
  m_modeling_opts.add_options()
    ("make-consistent",
     po::value<Boolean>()->default_value(Modeling::s_def_make_consistent),
     "make the orientation of facets consistent")
    ("triangulate-holes",
     po::value<Boolean>()->default_value(Modeling::s_def_triangulate_holes),
     "triangulate holes")
    ("refine",
      po::value<Boolean>()->
      default_value(Modeling::s_def_refine_hole_triangulation),
      "refine hole triangulation")
    ("fair",
     po::value<Boolean>()->
     default_value(Modeling::s_def_fair_hole_triangulation),
     "fair hole triangulation")
    ("triangulate-facets",
     po::value<Boolean>()->default_value(Modeling::s_def_triangulate_facets),
     "triangulate facets")
    ("split-ccs", po::value<Boolean>()->default_value(Modeling::s_def_split_ccs),
     "split connected components")
    ("remove-degeneracies",
     po::value<Boolean>()->default_value(Modeling::s_def_remove_degeneracies),
     "remove shape children the geometry of which is degenerate (zero volume)")
    ("remove-isolated-vertices",
     po::value<Boolean>()->default_value(Modeling::s_def_remove_isolated_vertices),
     "remove isolated vertices")
    ("repair-orientation",
     po::value<Boolean>()->default_value(Modeling::s_def_repair_orientation),
     "repair the orientation of facets reversing them all")
    ("repair-normals",
     po::value<Boolean>()->default_value(Modeling::s_def_repair_normals),
     "repair the normals of facets")
    ("mulitple-shapes",
     po::value<Boolean>()/* ->default_value(Modeling::s_def_mulitple_shapes) */,
     "construct multiple shapes when colors are present in the input")
    ("merge-coplanar-facets",
     po::value<Boolean>()->default_value(Modeling::s_def_merge_coplanar_facets),
     "merge coplanar facets")
    ("simplify",
     po::value<Boolean>()->default_value(Modeling::s_def_simplify),
     "simplify mesh surfaces")
    ("stop-simplification-strategy",
     po::value<String>()->
     default_value(Modeling::s_def_stop_simplification_strategy),
     "simplification stop strategy:\n  count\n  count-ratio")
    ("simplification-count-stop",
     po::value<Uint>()->
     default_value(Modeling::s_def_simplification_count_stop),
     "maximum number of retained edges when collapsing")
    ("simplification-count-ratio-stop",
     po::value<Float>()->
     default_value(Modeling::s_def_simplification_count_ratio_stop),
     "maximum ratio ofnumber of retained and intial edges when collapsing")
    ("simplification-edge-length-stop",
     po::value<Float>()->
     default_value(Modeling::s_def_simplification_edge_length_stop),
     "maximum length of retained edges when collapsing")
    ;
}

//! \brief destructs.
Modeling_option_parser::~Modeling_option_parser() {}

//! \brief applies the options.
void Modeling_option_parser::apply() {}

//! \brief sets the Configuration node.
void Modeling_option_parser::configure(Configuration* conf)
{
  if (! conf) return;

  const auto& var_map = get_variable_map();

  auto modeling = conf->get_modeling();
  if (! modeling) {
    modeling.reset(new Modeling);
    SGAL_assertion(modeling);
    conf->set_modeling(modeling);
  }

  if (var_map.count("make-consistent"))
    modeling->set_make_consistent(var_map["make-consistent"].as<Boolean>());

  if (var_map.count("triangulate-holes"))
    modeling->set_triangulate_holes(var_map["triangulate-holes"].as<Boolean>());

  if (var_map.count("refine"))
    modeling->set_refine_hole_triangulation(var_map["refine"].as<Boolean>());

  if (var_map.count("fair"))
    modeling->set_fair_hole_triangulation(var_map["fair"].as<Boolean>());

  if (var_map.count("triangulate-facets"))
    modeling->set_triangulate_facets(var_map["triangulate-facets"].
                                     as<Boolean>());

  if (var_map.count("split-ccs"))
    modeling->set_split_ccs(var_map["split-ccs"].as<Boolean>());

  if (var_map.count("remove-degeneracies"))
    modeling->set_remove_degeneracies(var_map["remove-degeneracies"].
                                      as<Boolean>());

  if (var_map.count("remove-isolated-vertices"))
    modeling->set_remove_isolated_vertices(var_map["remove-isolated-vertices"].
                                           as<Boolean>());

  if (var_map.count("repair-orientation"))
    modeling->set_repair_orientation(var_map["repair-orientation"].
                                     as<Boolean>());

  if (var_map.count("repair-normals"))
    modeling->set_repair_normals(var_map["repair-normals"].as<Boolean>());

  if (var_map.count("merge-coplanar-facets"))
    modeling->set_merge_coplanar_facets(var_map["merge-coplanar-facets"].
                                        as<Boolean>());
  if (var_map.count("simplify"))
    modeling->set_simplify(var_map["simplify"].as<Boolean>());

  if (var_map.count("stop-simplification-strategy"))
    modeling->set_stop_simplification_strategy
      (var_map["stop-simplification-strategy"].as<String>());

  if (var_map.count("simplification-count-stop"))
    modeling->set_simplification_count_stop
      (var_map["simplification-count-stop"].as<Uint>());

  if (var_map.count("simplification-count-ratio-stop"))
    modeling->set_simplification_count_ratio_stop
      (var_map["simplification-count-ratio-stop"].as<Float>());

  if (var_map.count("simplification-edge-length-stop"))
    modeling->set_simplification_edge_length_stop
      (var_map["simplification-edge-length-stop"].as<Float>());
}

SGAL_END_NAMESPACE
