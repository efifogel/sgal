// Copyright (c) 2014 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#if defined(_MSC_VER)
#pragma warning ( disable : 4512 )
#endif

#include <ostream>
#include <vector>
#include <string>

#include "SGAL/basic.hpp"
#include "SGAL/Io_option_parser.hpp"
#include "SGAL/Input_output.hpp"
#include "SGAL/Configuration.hpp"
#include "SGAL/Image_format.hpp"
#include "SGAL/Geometry_format.hpp"

namespace std {
std::ostream& operator<<(std::ostream& os, const std::vector<std::string>& sv)
{
  const auto separator = ":";
  const auto* sep = "";
  for (const auto& str : sv) {
    os << sep << str;
    sep = separator;
  }
  return os;
}
}

SGAL_BEGIN_NAMESPACE

//! \brief constructor.
Io_option_parser::Io_option_parser() :
  m_io_opts("SGAL IO options"),
  m_snapshot(false),
  m_export(false),
  m_interactive(true),
  m_binary(true)
{
  typedef std::vector<std::string> sv;

  // Extract geometry file format options:
  auto* gf = Geometry_format::get_instance();
  std::string gff_msg("geometry file formats:\n");
  for (auto it = gf->names_begin(); it != gf->names_end(); ++it)
    if (! it->empty()) gff_msg += "  " + *it + "\n";

  // Extract image file format options:
  auto* if_ = Image_format::get_instance();
  std::string iff_msg("image file formats:\n");
  for (auto it = if_->names_begin(); it != if_->names_end(); ++it)
    if (! it->empty()) iff_msg += "  " + *it + "\n";

  // Options allowed on the command line, config file, or env. variables
  m_io_opts.add_options()
    ("input-path", po::value<std::vector<std::string>>()->composing()->
     default_value(Input_output::s_def_input_paths), "input path")
    ("image-format", po::value<sv>()->multitoken(), iff_msg.c_str())
    ("geometry-format,f", po::value<sv>()->multitoken(), gff_msg.c_str())
    ("snapshot,S", po::value<Boolean>(&m_snapshot)->default_value(false),
     "snapshot")
    ("export,E", po::value<Boolean>(&m_export)->default_value(false), "export")
    ("interactive,I", po::value<Boolean>(&m_interactive)->default_value(true),
     "run in interactive mode.")
    ("output-file", po::value<std::string>(&m_output_file), "output file")
    ("output-path", po::value<std::string>(&m_output_path)->default_value("."),
     "output path")
    ("binary", po::value<Boolean>(&m_binary)->default_value(true), "binary")
    ("export-scene-root", po::value<std::string>(&m_export_scene_root)->
     default_value(Input_output::s_def_export_scene_root),
     "The name of the node root of the scene to export")
    ("export-scene-level", po::value<Uint>(&m_export_scene_level)->
     default_value(Input_output::s_def_export_scene_level),
     "The level of the scene to export; (0, 1, ...")
    ("export-non-visible", po::value<Boolean>(&m_export_non_visible)->
     default_value(Input_output::s_def_export_non_visible),
     "export non-visible geometries")
    ("export-flat", po::value<Boolean>()->
     default_value(Input_output::s_def_export_flat),
     "export flat geometries (applicable to formats that support transforms)")
    ;
}

//! \brief Destructor.
Io_option_parser::~Io_option_parser() {}

//! \brief applies the options.
void Io_option_parser::apply() {}

//! \brief sets the Configuration node.
void Io_option_parser::configure(Configuration* conf)
{
  typedef std::vector<std::string> sv;

  const auto& var_map = get_variable_map();

  auto* image_format = Image_format::get_instance();
  if (var_map.count("image-format")) {
    for (const auto& name : var_map["image-format"].as<sv>()) {
      auto code = image_format->find_code(name);
      if (code == Image_format::INVALID) {
        throw po::validation_error(po::validation_error::invalid_option_value,
                                   "--geometry-format", name);
      }
      m_image_formats.push_back(code);
    }
  }

  auto* geom_format = Geometry_format::get_instance();
  if (var_map.count("geometry-format")) {
    for (const auto& name : var_map["geometry-format"].as<sv>()) {
      auto code = geom_format->find_code(name);
      if (code == Geometry_format::INVALID) {
        throw po::validation_error(po::validation_error::invalid_option_value,
                                   "--geometry-format", name);
      }
      m_geometry_formats.push_back(code);
    }
  }

  if (! conf) return;
  auto io = conf->get_input_output();
  if (! io) return;

  if (var_map.count("export-scene-level"))
    io->set_export_scene_level(var_map["export-scene-level"].as<Uint>());
  if (var_map.count("export-scene-root"))
    io->set_export_scene_root(var_map["export-scene-root"].as<std::string>());

  if (var_map.count("export-non-visible")) {
    io->set_export_non_visible(var_map["export-non-visible"].as<Boolean>());
    io->set_override_export_non_visible(true);
  }
  if (var_map.count("export-flat")) {
    io->set_export_flat(var_map["export-flat"].as<Boolean>());
  }
  auto& src = var_map["input-path"].as<sv>();
  auto& trg = io->get_input_paths();
  // Push unique
  for (const auto& path : src) {
    if (std::find(trg.begin(), trg.end(), path) == trg.end())
      trg.push_back(path);
  }
}

SGAL_END_NAMESPACE
