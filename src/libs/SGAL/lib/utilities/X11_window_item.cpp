// Copyright (c) 2004  Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

/*! \file
 * X11 window system component of the window manager
 */

#include <stdio.h>
#include <string.h>
#include <GL/glx.h>

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>

#include "SGAL/basic.hpp"
#include "SGAL/Types.hpp"
#include "SGAL/errors.hpp"
#include "SGAL/Tracer.hpp"
#include "SGAL/X11_window_item.hpp"

SGAL_BEGIN_NAMESPACE

//! \brief constructor.
X11_window_item::X11_window_item() :
  m_display(nullptr),
  m_screen(0),
  m_window(0)
{ memset(&m_desk_mode, 0, sizeof(m_desk_mode)); }

template <typename T>
inline int* to_intp(const T* p)
{ return const_cast<int*>(reinterpret_cast<const int*>(p)); }

//! \brief verifies that the visual has been created properly.
bool X11_window_item::verify_visual(XVisualInfo* vi) const
{
  if (m_stencil_bits > 0) {
    auto rc = glXGetConfig(m_display, vi, GLX_STENCIL_SIZE,
                           to_intp(&m_stencil_bits));
    if (rc != 0) {
      throw Visual_configuration_error();
      return false;
    }
  }
  if (m_accum_red_bits > 0) {
    auto rc = glXGetConfig(m_display, vi, GLX_ACCUM_RED_SIZE,
                           to_intp(&m_accum_red_bits));
    if (rc != 0) {
      throw Visual_configuration_error();
      return false;
    }
  }
  if (m_accum_green_bits > 0) {
    auto rc = glXGetConfig(m_display, vi, GLX_ACCUM_GREEN_SIZE,
                           to_intp(&m_accum_green_bits));
    if (rc != 0) {
      throw Visual_configuration_error();
      return false;
    }
  }
  if (m_accum_blue_bits > 0) {
    auto rc = glXGetConfig(m_display, vi, GLX_ACCUM_BLUE_SIZE,
                           to_intp(&m_accum_blue_bits));
    if (rc != 0) {
      throw Visual_configuration_error();
      return false;
    }
  }
  if (m_accum_alpha_bits > 0) {
    auto rc = glXGetConfig(m_display, vi, GLX_ACCUM_ALPHA_SIZE,
                           to_intp(&m_accum_alpha_bits));
    if (rc != 0) {
      throw Visual_configuration_error();
      return false;
    }
  }
  if (m_number_of_samples > 0) {
    auto rc = glXGetConfig(m_display, vi, GLX_SAMPLES,
                           to_intp(&m_number_of_samples));
    if (rc != 0) {
      throw Visual_configuration_error();
      return false;
    }
  }
  return true;
}

//! \brief creates a visual.
XVisualInfo* X11_window_item::create_visual()
{
  // Get an appropriate visual:
  /* attributes for a visual in RGBA format with at least
   * 4 bits per color and a 24 bit depth buffer
   */
  int attributes[64];
  Uint index = 0;
  attributes[index++] = GLX_RGBA;
  if (m_double_buffer) attributes[index++] = GLX_DOUBLEBUFFER;
  attributes[index++] = GLX_RED_SIZE;
  attributes[index++] = m_red_bits;
  attributes[index++] = GLX_GREEN_SIZE;
  attributes[index++] = m_green_bits;
  attributes[index++] = GLX_BLUE_SIZE;
  attributes[index++] = m_blue_bits;
  attributes[index++] = GLX_ALPHA_SIZE;
  attributes[index++] = m_alpha_bits;
  attributes[index++] = GLX_DEPTH_SIZE;
  attributes[index++] = m_depth_bits;
  if (m_stencil_bits > 0) {
    attributes[index++] = GLX_STENCIL_SIZE;
    attributes[index++] = m_stencil_bits;
  }
  if (m_number_of_samples > 0) {
    attributes[index++] = GLX_SAMPLE_BUFFERS;
    attributes[index++] = 1;
    attributes[index++] = GLX_SAMPLES;
    attributes[index++] = m_number_of_samples;
  }
  if (m_accum_red_bits > 0) {
    attributes[index++] = GLX_ACCUM_RED_SIZE;
    attributes[index++] = m_accum_red_bits;
  }
  if (m_accum_green_bits > 0) {
    attributes[index++] = GLX_ACCUM_GREEN_SIZE;
    attributes[index++] = m_accum_green_bits;
  }
  if (m_accum_blue_bits > 0) {
    attributes[index++] = GLX_ACCUM_BLUE_SIZE;
    attributes[index++] = m_accum_blue_bits;
  }
  if (m_accum_alpha_bits > 0) {
    attributes[index++] = GLX_ACCUM_ALPHA_SIZE;
    attributes[index++] = m_accum_alpha_bits;
  }
  attributes[index++] = None;

  // Configure frame buffer
  auto* vi = glXChooseVisual(m_display, m_screen, attributes);
  if (vi == nullptr) throw Visual_selection_error();
  return vi;
}

//! \brief creates a window.
void X11_window_item::create(Display* display, Int32 screen)
{
#if !defined(NDEBUG) || defined(SGAL_TRACE)
  if (SGAL::TRACE(Tracer::GLX)) {
    int glx_major, glx_minor;
    auto brc = glXQueryVersion(display, &glx_major, &glx_minor);
    if (!brc) throw Glx_version_error();
    std::cout << "GLX version " << glx_major << "." << glx_minor << std::endl;
  }
#endif

  m_display = display;              // retain the display for further actions
  m_screen = screen;                // retain the screen for further actions

  if (do_render_offscreen()) create_offscreen();
  else create_onscreen();
}

//! \brief creates an off-screen window.
void X11_window_item::create_offscreen()
{
  // Configure frame buffer
  int attributes[64];
  Uint index = 0;
  attributes[index++] = GLX_RENDER_TYPE;   attributes[index++] = GLX_RGBA_BIT;
  attributes[index++] = GLX_DOUBLEBUFFER;  attributes[index++] = False;
  attributes[index++] = GLX_RED_SIZE;      attributes[index++] = m_red_bits;
  attributes[index++] = GLX_GREEN_SIZE;    attributes[index++] = m_green_bits;
  attributes[index++] = GLX_BLUE_SIZE;     attributes[index++] = m_blue_bits;
  attributes[index++] = GLX_ALPHA_SIZE;    attributes[index++] = m_alpha_bits;
  attributes[index++] = GLX_DRAWABLE_TYPE;
  attributes[index++] = GLX_PBUFFER_BIT;
  attributes[index++] = GLX_DEPTH_SIZE;    attributes[index++] = m_depth_bits;
  if (m_stencil_bits > 0) {
    attributes[index++] = GLX_STENCIL_SIZE;
    attributes[index++] = m_stencil_bits;
  }
  if (m_number_of_samples > 0) {
    attributes[index++] = GLX_SAMPLE_BUFFERS; attributes[index++] = 1;
    attributes[index++] = GLX_SAMPLES;
    attributes[index++] = m_number_of_samples;
  }
  if (m_accum_red_bits > 0) {
    attributes[index++] = GLX_ACCUM_RED_SIZE;
    attributes[index++] = m_accum_red_bits;
  }
  if (m_accum_green_bits > 0) {
    attributes[index++] = GLX_ACCUM_GREEN_SIZE;
    attributes[index++] = m_accum_green_bits;
  }
  if (m_accum_blue_bits > 0) {
    attributes[index++] = GLX_ACCUM_BLUE_SIZE;
    attributes[index++] = m_accum_blue_bits;
  }
  if (m_accum_alpha_bits > 0) {
    attributes[index++] = GLX_ACCUM_ALPHA_SIZE;
    attributes[index++] = m_accum_alpha_bits;
  }
  attributes[index++] = None;
  int count;
  auto* fbconfig = glXChooseFBConfig(m_display, m_screen, attributes, &count);
  if ((fbconfig == nullptr) || (count <= 0)) {
    std::cerr << "Requested frame buffer configuration not supported!"
              << std::endl;
    return;
  }

  // Create P-Buffer
  int attributes_2[] = {
    GLX_PBUFFER_WIDTH, static_cast<int>(m_width),
    GLX_PBUFFER_HEIGHT, static_cast<int>(m_height),
    GLX_NONE
  };

  auto pbuffer = glXCreatePbuffer(m_display, fbconfig[0], attributes_2);
  if (pbuffer == None) {
    std::cerr << "Failed to create Pixel Buffer!" << std::endl;
    return;
  }

  // Create graphics context
  m_context = glXCreateNewContext(m_display, fbconfig[0],
                                  GLX_RGBA_TYPE, NULL, GL_TRUE);
  if (! m_context) {
    std::cerr << "Failed to create graphics context!" << std::endl;
    return;
  }

  // Activate graphics context
  if (!glXMakeContextCurrent(m_display, pbuffer, pbuffer, m_context)) {
    std::cerr << "Failed to activate graphics context." << std::endl;
    return;
  }
}

//! \brief creates an on-screen window.
void X11_window_item::create_onscreen()
{
  // Create a visual
  auto vi = create_visual();
  if (vi == nullptr) return;
  if (! verify_visual(vi)) return;

  // Create a GLX context:
  m_context = glXCreateContext(m_display, vi, 0, GL_TRUE);
  if (! m_context) {
    std::cerr << "Failed to create graphics context!" << std::endl;
    return;
  }

  // Create a color map:
  Colormap cmap = XCreateColormap(m_display, RootWindow(m_display, vi->screen),
                                  vi->visual, AllocNone);
  m_win_attr.colormap = cmap;
  m_win_attr.border_pixel = 0;

  if (m_full_screen) {
#if !defined(NDEBUG) || defined(SGAL_TRACE)
    if (SGAL::TRACE(Tracer::GLX)) {
      // Determine the version:
      int vm_major, vm_minor;
      auto brc = XF86VidModeQueryVersion(m_display, &vm_major, &vm_minor);
      if (!brc)
        std::cerr << "Failed to connect to the X server!" << std::endl;
      else
        std::cout << "XF86 VideoMode extension version "
                  << vm_major << "." << vm_minor << std::endl;
    }
#endif

    XF86VidModeModeInfo** modes;
    int number_of_modes;
    auto brc =
      XF86VidModeGetAllModeLines(m_display, m_screen, &number_of_modes, &modes);
    // std::cout << "number_of_modes: " << number_of_modes << std::endl;

    int dpy_width = m_width;
    int dpy_height = m_height;
    if (brc) {
      int best_mode = 0;                  // set best mode to current

      // Save desktop-resolution before switching modes:
      m_desk_mode = *modes[0];

      // Look for mode with requested resolution:
      for (int i = 0; i < number_of_modes; ++i) {
        if ((modes[i]->hdisplay == m_width) && (modes[i]->vdisplay == m_height))
          best_mode = i;
      }
      // std::cout << "best_mode: " << best_mode << std::endl;

      // Switch to fullscreen:
      XF86VidModeSwitchToMode(m_display, m_screen, modes[best_mode]);
      XF86VidModeSetViewPort(m_display, m_screen, 0, 0);
      dpy_width = modes[best_mode]->hdisplay;
      dpy_height = modes[best_mode]->vdisplay;
      XFree(modes);
    }

    // Set window attributes:
    m_win_attr.override_redirect = True;
    m_win_attr.event_mask =
      ExposureMask |
      KeyPressMask | KeyReleaseMask |
      ButtonPressMask | ButtonReleaseMask |
      StructureNotifyMask |
      PointerMotionMask;
    m_window = XCreateWindow(m_display, RootWindow(m_display, vi->screen),
                             0, 0, dpy_width, dpy_height, 0, vi->depth,
                             InputOutput, vi->visual,
                             CWBorderPixel | CWColormap | CWEventMask |
                             CWOverrideRedirect, &m_win_attr);
    XWarpPointer(m_display, None, m_window, 0, 0, 0, 0, 0, 0);
    XMapRaised(m_display, m_window);
    XGrabKeyboard(m_display, m_window, True, GrabModeAsync, GrabModeAsync,
                  CurrentTime);
    XGrabPointer(m_display, m_window, True, ButtonPressMask,
                 GrabModeAsync, GrabModeAsync, m_window, None, CurrentTime);
  }
  else {
    // Create a window in window mode:
    m_win_attr.event_mask =
      ExposureMask |
      KeyPressMask | KeyReleaseMask |
      ButtonPressMask | ButtonReleaseMask |
      StructureNotifyMask |
      PointerMotionMask |
      FocusChangeMask;
    m_window = XCreateWindow(m_display, RootWindow(m_display, vi->screen),
                             0, 0, m_width, m_height, 1, vi->depth, InputOutput,
                             vi->visual,
                             CWBorderPixel | CWColormap | CWEventMask,
                             &m_win_attr);
    /* only set window title and handle wm_delete_events if in windowed mode */
    m_wm_delete = XInternAtom(m_display, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(m_display, m_window, &m_wm_delete, 1);
    XSetStandardProperties(m_display, m_window,
                           m_title.c_str(), m_title.c_str(),
                           None, nullptr, 0, nullptr);
    XMapRaised(m_display, m_window);
  }
  XSelectInput(m_display, m_window, m_win_attr.event_mask);
  m_width = 0;
  m_height = 0;

  make_current();           // make the context the current rendering context
}

//! \brief destroys the window.
void X11_window_item::destroy()
{
  set_accumulating(false);
  set_redraw(false);
  if (m_context) {
    if (m_full_screen) {
      if ((m_desk_mode.hdisplay != 0) && (m_desk_mode.vdisplay != 0))
        XF86VidModeSwitchToMode(m_display, m_screen, &m_desk_mode);
    }

    if (!glXMakeCurrent(m_display, None, nullptr)) {
      std::cerr << "Could not release drawing context!" << std::endl;
    }
    glXDestroyContext(m_display, m_context);
    m_context = nullptr;
  }
  if (m_window) {
    XUnmapWindow(m_display, m_window);
    XDestroyWindow(m_display, m_window);
    // Do not reset m_window here. It is used by the window manager when it
    // handles the DestroyNotify event.
  }
}

//! \brief swaps the window frame-buffer.
void X11_window_item::swap_buffers()
{
  if (!m_context) return;       // The window is being destroyed
  if (m_double_buffer) glXSwapBuffers(m_display, m_window);
}

//! \brief shows the window. Make the window current if it is not already.
void X11_window_item::show() {}

//! \brief hides the window. Make the window current if it is not already.
void X11_window_item::hide() {}

/*! \brief makes the context of the window item the current rendering context of
 * the calling thread
 */
void X11_window_item::make_current()
{
  if (! m_context || ! m_window) return;
  glXMakeCurrent(m_display, m_window, m_context);
}

SGAL_END_NAMESPACE
