// Copyright (c) 2021 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

/*! \file
 */

#include <iterator>
#include <type_traits>

#include <boost/variant.hpp>
#include <boost/unordered_map.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/version.hpp"
#include "SGAL/Container.hpp"
#include "SGAL/Transform.hpp"
#include "SGAL/Coord_array.hpp"
#include "SGAL/Off_formatter.hpp"

SGAL_BEGIN_NAMESPACE

//! \brief constructor.
Off_formatter::Off_formatter(const std::string& filename) :
  Text_formatter(filename) {
  m_matrices.emplace(Matrix4f());
  m_export_non_visible = false;
}

//! \brief constructs an output formatter.
Off_formatter::Off_formatter(const std::string& filename, std::ostream& os) :
  Text_formatter(filename, os) {
  m_matrices.emplace(Matrix4f());
  m_export_non_visible = false;
}

//! \brief constructs an input formatter.
Off_formatter::Off_formatter(const std::string& filename, std::istream& is) :
  Text_formatter(filename, is)
{ m_export_non_visible = false; }

//! \brief destructor
Off_formatter::~Off_formatter() { m_matrices.pop(); }

//! \brief writes the begin statement.
void Off_formatter::begin()
{
  SGAL_assertion(m_out != nullptr);
  out() << "OFF" << std::endl;
  new_line();
}

//! \brief writes the end statement.
void Off_formatter::end() {
  if (m_mesh_set.size() == 1) {
    export_mesh(m_mesh_set.front());
    return;
  }
  SGAL_error_msg("Not implemented yet");
}

template<typename... Lambdas>
struct lambda_visitor;

template<typename Lambda1, typename... Lambdas>
struct lambda_visitor<Lambda1, Lambdas...> :
  public lambda_visitor<Lambdas...>,
  public Lambda1
{
  using Lambda1::operator ();
  using lambda_visitor<Lambdas...>::operator ();

  lambda_visitor(Lambda1 l1, Lambdas... lambdas) :
    Lambda1(l1),
    lambda_visitor<Lambdas...>(lambdas...)
  {}
};

template<typename Lambda1>
struct lambda_visitor<Lambda1> : public Lambda1 {
  using Lambda1::operator ();
  lambda_visitor(Lambda1 l1) : Lambda1(l1) {}
};

template<class...Fs>
auto compose(Fs&& ...fs) {
  using visitor_type = lambda_visitor<std::decay_t<Fs>...>;
  return visitor_type(std::forward<Fs>(fs)...);
};

//! \brief export the facets.
void Off_formatter::export_facets(const Facet_indices& indices, bool is_ccw) {
  auto visitor =
    compose([&](const Triangle_indices& indices) { facets(indices); },
            [&](const Quad_indices& indices) { facets(indices); },
            [&](const Polygon_indices& indices) { facets(indices); },
            [&](const Flat_indices& indices) {
              auto it = indices.begin();
              while (it != indices.end()) {
                size_t size(0);
                auto tit = it;
                while (*tit++ != -1) ++size;
                indent();
                out() << size;
                while (*it != -1) out() << " " << *it++;
                new_line();
                ++it;
              }
            });
  boost::apply_visitor(visitor, indices);
}

//! \brief exports the vertices.
void Off_formatter::export_vertices(const std::vector<Vector3f>& points) {
  for (const auto& p : points) {
    indent();
    out() << p[0] << " " << p[1] << " " << p[2];
    new_line();
  }
}

//! \brief counts facets
size_t Off_formatter::number_of_facets(const Facet_indices& indices) const {
  size_t num_facets(0);
  auto visitor =
    compose([&](const Triangle_indices& indices) { num_facets += indices.size(); },
            [&](const Quad_indices& indices) { num_facets += indices.size(); },
            [&](const Polygon_indices& indices) { num_facets += indices.size(); },
            [&](const Flat_indices& indices) {
              for (auto i : indices) if (i == -1) ++num_facets;
            });
  boost::apply_visitor(visitor, indices);
  return num_facets;
}

//! \brief exports a single (old) mesh.
void Off_formatter::export_mesh(const Mesh_in& mesh) {
  const auto& verts = mesh.first;
  const auto& indices = mesh.second;
  size_t num_verts(0);
  num_verts += verts.size();
  auto num_facets = number_of_facets(indices);
  auto num_edges = num_verts + num_facets - 2;
  indent();
  out() << num_verts << " " << num_facets << " " << num_edges;
  new_line();
  export_vertices(verts);
  export_facets(indices);
}

// //! \brief collapse identical vertices
// Off_formatter::Mesh_out Off_formatter::get_single_mesh() const {
//   size_t num_facets(0);
//   size_t num_verts(0);
//   for (const auto& mesh : m_mesh_set) {
//     const auto& verts = mesh.first;
//     const auto& indices = mesh.second;
//     auto size_visitor =
//       compose([&](const Triangle_indices& indices) { num_facets += indices.size(); },
//               [&](const Quad_indices& indices) { num_facets += indices.size(); },
//               [&](const Polygon_indices& indices) { num_facets += indices.size(); },
//               [&](const Flat_indices& indices) {
//                 for (auto i : indices) if (i == -1) ++num_facets;
//               });
//     boost::apply_visitor(size_visitor, indices);
//     num_verts += verts.size();
//   }
//   std::cout << "# verts: " << num_verts << std::endl;
//   std::cout << "# triangles: " << num_facets << std::endl;
//   Mesh_out mesh_out;
//   auto& verts_out = mesh_out.first;
//   auto& polys_out = mesh_out.second;
//   polys_out.resize(num_facets);
//   size_t k(0);
//   boost::unordered_map<Vector3f, size_t> vertex_map;
//   size_t i(0);
//   for (const auto& mesh : m_mesh_set) {
//     const auto& verts = mesh.first;
//     const auto& indices = mesh.second;
//     boost::unordered_map<size_t, size_t> index_map;
//     size_t j(0);
//   //   for (const auto& p : verts) {
//   //     auto it = point_map.find(p);
//   //     if (it == point_map.end()) {
//   //       point_map[p] = i;
//   //       index_map[j++] = i++;
//   //     }
//   //     else index_map[j++] = it->second;
//   //   }
//   //   for (const auto& t : tris)
//   //     mtris[k++] = {index_map[t[0]], index_map[t[1]], index_map[t[2]]};
//   //   index_map.clear();
//   }
//   verts_out.resize(vertex_map.size());
//   for (const auto& item : vertex_map) verts_out[item.second] = item.first;
//   vertex_map.clear();
//   return mesh_out;
// }

/*! \brief adds a mesh.
 */
void Off_formatter::add_mesh(std::vector<Vector3f>&& vertices,
                             const Facet_indices& indices, bool is_ccw) {
  m_mesh_set.emplace_back(vertices, indices);
}

//! \brief writes a scene-graph container.
void Off_formatter::write(Shared_container container) {
  auto transform = boost::dynamic_pointer_cast<Transform>(container);
  if (transform) {
    // Push the transform matrix
    const Matrix4f& new_matrix = transform->get_matrix();
    const Matrix4f& top_matrix = m_matrices.top();
    m_matrices.emplace(Matrix4f(top_matrix, new_matrix));

    // Process the children.
    transform->write(this);

    // Pop the transform matrix
    m_matrices.pop();
    return;
  }
  container->write(this);
}

SGAL_END_NAMESPACE
