// Copyright (c) 2018 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

/*! \file
 */

#include <iterator>
#include <utility>
#include <bitset>
#include <sstream>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/version.hpp"
#include "SGAL/Container.hpp"
#include "SGAL/Coord_array_3d.hpp"
#include "SGAL/Tex_coord_array_2d.hpp"
#include "SGAL/Json_formatter.hpp"
#include "SGAL/Transform.hpp"
#include "SGAL/Shape.hpp"
#include "SGAL/Appearance.hpp"
#include "SGAL/Material.hpp"
#include "SGAL/Box.hpp"
#include "SGAL/Geo_set.hpp"
#include "SGAL/Mesh_set.hpp"
#include "SGAL/Lines_set.hpp"
#include "SGAL/Color_array.hpp"
#include "SGAL/Color_array_3d.hpp"
#include "SGAL/Color_array_4d.hpp"
#include "SGAL/Normal_array.hpp"
#include "SGAL/io_vector3f.hpp"
#include "SGAL/Vector3f.hpp"
#include "SGAL/Context.hpp"
#include "SGAL/Camera.hpp"
#include "SGAL/Frustum.hpp"
#include "SGAL/Spot_light.hpp"
#include "SGAL/Point_light.hpp"
#include "SGAL/Directional_light.hpp"
#include "SGAL/Indexed_face_set.hpp"
#include "SGAL/Indexed_line_set.hpp"

SGAL_BEGIN_NAMESPACE

//! Vertex color options.
const std::string Json_formatter::s_def_material_colors[] =
  {"NoColors", "FaceColors", "VertexColors"};

//! Version of json format.
const std::string Json_formatter::s_version("4.0");

//! \brief constructs.
Json_formatter::Json_formatter(const std::string& filename) :
  Text_formatter(filename),
  m_separated(true),
  m_bounding_sphere(nullptr),
  m_camera(nullptr)
{
  m_identity_matrix.make_identity();
  m_export_non_visible = true;
}

//! \brief constructs an output formatter.
Json_formatter::Json_formatter(const std::string& filename, std::ostream& os) :
  Text_formatter(filename, os),
  m_separated(true),
  m_bounding_sphere(nullptr),
  m_camera(nullptr)
{
  m_identity_matrix.make_identity();
  m_export_non_visible = true;
}

//! \brief constructs an input formatter.
Json_formatter::Json_formatter(const std::string& filename, std::istream& is) :
  Text_formatter(filename, is),
  m_separated(true),
  m_bounding_sphere(nullptr),
  m_camera(nullptr)
{
  m_identity_matrix.make_identity();
  m_export_non_visible = true;
}

//! \brief destruct.
Json_formatter::~Json_formatter()
{
  for (auto& item : m_geometries) item.second.clear();
  m_geometries.clear();
  m_appearances.clear();
  m_nodes.clear();
  m_basic_materials.clear();
}

//! \brief constructs.
Json_formatter::Basic_material::Basic_material(Material_color material_color,
                                               size_t color,
                                               float opacity,
                                               bool transparent,
                                               bool depth_test) :
  m_color(color),
  m_opacity(opacity),
  m_transparent(transparent),
  m_material_color(material_color),
  m_depth_test(depth_test)
{
  // Force a canonical form:
  if (opacity == 1.0f) m_transparent = false;
  if (! transparent) m_opacity = 1.0f;
}

//! \brief handles a geometry container.
void Json_formatter::handle_geometry(Shared_geometry geometry)
{
  // std::cout << "Json_formatter::add_geometry" << std::endl;
  auto geo_set = boost::dynamic_pointer_cast<Geo_set>(geometry);
  if (geo_set) {
    auto type = geo_set->get_primitive_type();
    if (is_polylines(type)) {
      auto lines_set = boost::dynamic_pointer_cast<Lines_set>(geo_set);
      SGAL_assertion(lines_set);
      const auto& indices = lines_set->get_lines_coord_indices();
      if (lines_set->empty_lines_indices(indices)) return;
      auto res =
        m_geometries.insert(std::make_pair(geometry, std::vector<String>()));
      auto it = res.first;
      auto size = lines_set->size_lines_indices(indices);
      it->second.resize(size);
      for (size_t i = 0; i < size; ++i) {
        auto uuid = boost::uuids::random_generator()();
        it->second[i] = boost::uuids::to_string(uuid);
      }
      return;
    }
  }

  auto uuid = boost::uuids::random_generator()();
  m_geometries[geometry].push_back(boost::uuids::to_string(uuid));
}

//! \brief handles an apperarance container.
void Json_formatter::handle_appearance(Shared_apperance appearance)
{
  auto uuid = boost::uuids::random_generator()();
  m_appearances[appearance] = boost::uuids::to_string(uuid);
}

//! \brief handles a shape container.
void Json_formatter::handle_shape(Shared_shape shape)
{
  auto app = shape->get_appearance();
  if (! app) return;
  auto transparent(app->is_transparent());
  auto depth_test(app->get_depth_enable());

  auto geometry = shape->get_geometry();
  if (! geometry) return;
  if (! geometry->has_color()) return;

  auto geo_set = boost::dynamic_pointer_cast<Geo_set>(geometry);
  if (! geo_set) {
    construct_basic_material(MC_NO_COLORS, depth_test);
    return;
  }

  // Handle lines:
  auto colors = geo_set->get_color_array();
  auto type = geo_set->get_primitive_type();
  if (is_lines(type) || is_polylines(type)) {
    auto colors_3d = boost::dynamic_pointer_cast<Color_array_3d>(colors);
    if (colors_3d) {
      construct_basic_material(MC_NO_COLORS, (*colors_3d)[0], depth_test);
      return;
    }

    auto colors_4d = boost::dynamic_pointer_cast<Color_array_4d>(colors);
    if (colors_4d) {
      construct_basic_material(MC_NO_COLORS, (*colors_4d)[0], transparent,
                               depth_test);
      return;
    }

    return;
  }

  // Handle mesh transparency:
  auto attach = geo_set->get_color_attachment();
  auto material_color = (Geo_set::AT_PER_VERTEX == attach) ?
    MC_VERTEX_COLORS : MC_FACE_COLORS;

  auto colors_3d = boost::dynamic_pointer_cast<Color_array_3d>(colors);
  if (colors_3d) {
    construct_basic_material(material_color, depth_test);
    return;
  }

  auto colors_4d = boost::dynamic_pointer_cast<Color_array_4d>(colors);
  if (colors_4d) {
    const auto& color = (*colors_4d)[0];
    construct_basic_material(material_color, color[3], transparent, depth_test);
    return;
  }
}

//! \brief construct a basic material object and inserts it into a map.
void Json_formatter::construct_basic_material(Material_color mc,
                                              bool depth_test)
{
  Basic_material basic_material(mc);
  basic_material.m_depth_test = depth_test;
  auto uuid = boost::uuids::random_generator()();
  m_basic_materials[basic_material] = boost::uuids::to_string(uuid);
}

//! \brief constructs a basic material object and inserts it into a map
void Json_formatter::construct_basic_material(Material_color mc,
                                              const Vector3f& color,
                                              bool depth_test)
{
  Basic_material basic_material(mc, to_hex(color));
  basic_material.m_depth_test = depth_test;
  auto uuid = boost::uuids::random_generator()();
  m_basic_materials[basic_material] = boost::uuids::to_string(uuid);
}

//! \brief constructs a basic material object and inserts it into a map
void Json_formatter::construct_basic_material(Material_color mc,
                                              const Vector4f& color,
                                              bool transparent,
                                              bool depth_test)
{
  Basic_material basic_material(mc, to_hex(color), color[3], transparent,
                                depth_test);
  auto uuid = boost::uuids::random_generator()();
  m_basic_materials[basic_material] = boost::uuids::to_string(uuid);
}

//! \brief constructs a basic material object and inserts it into a map
void Json_formatter::construct_basic_material(Material_color mc,
                                              float opacity, bool transparent,
                                              bool depth_test)
{
  Basic_material basic_material(mc, 0x0, opacity, transparent, depth_test);
  auto uuid = boost::uuids::random_generator()();
  m_basic_materials[basic_material] = boost::uuids::to_string(uuid);
}

//! \brief pre-processes the formatter.
void Json_formatter::
pre_process(const std::list<Shared_container>& containers,
            const std::map<std::string, Shared_container>& instances)
{
  for (auto container : containers) {
    auto geometry = boost::dynamic_pointer_cast<Geometry>(container);
    if (geometry) {
      handle_geometry(geometry);
      continue;
    }

    auto app = boost::dynamic_pointer_cast<Appearance>(container);
    if (app) {
      handle_appearance(app);
      continue;
    }

    auto shape = boost::dynamic_pointer_cast<Shape>(container);
    if (shape) handle_shape(shape);
  }

  for (auto instance : instances) {
    auto geometry = boost::dynamic_pointer_cast<Geometry>(instance.second);
    if (geometry) {
      handle_geometry(geometry);
      continue;
    }

    auto app = boost::dynamic_pointer_cast<Appearance>(instance.second);
    if (app) {
      handle_appearance(app);
      continue;
    }

    auto shape = boost::dynamic_pointer_cast<Shape>(instance.second);
    if (shape) handle_shape(shape);
  }
}

//! \brief converts a color in the HTML format to its hexadecimal string rep.
std::string Json_formatter::to_string_hex(size_t color)
{
  std::string result;
  std::stringstream ss;
  ss << std::hex << color;
  ss >> result;
  return std::string("0x") + result;
}

//! \brief converts a color to its hexadecimal string representation.
std::string Json_formatter::to_string_hex(const Vector3f& color)
{ return to_string_hex(to_hex(color)); }

//! \brief exports the attributes of all geometries.
void Json_formatter::export_geometries_data()
{
  // std::cout << "Json_formatter::export_geometries_data()" << std::endl;
  for (auto item : m_geometries) {
    Shared_geometry geometry = item.first;
    auto geo_set = boost::dynamic_pointer_cast<Geo_set>(geometry);
    if (geo_set) {
      auto type = geo_set->get_primitive_type();
      if (is_structured_mesh(type)) {
        auto mesh_set = boost::dynamic_pointer_cast<Mesh_set>(geo_set);
        SGAL_assertion(mesh_set);
        auto op = std::bind(&Json_formatter::export_structured_mesh_geometry,
                            this, mesh_set, item.second.front());
        single_object(op);
        continue;
      }

      if (is_lines(type)) {
        auto lines_set = boost::dynamic_pointer_cast<Lines_set>(geo_set);
        SGAL_assertion(lines_set);
        auto op = std::bind(&Json_formatter::export_lines_geometry, this,
                            lines_set, item.second.front());
        single_object(op);
        continue;
      }

      if (is_polylines(type)) {
        auto lines_set = boost::dynamic_pointer_cast<Lines_set>(geo_set);
        SGAL_assertion(lines_set);
        const auto& polyline_indices = lines_set->polyline_coord_indices();
        for (size_t i = 0; i < polyline_indices.size(); i++) {
          auto op = std::bind(&Json_formatter::export_polyline_geometry, this,
                              lines_set, i, item.second[i]);
          single_object(op);
        }
        continue;
      }

      // The rest is not supported yet.
      std::cerr << "ERROR: primitive type (" << type << ") not supported!"
                << std::endl;
    }

    auto box = boost::dynamic_pointer_cast<Box>(geometry);
    if (box) {
      auto op = std::bind(&Json_formatter::export_box_geometry, this, box,
                          item.second.front());
      single_object(op);
      continue;
    }

    //! \todo Handle primitive types.
    // The rest is not supported yet.
    std::cerr << "ERROR: Geometry (" << geometry->get_tag() << ") not supported!"
              << std::endl;
  }
}

//! \brief exports the attributes of all materials.
void Json_formatter::export_materials_data()
{
  for (auto item : m_basic_materials) {
    auto material_op = std::bind(&Json_formatter::export_basic_material, this,
                                 item.first, item.second);
    single_object(material_op);
  }

  for (auto item : m_appearances) {
    auto material_op = std::bind(&Json_formatter::export_material, this,
                                 item.first, item.second);
    single_object(material_op);
  }
}

//! \brief writes the begin statement.
void Json_formatter::begin()
{
  // std::cout << "Json_formatter::begin()" << std::endl;
  SGAL_assertion(m_out != nullptr);
  // m_old_out_mode = get_mode(*m_out);
  // set_ascii_mode(*m_out);

  object_begin();
  attribute_single("metadata",
                   [&]() {
                     attribute("version", s_version);
                     attribute("type", "Object");
                     attribute("generator", "SGAL");
                   });

  attribute_multiple("geometries", [&](){ export_geometries_data(); });
  attribute_multiple("materials", [&](){ export_materials_data(); });

  object_separator();
  indent();
  out_string("object");
  name_value_separator();
  object_begin();
  auto uuid = boost::uuids::random_generator()();
  attribute("uuid", boost::uuids::to_string(uuid));
  attribute("type", "Scene");
  attribute_multiple("matrix", [&](){ export_matrix(m_identity_matrix); }, true);
  attribute_single("boundingSphere",
                   [&]() { export_bounding_sphere(m_bounding_sphere); });
  object_separator();
  indent();
  out_string("children");
  name_value_separator();
  array_begin();
}

//! \brief writes the end statement.
void Json_formatter::end()
{
  array_end();
  object_end();
  object_end();
  new_line();
}

//! \brief exports a 3D point.
void Json_formatter::vertex(const Vector3f& p, bool compact)
{
  object_separator(compact);
  if (!compact) indent();
  out() << p[0] << "," << p[1] << "," << p[2];
  if (!compact) new_line();
  m_separated = false;
}

//! brief exports a 2D point.
void Json_formatter::vertex(const Vector2f& p, bool compact)
{
  object_separator(compact);
  if (!compact) indent();
  out() << p[0] << "," << p[1];
  if (!compact) new_line();
  m_separated = false;
}

//! \brief exports a facet.
void Json_formatter::facet(Shared_mesh_set mesh_set, unsigned int flags,
                           size_t i, bool compact)
{
  object_separator(compact);
  if (!compact) indent();

  out() << flags << ",";

  (Geo_set::PT_QUADS == mesh_set->get_primitive_type()) ?
    quad_facet(mesh_set, i) : tri_facet(mesh_set, i);

  if (!compact) new_line();
  m_separated = false;
}

//! \brief writes a triangular facet.
void Json_formatter::tri_facet(Shared_mesh_set mesh_set, size_t i)
{
  // Export vertex coordinates:
  const auto& coord_tris = mesh_set->triangle_coord_indices();
  const auto& tri = coord_tris[i];
  out() << tri[0] << "," << tri[1] << "," << tri[2];

  // Export texture coordinates:
  boost::shared_ptr<Tex_coord_array> tex_coord_array(nullptr);
  boost::shared_ptr<Tex_coord_array_2d> tex_coord_array_2d(nullptr);
  auto has_uvs = mesh_set->has_tex_coord();
  if (has_uvs) {
    tex_coord_array = mesh_set->get_tex_coord_array();
    tex_coord_array_2d =
      boost::dynamic_pointer_cast<Tex_coord_array_2d>(tex_coord_array);
    has_uvs = tex_coord_array_2d && ! tex_coord_array_2d->empty();
  }
  if (has_uvs) {
    const auto& tex_coord_indices = mesh_set->get_facet_tex_coord_indices();
    const auto& tex_coord_tris =
      (mesh_set->empty_facet_indices(tex_coord_indices)) ?
      mesh_set->triangle_coord_indices() :
      mesh_set->triangle_tex_coord_indices();
    const auto& tri = tex_coord_tris[i];
    out() << "," << tri[0] << "," << tri[1] << "," << tri[2];
  }

  // Export normal coordinates:
  auto normal_array = mesh_set->get_normal_array();
  auto has_normals = normal_array && ! normal_array->empty();
  if (has_normals) {
    switch (mesh_set->get_normal_attachment()) {
     case Geo_set::AT_PER_VERTEX:
      {
        const auto& normal_indices = mesh_set->get_facet_normal_indices();
        const auto& normal_tris =
          (mesh_set->empty_facet_indices(normal_indices)) ?
          mesh_set->triangle_coord_indices() :
          mesh_set->triangle_normal_indices();
        out() << "," << tri[0] << "," << tri[1] << "," << tri[2];
      }
      break;

     case Geo_set::AT_PER_PRIMITIVE: out() << "," << i; break;

     case Geo_set::AT_PER_MESH:
     default: SGAL_error();
    }
  }

  auto color_array = mesh_set->get_color_array();
  auto has_colors = color_array && ! color_array->empty();
  if (has_colors) {
    switch (mesh_set->get_color_attachment()) {
     case Geo_set::AT_PER_VERTEX:
      {
        const auto& color_indices = mesh_set->get_facet_color_indices();
        const auto& color_tris =
          (mesh_set->empty_facet_indices(color_indices)) ?
          mesh_set->triangle_coord_indices() :
          mesh_set->triangle_color_indices();
        out() << "," << tri[0] << "," << tri[1] << "," << tri[2];
      }
      break;

     case Geo_set::AT_PER_PRIMITIVE: out() << "," << i; break;

     case Geo_set::AT_PER_MESH: out() << "," << 0; break;

     default: SGAL_error();
    }
  }
}

//! \brief writes a triangular facet.
void Json_formatter::quad_facet(Shared_mesh_set mesh_set, size_t i)
{
  // Export vertex coordinates:
  const auto& coord_quads = mesh_set->quad_coord_indices();
  const auto& quad = coord_quads[i];
  out() << quad[0] << "," << quad[1] << "," << quad[2];

  // Export texture coordinates:
  boost::shared_ptr<Tex_coord_array> tex_coord_array(nullptr);
  boost::shared_ptr<Tex_coord_array_2d> tex_coord_array_2d(nullptr);
  auto has_uvs = mesh_set->has_tex_coord();
  if (has_uvs) {
    tex_coord_array = mesh_set->get_tex_coord_array();
    tex_coord_array_2d =
      boost::dynamic_pointer_cast<Tex_coord_array_2d>(tex_coord_array);
    has_uvs = tex_coord_array_2d && ! tex_coord_array_2d->empty();
  }
  if (has_uvs) {
    const auto& tex_coord_indices = mesh_set->get_facet_tex_coord_indices();
    const auto& tex_coord_quads =
      (mesh_set->empty_facet_indices(tex_coord_indices)) ?
      mesh_set->quad_coord_indices() :
      mesh_set->quad_tex_coord_indices();
    const auto& quad = tex_coord_quads[i];
    out() << ","
          << quad[0] << "," << quad[1] << "," << quad[2] << "," << quad[3];
  }

  // Export normal coordinates:
  auto normal_array = mesh_set->get_normal_array();
  auto has_normals = normal_array && ! normal_array->empty();
  if (has_normals) {
    switch (mesh_set->get_normal_attachment()) {
     case Geo_set::AT_PER_VERTEX:
      {
        const auto& normal_indices = mesh_set->get_facet_normal_indices();
        const auto& normal_quads =
          (mesh_set->empty_facet_indices(normal_indices)) ?
          mesh_set->quad_coord_indices() :
          mesh_set->quad_normal_indices();
        out() << ","
              << quad[0] << "," << quad[1] << "," << quad[2] << "," << quad[3];
      }
      break;

     case Geo_set::AT_PER_PRIMITIVE: out() << "," << i; break;

     case Geo_set::AT_PER_MESH:
     default: SGAL_error();
    }
  }

  auto color_array = mesh_set->get_color_array();
  auto has_colors = color_array && ! color_array->empty();
  if (has_colors) {
    switch (mesh_set->get_color_attachment()) {
     case Geo_set::AT_PER_VERTEX:
      {
        const auto& color_indices = mesh_set->get_facet_color_indices();
        const auto& color_quads =
          (mesh_set->empty_facet_indices(color_indices)) ?
          mesh_set->quad_coord_indices() :
          mesh_set->quad_color_indices();
        out() << ","
              << quad[0] << "," << quad[1] << "," << quad[2] << "," << quad[3];
      }
      break;

     case Geo_set::AT_PER_PRIMITIVE: out() << "," << i; break;

     case Geo_set::AT_PER_MESH: out() << "," << 0; break;

     default: SGAL_error();
    }
  }
}

//! \brief writes a scene-graph container.
void Json_formatter::write(Shared_container container)
{
  // std::cout << "Json_formatter::write(): " << container->get_tag() << std::endl;
  auto group = boost::dynamic_pointer_cast<Group>(container);
  if (group) {
    single_object([&](){ export_group(group); });
    return;
  }

  auto shape = boost::dynamic_pointer_cast<Shape>(container);
  if (shape) {
    auto geometry = shape->get_geometry();
    auto geo_set = boost::dynamic_pointer_cast<Geo_set>(geometry);
    if (geo_set) {
      auto type = geo_set->get_primitive_type();
      if (is_structured_mesh(type) || is_lines(type)) {
        single_object([&](){ export_structured_shape(shape); });
        return;
      }

      if (is_polylines(type)) {
        auto lines_set = boost::dynamic_pointer_cast<Lines_set>(geo_set);
        SGAL_assertion(lines_set);
        const auto& polylines_indices = lines_set->polyline_coord_indices();
        for (size_t i = 0; i < polylines_indices.size(); ++i)
          single_object([&](){ export_polyline_shape(shape, i); });
        return;
      }

      std::cerr << "ERROR: Geometry (" << geometry->get_tag()
                << ") not supported!" << std::endl;
      return;
    }
    single_object([&](){ export_primitive_shape(shape); });
    return;
  }

  auto light = boost::dynamic_pointer_cast<Light>(container);
  if (light) {
    single_object([&](){ export_light(light); });
    return;
  }

  container->write(this);
}

void Json_formatter::out_string(const char* str)
{ out() << '\"' << str << '\"'; }

//! \brief exports a separator between the name of an attribute and its value.
void Json_formatter::name_value_separator() { out() << ": "; }

//! \brief exports a separator between consecutive objects.
void Json_formatter::object_separator(bool compact)
{
  if (m_separated) return;
  out() << ",";
  if (! compact) new_line();
  m_separated = true;
}

void Json_formatter::object_begin(bool compact)
{
  if (! compact) indent();
  out() << "{";
  m_separated = true;
  if (! compact) {
    new_line();
    push_indent();
  }
}

void Json_formatter::object_end(bool compact)
{
  if (! compact) {
    new_line();
    pop_indent();
    indent();
  }
  out() << "}";
  m_separated = false;
}

//! \brief begins an array.
void Json_formatter::array_begin(bool compact)
{
  if (compact) {
    out() << "[";
    m_separated = true;
    return;
  }
  indent();
  out() << "[";
  m_separated = true;
  new_line();
  push_indent();
}

//! \brief ends an array.
void Json_formatter::array_end(bool compact)
{
  if (compact) {
    out() << "]";
    m_separated = false;
    return;
  }
  new_line();
  pop_indent();
  indent();
  out() << "]";
  m_separated = false;
}

//! \brief exports a matrix.
void Json_formatter::export_matrix(const Matrix4f& matrix)
{
  for (auto i = 0; i != 4; ++i)
    for (auto j = 0; j != 4; ++j)
      single_value(matrix.get(i, j), true);
}

//! exports the camera.
void Json_formatter::export_camera()
{
  auto uuid = boost::uuids::random_generator()();
  attribute("uuid", boost::uuids::to_string(uuid));
  const auto& name = m_camera->get_name();
  if (! name.empty()) attribute("name", name);

  auto& frustum = m_camera->get_frustum();

  // The viewing matrix of the camera is the OpenGL matrix, which seems to be
  // different than the one we need, so build it from scratch:

  Matrix4f mat;
  mat.make_identity();
  mat.set_col(0, m_camera->get_right());
  mat.set_col(1, m_camera->get_up());
  mat.set_col(2, m_camera->get_forward());

  // rotation:
  auto rotation = m_camera->get_orientation();
  const auto& newz = rotation.get_axis();
  const auto angle = rotation.get_angle();
  mat.post_rot(mat, newz[0], newz[1], newz[2], angle);

  // Position
  Vector3f translation = m_camera->get_position();
  translation.xform_pt(translation, mat);
  mat.set_row(3, translation);
  attribute_multiple("matrix", [&](){ export_matrix(mat); }, true);
  auto type = frustum.get_type_code();
  attribute("type", ((Frustum::ORTHOGONAL == type) ?
                     "OrthogonalCamera" : "PerspectiveCamera"));
  attribute("fov", rad2deg(frustum.get_horiz_fov()));
  attribute("aspect", frustum.get_aspect_ratio());
  Float near_dist, far_dist;
  frustum.get_near_far(near_dist, far_dist);
  attribute("near", near_dist);
  attribute("far", far_dist);
}

//! exports a group.
void Json_formatter::export_group(Shared_group group)
{
  auto uuid = boost::uuids::random_generator()();
  attribute("uuid", boost::uuids::to_string(uuid));
  const auto& name = group->get_name();
  if (! name.empty()) {
    auto it = m_nodes.find(group);
    size_t id(0);
    if (it == m_nodes.end()) m_nodes[group] = id;
    else id = ++(it->second);
    attribute("name", name + "_" + std::to_string(id));
  }

  attribute("type", group->get_tag());
  attribute("visible", group->is_visible() && is_visible());
  const auto* bs = &(group->get_bounding_sphere());
  attribute_single("boundingSphere", [&]() { export_bounding_sphere(bs); });
  auto transform = boost::dynamic_pointer_cast<Transform>(group);
  const auto& mat = (transform) ? transform->get_matrix() : m_identity_matrix;
  attribute_multiple("matrix", [&](){ export_matrix(mat); }, true);
  attribute_multiple("children", [&](){ group->write(this); });
}

//! \brief exports a light source.
void Json_formatter::export_light(Shared_light light)
{
  auto uuid = boost::uuids::random_generator()();
  attribute("uuid", boost::uuids::to_string(uuid));
  const auto& name = light->get_name();
  if (! name.empty()) attribute("name", name);
  attribute("intensity", light->get_intensity());
  attribute("color", to_hex(light->get_color()));
  attribute("type", light->get_tag());

  Matrix4f mat;
  mat.make_identity();
  auto spot_light = boost::dynamic_pointer_cast<Spot_light>(light);
  if (spot_light) {
    const auto& direction = spot_light->get_direction();
    //! \todo do something with the direction.
    Vector3f translation(spot_light->get_location());
    translation.xform_pt(translation, mat);
    mat.set_row(3, translation);
  }
  attribute_multiple("matrix", [&](){ export_matrix(mat); }, true);
  light->write(this);
}

//! \brief exports the attributes of a box geometry object.
void Json_formatter::export_box_geometry(Shared_box box, String& id)
{
  attribute("uuid", id);
  attribute("type", "BoxGeometry");
  const auto& name = box->get_name();
  if (! name.empty()) attribute("name", name);
  attribute("width", box->get_size()[0]);
  attribute("height", box->get_size()[1]);
  attribute("depth", box->get_size()[2]);
}

//! \brief exports a shape of a primitive geometry.
void Json_formatter::export_primitive_shape(Shared_shape shape)
{
  auto geometry = shape->get_geometry();
  if (! geometry) return;

  auto uuid = boost::uuids::random_generator()();
  attribute("uuid", boost::uuids::to_string(uuid));
  const auto& name = shape->get_name();
  if (! name.empty()) {
    auto it = m_nodes.find(shape);
    size_t id(0);
    if (it == m_nodes.end()) m_nodes[shape] = id;
    else id = ++(it->second);
    attribute("name", name + "_" + std::to_string(id));
  }

  // If the priority is zero, retain the default render-order. Otherwise,
  // set the render order to be the inverse of the priority. Apparently,
  // the 'renderOrder' attribute has an effect only when the shape is opaque.
  if (0 != shape->get_priority())
    attribute("renderOrder", SHRT_MAX - shape->get_priority());

  Matrix4f mat;
  mat.make_identity();
  attribute_multiple("matrix", [&](){ export_matrix(mat); }, true);
  attribute("visible", shape->is_visible() && is_visible());
  attribute("type", "Mesh");
  auto git = m_geometries.find(geometry);
  if (git == m_geometries.end())
    std::cerr << "ERROR: Failed to find geometry!" << std::endl;
  else attribute("geometry", git->second.front());

  auto app = shape->get_appearance();
  auto ait = m_appearances.find(app);
  if (ait == m_appearances.end())
    std::cerr << "ERROR: Failed to find material!" << std::endl;
  else attribute("material", ait->second);
  shape->write(this);
}

//! \brief exports a shape the geometry of which is structured.
void Json_formatter::export_structured_shape(Shared_shape shape)
{
  auto app = shape->get_appearance();
  auto geometry = shape->get_geometry();
  if (! geometry) return;
  auto geo_set = boost::dynamic_pointer_cast<Geo_set>(geometry);
  SGAL_assertion(geo_set);
  auto type = geo_set->get_primitive_type();

  auto uuid = boost::uuids::random_generator()();
  attribute("uuid", boost::uuids::to_string(uuid));
  const auto& name = shape->get_name();
  if (! name.empty()) {
    auto it = m_nodes.find(shape);
    size_t id(0);
    if (it == m_nodes.end()) m_nodes[shape] = id;
    else id = ++(it->second);
    attribute("name", name + "_" + std::to_string(id));
  }

  // If the priority is zero, retain the default render-order. Otherwise,
  // set the render order to be the inverse of the priority. Apparently,
  // the 'renderOrder' attribute has an effect only when the shape is opaque.
  if (0 != shape->get_priority())
    attribute("renderOrder", SHRT_MAX - shape->get_priority());

  Matrix4f mat;
  mat.make_identity();
  attribute_multiple("matrix", [&](){ export_matrix(mat); }, true);
  attribute("visible", shape->is_visible() && is_visible());
  if (is_structured_mesh(type)) attribute("type", "Mesh");
  else if (is_lines(type)) attribute("type", "LineSegments");
  else SGAL_error();
  auto git = m_geometries.find(geometry);
  if (git == m_geometries.end())
    std::cerr << "ERROR: Failed to find geometry!" << std::endl;
  else attribute("geometry", git->second.front());

  if (geometry->has_color()) {
    Material_color material_color(MC_NO_COLORS);
    size_t color(0x0);
    float opacity(1.0f);
    auto transparent(app->is_transparent());
    auto depth_test(app->get_depth_enable());
    auto colors = geo_set->get_color_array();
    if (is_lines(type)) {
      auto colors_3d = boost::dynamic_pointer_cast<Color_array_3d>(colors);
      if (colors_3d) color = to_hex((*colors_3d)[0]);
      else {
        // Here we do not override the color, cause we use the color of the
        // vertex or the color of the face
        auto colors_4d = boost::dynamic_pointer_cast<Color_array_4d>(colors);
        if (colors_4d) opacity = ((*colors_4d)[0])[3];
      }
    }
    else {
      auto attach = geo_set->get_color_attachment();
      material_color = (Geo_set::AT_PER_VERTEX == attach) ?
        MC_VERTEX_COLORS : MC_FACE_COLORS;
      auto colors_4d = boost::dynamic_pointer_cast<Color_array_4d>(colors);
      if (colors_4d) opacity = ((*colors_4d)[0])[3];
    }

    Basic_material basic_material(material_color, color, opacity, transparent,
                                  depth_test);
    auto bmit = m_basic_materials.find(basic_material);
    if (bmit == m_basic_materials.end())
      std::cerr << "ERROR: Failed to find basic material!" << std::endl;
    else attribute("material", bmit->second);
  }
  else {
    auto ait = m_appearances.find(app);
    if (ait == m_appearances.end())
      std::cerr << "ERROR: Failed to find material!" << std::endl;
    else attribute("material", ait->second);
  }
  shape->write(this);
}

/*! \brief exports an object that corresponds to the ith polyline of a shape the
 * geometry of which is polylines.
 */
void Json_formatter::export_polyline_shape(Shared_shape shape, size_t i)
{
  auto app = shape->get_appearance();
  auto geometry = shape->get_geometry();
  auto geo_set = boost::dynamic_pointer_cast<Lines_set>(geometry);
  SGAL_assertion(geo_set);
  auto type = geo_set->get_primitive_type();
  auto lines_set = boost::dynamic_pointer_cast<Lines_set>(geo_set);
  SGAL_assertion(lines_set);
  const auto& polylines_indices = lines_set->polyline_coord_indices();
  for (const auto& indices : polylines_indices) {
    auto uuid = boost::uuids::random_generator()();
    attribute("uuid", boost::uuids::to_string(uuid));
    const auto& name = shape->get_name();
    if (! name.empty()) attribute("name", name);

    // If the priority is zero, retain the default render-order. Otherwise,
    // set the render order to be the inverse of the priority. Apparently,
    // the 'renderOrder' attribute has an effect only when the shape is opaque.
    if (0 != shape->get_priority())
      attribute("renderOrder", SHRT_MAX - shape->get_priority());

    Matrix4f mat;
    mat.make_identity();
    attribute_multiple("matrix", [&](){ export_matrix(mat); }, true);
    attribute("visible", shape->is_visible() && is_visible());
    if (is_line_strips(type)) attribute("type", "Line");
    else if (is_line_loops(type)) attribute("type", "LineLoop");
    else SGAL_error();
    auto git = m_geometries.find(geometry);
    if (git == m_geometries.end())
      std::cerr << "ERROR: Failed to find geometry!" << std::endl;
    else attribute("geometry", git->second[i]);

    if (geometry->has_color()) {
      Material_color material_color(MC_NO_COLORS);
      float opacity(1.0f);
      size_t color(0x0);
      bool transparent(app->is_transparent());
      auto depth_test(app->get_depth_enable());
      auto colors = geo_set->get_color_array();
      auto colors_3d = boost::dynamic_pointer_cast<Color_array_3d>(colors);
      if (colors_3d) color = to_hex((*colors_3d)[0]);
      else {
        auto colors_4d = boost::dynamic_pointer_cast<Color_array_4d>(colors);
        if (colors_4d) {
          color = to_hex((*colors_4d)[0]);
          opacity = ((*colors_4d)[0])[3];
        }
      }
      Basic_material basic_material(material_color, color, opacity, transparent,
                                    depth_test);
      auto bmit = m_basic_materials.find(basic_material);
      if (bmit == m_basic_materials.end())
        std::cerr << "ERROR: Failed to find basic material!" << std::endl;
      else attribute("material", bmit->second);
    }
    else {
      auto ait = m_appearances.find(app);
      if (ait == m_appearances.end())
        std::cerr << "ERROR: Failed to find material!" << std::endl;
      else attribute("material", ait->second);
    }
  }
  shape->write(this);
}

//! \brief exports the bounding sphere of the scene.
void
Json_formatter::export_bounding_sphere(const Bounding_sphere* bounding_sphere)
{
  if (! bounding_sphere) return;
  attribute("radius", bounding_sphere->get_radius());
  attribute_single("center",
                   [&](){
                     const auto& center = bounding_sphere->get_center();
                     attribute("x", center[0], true);
                     attribute("y", center[1], true);
                     attribute("z", center[2], true);
                   }, true);
}

//! \brief exports the attributes of a material object.
void Json_formatter::export_material(Shared_apperance appearance, String& id)
{
  attribute("uuid", id);
  attribute("type", "MeshPhongMaterial");
  const auto& name = appearance->get_name();
  if (! name.empty()) attribute("name", name);
  //"vertexColors": false,
  //"depthTest": true,
  //"depthWrite": true,
  auto material = appearance->get_material();
  if (material) {
    attribute("color", to_hex(material->get_diffuse_color()));
    attribute("specular", to_hex(material->get_specular_color()));
    attribute("emissive", to_hex(material->get_emissive_color()));
    attribute("ambient", to_hex(material->get_ambient_color()));
    attribute("shininess", material->get_shininess());
  }
  attribute("blending", BLENDING_NORMAL);
  attribute_not_default("transparent", appearance->is_transparent(), false);
  attribute_not_default("depthTest", appearance->get_depth_enable(), true);
}

//! \brief exports the attributes of a basic material object.
void Json_formatter::export_basic_material(const Basic_material& basic_material,
                                           const String& id)
{
  attribute("uuid", id);
  attribute("type", "MeshBasicMaterial");
  attribute_not_default("vertexColors", basic_material.m_material_color,
                        MC_NO_COLORS);
  attribute("flatShading", true);
  if (MC_NO_COLORS == basic_material.m_material_color)
  attribute("color", to_string_hex(basic_material.m_color));
  attribute_not_default("opacity", basic_material.m_opacity, 1.0f);
  attribute_not_default("transparent", basic_material.m_transparent, false);
  attribute_not_default("depthTest", basic_material.m_depth_test, true);
}

//! \brief exports the attributes of a basic material object with color.
void Json_formatter::export_color_material(size_t color, String& id)
{
  attribute("uuid", id);
  attribute("type", "MeshBasicMaterial");
  attribute("color", to_string_hex(color));
}

//! \brief exports the attributes of a structured mesh geometry object.
void Json_formatter::export_structured_mesh_geometry(Shared_mesh_set mesh_set,
                                                     String& id)
{
  attribute("uuid", id);
  attribute("type", "Geometry");
  auto op = std::bind(&Json_formatter::export_structured_mesh_geometry_data,
                      this, mesh_set);
  attribute_single("data", op);
}

//! \brief exports the attributes of a lines-geometry object.
void
Json_formatter::export_lines_geometry(Shared_lines_set lines_set, String& id)
{
  attribute("uuid", id);
  attribute("type", "Geometry");
  auto op =
    std::bind(&Json_formatter::export_lines_geometry_data, this, lines_set);
  attribute_single("data", op);
}

//! \brief exports the attributes of a polylines-geometry object.
void Json_formatter::export_polyline_geometry(Shared_lines_set lines_set,
                                              size_t i, String& id)
{
  attribute("uuid", id);
  attribute("type", "Geometry");
  auto op = std::bind(&Json_formatter::export_polyline_geometry_data,
                      this, lines_set, i);
  attribute_single("data", op);
}

//! \brief exports the data record of a structured-mesh geometry item.
void
Json_formatter::export_structured_mesh_geometry_data(Shared_mesh_set mesh_set)
{
  const auto& name = mesh_set->get_name();
  if (! name.empty()) attribute("name", name);

  auto coord_array = mesh_set->get_coord_array();
  if (!coord_array || coord_array->empty()) {
    attribute_single("metadata",
                     [&]() {
                       attribute("version", s_version);
                       attribute("vertices", 0);
                     });
    return;
  }

  const auto& indices = mesh_set->get_facet_coord_indices();
  if (mesh_set->empty_facet_indices(indices)) {
    attribute_single("metadata",
                     [&]() {
                       attribute("version", s_version);
                       attribute("vertices", 0);
                     });
    return;
  }

  std::bitset<F_SIZE> flags;
  auto type = mesh_set->get_primitive_type();
  flags[F_IS_TRI] = (type == Geo_set::PT_QUADS);

  auto normal_array = mesh_set->get_normal_array();
  auto has_normals = normal_array && ! normal_array->empty();

  auto colors = mesh_set->get_color_array();
  auto has_colors = colors && ! colors->empty();

  boost::shared_ptr<Tex_coord_array> tex_coord_array(nullptr);
  boost::shared_ptr<Tex_coord_array_2d> tex_coord_array_2d(nullptr);
  auto has_uvs = mesh_set->has_tex_coord();
  if (has_uvs) {
    tex_coord_array = mesh_set->get_tex_coord_array();
    tex_coord_array_2d =
      boost::dynamic_pointer_cast<Tex_coord_array_2d>(tex_coord_array);
    has_uvs = tex_coord_array_2d && ! tex_coord_array_2d->empty();
  }

  auto num_primitives = mesh_set->get_num_primitives();
  auto coord_array_3d = boost::dynamic_pointer_cast<Coord_array_3d>(coord_array);
  auto num_vertices = coord_array->size();

  attribute_single("metadata",
                   [&]() {
                     attribute("version", s_version);
                     attribute("vertices", num_vertices);
                     if (has_normals)
                       attribute("normals", normal_array->size());
                     if (has_colors)
                       attribute("colors", colors->size());
                     if (has_uvs)
                       attribute ("uvs", tex_coord_array_2d->size());
                     attribute("faces", num_primitives);
                   });

  // Export vertices:
  attribute_multiple("vertices",
                     [&]() {
                       for (const auto& v : *coord_array_3d) vertex(v, true);
                     },
                     true);

  // Export normals:
  if (has_normals)
    attribute_multiple("normals",
                       [&]() {
                         for (const auto& n : *normal_array) vertex(n, true);
                       },
                       true);

  // Export colors:
  if (has_colors) {
    // The color array consists of RGBA colors, we need to convert to RGB,
    // cause threejs is deficient, and cannot handle RGBA!
    auto attach = mesh_set->get_color_attachment();
    bool is_rgb = Geo_set::AT_PER_VERTEX == attach;
    auto colors_3d = boost::dynamic_pointer_cast<Color_array_3d>(colors);
    if (colors_3d) export_colors(*colors_3d, is_rgb);
    else {
      auto colors_4d = boost::dynamic_pointer_cast<Color_array_4d>(colors);
      if (colors_4d) {
        std::vector<Vector3f> my_colors_3d(colors_4d->size());
        std::transform(colors_4d->begin(), colors_4d->end(),
                       my_colors_3d.begin(),
                       [](const Vector4f& v4)
                       { return Vector3f(v4[0], v4[1], v4[2]); });
        export_colors(my_colors_3d, is_rgb);
      }
    }
  }

  // Export texture coordinates:
  if (has_uvs)
    attribute_multiple("uvs",
                       [&]() {
                         multiple_objects([&]() {
                             for (const auto& v : *tex_coord_array_2d)
                               vertex(v, true);
                           }, true);
                       }, true);

  // We do not use the has_material option.
  flags[F_HAS_UVS] = has_uvs;

  if (has_normals) {
    switch (mesh_set->get_normal_attachment()) {
     case Geo_set::AT_PER_VERTEX: flags[F_FACE_VERTEX_NORMAL] = 1; break;
     case Geo_set::AT_PER_PRIMITIVE: flags[F_FACE_NORMAL] = 1; break;
     case Geo_set::AT_PER_MESH:
     default: SGAL_error();
    }
  }

  if (has_colors) {
    switch (mesh_set->get_color_attachment()) {
     case Geo_set::AT_PER_VERTEX: flags[F_FACE_VERTEX_COLOR] = 1; break;
     case Geo_set::AT_PER_PRIMITIVE: flags[F_FACE_COLOR] = 1; break;
     case Geo_set::AT_PER_MESH: flags[F_FACE_COLOR] = 1; break;
     default: SGAL_error();
    }
  }
  // std::cout << "flags: " << flags << std::endl;

  attribute_multiple("faces",
                     [&]() {
                       auto num_facets = mesh_set->get_num_primitives();
                       for (size_t i = 0; i < num_facets; ++i)
                         facet(mesh_set, flags.to_ulong(), i, true);
                     },
                     true);
}

//! \brief exports the data record of a Lines_set item.
void Json_formatter::export_lines_geometry_data(Shared_lines_set lines_set)
{
  const auto& name = lines_set->get_name();
  if (! name.empty()) attribute("name", name);

  auto coord_array = lines_set->get_coord_array();
  if (!coord_array || coord_array->empty()) {
    attribute_single("metadata",
                     [&]() {
                       attribute("version", s_version);
                       attribute("vertices", 0);
                     });
    return;
  }

  auto type = lines_set->get_primitive_type();
  SGAL_assertion(is_lines(type));
  auto num_primitives = lines_set->get_num_primitives();
  auto num_vertices = num_primitives * 2;

  attribute_single("metadata",
                   [&]() {
                     attribute("version", s_version);
                     attribute("vertices", num_vertices);
                     attribute("lineSegments", num_primitives);
                   });

  // Export vertices:
  auto coord_array_3d = boost::dynamic_pointer_cast<Coord_array_3d>(coord_array);
  const auto& lines_indices = lines_set->line_coord_indices();
  if (lines_indices.empty()) return;
  attribute_multiple("vertices",
                     [&]() {
                       for (const auto& indices : lines_indices) {
                         const auto& v1 = (*coord_array_3d)[indices[0]];
                         const auto& v2 = (*coord_array_3d)[indices[1]];
                         vertex(v1, true);
                         vertex(v2, true);
                       }
                     },
                     true);

  // If the geometry represents lines there are no faces to export.
  // three.js requires the presence of an empty faces array.
  attribute_multiple("faces", [&](){}, true);
}

//! \brief exports the data record of a polyline geometry item.
void Json_formatter::export_polyline_geometry_data(Shared_lines_set lines_set,
                                                   size_t index)
{
  const auto& name = lines_set->get_name();
  if (! name.empty()) attribute("name", name);

  auto coord_array = lines_set->get_coord_array();
  if (!coord_array || coord_array->empty()) {
    attribute_single("metadata",
                     [&]() {
                       attribute("version", s_version);
                       attribute("vertices", 0);
                     });
    return;
  }
  auto num_primitives = lines_set->get_num_primitives();
  auto type = lines_set->get_primitive_type();

  const auto& polyline_indices = lines_set->polyline_coord_indices();
  if (polyline_indices.empty()) return;
  const auto& indices = polyline_indices[index];
  if (indices.empty()) return;
  auto num_vertices = indices.size();

  attribute_single("metadata",
                   [&]() {
                     attribute("version", s_version);
                     attribute("vertices", num_vertices);
                   });

  // Export vertices:
  auto coord_array_3d = boost::dynamic_pointer_cast<Coord_array_3d>(coord_array);
  attribute_multiple("vertices",
                     [&]() {
                       for (auto index : indices) {
                         const auto& v = (*coord_array_3d)[index];
                         vertex(v, true);
                       }
                     },
                     true);

  // If the geometry represents lines there are no faces to export.
  // three.js requires the presence of an empty faces array.
  attribute_multiple("faces", [&](){}, true);
}

SGAL_END_NAMESPACE
