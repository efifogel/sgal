// Copyright (c) 2004 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_SCEME_HPP
#define SGAL_SCEME_HPP

/*! \file
 * A concrete scene type derived from this pure scene type manage the
 * scene. Typically there is a single scene per application.
 */

#include "SGAL/basic.hpp"
#include "SGAL/Agent.hpp"

SGAL_BEGIN_NAMESPACE

class Window_item;
class Main_option_parser;

class SGAL_SGAL_DECL Scene : public SGAL::Agent {
public:
  /*! Destruct.
   */
  virtual ~Scene();

  /*! Configure the scene according to the options selected by the user.
   * \param[in] option_parser the player option parser that holds the options.
   */
  virtual void configure_scene() {};

  /*! Create the scene.
   */
  virtual void create_scene();

  /*! Start the scene. */
  virtual void start_scene();

  /*! Indulge user requests from the command line. */
  virtual void indulge_user();

  /*! Destroy the scene. */
  virtual void destroy_scene();

  /*! Initialize the visual. */
  virtual void init_visual() = 0;

  /*! Process the visual. */
  virtual void process_visual() = 0;

  /*! Clear the scene. */
  virtual void clear_visual() = 0;

  /*! Create a window. */
  virtual void create_window();

  /*! Destroy a window.
   */
  virtual void destroy_window(SGAL::Window_item* window_item);

  /*! Initialize a window. Typically used to:
   * 1. create a context, and
   * 2. in case of off-screen, intialize the off-screen rendering.
   * \param[in] window_item the window to initialize
   */
  virtual void init_window(Window_item* window_item, Uint width, Uint height);

  /*! Clear a window.
   * \param[in] window_item the window to initialize
   */
  virtual void clear_window(Window_item* window_item);

  /*! Reshape the viewport of a window of the scene
   * It is assumed that the window context is the current context
   * \param[in] window_item the window to reshape
   * \param[in] width the new width of the window
   * \param[in] height the new height of the window
   */
  virtual void reshape_window(Window_item* window_item, Uint width, Uint height);

  /*! Draw into a window of the scene
   * It is assumed that the window context is the current context
   * \param[in] window_item the window to draw
   * \param[in] dont_accumulate indicates whether accumulation not be performed.
   */
  virtual void draw_window(Window_item* window_item, Boolean dont_accumulate);

  /*! Print the scene information.
   */
  virtual void print_scene_help(void);
};

//! \brief destruct.
inline Scene::~Scene() {}

//! \brief creates the scene.
inline void Scene::create_scene() {}

//! \brief starts the scene.
inline void Scene::start_scene() {};

//! \brief indulges user requests from the command line.
inline void Scene::indulge_user() {};

//! \brief destroys the scene.
inline void Scene::destroy_scene() {}

//! \brief creates a window.
inline void Scene::create_window() {}

//! \brief Destroys a window.
inline void Scene::destroy_window(SGAL::Window_item* /* window_item */) {}

//! \brief initializes a window.
inline void Scene::init_window(Window_item* /* window_item */,
                               Uint /* width */, Uint /* height */)
{}

//! \brief clears a window.
inline void Scene::clear_window(Window_item* /* window_item */) {}

//! \brief reshapes the viewport of a window of the scene.
inline void Scene::reshape_window(Window_item* /* window_item */,
                                  Uint /* width */, Uint /* height */)
{}

//! \brief draws into a window of the scene
inline void Scene::draw_window(Window_item* /* window_item */,
                               Boolean /* dont_accumulate */)
{}

//! \brief prints the scene information */
inline void Scene::print_scene_help(void) {}

SGAL_END_NAMESPACE

#endif
