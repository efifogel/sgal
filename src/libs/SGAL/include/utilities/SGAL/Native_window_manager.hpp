// Copyright (c) 2004  Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_NATIVE_WINDOW_MANAGER_HPP
#define SGAL_NATIVE_WINDOW_MANAGER_HPP

/*! \file
 * The native window manager, which manages the creation and destruction of
 * multiple windows.
 */

#include <time.h>

#include "SGAL/basic.hpp"
#include "SGAL/Types.hpp"
#include "SGAL/Window_manager.hpp"
#include "SGAL/Native_window_item.hpp"

SGAL_BEGIN_NAMESPACE

class Scene;

template <typename WindowItem>
class Native_window_manager : public Window_manager<WindowItem> {
public:
  typedef WindowItem                                            Window_item;

  typedef Window_manager<Window_item>    Base_window_manager;

  /*! Construct */
  Native_window_manager();

  /*! Destruct */
  virtual ~Native_window_manager();

  /*! Set the current window.
   * \param window_item the window to set.
   */
  void set_current_window(Window_item* window_item);

  /*! Obtain the current window.
   * \return the current window.
   */
  Window_item* get_current_window() const;

  /*! Set the current window and make the window's Context current as well.
   * \param window_item the window to make current.
   */
  void make_current_window(Window_item* window_item);

  /*! Set the flag that indicates whether the next window be made full screen.
   */
  void set_full_screen(Boolean flag);

  /*! Set the initial width of windows.
   * \param width the new width of the windows.
   */
  void set_window_width(Uint width);

  /*! Set the initial height of windows.
   * \param heigh the height of the windows.
   */
  void set_window_height(Uint height);

  /*! Set the initial size of windows.
   * \param width the new width of the windows.
   * \param heigh the new height of the windows.
   */
  void set_window_size(Uint width, Uint height);

  /*! Set the initial x-position of window otigins.
   * \param x the screen x-coordinate of the window origin.
   */
  void set_window_x(Uint x);

  /*! Set the initial y-position of window otigins.
   * \param x the screen y-coordinate of the window origin.
   */
  void set_window_y(Uint y);

  /*! Set the initial position of window otigins.
   * \param x the screen X coordinate of the window origin.
   * \param y the screen Y coordinate of the window origin.
   */
  void set_window_position(Uint x, Uint y);

  /*! Attach a scene to the window manager. */
  void set_scene(Scene* scene);

  /*! Set the flag that indicates whether to render offscreen. */
  void set_render_offscreen(Boolean flag);

  /*! Indicate whether to render offscreen. */
  Boolean do_render_offscreen() const;

protected:

  //! The attached scene.
  Scene* m_scene;

  //! The current window.
  Window_item* m_current_window;

  //! Mouse button states.
  Uint m_button_state;

  //! The time when the current tick starts in clock units.
  long int m_start_tick_time;

  //! The accumulated simulation time.
  Scene_time m_sim_time;

  //! The estimated tick duration.
  Scene_time m_est_tick_duration;

  //! The required tick duration.
  Scene_time m_required_tick_duration;

  //! Indicates whether to render off screen.
  Boolean m_render_offscreen;
};

//! \brief constructs.
template <typename WindowItem>
Native_window_manager<WindowItem>::Native_window_manager() :
  m_scene(nullptr),
  m_current_window(nullptr),
  m_start_tick_time(0),
  m_sim_time(0),
  m_est_tick_duration(0),
  m_required_tick_duration(0),
  m_render_offscreen(false)
{
  // Mouse button states:
  m_button_state = 0;
  m_est_tick_duration = 1.0f / 30;
}

//! \brief destructs.
template <typename WindowItem>
Native_window_manager<WindowItem>::~Native_window_manager() {}

//! \brief Set the current window.
template <typename WindowItem>
void Native_window_manager<WindowItem>::set_current_window(WindowItem* item)
{ m_current_window = item; }

//! \brief obtains the current window.
template <typename WindowItem>
WindowItem* Native_window_manager<WindowItem>::get_current_window() const
{ return m_current_window; }

//! \brief sets the current window and make the window's context current as well.
template <typename WindowItem>
void Native_window_manager<WindowItem>::make_current_window(WindowItem* item)
{ m_current_window = item; }

/*! \brief sets the flag that indicates whether succeeding windows are made full
 * screen.
 */
template <typename WindowItem>
void Native_window_manager<WindowItem>::set_full_screen(Boolean flag)
{ Native_window_item::set_init_full_screen(flag); }

//! \brief sets the flag that indicates whether to render offscreen.
template <typename WindowItem>
void Native_window_manager<WindowItem>::set_render_offscreen(Boolean flag)
{
  m_render_offscreen = flag;
  Native_window_item::set_init_render_offscreen(flag);
}

//! \brief indicates whether to render offscreen.
template <typename WindowItem>
Boolean Native_window_manager<WindowItem>::do_render_offscreen() const
{ return m_render_offscreen; }

//! \brief sets the initial width of windows.
template <typename WindowItem>
void Native_window_manager<WindowItem>::set_window_width(Uint width)
{ Native_window_item::set_init_width(width); }

//! \brief sets the initial height of windows.
template <typename WindowItem>
void Native_window_manager<WindowItem>::set_window_height(Uint height)
{ Native_window_item::set_init_height(height); }

//! \brief sets the initial size of windows.
template <typename WindowItem>
void Native_window_manager<WindowItem>::set_window_size(Uint width, Uint height)
{ Native_window_item::set_init_size(width, height); }

//! \brief sets the initial x-position of window otigins.
template <typename WindowItem>
void Native_window_manager<WindowItem>::set_window_x(Uint x)
{ Native_window_item::set_init_x(x); }

//! \brief sets the initial y-position of window otigins.
template <typename WindowItem>
void Native_window_manager<WindowItem>::set_window_y(Uint y)
{ Native_window_item::set_init_y(y); }

//! \brief sets the initial position of window otigins.
template <typename WindowItem>
void Native_window_manager<WindowItem>::set_window_position(Uint x, Uint y)
{ Native_window_item::set_init_position(x, y); }

//! \brief attaches a scene to the window manager.
template <typename WindowItem>
void Native_window_manager<WindowItem>::set_scene(Scene* scene)
{ m_scene = scene; }

SGAL_END_NAMESPACE

#endif
