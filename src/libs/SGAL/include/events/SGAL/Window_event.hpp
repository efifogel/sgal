// Copyright (c) 2022 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// $Source$
// $Revision: 1308 $
//
// Author(s) : Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_WINDOW_EVENT_HPP
#define SGAL_WINDOW_EVENT_HPP

#if (defined _MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "SGAL/basic.hpp"
#include "SGAL/Event.hpp"

SGAL_BEGIN_NAMESPACE

class Window_item;

/*!
 */
class SGAL_SGAL_DECL Window_event : public Event {
private:
  /*! The window where the event took place */
  Window_item* m_window_item;

public:
  /*! Construct default.
   */
  Window_event();

  /*! Set the window where the event took place */
  void set_window_item(Window_item* item);

  /*! Obtain the window where the event took place */
  Window_item* get_window_item() const;
};

//! \brief constructs default.
inline Window_event::Window_event() : m_window_item(nullptr) {}

//! \brief sets the window where the event took place.
inline void Window_event::set_window_item(Window_item* item)
{ m_window_item = item; }

//! \brief obtains the window where the event took place.
inline Window_item* Window_event::get_window_item() const
{ return m_window_item; }

SGAL_END_NAMESPACE

#endif
