// Copyright (c) 2021 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_INDEXED_POINT_SET_MASK_HPP
#define SGAL_INDEXED_POINT_SET_MASK_HPP

#include "SGAL/basic.hpp"
#include "SGAL/set_bits.hpp"

SGAL_BEGIN_NAMESPACE

// Coord index: no
// Fragment attachment: none
// Texture mapping: disabled
#define SGAL_CINO_FANO_TENO_VANO                                      \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_NONE,          \
      set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,0,              \
        set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0))))

// Coord index: no
// Fragment attachment: none
// Texture mapping: enabled
// Texture index: no
#define SGAL_CINO_FANO_TEYE_TINO_VANO                                 \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_NONE,          \
      set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,              \
        set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,0,            \
                 set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))

// Coord index: no
// Fragment attachment: none
// Texture mapping: enabled
// Texture index: yes
#define SGAL_CINO_FANO_TEYE_TIYE_VANO                                 \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_NONE,          \
      set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,              \
        set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,1,            \
          set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))

// Coord index: yes
// Fragment attachment: none
// Texture mapping: disabled
#define SGAL_CIYE_FANO_TENO_VANO                                      \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_NONE,          \
      set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,0,              \
        set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0))))

// Coord index: yes
// Fragment attachment: none
// Texture mapping: enabled
// Texture index: no
#define SGAL_CIYE_FANO_TEYE_TINO_VANO                                 \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_NONE,          \
      set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,              \
        set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,0,            \
                 set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))

// Coord index: yes
// Fragment attachment: none
// Texture mapping: enabled
// Texture index: yes
#define SGAL_CIYE_FANO_TEYE_TIYE_VANO                                 \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_NONE,          \
      set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,              \
        set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,1,            \
          set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))

// Coord index: no
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: disabled
#define SGAL_CINO_FSNO_FINO_FAPV_TENO_VANO                            \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,0,          \
            set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0))))))

// Coord index: no
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: no
#define SGAL_CINO_FSNO_FINO_FAPV_TEYE_TINO_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,0,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: no
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: yes
#define SGAL_CINO_FSNO_FINO_FAPV_TEYE_TIYE_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,1,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: no
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: disabled
#define SGAL_CINO_FSNO_FINO_FAPS_TENO_VANO                            \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,0,          \
            set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0))))))

// Coord index: no
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: no
#define SGAL_CINO_FSNO_FINO_FAPS_TEYE_TINO_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,0,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: no
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: yes
#define SGAL_CINO_FSNO_FINO_FAPS_TEYE_TIYE_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,1,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: no
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: disabled
#define SGAL_CINO_FSNO_FIYE_FAPV_TENO_VANO                            \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,0,          \
            set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0))))))

// Coord index: no
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: no
#define SGAL_CINO_FSNO_FIYE_FAPV_TEYE_TINO_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,0,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: no
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: yes
#define SGAL_CINO_FSNO_FIYE_FAPV_TEYE_TIYE_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,1,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: no
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: disabled
#define SGAL_CINO_FSNO_FIYE_FAPS_TENO_VANO                            \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,0,          \
            set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0))))))

// Coord index: no
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: no
#define SGAL_CINO_FSNO_FIYE_FAPS_TEYE_TINO_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,0,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: no
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: yes
#define SGAL_CINO_FSNO_FIYE_FAPS_TEYE_TIYE_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,1,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: no
// Fragment source: color
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: disabled
#define SGAL_CINO_FSCO_FINO_FAPV_TENO_VANO                            \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,0,          \
            set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0))))))

// Coord index: no
// Fragment source: color
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: no
#define SGAL_CINO_FSCO_FINO_FAPV_TEYE_TINO_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,0,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: no
// Fragment source: color
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: yes
#define SGAL_CINO_FSCO_FINO_FAPV_TEYE_TIYE_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,1,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: no
// Fragment source: color
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: disabled
#define SGAL_CINO_FSCO_FINO_FAPS_TENO_VANO                            \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,0,          \
            set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0))))))

// Coord index: no
// Fragment source: color
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: no
#define SGAL_CINO_FSCO_FINO_FAPS_TEYE_TINO_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,0,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: no
// Fragment source: color
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: yes
#define SGAL_CINO_FSCO_FINO_FAPS_TEYE_TIYE_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,1,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: no
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: disabled
#define SGAL_CINO_FSCO_FIYE_FAPV_TENO_VANO                            \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,0,          \
            set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0))))))

// Coord index: no
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: no
#define SGAL_CINO_FSCO_FIYE_FAPV_TEYE_TINO_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,0,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: no
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: yes
#define SGAL_CINO_FSCO_FIYE_FAPV_TEYE_TIYE_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,1,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: no
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: disabled
#define SGAL_CINO_FSCO_FIYE_FAPS_TENO_VANO                            \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,0,          \
            set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0))))))

// Coord index: no
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: no
#define SGAL_CINO_FSCO_FIYE_FAPS_TEYE_TINO_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,0,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: no
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: yes
#define SGAL_CINO_FSCO_FIYE_FAPS_TEYE_TIYE_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,0,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,1,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: yes
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: disabled
#define SGAL_CIYE_FSNO_FINO_FAPV_TENO_VANO                            \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,0,          \
            set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0))))))

// Coord index: yes
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: no
#define SGAL_CIYE_FSNO_FINO_FAPV_TEYE_TINO_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,0,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: yes
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: yes
#define SGAL_CIYE_FSNO_FINO_FAPV_TEYE_TIYE_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,1,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: yes
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: disabled
#define SGAL_CIYE_FSNO_FINO_FAPS_TENO_VANO                            \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,0,          \
            set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0))))))

// Coord index: yes
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: no
#define SGAL_CIYE_FSNO_FINO_FAPS_TEYE_TINO_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,0,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: yes
// Fragment source: normal
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: yes
#define SGAL_CIYE_FSNO_FINO_FAPS_TEYE_TIYE_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,1,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: yes
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: disabled
#define SGAL_CIYE_FSNO_FIYE_FAPV_TENO_VANO                            \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,0,          \
            set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0))))))

// Coord index: yes
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: no
#define SGAL_CIYE_FSNO_FIYE_FAPV_TEYE_TINO_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,0,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: yes
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: yes
#define SGAL_CIYE_FSNO_FIYE_FAPV_TEYE_TIYE_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,1,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: yes
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: disabled
#define SGAL_CIYE_FSNO_FIYE_FAPS_TENO_VANO                            \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,0,          \
            set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0))))))

// Coord index: yes
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: no
#define SGAL_CIYE_FSNO_FIYE_FAPS_TEYE_TINO_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,0,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: yes
// Fragment source: normal
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: yes
#define SGAL_CIYE_FSNO_FIYE_FAPS_TEYE_TIYE_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_NORMAL,                \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,1,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: yes
// Fragment source: color
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: disabled
#define SGAL_CIYE_FSCO_FINO_FAPV_TENO_VANO                            \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,0,          \
            set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0))))))

// Coord index: yes
// Fragment source: color
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: no
#define SGAL_CIYE_FSCO_FINO_FAPV_TEYE_TINO_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,0,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: yes
// Fragment source: color
// Fragment index: no
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: yes
#define SGAL_CIYE_FSCO_FINO_FAPV_TEYE_TIYE_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,1,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: yes
// Fragment source: color
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: disabled
#define SGAL_CIYE_FSCO_FINO_FAPS_TENO_VANO                            \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,0,          \
            set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0))))))

// Coord index: yes
// Fragment source: color
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: no
#define SGAL_CIYE_FSCO_FINO_FAPS_TEYE_TINO_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,0,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: yes
// Fragment source: color
// Fragment index: no
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: yes
#define SGAL_CIYE_FSCO_FINO_FAPS_TEYE_TIYE_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,0,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,1,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: yes
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: disabled
#define SGAL_CIYE_FSCO_FIYE_FAPV_TENO_VANO                            \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,0,          \
            set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0))))))

// Coord index: yes
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: no
#define SGAL_CIYE_FSCO_FIYE_FAPV_TEYE_TINO_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,0,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: yes
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per vertex
// Texture mapping: enabled
// Texture index: yes
#define SGAL_CIYE_FSCO_FIYE_FAPV_TEYE_TIYE_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_VERTEX,\
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,1,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: yes
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: disabled
#define SGAL_CIYE_FSCO_FIYE_FAPS_TENO_VANO                            \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,0,          \
            set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0))))))

// Coord index: yes
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: no
#define SGAL_CIYE_FSCO_FIYE_FAPS_TEYE_TINO_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,0,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

// Coord index: yes
// Fragment source: color
// Fragment index: yes
// Fragment attachment: per set
// Texture mapping: enabled
// Texture index: yes
#define SGAL_CIYE_FSCO_FIYE_FAPS_TEYE_TIYE_VANO                       \
  set_bits(PO_COORD_INDEX,PO_COORD_INDEX_,1,                          \
    set_bits(PO_FRAG_SOURCE,PO_FRAG_SOURCE_,FS_COLOR,                 \
      set_bits(PO_FRAG_INDEXED,PO_FRAG_INDEXED_,1,                    \
        set_bits(PO_FRAG_ATTACHMENT,PO_FRAG_ATTACHMENT_,AT_PER_MESH,  \
          set_bits(PO_TEXTURE_ENABLED,PO_TEXTURE_ENABLED_,1,          \
            set_bits(PO_TEXTURE_INDEXED,PO_TEXTURE_INDEXED_,1,        \
              set_bits(PO_VERTEX_ARRAY,PO_VERTEX_ARRAY_,0,0x0)))))))

SGAL_END_NAMESPACE

#endif
