// Copyright (c) 2021 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SCGAL_COMPUTE_PLANES_STABLE_HPP
#define SCGAL_COMPUTE_PLANES_STABLE_HPP

/*! \file
 * This file contains a free function that computes the equesions of or the
 * normal to the planes containing the facets of a given polyhedron in a stable
 * fashion. For each facet, It sums up the normals at all vertices of the
 * facet.
 */

#include <algorithm>
#include <queue>

#include <boost/type_traits.hpp>
#include <boost/graph/graph_traits.hpp>

#include <CGAL/boost/graph/properties.h>

#include "SGAL/basic.hpp"
#include "SGAL/triangle_normal.hpp"

SGAL_BEGIN_NAMESPACE

/*! Compute the plane of a facet.
 */
template <typename Kernel_, typename Polyhedron_>
void compute_plane_stable(const Kernel_& kernel, Polyhedron_& pmesh,
                          typename boost::graph_traits<Polyhedron_>::
                           face_descriptor fd,
                          boost::false_type) {
  auto he = CGAL::halfedge(fd, pmesh);
  auto v = CGAL::source(he, pmesh);
  auto the = CGAL::target(he, pmesh);
  he = next(he, pmesh);
  auto tnhe = CGAL::target(he, pmesh);

  const auto& pv = v->point();
  fd->plane() = kernel.construct_vector_3_object()(CGAL::NULL_VECTOR);

  while(v != tnhe) {
    const auto& pvn = the->point();
    const auto& pvnn = tnhe->point();
    const auto n = triangle_normal(pv, pvn, pvnn, kernel);
    fd->plane() = kernel.construct_sum_of_vectors_3_object()(fd->plane(), n);
    the = tnhe;
    he = CGAL::next(he, pmesh);
    tnhe = CGAL::target(he, pmesh);
  }
}

/*! Compute the planes of all facets.
 */
template <typename Kernel_, typename Polyhedron_>
void compute_planes_stable(const Kernel_& kernel, Polyhedron_& polyhedron,
                           boost::false_type) {
  for (auto fd : CGAL::faces(polyhedron))
    compute_plane_stable(kernel, polyhedron, fd, boost::false_type());
}

/*! Compute the plane of a facet.
 */
template <typename Kernel_, typename Polyhedron_>
void compute_plane_stable(const Kernel_& kernel, Polyhedron_& pmesh,
                          typename boost::graph_traits<Polyhedron_>::
                            face_descriptor fd,
                          boost::true_type)
{
  typedef Polyhedron_                           Polyhedron;
  typedef typename Polyhedron::Plane_3          Plane_3;

  auto he = CGAL::halfedge(fd, pmesh);
  auto v = CGAL::source(he, pmesh);
  auto the = CGAL::target(he, pmesh);
  he = next(he, pmesh);
  auto tnhe = CGAL::target(he, pmesh);

  const auto& pv = v->point();
  auto sum = kernel.construct_vector_3_object()(CGAL::NULL_VECTOR);

  while(v != tnhe) {
    const auto& pvn = the->point();
    const auto& pvnn = tnhe->point();
    const auto n = triangle_normal(pv, pvn, pvnn, kernel);
    sum = kernel.construct_sum_of_vectors_3_object()(sum, n);
    the = tnhe;
    he = CGAL::next(he, pmesh);
    tnhe = CGAL::target(he, pmesh);
  }
  fd->plane() = Plane_3(pv, sum);
}

/*! Compute the planes of all facets.
 */
template <typename Kernel_, typename Polyhedron_>
void compute_planes_stable(const Kernel_& kernel, Polyhedron_& polyhedron,
                    boost::true_type) {
  for (auto fd : CGAL::faces(polyhedron))
    compute_plane_stable(kernel, polyhedron, fd, boost::true_type());
}

SGAL_END_NAMESPACE

#endif
