// Copyright (c) 2021 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SCGAL_TRIANGLE_NORMAL_HPP
#define SCGAL_TRIANGLE_NORMAL_HPP

#include <boost/graph/graph_traits.hpp>

/*! Compute the normal of a triangular facet.
 */
template <typename Point_, typename Kernel_>
typename Kernel_::Vector_3
triangle_normal(const Point_& p0, const Point_& p1, const Point_& p2,
                const Kernel_& kernel)
{
  typedef Point_                        Point;
  typedef Kernel_                       Kernel;
  typedef typename Kernel::FT           FT;
  auto cross_product = kernel.construct_cross_product_vector_3_object();
  auto ctr_vector = kernel.construct_vector_3_object();
  auto n = cross_product(ctr_vector(p1, p2), ctr_vector(p1, p0));

  //cross-product(AB, AC)'s norm is the area of the parallelogram
  //formed by these 2 vectors.
  //the triangle's area is half of it
  return kernel.construct_scaled_vector_3_object()(n, FT(1)/FT(2));
}

#endif
