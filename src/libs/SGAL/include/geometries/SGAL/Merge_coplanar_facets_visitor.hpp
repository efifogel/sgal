// Copyright (c) 2008 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_MERGE_COPLANAR_FACETS_VISITOR_HPP
#define SGAL_MERGE_COPLANAR_FACETS_VISITOR_HPP

#include "SGAL/basic.hpp"

SGAL_BEGIN_NAMESPACE

template <typename Polyhedron_>
class Merge_coplanar_facets_visitor {
public:
  typedef Polyhedron_                           Polyhedron;
  typedef typename Polyhedron::Facet_handle     Facet_handle;

  virtual void before_merge(Facet_handle f1, Facet_handle f2) {}
  virtual void after_merge(Facet_handle f) {}
};

SGAL_END_NAMESPACE

#endif
