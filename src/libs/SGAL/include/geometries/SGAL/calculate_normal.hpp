// Copyright (c) 2019 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_CALCULATE_NORMAL_HPP
#define SGAL_CALCULATE_NORMAL_HPP

#include "SGAL/basic.hpp"

SGAL_BEGIN_NAMESPACE

/*! Calculate the normal of a polygonal facet.
 * Observe that if the middle vertex used to calculate the normal (that is,
 * v1) is a reflex vertex, the calculated normal is opposite of the tru normal.
 */
template <typename Poly_, typename Coords_>
bool calculate_normal(const Poly_& facet, const Coords_& coords,
                      Vector3f& normal, bool is_ccw)
{
  const auto& v0 = coords[facet[0]];
  const auto& v1 = coords[facet[1]];
  for (size_t j = 2; j < facet.size(); ++j) {
    const auto& v2 = coords[facet[j]];
    if (Vector3f::collinear(v0, v1, v2)) continue;
    if (is_ccw) normal.normal(v0, v1, v2);
    else normal.normal(v2, v1, v0);
    return true;
  }
  return false;
}

SGAL_END_NAMESPACE

#endif
