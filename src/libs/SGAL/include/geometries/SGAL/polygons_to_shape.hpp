// Copyright (c) 2018 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_POLYGONS_TO_SHAPE_HPP
#define SGAL_POLYGONS_TO_SHAPE_HPP

#include "SGAL/basic.hpp"
#include "SGAL/Shape.hpp"
#include "SGAL/Appearance.hpp"
#include "SGAL/Indexed_line_set.hpp"
#include "SGAL/Vector3f.hpp"
#include "SGAL/Coord_array_3d.hpp"
#include "SGAL/Color_array_3d.hpp"

SGAL_BEGIN_NAMESPACE

template <typename Polygons>
boost::shared_ptr<SGAL::Shape> polygons_to_shape(Scene_graph* scene_graph,
                                                 const Polygons& polygons)
{
  typedef boost::shared_ptr<Shape>              Shared_shape;
  typedef boost::shared_ptr<Appearance>         Shared_appearance;
  typedef boost::shared_ptr<Indexed_line_set>   Shared_indexed_line_set;
  typedef boost::shared_ptr<Coord_array_3d>     Shared_coord_array_3d;
  typedef boost::shared_ptr<Color_array_3d>     Shared_color_array_3d;

  auto shape = Shared_shape(new Shape);
  scene_graph->add_container(shape);
  shape->add_to_scene(scene_graph);

  auto app = Shared_appearance(new Appearance);
  scene_graph->add_container(app);
  app->set_depth_enable(false);
  app->add_to_scene(scene_graph);

  auto geo = Shared_indexed_line_set(new Indexed_line_set);
  scene_graph->add_container(geo);
  geo->add_to_scene(scene_graph);
  geo->set_num_primitives(polygons.size());
  geo->set_primitive_type(SGAL::Geo_set::PT_LINE_LOOPS);

  // Compute number of coordinates:
  size_t size(0);
  for (const auto& polygon : polygons) size += polygon.size();

  // Set the coordinates:
  Shared_coord_array_3d coords(new Coord_array_3d(size));
  scene_graph->add_container(coords);
  coords->add_to_scene(scene_graph);
  auto cit = coords->begin();
  for (const auto& png : polygons) {
    for (auto vit = png.vertices_begin(); vit != png.vertices_end(); ++vit) {
      auto x = CGAL::to_double(vit->x());
      auto y = CGAL::to_double(vit->y());
      cit->set(x, y, 0);
      ++cit;
    }
  }
  geo->set_coord_array(coords);

  // Set the indices:
  SGAL::Polyline_indices indices(polygons.size());
  auto it = indices.begin();
  size_t i(0);
  for (const auto& png : polygons) {
    it->resize(png.size());
    std::iota(it->begin(), it->end(), i);
    i += png.size();
    ++it;
  }
  geo->set_lines_coord_indices(std::move(indices));

  // Set the color
  Shared_color_array_3d colors(new SGAL::Color_array_3d(size_t(1)));
  (*colors)[0].set(1, 0, 0);
  scene_graph->add_container(colors);
  colors->add_to_scene(scene_graph);
  geo->set_color_array(colors);
  geo->set_color_attachment(SGAL::Geo_set::AT_PER_MESH);

  shape->set_appearance(app);
  shape->set_geometry(geo);
  return shape;
}

SGAL_END_NAMESPACE

#endif
