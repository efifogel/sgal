// Copyright (c) 2021 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SCGAL_TRIANGLE_PLANE_HPP
#define SCGAL_TRIANGLE_PLANE_HPP

#include <boost/graph/graph_traits.hpp>

/*! Compute the plane of a triangular facet.
 */
template <typename Kernel_, typename Polyhedron_>
typename Kernel_::Plane_3
triangle_plane(const Kernel_& kernel,
               typename boost::graph_traits<Polyhedron_>::halfedge_descriptor hd,
               const Polyhedron_& polyhedron)
{
  auto ctr_plane = kernel.construct_plane_3_object();

  const auto& p1 = CGAL::source(hd, polyhedron)->point();
  const auto& p2 = CGAL::target(hd, polyhedron)->point();
  hd = CGAL::next(hd, polyhedron);
  return ctr_plane(p1, p2, CGAL::target(hd, polyhedron)->point());
}

#endif
