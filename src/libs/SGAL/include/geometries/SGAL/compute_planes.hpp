// Copyright (c) 2008 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SCGAL_COMPUTE_PLANES_HPP
#define SCGAL_COMPUTE_PLANES_HPP

/*! \file
 * This file contains a free function that computes the equesions of or the
 * normal to the planes containing the facets of a given polyhedron.
 */

#include <algorithm>
#include <queue>

#include <boost/type_traits.hpp>
#include <boost/graph/graph_traits.hpp>

#include <CGAL/boost/graph/properties.h>

#include "SGAL/basic.hpp"
#include "SGAL/triangle_normal.hpp"
#include "SGAL/triangle_plane.hpp"

SGAL_BEGIN_NAMESPACE

/*! Find a halfedge on the face boundary the target vertex of which is extreme.
 */
template <typename Polyhedron_, typename Comparer_>
typename boost::graph_traits<Polyhedron_>::halfedge_descriptor
find_extreme(typename boost::graph_traits<Polyhedron_>::face_descriptor fd,
             const Polyhedron_& polyhedron, Comparer_ cmp)
{
  auto first = CGAL::halfedge(fd, polyhedron);
  auto range = CGAL::halfedges_around_face(first, polyhedron);
  auto second = range.begin();
  auto extreme_hd = *second++;
  typedef decltype(range)                  Halfedges_around_face_range;
  auto skip_first_range = Halfedges_around_face_range(second, range.end());

  for (auto hd : skip_first_range) {
    auto vd = CGAL::target(hd, polyhedron);
    auto extreme_vd = CGAL::target(extreme_hd, polyhedron);
    if (cmp(vd->point(), extreme_vd->point()) == CGAL::SMALLER) extreme_hd = hd;
  }
  return extreme_hd;
}

/*! A function object that lexicographicallycompares two points firt in the
 * y-dimension then in the z dimension.
 */
template <typename Kernel_>
class Compare_yz_3 {
private:
  typedef Kernel_                       Kernel;
  typedef typename Kernel::Point_3      Point_3;

  //! The geometric kernel
  const Kernel& m_kernel;

public:
  //! Construct
  Compare_yz_3(const Kernel& kernel) : m_kernel(kernel) {}

  //! Compare
  CGAL::Comparison_result operator()(const Point_3& p1, const Point_3& p2)
  {
    auto cmp_y = m_kernel.compare_y_3_object();
    auto cmp_z = m_kernel.compare_z_3_object();
    auto res = cmp_y(p1, p2);
    if (res != CGAL::EQUAL) return res;
    return cmp_z(p1, p2);
  }
};

/*! A function object that lexicographicallycompares two points firt in the
 * z-dimension then in the x dimension.
 */
template <typename Kernel_>
class Compare_zx_3 {
private:
  typedef Kernel_                       Kernel;
  typedef typename Kernel::Point_3      Point_3;

  //! The geometric kernel
  const Kernel& m_kernel;

public:
  //! Construct
  Compare_zx_3(const Kernel& kernel) : m_kernel(kernel) {}

  //! Compare
  CGAL::Comparison_result operator()(const Point_3& p1, const Point_3& p2)
  {
    auto cmp_z = m_kernel.compare_z_3_object();
    auto cmp_x = m_kernel.compare_x_3_object();
    auto res = cmp_z(p1, p2);
    if (res != CGAL::EQUAL) return res;
    return cmp_x(p1, p2);
  }
};

/*! Find a halfedge on the face boundary the target vertex of which is extreme.
 */
template <typename Kernel_, typename Polyhedron_>
typename boost::graph_traits<Polyhedron_>::halfedge_descriptor
find_extreme(const Kernel_& kernel,
             typename boost::graph_traits<Polyhedron_>::face_descriptor fd,
             const Polyhedron_& polyhedron,
             const typename Kernel_::Vector_3& normal)
{
  typedef Polyhedron_                                   Polyhedron;
  typedef Kernel_                                       Kernel;

  auto cmp_xy = kernel.compare_xy_3_object();
  // auto cmp_yz = kernel.compare_yz_3_object();
  Compare_yz_3<Kernel> cmp_yz(kernel);
  // auto cmp_zx = kernel.compare_zx_3_object();
  Compare_zx_3<Kernel> cmp_zx(kernel);

  // Comparing each coordinate with zero is error prone due to round-off errors.
  // Instead, project to the plane orthogonal to the largest component.
  return
    (normal.x() > normal.y()) ?
    ((normal.x() > normal.z()) ?
     find_extreme(fd, polyhedron, cmp_yz) :
     find_extreme(fd, polyhedron, cmp_xy)) :
    ((normal.y() > normal.z()) ?
     find_extreme(fd, polyhedron, cmp_zx) :
     find_extreme(fd, polyhedron, cmp_xy));
}

/*! Compute the plane of a facet.
 */
template <typename Kernel_, typename Polyhedron_>
void compute_plane(const Kernel_& kernel, Polyhedron_& polyhedron,
                   typename boost::graph_traits<Polyhedron_>::
                   face_descriptor fd,
                   boost::false_type, bool is_convex = false)
{
  typedef Polyhedron_                                     Polyhedron;
  typedef Kernel_                                         Kernel;

  typedef boost::graph_traits<Polyhedron>                 Graph_traits;
  typedef typename Graph_traits::halfedge_descriptor      halfedge_descriptor;

  auto collinear = kernel.collinear_3_object();
  auto cross_product = kernel.construct_cross_product_vector_3_object();

  auto first = CGAL::halfedge(fd, polyhedron);
  auto size = CGAL::halfedges_around_face(first, polyhedron).size();
  if (size == 3) {
    fd->plane() = triangle_normal(kernel, first, polyhedron);
    return;
  }

  const auto& p1 = CGAL::target(first, polyhedron)->point();
  auto hd = CGAL::next(first, polyhedron);
  const auto& p2 = CGAL::target(hd, polyhedron)->point();
  hd = CGAL::next(hd, polyhedron);
  while (collinear(p1, p2, CGAL::target(hd, polyhedron)->point())) {
    hd = CGAL::next(hd, polyhedron);
    SGAL_assertion(hd != first);
  }
  auto normal = cross_product(p2 - p1, hd->point() - p2);

  if (is_convex) {
    fd->plane() = normal;
    return;
  }

  // Find an extreme vertex
  auto hd_extreme = find_extreme(kernel, fd, polyhedron, normal);
  SGAL_assertion(hd_extreme != halfedge_descriptor());
  fd->plane() = triangle_normal(kernel, hd_extreme, polyhedron);
}

/*! Compute the planes of all facets.
 */
template <typename Kernel_, typename Polyhedron_>
void compute_planes(const Kernel_& kernel, Polyhedron_& polyhedron,
                    boost::false_type, bool is_convex = false)
{
  for (auto fd : CGAL::faces(polyhedron))
    compute_plane(kernel, polyhedron, fd, boost::false_type(), is_convex);
}

/*! Compute the plane of a facet.
 */
template <typename Kernel_, typename Polyhedron_>
void compute_plane(const Kernel_& kernel, Polyhedron_& polyhedron,
                   typename boost::graph_traits<Polyhedron_>::face_descriptor fd,
                   boost::true_type, bool is_convex = false)
{
  typedef Polyhedron_                                     Polyhedron;
  typedef Kernel_                                         Kernel;

  typedef boost::graph_traits<Polyhedron>                 Graph_traits;
  typedef typename Graph_traits::halfedge_descriptor      halfedge_descriptor;

  auto collinear = kernel.collinear_3_object();
  auto ctr_plane = kernel.construct_plane_3_object();

  auto first = CGAL::halfedge(fd, polyhedron);
  auto size = CGAL::halfedges_around_face(first, polyhedron).size();
  // std::cout << "size: " << size << std::endl;
  if (size == 3) {
    fd->plane() = triangle_plane(kernel, first, polyhedron);
    // std::cout << "1 normal: " << fd->plane().orthogonal_vector() << std::endl;
    return;
  }

  const auto& p1 = CGAL::target(first, polyhedron)->point();
  auto hd = CGAL::next(first, polyhedron);
  const auto& p2 = CGAL::target(hd, polyhedron)->point();
  hd = CGAL::next(hd, polyhedron);
  while (collinear(p1, p2, CGAL::target(hd, polyhedron)->point())) {
    hd = CGAL::next(hd, polyhedron);
    if (hd == first) {
      std::cerr << "All vertices are collinear\n";
      fd->plane() = typename Kernel::Plane_3(0, 0, 0, 0);
      // std::cout << "2 normal: " << fd->plane().orthogonal_vector() << std::endl;
      return;
    }
  }
  auto plane = ctr_plane(p1, p2, CGAL::target(hd, polyhedron)->point());

  if (is_convex) {
    fd->plane() = plane;
    // std::cout << "3 normal: " << fd->plane().orthogonal_vector() << std::endl;
    return;
  }

  // Find an extreme vertex
  auto extereme_hd =
    find_extreme(kernel, fd, polyhedron, plane.orthogonal_vector());
  SGAL_assertion(extereme_hd != halfedge_descriptor());
  fd->plane() = triangle_plane(kernel, extereme_hd, polyhedron);
  // std::cout << "4 normal: " << fd->plane().orthogonal_vector() << std::endl;
}

/*! Compute the planes of all facets.
 */
template <typename Kernel_, typename Polyhedron_>
void compute_planes(const Kernel_& kernel, Polyhedron_& polyhedron,
                    boost::true_type, bool is_convex = false)
{
  // std::cout << "polyhedron sizes: " << polyhedron.size_of_vertices()
  //           << ", " << polyhedron.size_of_halfedges()
  //           << ", " << polyhedron.size_of_facets()
  //           << std::endl;
  for (auto fd : CGAL::faces(polyhedron))
    compute_plane(kernel, polyhedron, fd, boost::true_type(), is_convex);
}

// template <typename Polyhedron>
// void print_face(typename boost::graph_traits<Polyhedron>::face_descriptor fd,
//                 const Polyhedron& polyhedron,
//                 const std::string& name)
// {
//   std::cout << "FACE " << name << std::endl;
//   auto rep_hd = CGAL::halfedge(fd, polyhedron);
//   for (auto hd : CGAL::halfedges_around_face(rep_hd, polyhedron)) {
//     std::cout << CGAL::target(hd, polyhedron)->point() << std::endl;
//   }
// }

SGAL_END_NAMESPACE

#endif
