// Copyright (c) 2008 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s) : Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_MERGE_COPLANAR_FACETS_HPP
#define SGAL_MERGE_COPLANAR_FACETS_HPP

/*! \file
 * This file contains a few function that accepts a polyhedron. It merges
 * cooplanar facets.
 */

#include <list>
#include <unordered_map>

#include <boost/type_traits.hpp>

#include <CGAL/Union_find.h>

#include "SGAL/basic.hpp"
#include "SGAL/Merge_coplanar_facets_visitor.hpp"

SGAL_BEGIN_NAMESPACE

template <typename FaceHandle, typename HandleMap>
typename CGAL::Union_find<FaceHandle>::handle
uf_get_handle(FaceHandle f, CGAL::Union_find<FaceHandle>& uf_faces,
              HandleMap& handles) {
  typedef FaceHandle                    Face_handle;
  typedef HandleMap                     Handle_map;
  typedef CGAL::Union_find<Face_handle> Uf_faces;
  typedef typename Uf_faces::handle     Uf_handle;

  std::pair<typename Handle_map::iterator, bool> insert_res =
    handles.emplace(f, Uf_handle());
  if (insert_res.second)
    insert_res.first->second = uf_faces.make_set(f);
  return insert_res.first->second;
}

template <typename FaceHandle, typename HandleMap>
bool uf_join_faces(FaceHandle f1, FaceHandle f2,
                   CGAL::Union_find<FaceHandle>& uf_faces,
                   HandleMap& handles) {
  typedef FaceHandle                    Face_handle;
  typedef HandleMap                     Handle_map;
  typedef CGAL::Union_find<Face_handle> Uf_faces;
  typedef typename Uf_faces::handle     Uf_handle;

  Uf_handle h1 = uf_get_handle(f1, uf_faces, handles);
  Uf_handle h2 = uf_get_handle(f2, uf_faces, handles);
  bool same_set = uf_faces.same_set(h1, h2);
  if (! same_set) uf_faces.unify_sets(h1, h2);
  return ! same_set;
}

/*! Merge coplanar facets.
 */
template <typename Polyhedron, typename Equal, typename Visitor>
void merge_coplanar_facets_impl(Polyhedron& polyhedron, Equal& eq, Visitor& vis)
{
  typedef typename Polyhedron::Halfedge_handle          Halfedge_handle;
  typedef typename Polyhedron::Face_handle              Face_handle;

  typedef CGAL::Union_find<Face_handle>                 Uf_faces;
  Uf_faces uf_faces;

  typedef std::unordered_map<Face_handle, typename Uf_faces::handle> Uf_handles;
  Uf_handles uf_handles;

  typedef std::list<Halfedge_handle>                    Edge_container;
  Edge_container edges;

  // Group coplanar facets in subsets
  for (auto it = polyhedron.edges_begin(); it != polyhedron.edges_end(); ++it) {
    auto ohe = it->opposite();
    if ((eq(it->facet()->plane(), ohe->facet()->plane())) &&
        (it->facet() != ohe->facet()))
      if (uf_join_faces(it->facet(), ohe->facet(), uf_faces, uf_handles))
        edges.emplace_back(it);
  }

  // std::cout << "No coplanar facets: " << uf_handles.size() << std::endl;
  // std::cout << "No sets: " << uf_faces.number_of_sets() << std::endl;

  // Traverse cooplanar edges and remove them.
  for (auto it = edges.begin(); it != edges.end(); ++it) {
    auto he = *it;
    auto ohe = he->opposite();
    vis.before_merge(he->facet(), ohe->facet());
    auto pred_he = polyhedron.join_facet(he);
    vis.after_merge(pred_he->facet());
  }

  // Traverse all vertices and remove antenas.
  bool done;
  do {
    done = true;
    for (auto it = polyhedron.vertices_begin(); it != polyhedron.vertices_end();
         ++it)
    {
      if (it->halfedge() == Halfedge_handle()) continue;  // bug in CGAL
      if (it->vertex_degree() != 1) continue;
      auto he = it->vertex_begin();
      polyhedron.erase_center_vertex(he);
      done = false;
    }
  } while (! done);

  // Traverse all vertices and remove vertices of degree 2.
  for (auto it = polyhedron.vertices_begin(); it != polyhedron.vertices_end();
         ++it)
  {
    if (it->halfedge() == Halfedge_handle()) continue;  // bug in CGAL
    if (it->vertex_degree() != 2) continue;
    auto he = it->vertex_begin();
    auto ohe = he->opposite();
    polyhedron.join_vertex(ohe);
  }
}

template <typename Kernel>
struct Direction_equal {
  Kernel& m_kernel;
  Direction_equal(Kernel& kernel) : m_kernel(kernel) {}
  template <typename Vector>
  bool operator()(Vector& v1, Vector& v2) {
    typename Kernel::Equal_3 eq = m_kernel.equal_3_object();
    return eq(v1.direction(), v2.direction());
  }
};

/*! Merge coplanar facets.
 */
template <typename Kernel, typename Polyhedron, typename Visitor>
void merge_coplanar_facets_impl(Kernel& kernel, Polyhedron& polyhedron,
                                Visitor& vis, boost::false_type) {
  Direction_equal<Kernel> eq(kernel);
  merge_coplanar_facets_impl(polyhedron, eq, vis);
}

/*! Merge coplanar facets.
 */
template <typename Kernel, typename Polyhedron, typename Visitor>
void merge_coplanar_facets_impl(Kernel& kernel, Polyhedron& polyhedron,
                                Visitor& vis, boost::true_type) {
  typename Kernel::Equal_3 eq = kernel.equal_3_object();
  merge_coplanar_facets_impl(polyhedron, eq,  vis);
}

/*! Merge coplanar facets.
 */
template <typename Kernel, typename Polyhedron>
void merge_coplanar_facets(Kernel& kernel, Polyhedron& polyhedron,
                           boost::false_type) {
  Direction_equal<Kernel> eq(kernel);
  SGAL::Merge_coplanar_facets_visitor<Polyhedron> vis;
  merge_coplanar_facets_impl(polyhedron, eq, vis);
}

/*! Merge coplanar facets.
 */
template <typename Kernel, typename Polyhedron>
void merge_coplanar_facets(Kernel& kernel, Polyhedron& polyhedron,
                           boost::true_type) {
  typename Kernel::Equal_3 eq = kernel.equal_3_object();
  SGAL::Merge_coplanar_facets_visitor<Polyhedron> vis;
  merge_coplanar_facets_impl(polyhedron, eq,  vis);
}

/*! Merge coplanar facets.
 */
template <typename Kernel, typename Polyhedron, typename Visitor>
void merge_coplanar_facets(Kernel& kernel, Polyhedron& polyhedron, Visitor& vis)
{
  typedef typename Kernel::Plane_3                    Ker_plane;
  typedef typename Polyhedron::Plane_3                Pol_plane;
  typedef boost::is_same<Pol_plane, Ker_plane>        Polyhedron_has_plane;
  merge_coplanar_facets_impl(kernel, polyhedron, vis, Polyhedron_has_plane());
}

/*! Merge coplanar facets.
 */
template <typename Kernel, typename Polyhedron>
void merge_coplanar_facets(Kernel& kernel, Polyhedron& polyhedron) {
  typedef typename Kernel::Plane_3                    Ker_plane;
  typedef typename Polyhedron::Plane_3                Pol_plane;
  typedef boost::is_same<Pol_plane, Ker_plane>        Polyhedron_has_plane;
  SGAL::Merge_coplanar_facets_visitor<Polyhedron> vis;
  merge_coplanar_facets_impl(kernel, polyhedron, vis, Polyhedron_has_plane());
}

SGAL_END_NAMESPACE

#endif
