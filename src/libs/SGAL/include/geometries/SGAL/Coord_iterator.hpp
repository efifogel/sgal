// Copyright (c) 2019 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include "SGAL/basic.hpp"

#ifndef SGAL_COORD_ITERATOR_HPP
#define SGAL_COORD_ITERATOR_HPP

SGAL_BEGIN_NAMESPACE

template <typename SharedCoords, typename Iterator_, typename ValueType>
class Coord_iterator {
private:
  typedef SharedCoords          Shared_coords;
  typedef Iterator_             Iterator;
  typedef ValueType             Value_type;

  SharedCoords m_coords;
  Iterator m_it;

public:
  typedef typename Iterator::iterator_category       iterator_category;
  typedef Value_type                                 value_type;
  typedef typename Iterator::difference_type         difference_type;
  typedef Value_type*                                pointer;
  typedef Value_type&                                reference;

public:
  /*! Construct default. */
  Coord_iterator(Shared_coords coords) :
    m_coords(coords),
    m_it()
  {}

  /*! Construct. */
  Coord_iterator(Shared_coords coords, Iterator it) :
    m_coords(coords),
    m_it(it)
  {}

  /*! operator* */
  const value_type operator*() const { return (*m_coords)[*m_it]; }
  value_type operator*() { return (*m_coords)[*m_it]; }

  /*! operator pre ++ */
  Coord_iterator& operator++()
  {
    ++m_it;
    return *this;
  }

  /*! operator post ++ */
  Coord_iterator operator++(int)
  {
    Coord_iterator tmp = *this;
    ++*this;
    return tmp;
  }

  /*! operator-> */
  pointer operator->() { return &((*m_coords)[*m_it]); }
  const pointer operator->() const { return &((*m_coords)[*m_it]); }

  /*! Comparison operators. */
  bool operator==(Coord_iterator other) { return m_it == other.m_it; }
  bool operator!=(Coord_iterator other) { return m_it != other.m_it; }
};

SGAL_END_NAMESPACE

#endif
