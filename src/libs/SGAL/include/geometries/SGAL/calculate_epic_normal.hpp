// Copyright (c) 2019 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_CALCULATE_EPIC_NORMAL_HPP
#define SGAL_CALCULATE_EPIC_NORMAL_HPP

#include "SGAL/basic.hpp"
#include "SGAL/Vector3f.hpp"
#include "SGAL/calculate_normal.hpp"

SGAL_BEGIN_NAMESPACE

class Coord_array_3d;
class Epic_coord_array_3d;

/*! Calculate the epic normal of a polygonal facet.
 * Observe that if the middle vertex used to calculate the normal (that is,
 * v1) is a reflex vertex, the calculated normal is opposite of the tru normal.
 * Also, the calculation uses limited precision.Therefore, it may fail. If
 * it fails, consider switching to unlimited precision.
 */
template <typename Poly_, typename Kernel>
bool calculate_epic_normal(const Poly_& facet,
                           const boost::shared_ptr<Coord_array_3d> coords,
                           Epic_vector_3& normal, bool is_ccw,
                           const Kernel& kernel)
{
  Vector3f tmp_normal;
  auto res = calculate_normal(facet, *coords, tmp_normal, is_ccw);
  if (! res) return res;
  normal = Epic_vector_3(tmp_normal[0], tmp_normal[1], tmp_normal[2]);
  return true;
}

/*! Calculate the epic normal of a polygonal facet.
 * Observe that if the middle vertex used to calculate the normal (that is,
 * v1) is a reflex vertex, the calculated normal is opposite of the tru normal.
 */
template <typename Poly_, typename Kernel>
bool calculate_epic_normal(const Poly_& facet,
                           const boost::shared_ptr<Epic_coord_array_3d> coords,
                           Epic_vector_3& normal, bool is_ccw,
                           const Kernel& kernel)
{
  const auto& p0 = (*coords)[facet[0]];
  const auto& p1 = (*coords)[facet[1]];
  for (size_t j = 2; j < facet.size(); ++j) {
    const auto& p2 = (*coords)[facet[j]];
    if (kernel.collinear_3_object()(p0, p1, p2)) continue;
    normal = (is_ccw) ?
      kernel.construct_normal_3_object()(p0, p1, p2) :
      kernel.construct_normal_3_object()(p2, p1, p0);
    return true;
  }
  return false;
}

SGAL_END_NAMESPACE

#endif
