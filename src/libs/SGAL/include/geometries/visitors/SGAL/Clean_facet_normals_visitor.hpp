// Copyright (c) 2016 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_CLEAN_FACET_NORMALS_VISITOR_HPP
#define SGAL_CLEAN_FACET_NORMALS_VISITOR_HPP

#include <boost/variant.hpp>
#include <boost/unordered_map.hpp>
#include <boost/type_traits.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/property_map/property_map.hpp>

#include <CGAL/Kernel_traits.h>
#include <CGAL/Polygon_mesh_processing/compute_normal.h>

#include "SGAL/basic.hpp"
#include "SGAL/Inexact_kernel.hpp"
#include "SGAL/Epec_kernel.hpp"
#include "SGAL/Epic_kernel.hpp"

// namespace pmp = CGAL::Polygon_mesh_processing;

SGAL_BEGIN_NAMESPACE

/*! Clean Polyhedron facets visitor. */
class Clean_facet_normals_visitor : public boost::static_visitor<> {
public:
  /*! Construct default. */
  Clean_facet_normals_visitor(bool flag = false) : m_is_triangular(flag) {}

  /*! Operate on a polyhedron that is not inexact.
   */
  template <typename Polyhedron_>
  void operator()(Polyhedron_& polyhedron) const
  {
    typedef Polyhedron_                                     Polyhedron;

    typedef typename Polyhedron::Point_3                    Point;
    typedef typename CGAL::Kernel_traits<Point>::Kernel     Kernel;
    Kernel kernel;
    this->compute(polyhedron, kernel);
  }

  /*! Operate on an inexact polyhedron.
   */
  void operator()(Inexact_polyhedron& polyhedron) const
  {
    Inexact_kernel kernel;
    std::transform(polyhedron.facets_begin(), polyhedron.facets_end(),
                   polyhedron.planes_begin(),
                   Facet_normal_calculator<Inexact_kernel>(kernel,
                                                           m_is_triangular));
  }

private:
  //! Indicates whether the polyhedron is a triangular mesh.
  bool m_is_triangular;

  /*! Apply the merge.
   */
  template <typename Polyhedron_, typename Kernel_>
  void compute(Polyhedron_& polyhedron, const Kernel_& kernel) const
  {
    typedef Polyhedron_                                     Polyhedron;
    typedef Kernel_                                         Kernel;

#if 0
    typedef boost::graph_traits<Polyhedron>                 Graph_traits;
    typedef typename Graph_traits::face_descriptor          face_descriptor;
    typedef typename Kernel::Vector_3                       Vector_3;
    typedef boost::unordered_map<face_descriptor, Vector_3> Normal_map;

    Normal_map normals;
    boost::associative_property_map<Normal_map> fnm(normals);
    pmp::compute_face_normals(polyhedron, fnm);

    std::cout << "# normals:" << normals.size() << std::endl;
    for (face_descriptor fd : CGAL::faces(polyhedron)) {
      std::cout << normals[fd] << std::endl;
      fd->set_normal(to_vector3f(normals[fd]));
    }
#endif

    // Compute the normal used only for drawing the polyhedron
    std::for_each(polyhedron.facets_begin(), polyhedron.facets_end(),
                  Plane_to_normal());

    // Convert the exact points to approximate used for drawing the polyhedron
    // std::for_each(polyhedron.vertices_begin(), polyhedron.vertices_end(),
    //               Point_to_vector());
  }

  /*! A functor that calculates the normal of a given facet. */
  template <typename Kernel>
  struct Facet_normal_calculator {
    const Kernel& m_kernel;
    bool m_is_triangular;

    Facet_normal_calculator(const Kernel& kernel,
                            bool is_triangular = false) :
      m_kernel(kernel),
      m_is_triangular(is_triangular)
    {}

    template <typename Facet>
    typename Facet::Plane_3 operator()(Facet& f) {
      typename Facet::Halfedge_handle h = f.halfedge();
      typename Facet::Vertex::Point_3& p1 = h->vertex()->point();
      typename Facet::Vertex::Point_3& p2 = h->next()->vertex()->point();
      h = h->next()->next();
      auto cross_product = m_kernel.construct_cross_product_vector_3_object();

      if (! m_is_triangular) {
        auto collinear = m_kernel.collinear_3_object();
        while (collinear(p1, p2, h->vertex()->point())) h = h->next();
      }

      Inexact_vector_3 normal =
        cross_product(p2 - p1, h->vertex()->point() - p2);
      return normal / CGAL::sqrt(normal.squared_length());
    }
  };

  /*! Convert Plane_3 to normal in Vector3f representation. */
  struct Plane_to_normal {
    template <typename Facet>
    void operator()(Facet& facet) {
      auto normal = to_vector3f(facet.plane().orthogonal_vector());
      normal.normalize();
      facet.set_normal(normal);
    }
  };
};

SGAL_END_NAMESPACE

#endif
