// Copyright (c) 2019 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_POLYHEDRON_FACETS_TRIANGULATOR_VISITOR_HPP
#define SGAL_POLYHEDRON_FACETS_TRIANGULATOR_VISITOR_HPP

#include <vector>
#include <list>

#include <boost/type_traits.hpp>
#include <boost/variant.hpp>

#include <CGAL/Kernel_traits.h>

#include "SGAL/basic.hpp"
#include "SGAL/Inexact_polyhedron.hpp"

SGAL_BEGIN_NAMESPACE

/*! A visitor that counts the number of polyhedron halfedges. */
class Polyhedron_facet_triangulator_visitor : public boost::static_visitor<> {
public:
  /*! Triangulate a given polyhedron.
   */
  template <typename Polyhedron>
  void operator()(Polyhedron& polyhedron) const
  { triangulate(polyhedron); }

  /*! Triangulate an inexact polyhedron.
   */
  void operator()(Inexact_polyhedron& polyhedron) const {
    std::cerr << "Triangulating an inexact polyhedron\n";
    triangulate(polyhedron);
  }

private:
  template <typename Plane, typename Kernel>
  bool
  is_valid(const Plane& plane, const Kernel& kernel, boost::false_type) const {
    auto eql = kernel.equal_3_object();
    Plane empty_plane;
    return ! eql(plane, empty_plane);
  }

  template <typename Plane, typename Kernel>
  bool
  is_valid(const Plane& plane, const Kernel& kernel, boost::true_type) const
  { return is_valid(plane.orthogonal_vector(), kernel, boost::false_type()); }

  /*! Triangulate a given polyhedron.
   */
  template <typename Polyhedron>
  void triangulate(Polyhedron& polyhedron) const {
    // There is a bug in CGAL related to the triangulation of the facets of
    // a face graph. Thus, we use a workaround.
    typedef boost::graph_traits<Polyhedron>               Graph_traits;
    typedef typename Graph_traits::face_descriptor        face_descriptor;

    typedef typename Polyhedron::Point_3                  Point_3;
    typedef typename CGAL::Kernel_traits<Point_3>::Kernel Kernel;
    typedef typename Kernel::Plane_3                      Ker_plane;
    typedef typename Polyhedron::Plane_3                  Pol_plane;
    typedef boost::is_same<Pol_plane, Ker_plane>          Polyhedron_has_plane;

    Kernel kernel;

    // Store facet handles, because the collection of the polyhedron facets
    // is modified during the loop, which invalidates the facet range.
    auto face_range = CGAL::faces(polyhedron);
    std::vector<face_descriptor> facets;
    facets.reserve(std::distance(boost::begin(face_range),
                                 boost::end(face_range)));

    // Filter out triangular faces
    for (auto fd : face_range) {
      auto hd = halfedge(fd, polyhedron);
      // Skip facets that have less than 3 vertices and degenerate facets
      typedef typename Polyhedron::Plane_3      Plane_3;
      auto size = CGAL::halfedges_around_face(hd, polyhedron).size();
      if ((size > 3) && is_valid(fd->plane(), kernel, Polyhedron_has_plane()))
        facets.push_back(fd);
    }

    // Iterates on the vector of face descriptors
    for (auto fd : facets) {
      auto hd = halfedge(fd, polyhedron);
      auto size = CGAL::halfedges_around_face(hd, polyhedron).size();
      // std::cout << "Size: " << size << std::endl;
      if (size == 4) {
        this->triangulate_quad(fd, polyhedron);
        continue;
      }
      this->triangulate_face(fd, polyhedron);
    }
  }

  /*! Triangulate a face of size at least 5.
   */
  template <typename Polyhedron_>
  void triangulate_quad(typename boost::graph_traits<Polyhedron_>::
                        face_descriptor fd,
                        Polyhedron_& polyhedron) const
  {
    typedef Polyhedron_                                     Polyhedron;

    auto h0 = CGAL::halfedge(fd, polyhedron);
    auto h1 = CGAL::next(h0, polyhedron);
    auto h2 = CGAL::next(h1, polyhedron);
    auto h3 = CGAL::next(h2, polyhedron);
    const auto& p0 = CGAL::target(h0, polyhedron)->point();
    const auto& p1 = CGAL::target(h1, polyhedron)->point();
    const auto& p2 = CGAL::target(h2, polyhedron)->point();
    const auto& p3 = CGAL::target(h3, polyhedron)->point();

    /* Chooses the diagonal that will split the quad in two triangles that
     * maximize the scalar product of of the unnormalized normals of the two
     * triangles.
     * The lengths of the unnormalized normals (computed using
     * cross-products of two vectors) are proportional to the area of the
     * triangles. Maximize the scalar product of the two normals will avoid
     * skinny triangles, and will also take into account the cosine of the
     * angle between the two normals.
     * In particular, if the two triangles are oriented in different
     * directions, the scalar product will be negative.
     */
    auto p21 = p2 - p1;
    auto p12 = p1 - p2;
    auto p32 = p3 - p2;
    auto p03 = p0 - p3;
    auto p30 = p3 - p0;
    auto p10 = p1 - p0;
    auto p1p3 = CGAL::cross_product(p21, p32) * CGAL::cross_product(p03, p10);
    auto p0p2 = CGAL::cross_product(p10, p12) * CGAL::cross_product(p32, p30);
    if(p0p2 > p1p3) CGAL::Euler::split_face(h0, h2, polyhedron);
    else CGAL::Euler::split_face(h1, h3, polyhedron);
  }

  //! The extention of the face type in the triangulation
  template <typename Polyhedron_>
  struct Face_info {
    typedef Polyhedron_                                   Polyhedron;

    typename boost::graph_traits<Polyhedron>::halfedge_descriptor e[3];
    int m_nesting_level;
    Boolean in_domain() { return m_nesting_level % 2 == 1; }
    // bool is_external;
  };

  /*! Mark facets in a triangulation that are inside the domain bounded by
   * the polygon.
   * \param tri (in/out) the triangulation.
   */
  template <typename Triangulation_>
  void mark_domains(Triangulation_& tri,
                    typename Triangulation_::Face_handle start,int index,
                    std::list<typename Triangulation_::Edge>& border) const
  {
    typedef Triangulation_                Triangulation;

    if (start->info().m_nesting_level != -1) return;
    std::list<typename Triangulation::Face_handle> queue;
    queue.push_back(start);
    while (! queue.empty()) {
      auto fh = queue.front();
      queue.pop_front();
      if (fh->info().m_nesting_level == -1) {
        fh->info().m_nesting_level = index;
        for (auto i = 0; i < 3; ++i) {
          typename Triangulation::Edge e(fh,i);
          auto n = fh->neighbor(i);
          if (n->info().m_nesting_level == -1) {
            if (tri.is_constrained(e)) border.push_back(e);
            else queue.push_back(n);
          }
        }
      }
    }
  }

  /*! \brief marks facets in a triangulation that are inside the domain.
   * Explores set of facets connected with non constrained edges,
   * and attribute to each such set a nesting level.
   * We start from facets incident to the infinite vertex, with a nesting
   * level of 0. Then we recursively consider the non-explored facets incident
   * to constrained edges bounding the former set and increase the nesting
   * level by 1.
   * Facets in the domain are those with an odd nesting level.
   */
  template <typename Triangulation_>
  void mark_domains(Triangulation_& tri) const
  {
    typedef Triangulation_                Triangulation;

    for (auto it = tri.all_faces_begin(); it != tri.all_faces_end(); ++it)
      it->info().m_nesting_level = -1;

    std::list<typename Triangulation::Edge> border;
    mark_domains(tri, tri.infinite_face(), 0, border);
    while (! border.empty()) {
      auto e = border.front();
      border.pop_front();
      auto n = e.first->neighbor(e.second);
      if (n->info().m_nesting_level == -1)
        mark_domains(tri, n, e.first->info().m_nesting_level+1, border);
    }
  }

  /*! Construct a constrained triangulation
   */
  template <typename Polyhedron_, typename Triangulation_>
  bool construct_triangulation(typename boost::graph_traits<Polyhedron_>::
                               face_descriptor fd,
                               Polyhedron_& polyhedron,
                               Triangulation_& tri) const
  {
    typedef Polyhedron_                                  Polyhedron;
    typedef boost::graph_traits<Polyhedron>              Graph_traits;
    typedef typename Graph_traits::vertex_descriptor     vertex_descriptor;
    typedef typename Graph_traits::halfedge_descriptor   halfedge_descriptor;

    bool res(true);
    auto hd = halfedge(fd, polyhedron);
    auto range = CGAL::halfedges_around_face(hd, polyhedron);
    auto range_begin = range.begin();
    auto first_hd = *range_begin;
    auto first_vd = CGAL::target(first_hd, polyhedron);
    auto start = tri.insert(first_vd->point());
    start->info() = first_hd;
    auto prev = start;

    halfedge_descriptor null_hd;
    typedef decltype(range) Range;
    ++range_begin;
    auto skip_first_range = Range(range_begin, range.end());
    for (auto hd : skip_first_range) {
      auto vd = CGAL::target(hd, polyhedron);
      auto next = tri.insert(vd->point());
      next->info() = (next->info() == null_hd) ? hd : null_hd;
      if (next->info() == null_hd) res = false;
      tri.insert_constraint(prev, next);
      prev = next;
    }
    tri.insert_constraint(prev, start);
    return res;
  }

  /*! Mark the halfedges on the boundary of a facet as border halfedges.
   */
  template <typename Polyhedron_>
  void make_hole(typename boost::graph_traits<Polyhedron_>::
                 halfedge_descriptor first_hd,
                 Polyhedron_& polyhedron) const
  {
    //we are not using Euler::make_hole because it has a precondition
    //that the hole is not made on the boundary of the mesh
    //here we allow making a hole on the boundary, and the pair(s) of
    //halfedges that become border-border are fixed by the connectivity
    //setting made in operator()
    SGAL_assertion(! CGAL::is_border(first_hd, polyhedron));
    auto fd = CGAL::face(first_hd, polyhedron);
    for (auto hd : CGAL::halfedges_around_face(first_hd, polyhedron))
      CGAL::internal::set_border(hd, polyhedron);
    remove_face(fd, polyhedron);
  }

  /*!
   */
  template <typename FaceHandle>
  bool is_external(FaceHandle fh) const { return ! fh->info().in_domain(); }

  /*! Obtain the normal to a facet
   */
  template <typename Polyhedron_>
  typename CGAL::Kernel_traits<typename Polyhedron_::Point_3>::Kernel::Vector_3
  get_normal(typename boost::graph_traits<Polyhedron_>::face_descriptor fd,
             boost::false_type) const
  { return fd->plane(); }

  /*! Obtain the normal to a facet
   */
  template <typename Polyhedron_>
  typename CGAL::Kernel_traits<typename Polyhedron_::Point_3>::Kernel::Vector_3
  get_normal(typename boost::graph_traits<Polyhedron_>::face_descriptor fd,
             boost::true_type) const
  { return fd->plane().orthogonal_vector(); }

  /*! Triangulate a face of size at least 5.
   */
  template <typename Polyhedron_>
  void triangulate_face(typename boost::graph_traits<Polyhedron_>::
                        face_descriptor fd,
                        Polyhedron_& polyhedron) const
  {
    typedef Polyhedron_                                     Polyhedron;

    typedef boost::graph_traits<Polyhedron>                 Graph_traits;
    typedef typename Graph_traits::vertex_descriptor        vertex_descriptor;
    typedef typename Graph_traits::halfedge_descriptor      halfedge_descriptor;
    typedef typename Graph_traits::face_descriptor          face_descriptor;

    typedef typename Polyhedron::Point_3                    Point;
    typedef typename CGAL::Kernel_traits<Point>::Kernel     Kernel;
    typedef typename Kernel::Plane_3                        Ker_plane;
    typedef typename Polyhedron::Plane_3                    Pol_plane;
    typedef boost::is_same<Pol_plane, Ker_plane>            Polyhedron_has_plane;

    typedef CGAL::Projection_traits_3<Kernel>                        Traits;
    typedef typename Graph_traits::halfedge_descriptor               Vi;
    typedef CGAL::Triangulation_vertex_base_with_info_2<Vi, Traits>  Vb;
    typedef Face_info<Polyhedron>                                    Fi;
    typedef CGAL::Triangulation_face_base_with_info_2<Fi, Traits>    Fbi;
    typedef CGAL::Constrained_triangulation_face_base_2<Traits, Fbi> Fb;
    typedef CGAL::Triangulation_data_structure_2<Vb, Fb>             Tds;
    // typedef CGAL::No_intersection_tag                             Itag;
    typedef CGAL::Exact_predicates_tag                               Itag;
    typedef CGAL::Constrained_Delaunay_triangulation_2<Traits, Tds, Itag> CDT;

    const auto& normal = get_normal<Polyhedron>(fd, Polyhedron_has_plane());
    Traits cdt_traits(normal);
    CDT cdt(cdt_traits);
    auto gp = this->construct_triangulation(fd, polyhedron, cdt);
    this->mark_domains(cdt);

    // If the facet is not in general position, simply split along one of the
    // edges induced by the triangulation, and recursively triangulate the two
    // resulting facets.
    if (! gp) {
      halfedge_descriptor null_hd;
      halfedge_descriptor hd;
      for (auto eit = cdt.finite_edges_begin(); eit != cdt.finite_edges_end();
           ++eit)
      {
        if (cdt.is_constrained(*eit)) continue;

        auto fh = eit->first;
        auto index = eit->second;
        auto fh_opp = fh->neighbor(index);
        if (this->is_external(fh) || this->is_external(fh_opp)) continue;

        const auto vh_a = fh->vertex(cdt.cw(index));
        const auto vh_b = fh->vertex(cdt.ccw(index));
        if ((vh_a->info() == null_hd) || (vh_b->info() == null_hd)) continue;

        hd = CGAL::Euler::split_face(vh_a->info(), vh_b->info(), polyhedron);
        break;
      }
      cdt.clear();

      auto hd_opp = CGAL::opposite(hd, polyhedron);
      auto fd1 = CGAL::face(hd, polyhedron);
      auto fd2 = CGAL::face(hd_opp, polyhedron);

      auto size = CGAL::halfedges_around_face(hd, polyhedron).size();
      auto size_opp = CGAL::halfedges_around_face(hd_opp, polyhedron).size();

      fd1->plane() = fd->plane();
      if (size == 4) this->triangulate_quad(fd1, polyhedron);
      else if (size > 4) this->triangulate_face(fd1, polyhedron);

      fd2->plane() = fd->plane();
      if (size_opp == 4) this->triangulate_quad(fd2, polyhedron);
      else if (size_opp > 4) this->triangulate_face(fd2, polyhedron);

      return;
    }

    auto hd = CGAL::halfedge(fd, polyhedron);
    this->make_hole(hd, polyhedron);

    halfedge_descriptor null_hd;
    for (auto eit = cdt.finite_edges_begin(), end = cdt.finite_edges_end();
         eit != end; ++eit)
    {
      auto fh = eit->first;
      auto index = eit->second;
      auto opposite_fh = fh->neighbor(index);
      auto opposite_index = opposite_fh->index(fh);

      const auto vh_a = fh->vertex(cdt.cw(index));
      const auto vh_b = fh->vertex(cdt.ccw(index));
      if ((vh_a->info() == null_hd) || (vh_b->info() == null_hd)) continue;

      //not both fh are external and edge is not constrained
      if ( ! (this->is_external(fh) && this->is_external(opposite_fh)) &&
           ! cdt.is_constrained(*eit))
      {
        // strictly internal edge
        auto hd_new = CGAL::halfedge(add_edge(polyhedron), polyhedron);
        auto hd_opp_new = CGAL::opposite(hd_new, polyhedron);
        fh->info().e[index] = hd_new;
        opposite_fh->info().e[opposite_index] = hd_opp_new;
        auto hd_a = vh_a->info();
        CGAL::set_target(hd_new, CGAL::target(hd_a, polyhedron), polyhedron);
        auto hd_b = vh_b->info();
        CGAL::set_target(hd_opp_new, CGAL::target(hd_b, polyhedron), polyhedron);
      }

      if (cdt.is_constrained(*eit)) {
        //edge is constrained
        if (! this->is_external(fh)) fh->info().e[index] = vh_a->info();
        if (! this->is_external(opposite_fh))
          opposite_fh->info().e[opposite_index] = vh_b->info();
      }
    }

    for (auto fit = cdt.finite_faces_begin(), end = cdt.finite_faces_end();
         fit != end; ++fit)
    {
      if (is_external(fit)) continue;

      halfedge_descriptor hd0 = fit->info().e[0];
      halfedge_descriptor hd1 = fit->info().e[1];
      halfedge_descriptor hd2 = fit->info().e[2];
      if ((hd0 == null_hd) || (hd1 == null_hd) || (hd2 == null_hd)) {
        std::cerr << "Ignoring a self-intersecting facet\n";
        continue;
      }
      SGAL_assertion(hd0 != null_hd);
      SGAL_assertion(hd1 != null_hd);
      SGAL_assertion(hd2 != null_hd);

      CGAL::set_next(hd0, hd1, polyhedron);
      CGAL::set_next(hd1, hd2, polyhedron);
      CGAL::set_next(hd2, hd0, polyhedron);

      CGAL::Euler::fill_hole(hd0, polyhedron);
    }
    cdt.clear();
  }
};

SGAL_END_NAMESPACE

#endif
