// Copyright (c) 2020 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

/*! \file
 * A visitor that computes the equesions of, or the unnormalized normal to, the
 * planes containing the facets of a given polyhedron. Observe that an inexact
 * polyhedron does not have either.
 */

#ifndef SGAL_CLEAN_PLANES_VISITOR_HPP
#define SGAL_CLEAN_PLANES_VISITOR_HPP

#include <boost/variant.hpp>
#include <boost/type_traits.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/property_map/property_map.hpp>

#include <CGAL/Kernel_traits.h>

#include "SGAL/basic.hpp"
#include "SGAL/compute_planes.hpp"
#include "SGAL/compute_planes_stable.hpp"
#include "SGAL/Inexact_kernel.hpp"
#include "SGAL/Epec_kernel.hpp"
#include "SGAL/Epic_kernel.hpp"
#include "SGAL/Epec_polyhedron.hpp"
#include "SGAL/Epic_polyhedron.hpp"

SGAL_BEGIN_NAMESPACE

/*! Clean Polyhedron facets visitor. */
class Clean_planes_visitor : public boost::static_visitor<> {
public:
  /*! Construct default. */
  Clean_planes_visitor(bool is_convex = false) : m_is_convex(is_convex) {}

  /*! Operate on a polyhedron that is not inexact.
   */
  template <typename Polyhedron_>
  void operator()(Polyhedron_& polyhedron) const {
    typedef Polyhedron_                                    Polyhedron;

    typedef typename Polyhedron::Point_3                   Point;
    typedef typename CGAL::Kernel_traits<Point>::Kernel    Kernel;
    typedef typename Kernel::Plane_3                       Ker_plane;
    typedef typename Polyhedron::Plane_3                   Pol_plane;
    typedef boost::is_same<Pol_plane, Ker_plane>           Polyhedron_has_plane;

    Kernel kernel;
    compute_planes(kernel, polyhedron, Polyhedron_has_plane(), m_is_convex);
  }

  /*! Operate on an Epic polyhedron.
   */
  void operator()(Epic_polyhedron& polyhedron) const {
    typedef typename Epic_kernel::Plane_3                  Ker_plane;
    typedef typename Epic_polyhedron::Plane_3              Pol_plane;
    typedef boost::is_same<Pol_plane, Ker_plane>           Polyhedron_has_plane;
    Epic_kernel kernel;
    compute_planes_stable(kernel, polyhedron, Polyhedron_has_plane());
  }

  /*! Operate on an Epec polyhedron.
   */
  void operator()(Epec_polyhedron& polyhedron) const {
    typedef typename Epec_kernel::Plane_3                  Ker_plane;
    typedef typename Epec_polyhedron::Plane_3              Pol_plane;
    typedef boost::is_same<Pol_plane, Ker_plane>           Polyhedron_has_plane;
    Epec_kernel kernel;
    compute_planes(kernel, polyhedron, Polyhedron_has_plane(), m_is_convex);
  }

  /*! Operate on an inexact polyhedron.
   */
  void operator()(Inexact_polyhedron& polyhedron) const
  {
    // Do nothing. An inexact polyhedron does not have planes or unnormalized
    // normals.
  }

private:
  //! Indicates whether the polyhedron facets are convex.
  bool m_is_convex;
};

SGAL_END_NAMESPACE

#endif
