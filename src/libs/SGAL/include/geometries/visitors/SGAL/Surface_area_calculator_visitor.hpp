// Copyright (c) 2016 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_SURFACE_AREA_CALCULATOR_VISITOR_HPP
#define SGAL_SURFACE_AREA_CALCULATOR_VISITOR_HPP

#include <boost/variant.hpp>

#include <CGAL/Polygon_mesh_processing/measure.h>
// #include <CGAL/Surface_mesh.h>

#include "SGAL/basic.hpp"
#include "SGAL/Inexact_kernel.hpp"
#include "SGAL/Epic_kernel.hpp"
#include "SGAL/Epec_kernel.hpp"
#include "SGAL/Vector3f.hpp"

namespace pmp = CGAL::Polygon_mesh_processing;

SGAL_BEGIN_NAMESPACE

//! \brief
// inline Vector3f operator-(const Vector3f& v1, const Vector3f& v2)
// { return Vector3f(v1[0] - v2[0], v1[1] - v2[1], v1[2] - v2[2]); }

/*! A visitor that calculates the surface area of a polyhedron. */
class Surface_area_calculator_visitor : public boost::static_visitor<Float> {
private:
  Geo_set::Primitive_type m_primitive_type;

public:
  Surface_area_calculator_visitor(Geo_set::Primitive_type type) :
    m_primitive_type(type)
  {}

  Float operator()(const Inexact_polyhedron& polyhedron) const
  { return surface_area<Inexact_kernel>(polyhedron); }

  Float operator()(const Epic_polyhedron& polyhedron) const
  { return surface_area<Epic_kernel>(polyhedron); }

  Float operator()(const Epec_polyhedron& polyhedron) const
  { return surface_area<Epec_kernel>(polyhedron); }

private:
  template <typename Kernel_, typename Polyhedron_>
  Float surface_area(const Polyhedron_& polyhedron) const
  {
    if (m_primitive_type == Geo_set::PT_TRIANGLES)
      return to_float(pmp::area(polyhedron));

    typedef Kernel_                             Kernel;
    typedef Polyhedron_                         Polyhedron;
    typedef typename Polyhedron::Facet          Polyhedron_facet;

    float result = 0;
    size_t i(0);
    std::for_each(polyhedron.facets_begin(), polyhedron.facets_end(),
                  [&](const Polyhedron_facet& facet)
                  {
                    Vector3f normal(0, 0, 0);
                    auto h1 = facet.halfedge();
                    auto a = to_vector3f(h1->vertex()->point());
                    auto h2 = h1->next();
                    auto b = to_vector3f(h2->vertex()->point());
                    auto h3 = h2->next();
                    auto c = to_vector3f(h3->vertex()->point());
                    auto size = CGAL::circulator_size(facet.facet_begin());
                    if (3 == size) {
                      normal.cross(b - a, c - a);
                    }
                    else if (4 == size) {
                      auto d = to_vector3f(h3->next()->vertex()->point());
                      normal.cross(c - a, d - b);
                    }
                    else {
                      auto h = h3;
                      do {
                        Vector3f v = c - a;
                        normal[0] += b[1] * v[2];
                        normal[1] += b[2] * v[0];
                        normal[2] += b[0] * v[1];

                        h = h->next();
                        a = b;
                        b = c;
                        const auto& p = h->vertex()->point();
                        c = to_vector3f(p);
                      } while (h != h3);
                    }
                    auto length = normal.length();
                    normal.scale(1.0f / length);
                    result += length;
                  });
    result *= 0.5f;
    return result;
  }
};

SGAL_END_NAMESPACE

#endif
