// Copyright (c) 2016 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_POLYHEDRON_FACETS_TYPE_VISITOR_HPP
#define SGAL_POLYHEDRON_FACETS_TYPE_VISITOR_HPP

#include <boost/variant.hpp>

#include "SGAL/basic.hpp"

SGAL_BEGIN_NAMESPACE

/*! A visitor that determines the type of polyhedron facets. */
class Polyhedron_facets_type_visitor :
  public boost::static_visitor<Geo_set::Primitive_type>
{
public:
  template <typename Polyhedron_>
  Geo_set::Primitive_type operator()(const Polyhedron_& polyhedron) const
  {
    return (polyhedron.is_pure_triangle()) ? Geo_set::PT_TRIANGLES :
    (polyhedron.is_pure_quad()) ? Geo_set::PT_QUADS : Geo_set::PT_POLYGONS;
  }
};

SGAL_END_NAMESPACE

#endif
