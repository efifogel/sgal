// Copyright (c) 2018 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_COPLANAR_FACETS_MERGER_VISITOR_HPP
#define SGAL_COPLANAR_FACETS_MERGER_VISITOR_HPP

#include <boost/variant.hpp>
#include <boost/type_traits.hpp>

#include <CGAL/Kernel_traits.h>

#include "SGAL/basic.hpp"
#include "SGAL/merge_coplanar_facets.hpp"
#include "SGAL/Inexact_kernel.hpp"
#include "SGAL/Epec_kernel.hpp"
#include "SGAL/Epic_kernel.hpp"

SGAL_BEGIN_NAMESPACE

/*! A visitor that counts the number of polyhedron halfedges. */
class Coplanar_facets_merger_visitor : public boost::static_visitor<bool> {
public:
  /*! Operate on a polyhedron that is not inexact.
   */
  template <typename Polyhedron_>
  bool operator()(Polyhedron_& polyhedron) const {
    typedef Polyhedron_                                     Polyhedron;

    typedef typename Polyhedron::Point_3                    Point;
    typedef typename CGAL::Kernel_traits<Point>::Kernel     Kernel;
    Kernel kernel;
    return merge(polyhedron, kernel);
  }

  /*! Operate on an inexact polyhedron.
   */
  bool operator()(Inexact_polyhedron& polyhedron) const {
    Inexact_kernel kernel;
    return merge(polyhedron, kernel);
  }

private:
  /*! Apply the merge.
   */
  template <typename Polyhedron_, typename Kernel_>
  bool merge(Polyhedron_& polyhedron, const Kernel_& kernel) const {
    auto num_facets = polyhedron.size_of_facets();
    merge_coplanar_facets(kernel, polyhedron);
    return num_facets != polyhedron.size_of_facets();
  }
};

SGAL_END_NAMESPACE

#endif
