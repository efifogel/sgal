// Copyright (c) 2020 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_SIMPLIFIER_VISITOR_HPP
#define SGAL_SIMPLIFIER_VISITOR_HPP

#include <boost/variant.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/Stop_simplification_strategy.hpp"

#include <CGAL/Surface_mesh.h>
#include <CGAL/Surface_mesh_simplification/edge_collapse.h>
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Edge_count_ratio_stop_predicate.h>
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Edge_count_stop_predicate.h>
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Edge_length_stop_predicate.h>
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Edge_length_cost.h>
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Midpoint_placement.h>

namespace SMS = CGAL::Surface_mesh_simplification;

SGAL_BEGIN_NAMESPACE

/*! A visitor that counts the number of polyhedron halfedges. */
class Polyhedron_simplifier_visitor : public boost::static_visitor<size_t> {
private:
  //! The simplification stop strategy.
  Stop_simplification_strategy m_stop_strategy;

  //! The maximum number of edge collapses to apply.
  Uint m_count_stop;

  //! The ratio of maximum number of edge collapses to apply.
  Float m_count_ratio_stop;

  //! The edge-length stop criterion..
  Float m_edge_length_stop;

public:
  Polyhedron_simplifier_visitor(Stop_simplification_strategy stop_strategy,
                                Uint count_stop,
                                Float count_ratio_stop,
                                Float edge_length_stop) :
    m_stop_strategy(stop_strategy),
    m_count_stop(count_stop),
    m_count_ratio_stop(count_ratio_stop),
    m_edge_length_stop(edge_length_stop)
  {}

  /*! Simplify a given polyhedron.
   */
  template <typename Polyhedron_>
  size_t operator()(Polyhedron_& polyhedron) const {
    using Polyhedron = Polyhedron_;
    using Graph_traits = boost::graph_traits<Polyhedron>;
    using halfedge_descriptor = typename Graph_traits::halfedge_descriptor;
    using vertex_descriptor = typename Graph_traits::vertex_descriptor;

    size_t index = 0;
    for (halfedge_descriptor hd : halfedges(polyhedron)) hd->id() = index++;
    index = 0;
    for (vertex_descriptor vd : vertices(polyhedron)) vd->id() = index++;

    switch (m_stop_strategy) {
     case Stop_simplification_strategy::COUNT:
      {
        SMS::Edge_count_stop_predicate<Polyhedron_> stop(m_count_stop);
        return SMS::edge_collapse(polyhedron, stop);
      }

     case Stop_simplification_strategy::COUNT_RATIO:
      {
        SMS::Edge_count_ratio_stop_predicate<Polyhedron_> stop(m_count_ratio_stop);
        return SMS::edge_collapse(polyhedron, stop);
      }

     case Stop_simplification_strategy::EDGE_LENGTH:
      {
        SMS::Edge_length_stop_predicate<float> stop(m_edge_length_stop);
        SMS::Edge_length_cost<Polyhedron_> cost;
        SMS::Midpoint_placement<Polyhedron_> placement;
        return SMS::edge_collapse(polyhedron, stop,
                                  CGAL::parameters::get_cost(cost).
                                  get_placement(placement));
      }

     default: return 0;
    }
  }

  size_t operator()(Epec_polyhedron& polyhedron) const {
    SGAL_error_msg("Cannot simplify an exact-construction polyhedron!");
    return 0;
  }
};

SGAL_END_NAMESPACE

#endif
