// Copyright (c) 2016 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_TYPE_POLYHEDRON_VISITOR_HPP
#define SGAL_TYPE_POLYHEDRON_VISITOR_HPP

#include <boost/variant.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/Geo_set.hpp"

SGAL_BEGIN_NAMESPACE

/*! Polyhedron type visitor. */
class Type_polyhedron_visitor :
  public boost::static_visitor<Geo_set::Primitive_type>
{
public:
  template <typename Polyhedron_>
  Geo_set::Primitive_type operator()(const Polyhedron_& polyhedron) const
  {
    Boolean triangles(true);
    Boolean quads(true);
    for (auto fit = polyhedron.facets_begin(); fit != polyhedron.facets_end();
         ++fit)
    {
      auto hh = fit->facet_begin();
      auto circ_size = CGAL::circulator_size(hh);
      if (circ_size != 3) triangles = false;
      if (circ_size != 4) quads = false;
    }
    return (triangles) ? Geo_set::PT_TRIANGLES :
      (quads) ? Geo_set::PT_QUADS : Geo_set::PT_POLYGONS;
  }
};

SGAL_END_NAMESPACE

#endif
