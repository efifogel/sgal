// Copyright (c) 2019 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_ASSIGN_UNIQUE_NAME_HPP
#define SGAL_ASSIGN_UNIQUE_NAME_HPP

#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/Container.hpp"

SGAL_BEGIN_NAMESPACE

/*! Obtain a unique name with the "SGAL_" prefix, e.g., for containers.
 * return a unique name.
 */
inline std::string get_unique_name()
{
  static const std::string prefix("SGAL_");
  auto uuid = boost::uuids::random_generator()();
  return prefix + boost::uuids::to_string(uuid);
}

/*! Assign a unique name with the "SGAL_" prefix to a given container.
 * \param[in] container the container to be named.
 */
inline void assign_unique_name(boost::shared_ptr<Container> container)
{ container->set_name(get_unique_name()); }

SGAL_END_NAMESPACE

#endif
