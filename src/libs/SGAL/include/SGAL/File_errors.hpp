// Copyright (c) 2020 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_FILE_ERRORS_HPP
#define SGAL_FILE_ERRORS_HPP

#include "SGAL/basic.hpp"
#include "SGAL/errors.hpp"

SGAL_BEGIN_NAMESPACE

/*! Class thrown when the opening of a plugin (shared library) fails. */
class SGAL_SGAL_DECL File_error : public error {
 private:
  std::string m_filename;

 public:
  const std::string& filename() const { return m_filename; }
  File_error(const std::string& message, const std::string& filename) :
    error(message),
    m_filename(filename)
  {}
  ~File_error() SGAL_NOTHROW {}
};

/*! Class thrown when a file cannot be found. */
class SGAL_SGAL_DECL File_not_found_error : public File_error {
public:
  File_not_found_error(const std::string& filename) :
    File_error(std::string("Error: File (").append(filename).append(") not found!"),
               filename)
  {}

  ~File_not_found_error() SGAL_NOTHROW {}
};

/*! Class thrown when a file name does not point at a regular file. */
class SGAL_SGAL_DECL Not_regular_file_error : public File_error {
public:
  Not_regular_file_error(const std::string& filename) :
    File_error(std::string("Error: File (").append(filename).
               append(") is not a regular file!"),
               filename)
  {}

  ~Not_regular_file_error() SGAL_NOTHROW {}
};

/*! Class thrown when the reading of a stream fails. */
class SGAL_SGAL_DECL Read_error : public File_error {
public:
  Read_error(const std::string& filename) :
    File_error(std::string("Error: Failed to read"), filename) {}
  ~Read_error() SGAL_NOTHROW {}
};

/*! Class thrown when the opening of a file failed. */
class SGAL_SGAL_DECL Open_file_error : public File_error {
public:
  Open_file_error(const std::string& filename) :
    File_error(std::string("Error: Failed to open file (").
               append(filename).append(")!"), filename) {}
  ~Open_file_error() SGAL_NOTHROW {}
};

/*! Class thrown when the file is empty. */
class SGAL_SGAL_DECL Empty_error : public File_error {
public:
  Empty_error(const std::string& filename) :
    File_error(std::string("Error: File (").append(filename).
               append(") is empty!"), filename) {}
  ~Empty_error() SGAL_NOTHROW {}
};

SGAL_END_NAMESPACE

#endif
