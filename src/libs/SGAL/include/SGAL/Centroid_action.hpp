// Copyright (c) 2019 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_CENTROID_ACTION_HPP
#define SGAL_CENTROID_ACTION_HPP

#include <list>
#include <stack>

#include <boost/shared_ptr.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/Types.hpp"
#include "SGAL/Action.hpp"
#include "SGAL/Vector3f.hpp"

SGAL_BEGIN_NAMESPACE

#if defined(_MSC_VER)
#pragma warning( push )
#pragma warning( disable: 4251 )
#endif

class Matrix4f;

/*! \class Centroid_action Centroid_action.hpp
 */
class SGAL_SGAL_DECL Centroid_action : public Action {
public:
  /*! Construct default.
   */
  Centroid_action();

  /*! Destruct. */
  virtual ~Centroid_action();

  /*! Apply the action to a given node.
   * \param[in] node  the node to apply the action on.
   */
  virtual Trav_directive apply(Shared_node node);

  /*! Obtain the centroid.
   * \param[out] centroid the centroid.
   */
  const Vector3f& centroid();

private:
  typedef boost::shared_ptr<Matrix4f>               Shared_matrix4f;

  /*! Compute the centroid from the points.
   */
  void clean();

  //! Indicates whether centroid has been computed.
  Boolean m_dirty;

  //! The centroid.
  Vector3f m_centroid;

  //! The points.
  std::list<Vector3f> m_points;

  //! The stack of viewing matrices.
  std::stack<Shared_matrix4f> m_matrices;
};

#if defined(_MSC_VER)
#pragma warning( pop )
#endif

SGAL_END_NAMESPACE

#endif
