// Copyright (c) 2020 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_PLUGIN_API_HPP
#define SGAL_PLUGIN_API_HPP

#include <string>

#include <boost/config.hpp>

#include "SGAL/basic.hpp"

SGAL_BEGIN_NAMESPACE

class Main_option_parser;
class Scene_graph;

class BOOST_SYMBOL_VISIBLE Plugin_api {
public:
  /*! Destruct. */
  virtual ~Plugin_api() {}

  /*! Obtain the name of the plugin.
   */
  virtual std::string name() const = 0;

  /*! Initalize the plugin.
   */
  virtual void init(Main_option_parser* option_parser) = 0;

  /*! Configure the plugin.
   */
  virtual void configure(Scene_graph* scene_graph) {}

  /*! Indulge the user, that is, apply the the command line specified options.
   */
  virtual void indulge(Scene_graph* scene_graph) {}
};

SGAL_END_NAMESPACE

#endif
