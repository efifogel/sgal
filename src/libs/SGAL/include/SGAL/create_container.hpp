// Copyright (c) 2004 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_CREATE_CONTAINER_HPP
#define SGAL_CREATE_CONTAINER_HPP

#include <boost/shared_ptr.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/Container_factory.hpp"
#include "SGAL/Scene_graph.hpp"

SGAL_BEGIN_NAMESPACE

/*! Obtain the prototype container of a given container type.
 */
template <typename Type>
boost::shared_ptr<Type> get_container_prototype(const char* name)
{
  auto* factory = SGAL::Container_factory::get_instance();
  auto cont_proto = factory->get_prototype(name);
  return boost::dynamic_pointer_cast<Type>(cont_proto);
}

/*! Create a new container from its prototype.
 */
template <typename Type>
boost::shared_ptr<Type> create_container(boost::shared_ptr<Type> proto,
                                         Scene_graph* sg)
{
  typedef boost::shared_ptr<SGAL::Container> Shared_container;
  auto cont = Shared_container(proto->create());
  SGAL_assertion(cont);
  cont->add_to_scene(sg);
  sg->add_container(cont);
  return boost::dynamic_pointer_cast<Type>(cont);
}

/*! Create a new container from its prototype.
 */
template <typename Type>
boost::shared_ptr<Type> create_container(const char* name, Scene_graph* sg)
{
  auto proto = get_container_prototype<Type>(name);
  return create_container<Type>(proto, sg);
}

SGAL_END_NAMESPACE

#endif
