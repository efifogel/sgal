// Copyright (c) 2020 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Author(s): Efi Fogel        <efifogel@gmail.com>

#ifndef SGAL_STOP_SIMPLIFICATION_STRATEGY_HPP
#define SGAL_STOP_SIMPLIFICATION_STRATEGY_HPP

#include "SGAL/basic.hpp"
#include "SGAL/Types.hpp"

SGAL_BEGIN_NAMESPACE

//! Indicates which type of halfedges to render
enum class Stop_simplification_strategy : Uint {
  COUNT = 0,
  COUNT_RATIO,
  EDGE_LENGTH
};

/*! Obtain the name of the stop_simplification strategy-option.
 * \return the name of the stop_simplification strategy-option.
 */
extern const char* get_stop_simplification_strategy_name(size_t id);

/*! Find the code of a stop_simplification strategy given by its name.
 */
size_t find_stop_simplification_strategy_code(const char* name);

SGAL_END_NAMESPACE

#endif
