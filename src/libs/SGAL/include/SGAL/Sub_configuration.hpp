// Copyright (c) 2020 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

/*! A base class representing a sub-configuration node.
 */

#ifndef SGAL_SUB_CONFIGURATION_HPP
#define SGAL_SUB_CONFIGURATION_HPP

#include "SGAL/basic.hpp"
#include "SGAL/Node.hpp"

SGAL_BEGIN_NAMESPACE

class Container_proto;
class Element;

class SGAL_SGAL_DECL Sub_configuration : public Node {
public:
  enum {
    FIRST = Node::LAST - 1,
    LAST
  };

  /*! Construct. */
  Sub_configuration(Boolean proto = false);

  /*! Destruct. */
  virtual ~Sub_configuration();

  /*! Initialize the node prototype. */
  virtual void init_prototype();

  /*! Delete the node prototype. */
  virtual void delete_prototype();

  /*! Obtain the node prototype. */
  virtual Container_proto* get_prototype();

  /*! Set the attributes of this node. */
  virtual void set_attributes(Element* elem);

  /*! Enable the node---re-route all fields. */
  virtual void enable() = 0;

private:
  //! The node prototype.
  static Container_proto* s_prototype;
};

SGAL_END_NAMESPACE

#endif
