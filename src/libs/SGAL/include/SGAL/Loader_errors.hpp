// Copyright (c) 2015 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_LOADER_ERRORS_HPP
#define SGAL_LOADER_ERRORS_HPP

#include "SGAL/basic.hpp"
#include "SGAL/File_errors.hpp"

SGAL_BEGIN_NAMESPACE

/*! Class thrown when a source file cannot be found. */
class SGAL_SGAL_DECL Find_file_error : public File_error {
public:
  Find_file_error(const std::string& filename) :
    File_error(std::string("Error: Failed to find file ").append(filename),
               filename)
  {}
  ~Find_file_error() SGAL_NOTHROW {}
};

/*! Class thrown when the parsing of a stream fails. */
class SGAL_SGAL_DECL Parse_error : public File_error {
public:
  Parse_error(const std::string& filename) :
    File_error(std::string("Error: Failed to parse"), filename) {}
  Parse_error(const std::string& filename, const std::string& reason) :
    File_error(std::string("Error: Failed to parse; ")+reason, filename) {}
  ~Parse_error() SGAL_NOTHROW {}
};

/*! Class thrown when the data is inconistent. */
class SGAL_SGAL_DECL Inconsistent_error : public File_error {
public:
  Inconsistent_error(const std::string& filename) :
    File_error(std::string("Error: Inconsistent data"), filename) {}
  ~Inconsistent_error() SGAL_NOTHROW {}
};

/*! Class thrown when an overflow occurred. */
class SGAL_SGAL_DECL Overflow_error : public File_error {
public:
  Overflow_error(const std::string& filename) :
    File_error(std::string("Error: Overflow occurred"), filename) {}
  ~Overflow_error() SGAL_NOTHROW {}
};

SGAL_END_NAMESPACE

#endif
