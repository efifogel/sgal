// Copyright (c) 2020 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_PLUGIN_ERRORS_HPP
#define SGAL_PLUGIN_ERRORS_HPP

#include "SGAL/basic.hpp"
#include "SGAL/File_errors.hpp"

SGAL_BEGIN_NAMESPACE

/*! Class thrown when a file is not a shared_library. */
class SGAL_SGAL_DECL Not_shared_library_error : public error {
public:
  Not_shared_library_error(const std::string& filename) :
    error(std::string("File (").append(filename).
          append(") is not a shared library!")) {}

  ~Not_shared_library_error() SGAL_NOTHROW {}
};

/*! Class thrown when a shared library cannot failed to load. */
class SGAL_SGAL_DECL Shared_library_load_error : public File_error {
public:
  Shared_library_load_error(const std::string& filename) :
    File_error(std::string("Error: Failed to load shared library (").
               append(filename).append(")!"), filename) {}
  ~Shared_library_load_error() SGAL_NOTHROW {}
};

/*! Class thrown when a shared library does not support create_plugin. */
class SGAL_SGAL_DECL Shared_library_has_error : public File_error {
public:
  Shared_library_has_error(const std::string& filename,
                           const std::string& fnc_name) :
    File_error(std::string("Error: Shared library (").
               append(filename).append(") does not have ").
               append(fnc_name).append("()!"),
               filename) {}
  ~Shared_library_has_error() SGAL_NOTHROW {}
};

SGAL_END_NAMESPACE

#endif
