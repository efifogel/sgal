// Copyright (c) 2014 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <iostream>

#include "SGAL/basic.hpp"
#include "SGAL/Bounding_box_2d.hpp"

#ifndef SGAL_IO_BOUNDING_BOX_2D_HPP
#define SGAL_IO_BOUNDING_BOX_2D_HPP

SGAL_BEGIN_NAMESPACE

//! \brief exporter.
inline std::ostream& operator<<(std::ostream& os, const Bounding_box_2d& bbox)
{
  os << bbox.xmin() << " " << bbox.ymin() << " "
     << bbox.xmax() << " " << bbox.ymax();
  return os;
}

//! \brief importer.
inline std::istream& operator>>(std::istream& in, Bounding_box_2d& bbox)
{
  Float x_min, y_min, x_max, y_max;
  in >> x_min >> y_min >> x_max >> y_max;
  bbox.set(x_min, y_min, x_max, y_max);
  return in;
}

SGAL_END_NAMESPACE

#endif
