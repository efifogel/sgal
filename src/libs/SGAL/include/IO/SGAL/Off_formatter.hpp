// Copyright (c) 2021 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_OFF_FORMATTER_HPP
#define SGAL_OFF_FORMATTER_HPP

#include <iostream>
#include <string>
#include <stack>

#include "SGAL/basic.hpp"
#include "SGAL/Text_formatter.hpp"
#include "SGAL/Matrix4f.hpp"
#include "SGAL/Indices_types.hpp"

SGAL_BEGIN_NAMESPACE

class Vector3f;
class Container;

#if defined(_MSC_VER)
#pragma warning( push )
#pragma warning( disable: 4251 )
#endif

/*! \class Off_formatter Off_formatter.hpp
 * Off_formater formats the scene, which is exported to an output stream,
 * in the OFF format.
 */
class SGAL_SGAL_DECL Off_formatter : public Text_formatter {
public:
  /*! Construct from file name.
   * \param[in] filename The file name.
   */
  Off_formatter(const std::string& filename);

  /*! Construct an output formatter from an output stream.
   * \param[in] filename The file name.
   * \param[in] os the output stream.
   */
  Off_formatter(const std::string& filename, std::ostream& os);

  /*! Construct an input formatter from an input stream.
   * \param[in] filename The file name.
   * \param[in] is the input stream.
   */
  Off_formatter(const std::string& filename, std::istream& is);

  /*! Destructor */
  virtual ~Off_formatter();

  /// \name Export functions
  //@{

  /*! Export the headers of the scene graph.
   */
  virtual void begin();

  /*! Export the routing statements.
   */
  virtual void end();

  /*! Export a scene-graph container.
   */
  virtual void write(Shared_container container);

  /*! Add a mesh.
   */
  void add_mesh(std::vector<Vector3f>&& vertices,
                const Facet_indices& indices, bool is_ccw = true);

  //@}

  /*! Obtain the viewing matrix at the top of the stack.
   * \return the viewing matrix at the top of the stack.
   */
  const Matrix4f& top_matrix() const;

private:
  typedef std::pair<std::vector<Vector3f>, const Facet_indices&>    Mesh_in;
  typedef std::pair<std::vector<Vector3f>, Polygon_indices>         Mesh_out;

  template <typename Indices>
  void facets(const Indices& indices) {
    for (const auto& facet : indices) {
      indent();
      auto it = facet.begin();
      out() << facet.size();
      for (auto it = facet.begin(); it != facet.end(); ++it)
        out() << " " << *it;
      new_line();
    }
  }

  /*! \brief collapse identical vertices
   */
  // Mesh_out get_single_mesh() const;

  /*! Count facets.
   */
  size_t number_of_facets(const Facet_indices& indices) const;

  /*! Export the vertices.
   * \param points the vertex points.
   */
  void export_vertices(const std::vector<Vector3f>& points);

  /*! Export the facets.
   */
  void export_facets(const Facet_indices& indices, bool is_ccw = true);

  /*! Export a single (old) mesh.
   */
  void export_mesh(const Mesh_in& mesh);

  //! The stack of viewing matrices.
  std::stack<Matrix4f> m_matrices;

  //! The list of meshes.
  std::list<Mesh_in> m_mesh_set;
};

#if defined(_MSC_VER)
#pragma warning( pop )
#endif

//! \brief obtains the viewing matrix at the top of the stack.
inline const Matrix4f& Off_formatter::top_matrix() const
{ return m_matrices.top(); }

SGAL_END_NAMESPACE

#endif
