// Copyright (c) 2019 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_IO_PASS_HPP
#define SGAL_IO_PASS_HPP

#include <string>
#include <algorithm>

#include "SGAL/basic.hpp"
#include "SGAL/Types.hpp"
#include "SGAL/Pass.hpp"

SGAL_BEGIN_NAMESPACE

/*! Export a Pass enumeration.
 */
template <typename OutputStream>
OutputStream& operator<<(OutputStream& os, Pass pass)
{
  os << s_pass_names[static_cast<size_t>(pass)];
  return os;
}

/*! Import a Pass enumeration.
 */
template <typename inputStream>
inputStream& operator>>(inputStream& is, Pass& pass)
{
  std::string value;
  is >> value;

  auto num = static_cast<size_t>(Pass::NUM);
  const auto** found =
    std::find(s_pass_names, &s_pass_names[num], value);
  auto index = std::distance(s_pass_names, found);
  if (index < num) pass = static_cast<Pass>(index);
  return is;
}

SGAL_END_NAMESPACE

#endif
