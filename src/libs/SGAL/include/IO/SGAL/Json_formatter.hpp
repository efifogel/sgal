// Copyright (c) 2018 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_JSON_FORMATTER_HPP
#define SGAL_JSON_FORMATTER_HPP

#include <iostream>
#include <string>
#include <stack>
#include <vector>
#include <list>
#include <map>

#include <boost/shared_ptr.hpp>
#include <boost/unordered_map.hpp>
#include <boost/io/ios_state.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/Text_formatter.hpp"
#include "SGAL/Matrix4f.hpp"
#include "SGAL/Indices_types.hpp"
#include "SGAL/Geo_set.hpp"

SGAL_BEGIN_NAMESPACE

class Container;
class Group;
class Shape;
class Geometry;
class Box;
class Geo_set;
class Mesh_set;
class Lines_set;
class Appearance;
class Matrix4f;
class Vector3f;
class Vector2f;
class Camera;
class Light;
class Node;
class Bounding_sphere;

#if defined(_MSC_VER)
#pragma warning( push )
#pragma warning( disable: 4251 )
#endif

/*! \class Json_formatter Json_formatter.hpp
 * Json_formater formats the scene, which is exported to an output stream,
 * in the Json format.
 */
class SGAL_SGAL_DECL Json_formatter : public Text_formatter {
public:
  typedef boost::shared_ptr<Container>              Shared_container;
  typedef boost::shared_ptr<Group>                  Shared_group;
  typedef boost::shared_ptr<Shape>                  Shared_shape;
  typedef boost::shared_ptr<Geometry>               Shared_geometry;
  typedef boost::shared_ptr<Appearance>             Shared_apperance;
  typedef boost::shared_ptr<Box>                    Shared_box;
  typedef boost::shared_ptr<Geo_set>                Shared_geo_set;
  typedef boost::shared_ptr<Mesh_set>               Shared_mesh_set;
  typedef boost::shared_ptr<Lines_set>              Shared_lines_set;
  typedef boost::shared_ptr<Matrix4f>               Shared_matrix4f;
  typedef boost::shared_ptr<Light>                  Shared_light;
  typedef boost::shared_ptr<Node>                   Shared_node;

  enum Blending {
    BLENDING_NO = 0,
    BLENDING_NORMAL,
    BLENDING_ADDITIVE,
    BLENDING_SUBTRACTIVE,
    BLENDING_MULTIPLY,
    BLENDING_CUSTOM
  };

  enum Material_color {
    MC_NO_COLORS = 0,
    MC_FACE_COLORS,
    MC_VERTEX_COLORS,
    MC_SIZE
  };

  enum Flags {
    F_IS_TRI = 0,
    F_HAS_MATERIAL,
    F_UNDEINED1,
    F_HAS_UVS,
    F_FACE_NORMAL,
    F_FACE_VERTEX_NORMAL,
    F_FACE_COLOR,
    F_FACE_VERTEX_COLOR,
    F_SIZE
  };

  /*! Construct from file name.
   * \param[in] filename The file name.
   */
  Json_formatter(const std::string& filename);

  /*! Construct an output formatter from an output stream.
   * \param[in] filename The file name.
   * \param[in] os the output stream.
   */
  Json_formatter(const std::string& filename, std::ostream& os);

  /*! Construct an input formatter from an input stream.
   * \param[in] filename The file name.
   * \param[in] is the input stream.
   */
  Json_formatter(const std::string& filename, std::istream& is);

  /*! Destructor */
  virtual ~Json_formatter();

  /*! Set the bounding sphere of the scene.
   * \param[in] bs the bounding sphere.
   */
  void set_bounding_sphere(const Bounding_sphere* bs);

  /*! Set the bounding sphere of the scene.
   * \param[in] camera the camera.
   */
  void set_camera(Camera* camera);

  /*! Construct a basic material object and inserts it into a map.
   */
  void construct_basic_material(Material_color mc, bool depth_test);

  /*! Construct a basic material object and inserts it into a map.
   */
  void construct_basic_material(Material_color mc, const Vector3f& color,
                                bool depth_test);

  /*! Construct a basic material object and inserts it into a map
   */
  void construct_basic_material(Material_color mc, const Vector4f& color,
                                bool transparent, bool depth_test);

  /*! Construct a basic material object and inserts it into a map
   */
  void construct_basic_material(Material_color mc, float opacity,
                                bool transparent, bool depth_test);

  /*! Pre-process the formatter. Traverse all input containers (An instance
   * is a container that has a name.) and assign a unique id to each geometry
   * and each material container and store these id in respective maps.
   * \param[in] containers.
   * \param[in] instances.
   */
  void pre_process(const std::list<Shared_container>& containers,
                   const std::map<std::string, Shared_container>& instances);

  /// \name Export functions
  //@{

  /*! Export the headers of the scene graph.
   */
  virtual void begin();

  /*! Export the routing statements.
   */
  virtual void end();

  /*! Export a scene-graph container.
   */
  virtual void write(Shared_container container);

  /*! Export a 3D point.
   * \param[in] p the point.
   * \param compact[in] indicates whether to export in a compact manner
   *        (without spaces).
   */
  void vertex(const Vector3f& p, bool compact = false);

  /*! Export a 2D point.
   * \param[in] p the point.
   * \param compact[in] indicates whether to export in a compact manner
   *        (without spaces).
   */
  void vertex(const Vector2f& p, bool compact = false);

  /*! Export a facet.
   * \param[in] flags the flags of the facet.
   * \param[in] i the index of the facet.
   * \param[in] compact
   */
  void facet(Shared_mesh_set mesh_set, unsigned int flags, size_t i,
             bool compact = false);

  /*! Export a triangular facet.
   * \param[in] i the index of the facet.
   */
  void tri_facet(Shared_mesh_set mesh_set, size_t i);

  /*! Export a quad facet.
   * \param[in] i the index of the facet.
   */
  void quad_facet(Shared_mesh_set mesh_set, size_t i);
  //@}

  /*! Indicate whether the geometry represents a structured mesh type.
   */
  bool is_structured_mesh(Geo_set::Primitive_type type) const;

  /*! Indicate whether the geometry represents a sequence of segments.
   */
  bool is_lines(Geo_set::Primitive_type type) const;

  /*! Indicate whether the geometry represents a sequence of line strips.
   */
  bool is_line_strips(Geo_set::Primitive_type type) const;

  /*! \brief indicates whether the geometry represents a sequence of line loops.
   */
  bool is_line_loops(Geo_set::Primitive_type type) const;

  /*! Indicate whether the geometry represents a sequence of polylines.
   */
  bool is_polylines(Geo_set::Primitive_type type) const;

private:
  /*! A utility strcture used when dispatching the function that exports
   * a value.
   */
  template <typename T>
  struct identity { typedef T type; };

  //! A basic-material record.
  struct Basic_material {
    Material_color m_material_color;
    size_t m_color;
    float m_opacity;
    bool m_transparent;
    bool m_depth_test;

    /*! Construct.
     */
    Basic_material(Material_color material_color = MC_NO_COLORS,
                   size_t color = 0x0,
                   float opacity = 1.0f,
                   bool transparent = false,
                   bool depth_test = true);
  };

  /*! Export a string.
   * \param[in] str the string to export.
   */
  void out_string(const char* str);

  /*! Export a value.
   * \param[in] value the value to export.
   */
  template <typename Value>
  void out_value(Value value);

  /*! Export a generic value.
   * \param[in] value the value to export.
   */
  template <typename Value>
  void out_value(Value value, identity<Value>);

  /*! Export a (specific) sting value.
   */
  void out_value(const std::string& value, identity<std::string>);

  /*! Export a (specific) sting value.
   * \param[in] value the value to export.
   */
  void out_value(const char* value, identity<const char*>);

  /*! Export a (specific) unsigned int value.
   */
  void out_value(Uint value, identity<Uint>);

  /*! Export a (specific) Boolean value.
   * \param[in] value the value to export.
   */
  void out_value(Boolean value, identity<Boolean>);

  /*! Export a single value.
   * \param[in] value the value to export.
   */
  template <typename Value>
  void single_value(Value value, bool compact = false);

  /*! Convert a color to its hexadecimal representation.
   */
  template <typename Vector_>
  Uint to_hex(const Vector_& color);

  /*! Convert a color in the HTML format to its hexadecimal string
   * representation.
   */
  std::string to_string_hex(size_t color);

  /*! Convert a color to its hexadecimal string representation.
   */
  std::string to_string_hex(const Vector3f& color);

  /*! Export a separator between the name of an attribute and its value.
   */
  void name_value_separator();

  //! Object manipulators.
  //@{

  /*! Begin an object.
   */
  void object_begin(bool compact = false);

  /*! End an object.
   */
  void object_end(bool compact = false);

  /*! Export a separator between consecutive objects.
   */
  void object_separator(bool compact = false);
  //@}

  //! Array manipulators.
  //@{

  //! Begin an array.
  void array_begin(bool compact = false);

  //! End an array.
  void array_end(bool compact = false);
  //@}

  /*! Export an object.
   * \param[in] op the main export operation.
   * \param[in] start_op the starting export operation.
   * \param[in] end_op the ending export operation.
   */
  template <typename UnaryOperation,
            typename StartOperation, typename EndOperation>
  void object(UnaryOperation op,
              StartOperation start_op, EndOperation end_op);

  /*! Export a complex attribute.
   * \param[in] name the name of the attribute.
   * \param[in] op the main export operation.
   * \param[in] start_op the starting export operation.
   * \param[in] end_op the ending export operation.
   */
  template <typename UnaryOperation,
            typename StartOperation, typename EndOperation>
  void attribute_object(const std::string& name, UnaryOperation op,
                        StartOperation start_op, EndOperation end_op);

  /*! Export a single object.
   */
  template <typename UnaryOperation>
  void single_object(UnaryOperation op, bool compact = false);

  /*! Export multiple objects.
   */
  template <typename UnaryOperation>
  void multiple_objects(UnaryOperation op, bool compact = false);

  /*! Export a simple attribute.
   * \param[in] name the attribute name.
   * \param[in] value the attribute value.
   */
  template <typename Value>
  void attribute(const std::string& name, Value value, bool compact = false);

  /*! Export a simple attribute if not the default.
   * \param[in] name the attribute name.
   * \param[in] value the attribute value.
   * \param[in] default the attribute default value.
   */
  template <typename Value>
  void attribute_not_default(const std::string& name, Value value,
                             Value default_value, bool compact = false);

  /*! Export a single-object attribute.
   * \param[in] name the attribute name.
   * \param[in] op the operation that exposrts the attributes of the object.
   */
  template <typename UnaryOperation>
  void attribute_single(const std::string& name, UnaryOperation op,
                        bool compact = false);

  /*! Export an array-object attribute.
   * \param[in] name the attribute name.
   * \param[in] op the operation that exposrts the objects of the array.
   */
  template <typename UnaryOperation>
  void attribute_multiple(const std::string& name, UnaryOperation op,
                          bool compact = false);

  /*! Export the attributes of all geometries.
   */
  void export_geometries_data();

  /*! Export the attributes of all materials.
   */
  void export_materials_data();

  /*! Export the bounding sphere of the group.
   * \param[in] group the group.
   */
  void export_bounding_sphere(const Bounding_sphere* bounding_sphere);

  /*! Export the attributes of a box geometry object.
   * \param[in] box the geometry container.
   * \param[in] id the unique id of the geometry.
   */
  void export_box_geometry(Shared_box box, String& id);

  /*! Export the attributes of a structured mesh geometry object.
   * \param[in] mesh_set the geometry container.
   * \param[in] id the unique id of the geometry.
   */
  void export_structured_mesh_geometry(Shared_mesh_set mesh_set, String& id);

  /*! Export the attributes of a lines-geometry object.
   * \param[in] line_set the geometry container.
   * \param[in] id the unique id of the geometry.
   */
  void export_lines_geometry(Shared_lines_set line_set, String& id);

  /*! Export the attributes of a polylines-geometry object.
   * \param[in] line_set the geometry container.
   * \param[in] i the index of the polylines.
   * \param[in] id the unique id of the geometry.
   */
  void export_polyline_geometry(Shared_lines_set line_set, size_t i, String& id);

  /*! Export the attributes of a material object.
   * \param[in] appearance the appearance container.
   * \param[in] id the unique id of the material.
   */
  void export_material(Shared_apperance appearance, String& id);

  /*! Export the attributes of a basic material object.
   */
  void export_basic_material(const Basic_material& basic_material,
                             const String& id);

  /*! Export the attributes of a basic material object with color.
   * \param[in] color the color.
   * \param[in] id the unique id of the material.
   */
  void export_color_material(size_t color, String& id);

  /*! Export the data record of a structured-mesh geometry item.
   */
  void export_structured_mesh_geometry_data(Shared_mesh_set mesh_set);

  /*! Export the data record of a lines geometry item.
   */
  void export_lines_geometry_data(Shared_lines_set lines_set);

  /*! Export the data record of a polyline geometry item.
   */
  void export_polyline_geometry_data(Shared_lines_set lines_set, size_t i);

  /*! Export the camera.
   */
  void export_camera();

  /*! Export a group.
   */
  void export_group(Shared_group group);

  /*! Export a light source.
   */
  void export_light(Shared_light light);

  /*! Export a shape of a primitive geometry.
   */
  void export_primitive_shape(Shared_shape shape);

  /*! Export a shape the geometry of which is structured.
   */
  void export_structured_shape(Shared_shape shape);

  /*! Export an object that corresponds to the ith polyline of a shape the
   * geometry of which is polylines.
   */
  void export_polyline_shape(Shared_shape shape, size_t i);

  /*! Export a matrix.
   * \param[in] the matrix.
   */
  void export_matrix(const Matrix4f& matrix);

  /*! Handle a geometry container; in paerticular, add to the geometries map.
   */
  void handle_geometry(Shared_geometry geometry);

  /*! Handle an apperarance container; in paerticular, add to the appearances
   * map.
   */
  void handle_appearance(Shared_apperance appearance);

  /*! Handle a shape container; in paerticular, add a basic-material to the
   * basic-materials if needed.
   *
   * In SGAL a shape is associated with a geometry and an apperance nodes.
   * However, in the json world a shape is associated with a json geometry and a
   * jason material records, and they do not match. In particular, ThreeJS has
   * some deficiencies that force us to make some trade offs.

   * First, it turns out that the color of non-lit lines must be specified in a
   * the json material (of type MeshBasicMaterial), as oppoed to the SGAL
   * case where colors can be associated with the vertices of the lines. It
   * implies that a non-lit line has one single color in the Json world.

   * Secondly, it turns out that alpha values, used, for example, for blending
   * (a.k.a transparency) non-lit objects must also be specified in the json
   * material. Similarly It implies that a transparent geometry has also a
   * single color.

   * Finally, we need to generate the material records (and geometry records)
   * a priori and only then generate a record per shape that associates a
   * predefined geometry and a predefined material with the newly constructed
   * shape. Therefore, we apply the following procedure:

   * Given a shape we extract its geometry and apperance. If the colors in the
   * color array of the (SGAL) geometry include an alpha channel, and this alpha
   * channel is used for transparency, we create a (Json) material of type
   * MeshBasicMaterial and set its color to be the first color of the color
   * array of the geometry (regardless of the color attachment). We also set the
   * opacity of the material based on the alpha value of the first color and
   * turn on its transparency flag.

   * Otherwise, if the type of geometry is lines (or polylines), we create one
   * material of type MeshBasicMaterial and set its color to be the first color
   * of the color array of the lines geometry. We ignore the remaining colors if
   * exist. When processing the (SGAL) geometry we generate a (Json) geomtery
   * record without colors at all.
   */
  void handle_shape(Shared_shape shape);

  /*! Export color array.
   */
  template <typename Array_>
  void export_colors(const Array_& colors, bool is_rgb);

  /*! Indicated whether the attribute is separated. */
  bool m_separated;

  //! The bounding sphere of the scene.
  const Bounding_sphere* m_bounding_sphere;

  //! The camera of the scene.
  Camera* m_camera;

  //! A mapping from geometries to uuids.
  boost::unordered_map<Shared_geometry, std::vector<String> > m_geometries;

  //! A mapping from appearances to uuids.
  boost::unordered_map<Shared_apperance, String> m_appearances;

  //! A mapping from nodes to instance number.
  boost::unordered_map<Shared_node, size_t> m_nodes;

  /*! Custom hash function for Basic_material
   */
  struct Basic_material_hash {
    std::size_t operator() (const Basic_material& key) const
    {
      std::size_t seed(0);
      boost::hash_combine(seed, key.m_color);
      boost::hash_combine(seed, key.m_opacity);
      boost::hash_combine(seed, key.m_transparent);
      boost::hash_combine(seed, key.m_material_color);
      boost::hash_combine(seed, key.m_depth_test);
      return seed;
    }
  };

  /*! Custom compare function for Basic_material
   */
  struct Basic_material_comparer {
    bool operator() (const Basic_material& a, const Basic_material& b) const
    {
      return ((a.m_color == b.m_color) && (a.m_opacity == b.m_opacity) &&
              (a.m_transparent == b.m_transparent) &&
              (a.m_material_color == b.m_material_color) &&
              (a.m_depth_test == b.m_depth_test));
    }
  };

  //! A mapping from Basic_material to a uuid string.
  boost::unordered_map<Basic_material, String, Basic_material_hash,
                       Basic_material_comparer>
    m_basic_materials;

  Matrix4f m_identity_matrix;

  //! Vertex color options.
  static const std::string s_def_material_colors[MC_SIZE];

  //! Version of json format.
  static const std::string s_version;
};

#if defined(_MSC_VER)
#pragma warning( pop )
#endif

//! \brief converts a color to its hexadecimal representation.
template <typename Vector_>
inline Uint Json_formatter::to_hex(const Vector_& color)
{
  auto red = static_cast<size_t>(color[0] * 255);;
  auto green = static_cast<size_t>(color[1] * 255);;
  auto blue = static_cast<size_t>(color[2] * 255);;

  Uint r = (red << 16) & 0xff0000;
  Uint g = (green << 8) & 0x00ff00;
  Uint b = (blue << 0) & 0x0000ff;
  return r | g | b;
}

//! \brief indicatess whether the geometry represents a supported mesh type.
inline
bool Json_formatter::is_structured_mesh(Geo_set::Primitive_type type) const
{ return ((Geo_set::PT_TRIANGLES == type) || (Geo_set::PT_QUADS == type)); }

//! \brief indicates whether the geometry represents a sequence of segments.
inline bool Json_formatter::is_lines(Geo_set::Primitive_type type) const
{ return (type == Geo_set::PT_LINES); }

//! \brief indicates whether the geometry represents a sequence of line strips.
inline bool Json_formatter::is_line_strips(Geo_set::Primitive_type type) const
{ return (type == Geo_set::PT_LINE_STRIPS); }

//! \brief indicates whether the geometry represents a sequence of line loops.
inline bool Json_formatter::is_line_loops(Geo_set::Primitive_type type) const
{ return (type == Geo_set::PT_LINE_LOOPS); }

//! \brief indicates whether the geometry represents a sequence of polylines.
inline bool Json_formatter::is_polylines(Geo_set::Primitive_type type) const
{ return (is_line_strips(type) || is_line_loops(type)); }

//! \brief sets the bounding sphere of the scene.
inline void Json_formatter::set_bounding_sphere(const Bounding_sphere* bs)
{ m_bounding_sphere = bs; }

//! \brief sets the bounding sphere of the scene.
inline void Json_formatter::set_camera(Camera* camera) { m_camera = camera; }

//! \brief exports a generic value.
template <typename Value>
inline void Json_formatter::out_value(Value value)
{ out_value(value, Json_formatter::identity<Value>()); }

//! \brief exports a generic value.
template <typename Value>
inline void Json_formatter::out_value(Value value, identity<Value>)
{ out() << value; }

//! brief exports a (specific) sting value.
inline void Json_formatter::out_value(const std::string& value,
                                      identity<std::string>)
{ out_string(value.c_str()); }

//! \brief exports a (specific) sting value.
inline void Json_formatter::out_value(const char* str, identity<const char*>)
{ out_string(str); }

//! Export a (specific) unsigned int.
inline void Json_formatter::out_value(Uint value, identity<Uint>)
{
  // Normally, we would print the value in the hexadecimal format, where the
  // number is preceeded with "0x", but json does not support this.
  // boost::io::ios_flags_saver ifs(out());
  // out() << std::hex << "0x" << value;
  out() << value;
}

//! \brief exports a (specific) Boolean value.
inline void Json_formatter::out_value(Boolean value, identity<Boolean>)
{ out() << ((value) ? "true" : "false"); }

//! \brief exports a single value.
template <typename Value>
inline void Json_formatter::single_value(Value value, bool compact)
{
  object_separator(compact);
  indent();
  out_value(value);
  m_separated = false;
}

//! \brief exports a simple attribute if not the default.
template <typename Value>
inline void Json_formatter::attribute_not_default(const std::string& name,
                                                  Value value,
                                                  Value default_value,
                                                  bool compact)
{ if (value != default_value) attribute(name, value, compact); }

//! \brief exports a simple attribute if not the default.
template <typename Value>
inline void Json_formatter::attribute(const std::string& name, Value value,
                                      bool compact)
{
  object_separator(compact);
  indent();
  out_string(name.c_str());
  name_value_separator();
  out_value(value);
  m_separated = false;
}

//! \brief exports an object.
template <typename UnaryOperation,
          typename StartOperation, typename EndOperation>
inline void Json_formatter::object(UnaryOperation op,
                                   StartOperation start_op, EndOperation end_op)
{
  start_op();
  op();
  end_op();
}

//! \brief exports a complex attribute.
template <typename UnaryOperation,
          typename StartOperation, typename EndOperation>
inline void Json_formatter::attribute_object(const std::string& name,
                                             UnaryOperation op,
                                             StartOperation start_op,
                                             EndOperation end_op)
{
  object_separator();
  indent();
  out_string(name.c_str());
  name_value_separator();
  object(op, start_op, end_op);
}

template <typename UnaryOperation>
inline void Json_formatter::single_object(UnaryOperation op, bool compact)
{
  object_separator();
  auto start_op = std::bind(&Json_formatter::object_begin, this, compact);
  auto end_op = std::bind(&Json_formatter::object_end, this, compact);
  object(op, start_op, end_op);
}

//! \brief exports multiple objects.
template <typename UnaryOperation>
inline void Json_formatter::multiple_objects(UnaryOperation op, bool compact)
{
  object_separator();
  auto start_op = std::bind(&Json_formatter::array_begin, this, compact);
  auto end_op = std::bind(&Json_formatter::array_end, this, compact);
  object(op, start_op, end_op);
}

//! \brief exports a single-object attribute.
template <typename UnaryOperation>
inline void Json_formatter::attribute_single(const std::string& name,
                                             UnaryOperation op, bool compact)
{
  auto start_op = std::bind(&Json_formatter::object_begin, this, compact);
  auto end_op = std::bind(&Json_formatter::object_end, this, compact);
  attribute_object(name, op, start_op, end_op);
}

//! \brief exports an array-object attribute.
template <typename UnaryOperation>
inline void Json_formatter::attribute_multiple(const std::string& name,
                                               UnaryOperation op, bool compact)
{
  auto start_op = std::bind(&Json_formatter::array_begin, this, compact);
  auto end_op = std::bind(&Json_formatter::array_end, this, compact);
  attribute_object(name, op, start_op, end_op);
}

//! \brief exports color array.
template <typename Array_>
void Json_formatter::export_colors(const Array_& colors, bool is_rgb)
{
  // For some reason, if the color is per vertex, the ThreeJS loader expects
  // each color in the RGB format (3 real numbers); if the color is per face,
  // the loader expects each color in the HTML (hex) format.
  if (is_rgb)
    attribute_multiple("colors",
                       [&]() { for (const auto& c : colors) vertex(c, true); },
                       true);
  else attribute_multiple("colors",
                          [&]() {
                            for (const auto& c : colors)
                              single_value(to_string_hex(c), true);
                          },
                          true);
}

SGAL_END_NAMESPACE

#endif
