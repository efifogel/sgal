// Copyright (c) 2004 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_SQUARED_DISTANCE_HPP
#define SGAL_SQUARED_DISTANCE_HPP

#include "SGAL/basic.hpp"

SGAL_BEGIN_NAMESPACE

/*! Computes the squared distance between a point and a segment and the point
 * on the segment the distance from which is the minimum.
 */
template <typename Kernel>
typename Kernel::FT squared_distance(const typename Kernel::Point_2& p,
                                     typename Kernel::Segment_2& seg,
                                     typename Kernel::Point_2& min_point,
                                     const Kernel& kernel)
{
  auto v = kernel.construct_vector_2_object()(seg);
  auto p1 = kernel.construct_source_2_object()(seg);
  auto v1 = kernel.construct_vector_2_object()(p1, p);
  auto s = kernel.compute_scalar_product_2_object()(v, v);
  auto t = kernel.compute_scalar_product_2_object()(v1, v) / s;
  if (t > 1) t = 1;
  else if (t < 0) t = 0;
  min_point = p1 + kernel.construct_scaled_vector_2_object()(v, t);
  return CGAL::squared_distance(p, min_point);
}

SGAL_END_NAMESPACE

#endif
