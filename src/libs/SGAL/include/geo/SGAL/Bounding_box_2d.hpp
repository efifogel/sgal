// Copyright (c) 2014 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_BOUNDING_BOX_2D_HPP
#define SGAL_BOUNDING_BOX_2D_HPP

#include <algorithm>
#include <boost/tuple/tuple.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/Types.hpp"

SGAL_BEGIN_NAMESPACE

class SGAL_SGAL_DECL Bounding_box_2d {
private:
  Float m_x_min;
  Float m_y_min;
  Float m_x_max;
  Float m_y_max;

  bool m_empty = true;

public:
  /*! Construct default. */
  Bounding_box_2d();

  /*! Construct from elements. */
  Bounding_box_2d(Float x_min, Float y_min, Float x_max, Float y_max);

  /*! Construct from partial elements. */
  Bounding_box_2d(Float x, Float y);

  /*! Clear the bounding box.
   */
  void clear();

  /*! Insert a point.
   */
  void insert(float x, float y);

  /*! Shift the coordinates.
   */
  void shift(float x, float y);

  /*! Expand the bounding box by an absolute value in all dimensions.
   */
  void expand(float delta);

  /*! Expand the bounding box by a scale factor in all dimensions.
   */
  void expand_scale(float factor);

  /*! Set */
  void set(Float x_min, Float y_min, Float x_max, Float y_max);

  /*! Set */
  void set(Float x, Float y);

  /*! Determine whether the bounding box has been initialized.
   */
  bool empty() const;

  /*! Obtain min x. */
  Float xmin() const;

  /*! Obtain min y. */
  Float ymin() const;

  /*! Obtain max x. */
  Float xmax() const;

  /*! Obtain max y. */
  Float ymax() const;

  /*! Obtain min. */
  Float min(Uint dim) const;

  /*! Obtain max. */
  Float max(Uint dim) const;

  /*! Obtain middle x. */
  Float xmid() const;

  /*! Obtain middle y. */
  Float ymid() const;

  /*! Obtain a bounding box of the current bounding box and a given one.
   * \param bb (in) the given boundng box.
   */
  Bounding_box_2d operator+(const Bounding_box_2d& bb) const;

  /*! Update the bounding box to be the bounding box of the current bounding
   * box and a given one.
   * \param bb (in) the given boundng box.
   * \return the current bounding box.
   */
  Bounding_box_2d& operator+=(const Bounding_box_2d& bb);

  /*! Obtain the dimensions in order of their length.
   * \return a tuple of dimension indicess in order of dimension length.
   */
  boost::tuple<Uint, Uint> get_longest_dimensions() const;

  /*! Determine whether the bounding box contains a point.
   */
  bool contain(float x, float y) const;

  /*! Determine whether the bounding box overlaps with a given one.
   */
  bool overlap(const Bounding_box_2d& other) const;

  /*! Determine whether a given bounding box is approximately the same.
   */
  bool is_approx_same(const Bounding_box_2d& other, float epsilon = 1e-6f) const;
};

//! \brief constructs default.
inline Bounding_box_2d::Bounding_box_2d() :
  m_x_min(0), m_y_min(0),
  m_x_max(0), m_y_max(0),
  m_empty(false)
{}

//! \brief constructs from elements.
inline Bounding_box_2d::Bounding_box_2d(Float x_min, Float y_min,
                                        Float x_max, Float y_max)
{ set(x_min, y_min, x_max, y_max); }

//! \brief constructs from partial elements.
inline Bounding_box_2d::Bounding_box_2d(Float x, Float y) { set(x, y, x, y); }

//! \brief sets the bounding box.
inline void Bounding_box_2d::set(Float x_min, Float y_min,
                                 Float x_max, Float y_max)
{
  m_x_min = x_min; m_y_min = y_min;
  m_x_max = x_max; m_y_max = y_max;
  m_empty = false;
}

//! \brief sets from from partial elements.
inline void Bounding_box_2d::set(Float x, Float y) { set(x, y, x, y); }

//! \brief determines whether the bounding box has been initialized.
inline bool Bounding_box_2d::empty() const { return m_empty; }

inline Float Bounding_box_2d::xmin() const { return m_x_min; }
inline Float Bounding_box_2d::ymin() const { return m_y_min; }
inline Float Bounding_box_2d::xmax() const { return m_x_max; }
inline Float Bounding_box_2d::ymax() const { return m_y_max; }

inline Float Bounding_box_2d::min(Uint dim) const
{ return (dim == 0) ? xmin() : ymin(); }
inline Float Bounding_box_2d::max(Uint dim) const
{ return (dim == 0) ? xmax() : ymax(); }

//! \brief clears the bounding box.
inline void Bounding_box_2d::clear() { *this = Bounding_box_2d{}; }

//! \brief determines whether the bounding box contains a point.
inline bool Bounding_box_2d::contain(float x, float y) const
{
  return ((m_x_min <= x) && (x <= m_x_max) &&
          (m_y_min <= y) && (y <= m_y_max));
}

//! determines whether the bounding box overlaps with a given one.
inline bool Bounding_box_2d::overlap(const Bounding_box_2d& other) const
{
  if ((other.m_x_min > m_x_max) || (other.m_x_max < m_x_min) ||
      (other.m_y_min > m_y_max) || (other.m_y_max < m_y_min))
    return false;
  return true;
}

//! \brief obtains middle x.
inline Float Bounding_box_2d::xmid() const { return 0.5f * (m_x_min + m_x_max); }

//! \brief obtains middle y.
inline Float Bounding_box_2d::ymid() const { return 0.5f * (m_y_min + m_y_max); }

//! \todo shifts the coordinates.
inline void Bounding_box_2d::shift(float x, float y)
{ m_x_min += x; m_y_min += y; m_x_max += x; m_y_max += y; }

//! \brief expands the bounding box.
inline void Bounding_box_2d::expand(float delta)
{
  m_x_min -= delta; m_y_min -= delta;
  m_x_max += delta; m_y_max += delta;
}

SGAL_END_NAMESPACE

#endif
