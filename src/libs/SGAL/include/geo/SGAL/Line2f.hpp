// Copyright (c) 2011 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_LINE2F_HPP
#define SGAL_LINE2F_HPP

/*! \file
 *
 */

#include "SGAL/basic.hpp"
#include "SGAL/Vector2f.hpp"

SGAL_BEGIN_NAMESPACE

class SGAL_SGAL_DECL Line2f {
public:
  /*! Constructs. */
  Line2f(const Vector2f& p1, const Vector2f& p2);

  /*! Set representative point, which lies on the line. */
  void set_point(Float x, Float y) { m_point.set(x, y); }

  /*! Set the direction. */
  void set_point(const Vector2f & d) { m_point = d; }

  /*! Obtain the direction. */
  const Vector2f& get_point() const { return m_point; }

  /*! Set the direction. */
  void set_direction(Float x, Float y) { m_direction.set(x, y); }

  /*! Set the direction */
  void set_direction(const Vector2f & d) { m_direction = d; }

  /*! Obtain the direction. */
  const Vector2f& get_direction() const { return m_direction; }

private:
  //! A point on the line.
  Vector2f m_point;

  //! The direction of the line.
  Vector2f m_direction;
};

SGAL_END_NAMESPACE

#endif
