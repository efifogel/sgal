[DEFAULT]
name = Modeling

[class]
bases = ["Container"]

friends = ["Indexed_face_set", "Hole_filler", "Modeling_option_parser"]

includes = ["SGAL/Stop_simplification_strategy.hpp"]

protected-data = ["dirty_stop_simplification_strategy",
  "dirty_stop_simplification_strategy_code"]

public-functions = ["add_to_scene", "reset"]

protected-functions = ["stop_simplification_strategy_changed",
  "clean_stop_simplification_strategy",
  "stop_simplification_strategy_code_changed",
  "clean_stop_simplification_strategy_code"]

[dirty_stop_simplification_strategy]
desc = Indicates whether the stop_simplification_strategy option is dirty, and thus must be cleaned.
type = bool
name = m_dirty_stop_simplification_strategy
value = false
qualifier = mutable

[dirty_stop_simplification_strategy_code]
desc = Indicates whether the stop_simplification_strategy_code option is dirty, and thus must be cleaned.
type = bool
name = m_dirty_stop_simplification_strategy_code
value = false
qualifier = mutable

[add_to_scene]
desc = "Add the container to a given scene."
signature = virtual void add_to_scene(Scene_graph* scene_graph)

[reset]
desc = "Reset to factory settings."
signature = void reset(Boolean def_make_consistent = s_def_make_consistent,
                       Boolean def_triangulate_holes = s_def_triangulate_holes,
                       Boolean def_refine = s_def_refine_hole_triangulation,
                       Boolean def_fair = s_def_fair_hole_triangulation,
                       Boolean def_triangulate_facets = s_def_triangulate_facets,
                       Boolean def_split_ccs = s_def_split_ccs,
                       Boolean def_remove_degeneracies = s_def_remove_degeneracies,
                       Boolean def_repair_orientation = s_def_repair_orientation,
                       Boolean def_remove_isolated_vertices = s_def_remove_isolated_vertices,
                       Boolean def_repair_normals = s_def_repair_normals,
                       Boolean def_merge_coplanar_facets = s_def_merge_coplanar_facets,
		       Boolean def_simplify = s_def_simplify,
		       String stop_simplification_strategy = s_def_stop_simplification_strategy,
		       Uint stop_simplification_strategy_code = s_def_stop_simplification_strategy_code,
		       Uint simplification_count_stop = s_def_simplification_count_stop,
		       Float simplification_count_ratio_stop = s_def_simplification_count_ratio_stop,
		       Float simplification_edge_length_stop = s_def_simplification_edge_length_stop)

[stop_simplification_strategy_changed]
desc = Process change of stop_simplification_strategy.
signature = void
  stop_simplification_strategy_changed(const Field_info* field_info)

[clean_stop_simplification_strategy]
desc = Clean the stop_simplification_strategy options set.
signature = void clean_stop_simplification_strategy() const

[stop_simplification_strategy_code_changed]
desc = Process change of stop_simplification_strategy_code_.
signature = void
  stop_simplification_strategy_code_changed(const Field_info* field_info)

[clean_stop_simplification_strategy_code]
desc = Clean the stop_simplification_strategy_code_ options set.
signature = void clean_stop_simplification_strategy_code() const

[fields]
spec = %(name)s_fields.txt
