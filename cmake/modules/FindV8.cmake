# - Try to find v8 (v8.lib on Windows and v8_base.x64.a on Linux)
# Once done this will define
#  V8_FOUND - System has v8
#  V8_INCLUDE_DIR - The v8 include directories
#  V8_LIBRARIES - The libraries needed to use V8
#  V8_LIBRARY_DIR - The directory where lib files are.

set(V8_NAMES_RELEASE v8)
set(V8_NAMES_DEBUG v8d ${V8_NAMES_RELEASE})
set(V8_PLATFORM_NAMES_RELEASE v8_libplatform)
set(V8_PLATFORM_NAMES_DEBUG v8_libplatformd ${V8_PLATFORM_NAMES_RELEASE})

find_path(V8_INCLUDE_DIR NAMES v8.h
  PATH_SUFFIXES include v8 include/v8
  PATHS /opt/libv8-$ENV{V8_VER} # ppa:pinepain/libv8
  HINTS ENV V8_INC_DIR ENV V8_DIR)

if(CMAKE_CL_64)
    set(V8_ARCH x64)
else()
    set(V8_ARCH x86)
endif()

# CMake>=2.6 supports the notation "debug XXd optimized XX"
set(V8_HINTS ENV V8_LIB_DIR ENV V8_DIR)
if (UNIX)
  set(V8_HINTS_DEBUG ${V8_DIR}/out/x64.debug $ENV{V8_DIR}/out/x64.debug)
  set(V8_HINTS_RELEASE ${V8_DIR}/out/x64.release $ENV{V8_DIR}/out/x64.release)
  set(V8_PATHS /opt/libv8-$ENV{V8_VER}) # ppa:pinepain/libv8
elseif(WIN32)
  set(V8_HINTS_DEBUG ${V8_DIR}/build/Debug $ENV{V8_DIR}/build/Debug)
  set(V8_HINTS_RELEASE ${V8_DIR}/out/build/Release $ENV{V8_DIR}/build/Release)
else()
endif()

if(WIN32)
  if(MSVC_VERSION EQUAL 1600)
    set(V8_RUNTIME vc10)
  elseif(MSVC_VERSION EQUAL 1700)
    set(V8_RUNTIME vc11)
  endif()
endif()

find_library(V8_LIBRARY_RELEASE
  NAMES ${V8_NAMES_RELEASE}
  PATH_SUFFIXES lib
  HINTS ${V8_HINTS} ${V8_HINTS_RELEASE}
  PATHS ${V8_PATHS}
  DOC "Google V8 JavaScript Engine Library (Release)")

find_library(V8_LIBRARY_DEBUG
  NAMES ${V8_NAMES_DEBUG}
  PATH_SUFFIXES lib
  HINTS ${V8_HINTS} ${V8_HINTS_DEBUG}
  PATHS ${V8_PATHS}
  DOC "Google V8 JavaScript Engine Library (Debug)")

if(CMAKE_BUILD_TYPE EQUAL "Release")
  get_filename_component(V8_LIBRARY_DIR ${V8_LIBRARY_RELEASE} PATH)
  set(V8_LIBRARY ${V8_LIBRARY_RELEASE})
else()
  get_filename_component(V8_LIBRARY_DIR ${V8_LIBRARY_DEBUG} PATH)
  set(V8_LIBRARY_DEBUG ${V8_LIBRARY_RELEASE})
endif()

find_library(V8_PLATFORM_LIBRARY_RELEASE
  NAMES ${V8_PLATFORM_NAMES_RELEASE}
  PATH_SUFFIXES lib
  HINTS ${V8_LIBRARY_DIR}
  DOC "Google V8 JavaScript Engine platform Library (Release)")

find_library(V8_PLATFORM_LIBRARY_DEBUG
  NAMES ${V8_PLATFORM_NAMES_DEBUG}
  PATH_SUFFIXES lib
  HINTS ${V8_LIBRARY_DIR}
  DOC "Google V8 JavaScript Engine platform Library (Debug)")

set(V8_LIBRARIES
  optimized ${V8_LIBRARY_RELEASE} debug ${V8_LIBRARY_DEBUG}
  optimized ${V8_PLATFORM_LIBRARY_RELEASE} debug ${V8_PLATFORM_LIBRARY_DEBUG})

include(FindPackageHandleStandardArgs)

# handle the QUIETLY and REQUIRED arguments and set V8_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(V8 DEFAULT_MSG
                                  V8_LIBRARIES
				  V8_INCLUDE_DIR)

# Detect V8 version
if(V8_FOUND AND V8_INCLUDE_DIR)
  file(READ ${V8_INCLUDE_DIR}/v8-version.h V8_VERSION_FILE)

  string(REGEX MATCH "#define V8_MAJOR_VERSION ([0-9]+)" V8_VERSION_MAJOR_DEF ${V8_VERSION_FILE})
  string(REGEX MATCH "([0-9]+)$" V8_VERSION_MAJOR ${V8_VERSION_MAJOR_DEF})

  string(REGEX MATCH "#define V8_MINOR_VERSION ([0-9]+)" V8_VERSION_MINOR_DEF ${V8_VERSION_FILE})
  string(REGEX MATCH "([0-9]+)$" V8_VERSION_MINOR ${V8_VERSION_MINOR_DEF})

  string(REGEX MATCH "#define V8_BUILD_NUMBER ([0-9]+)" V8_VERSION_PATCH_DEF ${V8_VERSION_FILE})
  string(REGEX MATCH "([0-9]+)$" V8_VERSION_PATCH ${V8_VERSION_PATCH_DEF})

  string(REGEX MATCH "#define V8_PATCH_LEVEL ([0-9]+)" V8_VERSION_TWEAK_DEF ${V8_VERSION_FILE})
  string(REGEX MATCH "([0-9]+)$" V8_VERSION_TWEAK ${V8_VERSION_TWEAK_DEF})

  set(V8_VERSION "${V8_VERSION_MAJOR}.${V8_VERSION_MINOR}.${V8_VERSION_PATCH}.${V8_VERSION_TWEAK}")

  set(V8_VERSION_HEX 0x0${V8_VERSION_MAJOR}${V8_VERSION_MINOR}${V8_VERSION_PATCH}${V8_VERSION_TWEAK})
  string(LENGTH "${V8_VERSION_HEX}" V8_VERSION_HEX_LENGTH)

  while(V8_VERSION_HEX_LENGTH LESS 8)
    set(V8_VERSION_HEX "${V8_VERSION_HEX}0")
    string(LENGTH "${V8_VERSION_HEX}" V8_VERSION_HEX_LENGTH)
  endwhile()
endif()

mark_as_advanced(V8_INCLUDE_DIR V8_LIBRARIES V8_LIBRARY_DIR)

if(V8_CMAKE_DEBUG)
  message(STATUS "V8_INCLUDE_DIR: ${V8_INCLUDE_DIR}")
  message(STATUS "V8_LIBRARIES: ${V8_LIBRARIES}")
  message(STATUS "V8_LIBRARIES_DEPENDS: ${V8_LIBRARIES_DEPENDS}")
  message(STATUS "V8_VERSION: ${V8_VERSION}")
  message(STATUS "V8_VERSION_HEX: ${V8_VERSION_HEX}")
  message(STATUS "V8_EXECUTABLE: ${V8_EXECUTABLE}")
endif()
